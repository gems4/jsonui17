#ifndef TMAINWINDOW_H
#define TMAINWINDOW_H

#include <string>
#include <QMainWindow>
#include <QSettings>


//#include "jsonio17/thrift_schema.h"
#include "jsonui17/BaseWidget.h"

namespace Ui {
class TJSONUITestWin;
}

namespace jsonui17 {
class  TableEditWidget;
}


class TJSONUITestWin : public QMainWindow
{
    Q_OBJECT

    // Internal data
    std::list<jsonui17::BaseWidget*> jsonuiWindows;
    jsonui17::OnCloseEvent_f onCloseEvent;
    jsonui17::ShowVertexEdge_f showWidget;

    // Work functions
    void setActions();
    void closeEvent(QCloseEvent* e);

public slots:

    // update after change preferences
    void updateViewMenu();
    void updateModel();
    void updateTable();
    void updateDB();
    /// Close All JSONUIWidget windows
    void closeAll();

    // File
    //void CmReadSchema();
    void CmSetWorkDir();
    //void CmSetDBDir();
    void CmNewVertex();
    void CmNewEdge();
    void CmNewBson();
    void CmNewCollection();
    void CmNewCsv();
    void CmNewImpexFormat();

    // Help
    void CmHelpAbout();
    void CmHelpAuthors();
    void CmHelpLicense();
    void CmHelpContens();

    void onDeleteVertex();
    void onDeleteEdge();
    void onLoadGraph();

public:
    explicit TJSONUITestWin(QWidget *parent = nullptr);
    ~TJSONUITestWin();

    /// Open new EdgesWidget  or JSONUIWidget  window
    void OpenNewWidget( bool isVertex, const std::string& testschema,
                        const std::string& recordkey, const jsonio17::DBQueryBase& query  );

private:

    Ui::TJSONUITestWin *ui;
    jsonui17::TableEditWidget* csv_window = nullptr;

};

#endif // TMAINWINDOW_H
