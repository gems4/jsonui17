#-------------------------------
#
# Project created by QtCreator 2016-02-08T16:01:53
#
#-------------------------------------------------


TARGET = jsonui17-test
TEMPLATE = app

CONFIG += thread
CONFIG += c++17
#CONFIG += sanitizer sanitize_thread

#internal help configuraion
#DEFINES  +=  NO_QWEBENGINE
#DEFINES  +=  USE_MARKDOWN
#win32:DEFINES += IMPEX_OFF
#DEFINES += IMPEX_OFF
#DEFINES += USE_THRIFT

!win32 {
  DEFINES += __unix

  QMAKE_CXXFLAGS += -Wall -Wextra -Wformat-nonliteral -Wcast-align -Wpointer-arith \
 -Wmissing-declarations -Wundef \    # -Winline
 -Wcast-qual -Wwrite-strings -Wno-unused-parameter \
 -pedantic -ansi -Wno-deprecated-copy
#QMAKE_CXXFLAGS_DEBUG += -fsanitize=thread
#QMAKE_LFLAGS_DEBUG += -fsanitize=thread
}

win32{
 #DEFINES  +=  IMPEX_OFF
 #DEFINES  +=  NO_QWEBENGINE
}

QT   += core gui widgets
QT   += svg printsupport concurrent
QT   += charts
!contains(DEFINES, NO_QWEBENGINE) {
    QT   += webenginewidgets
}

# Define the directory where source code is located
SRC_CPP       = .
SRC_H   =  $$SRC_CPP

# Define the directory where jsonio source code is located
JSONIO17_DIR =  $$PWD/../../jsonio17/src
JSONIO17_HEADERS_DIR =  $$PWD/../../jsonio17/include
JSONUI17_DIR =  $$PWD/../src
JSONUI17_HEADERS_DIR =  $$PWD/../include

DEPENDPATH   += $$SRC_H
DEPENDPATH   += $$JSONIO17_HEADERS_DIR
DEPENDPATH   += $$JSONUI17_HEADERS_DIR

INCLUDEPATH   += $$SRC_H
INCLUDEPATH   += $$JSONIO17_HEADERS_DIR
INCLUDEPATH   += $$JSONUI17_HEADERS_DIR

## add jsonimpex
!contains(DEFINES, IMPEX_OFF) {

JSONIMPEX17_DIR =  $$PWD/../../jsonimpex17/src
JSONIMPEX17_HEADERS_DIR =  $$PWD/../../jsonimpex17/include
DEPENDPATH   += $$JSONIMPEX17_HEADERS_DIR
INCLUDEPATH   += $$JSONIMPEX17_HEADERS_DIR

!win32::LIBS += -lyaml-cpp  -lpugixml
win32::LIBS += -lyaml-cppd  -lpugixml
unix:!macx-clang:LIBS += -llua -ldl
macx-clang:LIBS += -llua
win32:LIBS += -llua

include($$JSONIMPEX17_DIR/jsonimpex17.pri)

contains(DEFINES, USE_THRIFT) {

THRIFT_DIR    = $$PWD/../../jsonimpex17/thrift
THRIFT_HEADERS_DIR =  $$THRIFT_DIR/gen-cpp
DEPENDPATH   += $$THRIFT_HEADERS_DIR
INCLUDEPATH   += $$THRIFT_HEADERS_DIR
LIBS += -lthrift

include($$THRIFT_DIR/thrift.pri)
}

}
## end jsonimpex

macx-clang {

  DEFINES += __APPLE__
  #QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.15
  CONFIG -= warn_on
  CONFIG += warn_off
  INCLUDEPATH   += "/usr/local/include"
  DEPENDPATH   += "/usr/local/include"
  LIBPATH += "/usr/local/lib/"
}

win32 {
  INCLUDEPATH   += "C:\usr\local\include"
  DEPENDPATH   += "C:\usr\local\include"
  LIBPATH += "C:\usr\local\lib"
}

## Markdown editor
contains(DEFINES, USE_MARKDOWN) {
  JSONUI_APP_STATIC =  $$PWD/../app-static
  DEPENDPATH   += $$JSONUI_APP_STATIC
  INCLUDEPATH   += $$JSONUI_APP_STATIC
  LIBS +=  -lmarkdown
  include($$JSONUI_APP_STATIC/app-static.pri)
}
else
{
 contains(DEFINES, NO_QWEBENGINE) {
  LIBS +=  -lmarkdown
 }
}

## end markdown

win32:LIBS +=  -ljsonarango-static
!win32:LIBS += -ljsonarango

MOC_DIR = tmp
UI_DIR        = $$MOC_DIR
UI_SOURSEDIR  = $$MOC_DIR
UI_HEADERDIR  = $$MOC_DIR
OBJECTS_DIR   = obj

include( $$JSONIO17_DIR/jsonio17.pri)
include( $$JSONUI17_DIR/jsonui17.pri)
include( $$SRC_CPP/src.pri)

RESOURCES += \
    ../jsonui.qrc


#thrift -r -v --gen cpp
# sudo ln -s /usr/include/lua5.3/lua.hpp /usr/include/lua.hpp
# /usr/lib/x86_64-linux-gnu/liblua5.2.so.

DISTFILES += \
    ../Resources/docs/source.md \
    ../Resources/jsonui17-config.json


