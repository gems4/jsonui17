#include <functional>
#include <QFile>
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>
#include "JSONUITestMainWindow.h"
#include "ui_JSONUITestMainWindow.h"
#include "jsonui17/models/schema_model.h"
#include "jsonui17/JsonEditWidget.h"
#include "jsonui17/VertexWidget.h"
#include "jsonui17/EdgesWidget.h"
#include "jsonui17/FormatImpexWidget.h"
#include "jsonui17/TableEditWindow.h"
#include "jsonui17/HelpMainWindow.h"
#include "jsonui17/FileDialogs.h"
#include "jsonui17/SelectDialog.h"
#include "jsonui17/preferences.h"

TJSONUITestWin::TJSONUITestWin(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TJSONUITestWin)
{
    ui->setupUi(this);

    onCloseEvent = [=](QMainWindow* win)
    {
        auto it = jsonuiWindows.begin();
        while( it != jsonuiWindows.end() )
            if( *it == win ) // pointer
            {  jsonuiWindows.erase(it);
                break;
            } else
                it++;
        return true;
    };

    showWidget =  [=]( bool is_vertex, const std::string& schema_name,
            const std::string& document_key, const jsonio17::DBQueryBase& query )
    {
        OpenNewWidget( is_vertex, schema_name, document_key, query );
    };

    //set up main parameters
    setActions();

    // init help window
#ifndef _MSC_VER
    new jsonui17::HelpMainWindow( nullptr);
#endif
}

TJSONUITestWin::~TJSONUITestWin()
{
    delete csv_window;
    delete ui;
}

//  Connect all actions
void TJSONUITestWin::setActions()
{
    // File
    connect( ui->actionSet_I_O_Files_Location, &QAction::triggered, this, &TJSONUITestWin::CmSetWorkDir);
    connect( ui->actionNewVertex, &QAction::triggered, this, &TJSONUITestWin::CmNewVertex);
    connect( ui->actionNew_Edge_Editor_Window, &QAction::triggered, this, &TJSONUITestWin::CmNewEdge);
    connect( ui->actionNew_Bson, &QAction::triggered, this, &TJSONUITestWin::CmNewBson);
    connect( ui->actionNew_Collection_Data_Editor, &QAction::triggered, this, &TJSONUITestWin::CmNewCollection);
    connect( ui->actionNew_CSV, &QAction::triggered, this, &TJSONUITestWin::CmNewCsv);
    connect( ui->actionNew_Impex, &QAction::triggered, this, &TJSONUITestWin::CmNewImpexFormat);
    //    connect( ui->actionE_xit, &QAction::triggered, this, QDialog::close);
    connect( ui->actionE_xit, &QAction::triggered, qApp, &QApplication::quit);

    // Help
    connect( ui->action_Help_About, &QAction::triggered, this, &TJSONUITestWin::CmHelpAbout);
    connect( ui->actionContents, &QAction::triggered, this, &TJSONUITestWin::CmHelpContens);
    connect( ui->actionAuthors, &QAction::triggered, this, &TJSONUITestWin::CmHelpAuthors);
    connect( ui->actionLicense, &QAction::triggered, this, &TJSONUITestWin::CmHelpLicense);
    connect( ui->actionPreferences, &QAction::triggered, &jsonui17::uiSettings(), &jsonui17::UIPreferences::CmSettingth);

    // View
    connect( ui->action_Show_comments, &QAction::toggled, &jsonui17::uiSettings(), &jsonui17::UIPreferences::CmShowComments);
    connect( ui->action_Display_enums, &QAction::toggled, &jsonui17::uiSettings(), &jsonui17::UIPreferences::CmDisplayEnums);
    connect( ui->action_Edit_id, &QAction::toggled, &jsonui17::uiSettings(), &jsonui17::UIPreferences::CmEditID);

    /// connect( &uiSettings(), SIGNAL(dbChanged()), this, SLOT(closeAll()));
    connect( &jsonui17::uiSettings(), &jsonui17::UIPreferences::dbChanged, this, &TJSONUITestWin::updateDB);
    connect( &jsonui17::uiSettings(), &jsonui17::UIPreferences::schemaChanged, this, &TJSONUITestWin::closeAll);
    connect( &jsonui17::uiSettings(), &jsonui17::UIPreferences::viewMenuChanged, this, &TJSONUITestWin::updateViewMenu);
    connect( &jsonui17::uiSettings(), &jsonui17::UIPreferences::modelChanged, this, &TJSONUITestWin::updateModel);
    connect( &jsonui17::uiSettings(), &jsonui17::UIPreferences::tableChanged, this, &TJSONUITestWin::updateTable);
    connect( &jsonui17::uiSettings(), SIGNAL(errorSettings(std::string)), this, SLOT(closeAll()));

}

void TJSONUITestWin::CmHelpContens()
{
    jsonui17::helpWin( "jsonui-test", "" );
}

void TJSONUITestWin::CmHelpAbout()
{
    jsonui17::helpWin( "AboutJsonui", "" );
}

void TJSONUITestWin::CmHelpAuthors()
{
    jsonui17::helpWin( "AuthorsJsonui", "" );
}

void TJSONUITestWin::CmHelpLicense()
{
    jsonui17::helpWin( "LicenseJsonui", "" );
}


/// Define new input/output directory
void TJSONUITestWin::CmSetWorkDir()
{
    try{
        QString dirPath = QFileDialog::getExistingDirectory(  this, "Select I/O Files Location",
                                                              jsonio17::ioSettings().userDir().c_str(),
                                                              QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks );
        if( dirPath.isEmpty() )
            return;
        jsonio17::ioSettings().setUserDir(dirPath.toStdString());
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


// Open new EdgesWidget  or VertexWidget  window
void TJSONUITestWin::OpenNewWidget( bool isVertex, const std::string& testschema,
                                    const std::string& recordkey, const jsonio17::DBQueryBase& query )
{
    jsonui17::BaseWidget* testWidget;
    if( isVertex )
    {
        testWidget = new jsonui17::VertexWidget( testschema/*, this*/ );
        connect( dynamic_cast<jsonui17::VertexWidget*>(testWidget), &jsonui17::VertexWidget::vertexDeleted,
                 this, &TJSONUITestWin::onDeleteVertex );
        connect( dynamic_cast<jsonui17::VertexWidget*>(testWidget), &jsonui17::VertexWidget::graphLoaded,
                 this, &TJSONUITestWin::onLoadGraph);
    }
    else
    {
        testWidget = new jsonui17::EdgesWidget( testschema, query/*, this*/ );
        connect( dynamic_cast<jsonui17::EdgesWidget*>(testWidget), &jsonui17::EdgesWidget::edgeDeleted,
                 this, &TJSONUITestWin::onDeleteEdge);
        connect( dynamic_cast<jsonui17::EdgesWidget*>(testWidget), &jsonui17::EdgesWidget::graphLoaded,
                 this, &TJSONUITestWin::onLoadGraph);
    }
    testWidget->setOnCloseEventFunction(onCloseEvent);
    testWidget->setShowWidgetFunction(showWidget);
    jsonuiWindows.push_back(testWidget);

    if( !recordkey.empty() )
    {
        testWidget->openRecordKey(  recordkey  );
        //if( isVertex )
        //    testWidget->changeKeyList();
    }
    testWidget->show();
}

/// Open new JsonEditWidget window
void TJSONUITestWin::CmNewBson()
{
    try{
        std::string testschema ="";

        auto sch_lst = jsonio17::ioSettings().Schema().getStructs( jsonui17::JsonSchemaModel::showComments );
        // add comment
        if( !sch_lst.empty() )
        {
            jsonui17::SelectDialog selDlg( false, this, "Please, select schema to edit data", sch_lst, ',' );
            if( selDlg.exec() )
                testschema = sch_lst[selDlg.getSelectedIndex()].substr( 0, sch_lst[selDlg.getSelectedIndex()].find_first_of(","));
        }
        jsonui17::JsonEditWidget* testWidget = new jsonui17::JsonEditWidget(  testschema, "icomps"/*, this*/ );
        testWidget->setOnCloseEventFunction(onCloseEvent);
        testWidget->setShowWidgetFunction(showWidget);
        jsonuiWindows.push_back(testWidget);
        testWidget->show();
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Open new JSONUIWidget window
void TJSONUITestWin::CmNewCollection()
{
    try{
        std::string testcollection ="elements";
        auto collist = jsonui17::uiSettings().database().theDriver()->get_collections_names(jsonio17::AbstractDBDriver::clAll);

        if( !collist.empty() )
        {
            std::vector<std::string> list;
            list.insert( list.end(), collist.begin(), collist.end() );
            jsonui17::SelectDialog selDlg( false, this, "Please, select collection to edit data", list, ',' );
            if( selDlg.exec() )
                testcollection = list[selDlg.getSelectedIndex()];
        }
        auto* testWidget = new jsonui17::JsonEditWidget( "", testcollection/*, this*/ );

        testWidget->setOnCloseEventFunction(onCloseEvent);
        testWidget->setShowWidgetFunction(showWidget);
        jsonuiWindows.push_back(testWidget);
        testWidget->show();
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Open new JSONUIWidget with ImpexFormat&database window
void TJSONUITestWin::CmNewImpexFormat()
{
    try{
        auto* testWidget = new jsonui17::FormatImpexWidget(
                    jsonui17::FormatImpexWidget::editMode /*, "", this*/ );

        testWidget->setOnCloseEventFunction(onCloseEvent);
        testWidget->setShowWidgetFunction(showWidget);
        jsonuiWindows.push_back(testWidget);
        testWidget->show();
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Open new VertexWidget window
void TJSONUITestWin::CmNewVertex()
{
    try{
        std::string testschema ="";

        //vector<std::string> sch_lst = schema.getStructsList( TSchemaNodeModel::showComments );
        auto sch_lst = jsonio17::DataBase::getVertexesList();
        // add comment
        if( !sch_lst.empty() )
        {
            jsonui17::SelectDialog selDlg( false, this, "Please, select vertex schema to edit data", sch_lst, ',' );
            if( selDlg.exec() )
                testschema = sch_lst[selDlg.getSelectedIndex()].substr( 0, sch_lst[selDlg.getSelectedIndex()].find_first_of(","));
        }
        if( !testschema.empty())
            OpenNewWidget( true, testschema, "", jsonio17::DBQueryBase::emptyQuery() );
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Open new EdgesWidget window
void TJSONUITestWin::CmNewEdge()
{
    try{
        OpenNewWidget( false, "", "", jsonio17::DBQueryBase::emptyQuery() );
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Open new CSVEditWidget window
void TJSONUITestWin::CmNewCsv()
{
    try{

        std::string fileName;
        if(  !jsonui17::ChooseFileOpen( this, fileName, "Please, select file with csv object", jsonui17::csvFilters  ))
            return;
        // define new dialog
        if( !csv_window )
        {
            csv_window = new jsonui17::TableEditWidget( "CSV editor ", fileName,
                                                        jsonui17::MatrixTable::tbEdit|jsonui17::MatrixTable::tbGraph/*|MatrixTable::tbSort*/ );
        }
        else
        {
            csv_window->openNewCSV(fileName);
            csv_window->raise();
        }
        csv_window->show();
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


//-----------------------------------------------------


/// Update menu for all windows
void TJSONUITestWin::updateViewMenu()
{
    ui->action_Show_comments->setChecked( jsonui17::JsonSchemaModel::showComments );
    ui->action_Display_enums->setChecked( jsonui17::JsonSchemaModel::useEnumNames);
    ui->action_Edit_id->setChecked( jsonui17::JsonSchemaModel::editID );

    auto it = jsonuiWindows.begin();
    while( it != jsonuiWindows.end() )
    {  (*it)->updateViewMenu(); it++; }
}


/// Update model for all windows
void TJSONUITestWin::updateModel()
{
    auto it = jsonuiWindows.begin();
    while( it != jsonuiWindows.end() )
    {  (*it)->updateModel(); it++; }
}


/// Update table for all windows
void TJSONUITestWin::updateTable()
{
    auto it = jsonuiWindows.begin();
    while( it != jsonuiWindows.end() )
    {  (*it)->updateTable(); it++; }
}


/// Update DB for all windows
void TJSONUITestWin::updateDB()
{
    if(jsonui17::HelpMainWindow::pDia)
       jsonui17::HelpMainWindow::pDia->resetDBClient();
    auto it = jsonuiWindows.begin();
    while( it != jsonuiWindows.end() )
    {  (*it)->updateDB(); it++; }
}

/// close all json windows
void TJSONUITestWin::closeAll()
{
    auto it = jsonuiWindows.begin();
    while( it != jsonuiWindows.end() )
    {  (*it)->close(); it = jsonuiWindows.begin(); }
}

void TJSONUITestWin::closeEvent(QCloseEvent* e)
{
    if( jsonui17::HelpMainWindow::pDia )
        delete jsonui17::HelpMainWindow::pDia;

    if( csv_window )
        csv_window->close();

    closeAll();
    QWidget::closeEvent(e);
}


void TJSONUITestWin::onLoadGraph()
{
    auto it = jsonuiWindows.begin();
    while( it != jsonuiWindows.end() )
    {
        jsonui17::EdgesWidget* edgWin = dynamic_cast<jsonui17::EdgesWidget*>(*it);
        if( edgWin )
            edgWin->updateQuery();
        else
        {
            jsonui17::VertexWidget* verWin = dynamic_cast<jsonui17::VertexWidget*>(*it);
            if( verWin )
                verWin->updateQuery();
        }
        it++;
    }
}

void TJSONUITestWin::onDeleteEdge()
{
    auto it = jsonuiWindows.begin();
    while( it != jsonuiWindows.end() )
    {
        jsonui17::EdgesWidget* edgWin = dynamic_cast<jsonui17::EdgesWidget*>(*it);
        if( edgWin )
            edgWin->changeKeyList();
        it++;
    }
}

void TJSONUITestWin::onDeleteVertex()
{
    auto it = jsonuiWindows.begin();
    while( it != jsonuiWindows.end() )
    {
        jsonui17::EdgesWidget* edgWin = dynamic_cast<jsonui17::EdgesWidget*>(*it);
        if( edgWin )
            edgWin->changeKeyList();
        else
        {
            jsonui17::VertexWidget* verWin = dynamic_cast<jsonui17::VertexWidget*>(*it);
            if( verWin )
                verWin->changeKeyList();
        }
        it++;
    }
}


