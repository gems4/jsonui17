#include "JSONUITestMainWindow.h"
#include "jsonui17/preferences.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    jsonio17::JsonioSettings::settingsFileName = "jsonui17-config.json";

    QApplication a(argc, argv);
    a.setWindowIcon(QIcon(":/menu/Icons/jsonui-logo-icon.png"));
    TJSONUITestWin w;
    w.show();

    return a.exec();
}
