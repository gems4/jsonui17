#pragma once

#include <QDialog>
#include "jsonui17/models/table_view.h"

namespace jsonio17 {
class DBDocumentBase;
}

namespace jsonui17 {

/// Select document key from current query result for collection
std::string SelectKeyFrom( const jsonio17::DBDocumentBase* documents_collection, QWidget* parent,
                           const char* title, int mode = MatrixTable::tbNoMenu|MatrixTable::tbSort );
/// Select document keys from current query result for collection
std::vector<std::string> SelectKeysFrom( const jsonio17::DBDocumentBase* documents_collection, QWidget* parent,
                                         const char* title, int mode = MatrixTable::tbNoMenu|MatrixTable::tbSort );


/// Class for internal table data container
class SelectTableContainer : public AbstractContainer
{

public:

    /// Construct table from list of lines.
    /// Split each line into a list of columns by delimiter
    explicit SelectTableContainer( const char * aname, const std::vector<std::string>& list,
                                   const char  split_col_symb = '\t' );
    /// Construct table from 2D array.
    explicit SelectTableContainer( const char * aname, const jsonio17::values_table_t& table_values,
                                   const jsonio17::values_t& header_data = {} );
    /// Construct table from 2D array.
    explicit SelectTableContainer( const char * aname, const QVector< QVector<QVariant> >& table_values,
                                   const QVector< QString >& header_data = {}):
        AbstractContainer(aname), col_heads(header_data), matrix(table_values)
    {
        if( col_heads.empty() && !matrix.empty() )
            for(int jj=0; jj< matrix[0].size(); jj++ )
                col_heads.push_back( QString("%1").arg(jj) );
    }

    virtual ~SelectTableContainer()
    {}

    int rowCount() const override
    {
        return matrix.size();
    }
    int columnCount() const override
    {
        return col_heads.size();
    }
    QVariant data( int line, int column ) const override
    {
        return matrix.at( line ).at( column );
    }
    bool setData( int line, int column, const QVariant& value ) override
    {
        Q_UNUSED( line );
        Q_UNUSED( column );
        Q_UNUSED( value );
        return true;
    }
    virtual QString headerData ( int section ) const override
    {
        return col_heads[section];
    }
    virtual bool  isEditable( int line, int column ) const override
    {
        Q_UNUSED( line );
        Q_UNUSED( column );
        return false;
    }
    void resetData() override {}

protected:

    QVector< QString > col_heads;
    QVector< QVector<QVariant> > matrix;

};

class SelectDialogPrivate;

/// \class SelectDialog - dialog for single or multiple
/// selection from list/table;
class SelectDialog : public QDialog
{
    Q_OBJECT

    Q_DISABLE_COPY(SelectDialog)

public slots:

    void objectChanged() {}
    void CmHelp();
    void CmSelectAll();
    void CmClearAll();

public:

    /// Construct table from list of lines.
    /// Split each line into a list of columns by delimiter
    explicit SelectDialog( bool is_multi_selection, QWidget* parent, const char* title,
                           const std::vector<std::string>& list, const char  split_col_symb = '\t',
                           int mode = MatrixTable::tbNoMenu );

    /// Construct table from 2D array.
    explicit SelectDialog( bool is_multi_selection, QWidget* parent, const char* title,
                           const jsonio17::values_table_t& table_values, const jsonio17::values_t& header_data = {},
                           int mode = MatrixTable::tbNoMenu );

    /// Construct table from 2D array.
    explicit SelectDialog( bool is_multi_selection, QWidget* parent, const char* title,
                           const QVector< QVector<QVariant> >& table_values,  const QVector< QString >& header_data = {},
                           int mode = MatrixTable::tbNoMenu );

    /// Destructor
    virtual ~SelectDialog();

    /// Set Singler selection
    void setSelection( int selrow );

    /// Set Singler selection
    template <class T>
    void setSelection( const T& selrow )
    {
        setSelection(  static_cast<int>(selrow) );
    }

    /// Set Multiple selection
    void setSelection( std::set<size_t>& selrows );

    /// Set Multiple selection
    template<typename T,
             template <typename, typename > class Container>
    void setSelection( const Container<T, std::allocator<T>>& selrows )
    {
        std::set<size_t> sellst;
        for( auto it = selrows.begin(); it != selrows.end(); ++it )
            sellst.insert(static_cast<size_t>(*it));
        setSelection( sellst );
    }

    /// Returns single selection, \returns '-1' if nothing selected
    int selIndex() const;

    /// Returns single selection, \throw exeption if nothing selected
    std::size_t getSelectedIndex() const;

    /// Returns selection array, array is empty if nothing is selected
    std::set<size_t> allSelected() const;

    /// Get multiple selection
    template<typename T,
             template <typename, typename > class Container>
    void getSelection( Container<T, std::allocator<T>>& selrows )
    {
        selrows.clear();
        auto allrows = allSelected();
        for ( const auto& row: allrows )
            selrows.push_back(static_cast<T>(row));
    }

protected:

    QScopedPointer<SelectDialogPrivate> const impl_ptr;
    inline SelectDialogPrivate* impl_func()
              { return reinterpret_cast<SelectDialogPrivate *>(impl_ptr.get()); }
    inline const SelectDialogPrivate* impl_func() const
              { return reinterpret_cast<const SelectDialogPrivate *>(impl_ptr.get()); }

};

} // namespace jsonui17

