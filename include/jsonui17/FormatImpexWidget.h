#pragma once

#include "jsonui17/BaseWidget.h"

namespace jsonio17 {
   class DBSchemaDocument;
   class DBQueryBase;
}


namespace jsonui17 {


using  ExecuteImpex_f = std::function<void( std::string impex_data_name,
                                            std::string impex_format_type, std::string impex_format_string,
                                            std::string data_file, bool overwrite )>;


class FormatImpexWidgetPrivate;

/// \class FormatImpexWidget - Widget to work with Foreign Import/Export File Format
class FormatImpexWidget : public BaseWidget
{
    Q_OBJECT

    Q_DISABLE_COPY(FormatImpexWidget)

public slots:

    void openRecordKey(  const std::string& , bool  = false  ) override {}
    void changeKeyList() override {}

    void CmNew();
    void CmClone();
    //void CmEditID(bool);

    void CmExportJSON();
    void CmImportJSON();
    void CmSelectDataFile();
    void CmRunScript();

    void CmRead();
    void CmUpdate();
    void CmDelete();

public:

    enum MODE_ {
         editMode = 0,
         runModeImport = 1,
         runModeExport = 2,
         runModeUpdate = 3
       };

    explicit FormatImpexWidget( MODE_ mode, const std::string& run_schema_name = "", QWidget *parent = nullptr);
    ~FormatImpexWidget();

    /// Set function to execute script
    void setExecuteFunction( ExecuteImpex_f afunc );

    void setQueryEdited( QueryWidget*  ) override {}

private:

    inline FormatImpexWidgetPrivate* impl_func()
              { return reinterpret_cast<FormatImpexWidgetPrivate *>(impl_ptr.get()); }
    inline const FormatImpexWidgetPrivate* impl_func() const
              { return reinterpret_cast<const FormatImpexWidgetPrivate *>(impl_ptr.get()); }

};

} // namespace jsonui17

