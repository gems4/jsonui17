#pragma once

#include "jsonui17/BaseWidget.h"

namespace jsonui17 {

class JsonEditWidgetPrivate;

/// \class JsonEditWidget - window to edit/create free format json or
/// schema based json
class JsonEditWidget : public BaseWidget
{
    Q_OBJECT

    Q_DISABLE_COPY(JsonEditWidget)

public slots:

    void openRecordKey(  const std::string& , bool  = false  ) override {}
    void openRecordKey(  const QModelIndex&  ) {}
    void changeKeyList() override {}

    /// Set empty or default json document
    void CmNew();
    /// Clone json document (clear _id, _rev ... )
    void CmClone();

    void CmExportJSON();
    void CmImportJSON();
#ifndef IMPEX_OFF
    void CmExportYAML();
    void CmImportYAML();
    void CmExportXML();
    void CmImportXML();
#endif

    /// Save current document to database as new
    void CmCreateInsert();
    /// Read new document from database
    void CmRead();
    /// Save current document to database
    void CmUpdate();
    /// Delete current document from database
    void CmDelete();

public:

    explicit JsonEditWidget( const std::string& aschema_name = "",
                             const std::string& collect_name = "",
                             QWidget *parent = nullptr );
    ~JsonEditWidget();

    void setQueryEdited( QueryWidget*  ) override {}

private:

    inline JsonEditWidgetPrivate* impl_func()
              { return reinterpret_cast<JsonEditWidgetPrivate *>(impl_ptr.get()); }
    inline const JsonEditWidgetPrivate* impl_func() const
              { return reinterpret_cast<const JsonEditWidgetPrivate *>(impl_ptr.get()); }

};

} // namespace jsonui17

