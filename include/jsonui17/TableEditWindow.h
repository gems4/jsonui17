#pragma once

#include <QMainWindow>
#include "jsonui17/models/table_view.h"


namespace jsonui17 {

class TableEditWidgetPrivate;

/// \class TableEditWidget  widget to work with table data.
class TableEditWidget : public QMainWindow
{
    Q_OBJECT

    Q_DISABLE_COPY(TableEditWidget)

public slots:

    void updateTable();
    /// Graphics
    void plotTable();
    void CmHelp();

    void CmExportCSV();
    void CmExportSelectedCSV();
    void CmImportCSV();

    void slotPopupContextMenu(const QPoint &pos);

public:

    /// Constractor connect to extern table model
    explicit TableEditWidget( const char* title, std::shared_ptr<AbstractContainer> datacontainer,
                              int mode = MatrixTable::tbEdit, QWidget *parent = nullptr );
    /// Constructor from csv file
    explicit TableEditWidget( const char* title, const std::string& file_path,
                              int mode = MatrixTable::tbEdit, QWidget *parent = nullptr );
    /// Destructor
    ~TableEditWidget();

    /// Read new CSV file ( now only in case Constructor from csv file )
    void openNewCSV( const std::string& file_path );

    /// Set the X axis column index and a list of Y axis indexes
    void setXYaxis(int ndxX, std::vector<int> ndxY);

private:

    QScopedPointer<TableEditWidgetPrivate> impl_ptr;
    inline TableEditWidgetPrivate* impl_func()
              { return reinterpret_cast<TableEditWidgetPrivate *>(impl_ptr.get()); }
    inline const TableEditWidgetPrivate* impl_func() const
              { return reinterpret_cast<const TableEditWidgetPrivate *>(impl_ptr.get()); }

    void closeEvent(QCloseEvent* e) override;
    QString read_new_CSV( const std::string& file_path );

};

} // namespace jsonui17

