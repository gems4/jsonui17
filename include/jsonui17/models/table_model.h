#pragma once

#include <QSortFilterProxyModel>
#include <QAbstractTableModel>
#include "jsonui17/models/table_container.h"


namespace jsonui17 {

///  \class MatrixModel
/// class for represents the data set and is responsible for fetchin
/// the data is neaded for viewing and for writing back any changes.
/// Reading/writing data from/to inherited classes from AbstractContainer interface
class MatrixModel: public QAbstractTableModel
{
    friend class MatrixDelegate;
    friend class MatrixTable;

    Q_OBJECT

public:

    enum NewRole{
            TypeRole = Qt::UserRole
    };


    explicit MatrixModel( std::shared_ptr<AbstractContainer> datacontainer, QObject * parent = nullptr ):
        QAbstractTableModel(parent), data_container( datacontainer )
    {
        if( parent )
            connect( this, SIGNAL(dataChanged(const QModelIndex&,const QModelIndex&)),
                     parent,  SLOT(objectChanged()) );
    }

    ~MatrixModel() {}

    int rowCount( const QModelIndex& parent ) const
    {
        Q_UNUSED( parent );
        return data_container->rowCount();
    }
    int columnCount ( const QModelIndex& parent  ) const
    {
        Q_UNUSED( parent );
        return data_container->columnCount();
    }

    QVariant data ( const QModelIndex & index, int role ) const;
    bool setData ( const QModelIndex & index, const QVariant & value, int role );
    QVariant headerData ( int section, Qt::Orientation orientation, int role ) const;
    Qt::ItemFlags flags( const QModelIndex & index ) const;

    void resetMatrixData();

protected:

    std::shared_ptr<AbstractContainer> data_container;

};

/// The SortFilterProxyModel class provides support for sorting
/// and filtering data passed between another model and a view.
class SortFilterProxyModel : public QSortFilterProxyModel
 {
     Q_OBJECT

 public:

     SortFilterProxyModel( QObject *parent = nullptr ):
         QSortFilterProxyModel(parent)
     { }

 protected:
     bool lessThan(const QModelIndex &left, const QModelIndex &right) const;

 };


///  \class StringVectorModel
/// class for represents the data set and is responsible for fetchin
/// the data is neaded for viewing and for writing back any changes.
/// Reading/writing data from/to string vector
class StringVectorModel : public QAbstractListModel
{
    Q_OBJECT

public:

    explicit StringVectorModel( const jsonio17::values_t &stdvector, QObject *parent = nullptr ):
        QAbstractListModel(parent)
    {
        from_values_t(stdvector);
    }

    int rowCount(const QModelIndex &parent = QModelIndex()) const override
    {
        if( parent.isValid() )
            return 0;
        return internal_lst.size();
    }

    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    void setStringList( const jsonio17::values_t& stdvector );

    jsonio17::values_t stringList() const
    {
        return to_values_t();
    }

protected:

    QVector<QString> internal_lst;

    void from_values_t( const jsonio17::values_t &stdvector );
    jsonio17::values_t to_values_t() const;

};


} // namespace jsonui17

