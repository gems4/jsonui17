#pragma once

#include <memory>
#include <QItemDelegate>
#include "jsonui17/models/base_model.h"
#include "jsonio17/jsonschema.h"


namespace jsonui17 {


/// \class JsonSchemaModel
/// Class for represents the data set and is responsible for fetching the data
/// is needed for viewing and for writing back any changes.
/// Reading/writing data from/to schema based json objects
class JsonSchemaModel: public JsonBaseModel
{
    Q_OBJECT

    friend class JsonSchemaDelegate;

public:

    /// Extern flag to show schema comments into editor
    static bool showComments;
    /// Extern flag to show enum names into editor
    static bool useEnumNames;
    /// Extern flag to edit system data
    static bool editID;

    JsonSchemaModel(  const std::string& json_string, const std::string& schema_name,
                      const QStringList& header_names,  QObject* parent = nullptr  );
    ~JsonSchemaModel() {}

    /// Return internal data to const link
    const jsonio17::JsonBase& current_object() const override
    {
        return  root_node;
    }

    /// Extern update data
    void setupModelData( const std::string& json_string, const std::string& schema ) override;
    /// Extern update data
    //void setupModelData( const jsonio17::JsonBase& new_object, const std::string& schema ) override;

private:

    QModelIndex index( int row, int column, const QModelIndex& parent ) const override;
    QModelIndex parent( const QModelIndex& child ) const override;
    int rowCount ( const QModelIndex& parent ) const override;
    int columnCount ( const QModelIndex& parent  ) const override;
    QVariant data ( const QModelIndex& index, int role ) const override;
    bool setData ( const QModelIndex& index, const QVariant& value, int role ) override;
    QVariant headerData ( int section, Qt::Orientation orientation, int role ) const override;
    Qt::ItemFlags flags ( const QModelIndex& index ) const override;


    bool isCanBeResized( const QModelIndex& index ) const override;
    bool isCanBeAdd( const QModelIndex& index ) const override;
    bool isCanBeRemoved( const QModelIndex& index ) const override;
    bool isCanBeCloned( const QModelIndex& index ) const override;
    bool isUnion( const QModelIndex&  index ) const override;

    void resizeArray( QWidget* parent, const QModelIndex& index ) override;
    void delObject( QWidget* parent, const QModelIndex& index ) override;
    void delObjects( QWidget* parent, const QModelIndex& index  ) override;
    const QModelIndex addObjects( QWidget* parent, const QModelIndex& index ) override;
    const QModelIndex addObject( QWidget* parent, const QModelIndex& index ) override;
    const QModelIndex cloneObject( QWidget* parent, const QModelIndex& index  ) override;

    void delObjectsUnion( QWidget* , const QModelIndex&  ) override;
    void setFieldData( const QModelIndex& index, const std::string& data ) override;

protected:

    /// Names of columns
    QStringList header_data;
    /// Current document schema
    std::string currentschema;
    /// Current document object
    jsonio17::JsonSchema root_node;

    /// Return internal data link
    jsonio17::JsonSchema& current_data() const override
    {
        return  const_cast<jsonio17::JsonSchema&>(root_node);
    }

    jsonio17::JsonBase* lineFromIndex(const QModelIndex& index) const override;

    QString get_value(int column, const jsonio17::JsonBase *object) const;
    const jsonio17::EnumDef *get_map_enumdef(const QModelIndex &index) const;
    const jsonio17::EnumDef *get_i32_enumdef(const QModelIndex &index) const;

};


/// \class JsonSchemaDelegate individual items in views are rendered and edited using delegates
class JsonSchemaDelegate: public QItemDelegate
{
    Q_OBJECT

    QWidget *enums_to_combobox( QWidget *parent, const jsonio17::EnumDef *enumdef) const;

public:

    JsonSchemaDelegate( QObject * parent = nullptr ):
        QItemDelegate( parent )
    {}
    QWidget *createEditor( QWidget *parent,
                           const QStyleOptionViewItem &option,
                           const QModelIndex &index) const;
    void setEditorData( QWidget *editor, const QModelIndex &index) const;
    void setModelData(  QWidget *editor, QAbstractItemModel *model,
                        const QModelIndex &index) const;
};


} // namespace jsonui17

