#pragma once

#include <QVariant>
#include "jsonui17/models/table_container.h"

namespace jsonui17 {

/// \class  CSVContainer scv table data container
/// with description of one plot in graph screen
class CSVContainer : public AbstractContainer
{

public:

    explicit CSVContainer( const char* aname, QString data ):
        AbstractContainer(aname)
    {
        matrix_from_csv_string( data );
    }

    explicit CSVContainer( ):
        AbstractContainer("csv")
    {}

    virtual ~CSVContainer() {}

    int rowCount() const override
    {
        return matrix.size();
    }
    int columnCount() const override
    {
        return col_heads.size();
    }
    QVariant data( int line, int column ) const override
    {
        QVariant val = matrix.at(line).at(column);
        return val;
    }
    bool setData( int line, int column, const QVariant &value ) override
    {
        matrix[line].replace( column, value );
        return true;
    }
    virtual QString headerData ( int section ) const override
    {
        QString head = col_heads[section];
        int ii = x_colms.indexOf( section );
        if( ii != -1)
            head += "(x)";
        ii = y_colms.indexOf( section );
        if( ii != -1)
            head += "(y)";
        return head;
    }
    virtual Data_Types getType( int line, int column ) const override
    {
        Q_UNUSED( line );
        if( col_double[column] )
            return ftDouble;
        else
            return ftString;
    }
    virtual QString getToolTip( int line, int column ) const override
    {
        return QString("%1[%2]").arg( col_heads[column] ).arg(line);
    }

    void resetData() override
    {
        matrix_from_csv_string( csv_data );
    }

    void setCsvString( const QString& value_csv )
    {
        csv_data = value_csv;
    }

    QString getCsvString()
    {
        return  matrix_to_csv_string();
    }

    void setXColumns( const std::vector<int>& clmns )
    {
#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
        x_colms = QVector<int>( clmns.begin(), clmns.end() );
#else
        x_colms.clear();
        for( auto cl:clmns )
            x_colms.append( cl );
#endif

    }

    void setYColumns( const std::vector<int>& clmns )
    {
#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
        y_colms = QVector<int>( clmns.begin(), clmns.end() );
#else
        y_colms.clear();
        for( auto cl:clmns )
            y_colms.append( cl );
#endif
    }

protected:

    /// Current data
    QString csv_data;
    /// Heards of columns
    QVector< QString > col_heads;
    /// Numerical column condition
    QVector< bool > col_double;
    /// CSV matrix data
    QVector< QVector<QVariant> > matrix;

    /// Abscissa columns list
    QVector<int> x_colms;
    /// Ordinate columns list
    QVector<int> y_colms;
    void matrix_from_csv_string( const QString& value_csv );
    QString matrix_to_csv_string();

};


} // namespace jsonui17

