#pragma once

#include <QVariant>
#include <QColor>
#include <QVector>
#include "jsonio17/dbquerybase.h"

namespace jsonui17 {

/// Function fetching document from a collection that match the specified condition
using  GetColor_f = std::function<QColor( int line, int column )>;

/// Interface for table data container
class AbstractContainer
{

public:

    /// Item types into table
    enum Data_Types
    {
        ftString = 0,
        ftDouble = 1,
        ftNumeric = 2
    };


    explicit AbstractContainer( const char* aname ):
        table_name( aname )
    { }

    virtual ~AbstractContainer() {}

    virtual int rowCount() const = 0;
    virtual int columnCount() const = 0;
    virtual QVariant data( int line, int column ) const = 0;
    virtual bool setData( int line, int column, const QVariant & value ) = 0;
    virtual QString headerData ( int section ) const = 0;

    virtual bool  isEditable( int line, int column ) const
    {
        Q_UNUSED( line );
        Q_UNUSED( column );
        return true;
    }

    virtual Data_Types getType( int line, int column ) const
    {
        Q_UNUSED( line );
        Q_UNUSED( column );
        return ftString;
    }

    virtual QString getToolTip( int line, int column ) const
    {
        return QString("%1[%2,%3]").arg( table_name ).arg(line).arg(column);
    }

    virtual bool useColor() const
    {
        return false;
    }

    virtual QColor getColor( int line, int column ) const
    {
        Q_UNUSED( line );
        Q_UNUSED( column );
        return QColor(Qt::black);
    }

    virtual void resetData() = 0;

protected:

    QString table_name;

};


/// Class for colorized internal table data container
class ColorAbstractContainer : public AbstractContainer
{

public:

    /// Construct table from list of headers.
    explicit ColorAbstractContainer(const char * aname):
        AbstractContainer(aname)
    {
        set_default_color_function();
    }

    void setColorFunction( GetColor_f func )
    {
        use_color_function = true;
        color_function = func;
    }

    bool useColor() const override
    {
        return use_color_function;
    }

    QColor getColor( int line, int column ) const override
    {
        return color_function(line, column);
    }

protected:

    bool use_color_function = false;
    GetColor_f color_function;

    void set_default_color_function()
    {
        color_function =  []( int, int ) {  return QColor(Qt::black);  };
    }

};


} // namespace jsonui17

