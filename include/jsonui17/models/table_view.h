#pragma once

#include <memory>
#include <QItemDelegate>
#include <QTableView>
#include "jsonui17/models/table_model.h"

namespace jsonui17 {

#ifdef __APPLE__
const char  splitRow = '\r';
const char  splitCol = '\t';
#else
const char  splitRow = '\n';
const char  splitCol = '\t';
#endif

class Selection;
using  ShowRecord_f = std::function<void( const QModelIndex& index)>;


///  \class MatrixDelegate
/// individual items in views are rendered and edited using delegates
class MatrixDelegate: public QItemDelegate
{
    Q_OBJECT

public:

    explicit MatrixDelegate( QObject * parent = nullptr ):
        QItemDelegate( parent )
    {}

    QWidget *createEditor( QWidget *parent, const QStyleOptionViewItem &option,
                           const QModelIndex &index) const override;
};


///  \class MatrixTable implements a table view
/// that displays items from a MatrixModel model.
class MatrixTable: public QTableView
{
    Q_OBJECT


public slots:

    void slotPopupContextMenu(const QPoint& pos);
    void selRow();
    void selColumn();
    void copyData();
    void copyDataHeader();

    // only if editable
    void runCalc();
    void cutData();
    void clearData();
    void pasteData();
    void pasteTransposedData();

public:

    /// Types of Matrix table mode
    enum ModeTypes {
        tbSort =   0x0010,  ///< Added sorting into colums
        tbGraph =  0x0004,  ///< Connect 2d graphic for columns
        tbEdit =   0x0002,  ///< Enable editing
        tbNoMenu = 0x0001,  ///< Disable context menu
        tbShow = 0          ///< Use only show mode
    };

    explicit MatrixTable( QWidget * parent = nullptr, int mode = tbEdit );

    /// Save selected data to csv format file
    void saveSelectedToCSV( const std::string& file_name );
    /// Save all table data to csv format file
    void saveAllToCSV( const std::string& file_name )
    {
        selectAll();
        saveSelectedToCSV( file_name );
        clearSelection();
    }
    /// Load new table from csv format file
    void loadAllFromCSV( const std::string& file_name )
    {
        Q_UNUSED( file_name );
    }

    void makePopupContextMenu( QMenu *menu, QModelIndex index );

    /// Get header data
    QVector<QString> headerData() const;

    /// Get table data
    QVector<QVector<QVariant>> tableValues() const;

protected:

    int mode_using;
    void keyPressEvent(QKeyEvent* e) override;

    Selection get_selection_range( bool to_paste = false );
    QString create_string( Selection& sel, char split_col_symb );
    QString create_header( char split_col_symb  );
    void paste_into_area( Selection& sel, bool transpose, char split_col_symb );
    void set_from_string( char splitrow, const QString& str, Selection sel, bool transpose );

};

///  \class MatrixTableProxy implements a table view
/// that displays items from a MatrixModel model.
/// Provides support for sorting and filtering data.
class MatrixTableProxy: public MatrixTable
{
    Q_OBJECT

public:

    explicit MatrixTableProxy(  QWidget * parent = nullptr, int mode = tbEdit ):
        MatrixTable( parent, mode)
    {}

    /// Add proxy model
    void setModel(QAbstractItemModel *model) override;

    /// Set&select current row
    virtual void setCurrentRow( int row );
    /// Returns single selection, throw exeption if nothing selected
    virtual std::size_t getCurrentRow() const;
    /// Returns single selection, returns '-1' if nothing selected
    virtual int selectedRow() const;

    /// Set selected rows
    virtual void selectRows( const std::set<size_t>& rows );
    /// Return all selected rows
    virtual std::set<std::size_t> allSelectedRows();

    /// Set Multiple selection
    template< typename T,
              template <typename, typename> class Container>
    void setSelection( const Container<T, std::allocator<T> >& selrows )
    {
        std::set<size_t> sellst;
        for (auto it = selrows.begin(); it != selrows.end(); ++it)
            sellst.insert(static_cast<size_t>(*it));
        selectRows( sellst );
    }

    /// Return the model index in the source model that corresponds to the proxyIndex in the proxy model.
    QModelIndex sourceModelIndex(const QModelIndex &proxyIndex) const;
};

///  \class MatrixUniqueSelection implements a table view
/// that displays items from a MatrixModel model.
/// Can be selected only unique values into defined column.
class MatrixUniqueSelection: public MatrixTableProxy
{
    Q_OBJECT

protected slots:

    void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected) override
    {
        MatrixTable::selectionChanged( selected, deselected);
        slotSelectionChange(selected, deselected);
    }

    void slotSelectionChange( const QItemSelection &, const QItemSelection & );

public:

    explicit MatrixUniqueSelection( const std::vector<int>& col_uniq, QWidget * parent = nullptr, int mode = tbEdit ):
        MatrixTableProxy( parent, mode), column_to_be_unique(col_uniq)
    {}

protected:

    std::vector<int> column_to_be_unique;

};

///  \class DBKeyTable implements a table view that displays
/// items from database keys/fields table.
class DBKeysTable: public MatrixTableProxy
{
    Q_OBJECT

    ShowRecord_f open_function;

protected:
    void keyPressEvent(QKeyEvent* e);

public slots:
    void CmNext();
    void CmPrevious();

public:

    DBKeysTable( QWidget * parent, ShowRecord_f show_fn ):
        MatrixTableProxy(parent, MatrixTable::tbNoMenu|MatrixTable::tbSort ), open_function(show_fn)
    {}

    /// Move to current id
    void move_to_key( std::string id_key );

};


} // namespace jsonui17

