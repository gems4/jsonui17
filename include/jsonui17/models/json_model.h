#pragma once

#include <memory>
#include <QItemDelegate>
#include "jsonui17/models/base_model.h"
#include "jsonio17/jsonfree.h"

namespace jsonui17 {


/// \class JsonFreeModel
/// Class for represents the data set and is responsible for fetching the data
/// is needed for viewing and for writing back any changes.
/// Reading/writing data from/to json objects
class JsonFreeModel: public JsonBaseModel
{
    Q_OBJECT

    friend class JsonFreeDelegate;

public:

    explicit JsonFreeModel( const std::string& json_string, const QStringList& header_names,  QObject* parent = nullptr );
    ~JsonFreeModel() {}

    /// Return internal data to const link
    const jsonio17::JsonBase& current_object() const override
    {
        return  root_node;
    }

    /// Extern update data
    void setupModelData( const std::string& json_string, const std::string& schema ) override;
    /// Extern update data
    //void setupModelData( const jsonio17::JsonBase& new_object, const std::string& schema ) override;

private:

    QModelIndex index(int row, int column, const QModelIndex& parent) const override;
    QModelIndex parent(const QModelIndex& child) const override;
    int rowCount ( const QModelIndex& parent ) const override;
    int columnCount ( const QModelIndex& parent  ) const override;
    QVariant data ( const QModelIndex& index, int role ) const override;
    bool setData ( const QModelIndex& index, const QVariant& value, int role ) override;
    QVariant headerData ( int section, Qt::Orientation orientation, int role ) const override;
    Qt::ItemFlags flags ( const QModelIndex& index ) const override;

    bool isCanBeResized( const QModelIndex& index ) const override
    {
        return  lineFromIndex(index)->isArray();
    }
    bool isCanBeAdd( const QModelIndex& index ) const override
    {
        auto line = lineFromIndex(index);
        return ( line->isTop() || line->getParent()->isObject() ||
                 ( line->isObject()  && line->size() < 1 ) ) ;
    }
    bool isCanBeRemoved( const QModelIndex& index ) const override
    {
        auto line = lineFromIndex(index);
        return  !line->isTop() && line->getParent()->isStructured() ;
    }
    bool isCanBeCloned( const QModelIndex& index ) const override
    {
        auto line = lineFromIndex(index);
        return  !line->isTop() && line->getParent()->isArray();
    }

    void resizeArray( QWidget* parent, const QModelIndex& index ) override;
    void delObject( QWidget* parent, const QModelIndex& index ) override;
    void delObjects( QWidget* parent, const QModelIndex& index  ) override;
    const QModelIndex addObjects( QWidget* parent, const QModelIndex& index ) override;
    const QModelIndex addObject( QWidget* parent, const QModelIndex& index ) override;
    const QModelIndex cloneObject( QWidget* parent, const QModelIndex& index  ) override;

    // void delObjectsUnion( QWidget* , const QModelIndex&  );

protected:

    /// Names of columns
    QStringList header_data;
    /// Current document object
    jsonio17::JsonFree root_node;

    /// Return internal data link
    jsonio17::JsonFree& current_data() const override
    {
        return  const_cast<jsonio17::JsonFree&>(root_node);
    }

    jsonio17::JsonBase* lineFromIndex(const QModelIndex& index) const override;

};


/// \class JsonFreeDelegate individual items in views are rendered and edited using delegates
class JsonFreeDelegate: public QItemDelegate
{
    Q_OBJECT

public:

    JsonFreeDelegate( QObject * parent = nullptr ):
        QItemDelegate( parent ) {}
    QWidget *createEditor( QWidget *parent,
                           const QStyleOptionViewItem &option,
                           const QModelIndex &index) const;
};


} // namespace jsonui17

