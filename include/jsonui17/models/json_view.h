#pragma once

#include <QTreeView>
#include "jsonui17/models/base_model.h"

namespace jsonui17 {

class ExpandedLevelLine;

/// \class JsonView implements a tree view of json based structure
/// that displays items from a JsonBaseModel model.
class JsonView: public QTreeView
{
    Q_OBJECT

public slots:

    void CmHelp();
    void CmCalc();
    void CmAddObject();
    void CmAddObjects();
    void CmDelObject();
    void CmDelObjects();
    void CmDelObjectsUnion();
    void CmResizeArray();
    void CmCloneObject();
    void CmResetObject();

    void CopyFieldPath();
    void CopyField();
    void PasteField();

protected slots:

    void slotPopupContextMenu( const QPoint& pos );
    void changeCurrent( int section );

public:

    /// Extern flag to keep data fields expanded
    static bool expandedFields;

    /// Constructor
    JsonView( QWidget * parent = nullptr );

    /// Update model data with restore Expanded Level
    void updateModelData( const std::string& json_string, const std::string& schema_name );

    /// Reset model data with restore Expanded Level
    void resetModel();

    /// Return internal data to const link
    const jsonio17::JsonBase& current_object()
    {
        return  view_model()->current_object();
    }

    /// Return internal data to json string
    virtual std::string saveToJson()
    {
        auto themodel = view_model();
        if( themodel )
            return themodel->current_data().dump();
        else
            return "";
    }

    /// Explicit type conversion between the JSON path value and a compatible primitive value.
    /// The following jsonpath expression could be used
    ///     "name1.name2.3.name3"
    ///     "name1.name2[3].name3"
    ///     "/name1/name2/3/name3"
    ///     "/name1/name2[3]/name3"
    ///     "[\"name1\"][\"name2\"][3][\"name3\"]"
    /// The value is filled into the input parameter.
    /// @return true if JSON value exist and can be converted to value type.
    template <typename T>
    bool getValueViaPath( const std::string& jsonpath, T& val, const T& defval   ) const
    {
        auto themodel = dynamic_cast<JsonBaseModel*>( model() );
        if( themodel )
            return themodel->current_data().get_value_via_path( jsonpath, val, defval );
        else
        {
            val = defval;
            return false;
        }
    }

    /// Use jsonpath to modify any value in a JSON object.
    /// The following jsonpath expression could be used
    ///     "name1.name2.3.name3"
    ///     "name1.name2[3].name3"
    ///     "/name1/name2/3/name3"
    ///     "/name1/name2[3]/name3"
    ///     "[\"name1\"][\"name2\"][3][\"name3\"]"
    /// @return true if jsonpath present in a JSON object.
    template <typename T>
    bool setValueViaPath( const std::string& jsonpath, const T& val  )
    {
        auto themodel = view_model();
        if( themodel )
            return themodel->current_data().set_value_via_path( jsonpath, val );
        else
            return false;
    }

    void setOid( const std::string& oid  )
    {
        auto themodel = view_model();
        if( themodel )
            themodel->current_data().set_oid( oid );
    }

protected:

    const JsonBaseModel* current_index_model( const QModelIndex& index ) const
    {
        if( index.isValid() )
            return dynamic_cast<const JsonBaseModel*>( index.model() );
        return nullptr;
    }

    JsonBaseModel* view_model()
    {
        return dynamic_cast<JsonBaseModel*>( model() );
    }

    void keyPressEvent(QKeyEvent* e) override;

    void save_expanded_state(std::shared_ptr<ExpandedLevelLine> topList);
    void restore_expanded_state(std::shared_ptr<ExpandedLevelLine> topList);
    void save_expanded_on_level(const QModelIndex& index, std::shared_ptr<ExpandedLevelLine> parent);
    void restore_expanded_on_level(const QModelIndex& index, std::shared_ptr<ExpandedLevelLine> top_data);
};

} // namespace jsonui17

