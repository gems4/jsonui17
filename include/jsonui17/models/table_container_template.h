#pragma once

#include "jsonio17/type_test.h"
#include "jsonui17/models/table_container.h"

namespace jsonui17 {

/// Class for numeric vector line
template < class T,
           std::enable_if_t<is_container<T>{}, int> = 0 >
class ContainerLine
{

public:

    explicit ContainerLine( T& line_fields )
    {
        using val_t = std::remove_const_t<typename T::value_type>;

        if( std::is_integral<val_t>::value )
            val_type = AbstractContainer::ftNumeric;
        else if( std::is_floating_point<val_t>::value )
            val_type = AbstractContainer::ftDouble;
        else
            val_type = AbstractContainer::ftString;

        copy_data( line_fields );
    }

    virtual ~ContainerLine() {}

    int size() const
    {
        return column_values.size();
    }

    QString data( int column ) const
    {
        return column_values[column];
    }

    bool setData( int column, const QString &value )
    {
        column_values[column] = value;
        return true;
    }

    virtual AbstractContainer::Data_Types type() const
    {
        return val_type;
    }

protected:

    AbstractContainer::Data_Types val_type;
    QVector<QString> column_values;

    void copy_data( T& line_fields )
    {
        for( const auto& data: line_fields )
        {
            switch( val_type )
            {
            case AbstractContainer::ftString:
                column_values.append( QString("%1").arg( data.c_str() ));
                break;
            case AbstractContainer::ftDouble:
            case AbstractContainer::ftNumeric:
                column_values.append( QString("%1").arg(data));
                break;
            }
        }
    }

};


/// Class for numeric matrix container
template < template<class ...> class H, class C,
           std::enable_if_t< is_container<H<C>>{} &is_container<C>{}, int> = 0 >
class MatrixContainer : public AbstractContainer
{

public:

    MatrixContainer( const char * aname, H<C>& afields ):
        AbstractContainer(aname)
    {
        copy_data( afields );
    }

    virtual ~MatrixContainer() {}

    int rowCount() const
    {
        return lines_values.size();
    }

    int columnCount() const
    {
        if( lines_values.size() )
            return 0;
        else
            lines_values[0].size();
    }

    QVariant data( int line, int column ) const
    {
        return lines_values[line].data( column );
    }

    bool setData( int line, int column, const QVariant &value )
    {
        lines_values[line].setData( column, value );
        return true;
    }

    virtual QString headerData ( int section ) const
    {
        return QString("%1").arg( section );
    }

    virtual AbstractContainer::Data_Types getType( int line, int column ) const override
    {
        Q_UNUSED( line );
        Q_UNUSED( column );
        return val_type;
    }

    void resetData() {}

protected:

    AbstractContainer::Data_Types val_type = AbstractContainer::ftString;
    QVector< ContainerLine<C> > lines_values;

    void copy_data( H<C>& afields )
    {
        for( const auto& data: afields )
        {
            lines_values.append( ContainerLine( data) );
        }
        if( !lines_values.empty() )
            val_type = lines_values[0].type();
    }

};

} // namespace jsonui17
