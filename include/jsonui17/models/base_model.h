#pragma once

#include <QAbstractItemModel>
#include "jsonio17/jsonbase.h"


namespace jsonui17 {

/// \class TBsonAbstractModel
/// Abstract class for represents the data set and is responsible for fetching the data
/// is needed for viewing and for writing back any changes.
/// Reading/writing data from/to json type objects
class JsonBaseModel: public QAbstractItemModel
{
    Q_OBJECT

    friend class ModelLineDialog;
    friend class JsonView;

public:

    JsonBaseModel( QObject* parent = nullptr ):
        QAbstractItemModel(parent)
    {}
    ~JsonBaseModel() {}

    /// Return internal data to const link
    virtual const jsonio17::JsonBase& current_object() const = 0;

    /// Extern update data
    virtual void setupModelData( const std::string& json_string, const std::string& schema ) = 0;
    /// Extern update data
    //virtual void setupModelData( const jsonio17::JsonBase& new_object, const std::string& schema ) = 0;

private:

    virtual bool isNumber( const QModelIndex& index ) const
    {
        return  lineFromIndex(index)->isNumber();
    }

    virtual bool isCanBeResized( const QModelIndex&  ) const
    {
        return false;
    }

    virtual bool isCanBeAdd( const QModelIndex&  ) const
    {
        return false;
    }

    virtual bool isUnion( const QModelIndex&  ) const
    {
        return false;
    }

    virtual bool isCanBeRemoved( const QModelIndex& ) const
    {
        return false;
    }

    virtual bool isCanBeCloned( const QModelIndex&  ) const
    {
        return false;
    }

    virtual void resizeArray( QWidget* , const QModelIndex&  ) {}
    virtual void delObject( QWidget* , const QModelIndex&  ) {}
    virtual void delObjects( QWidget* , const QModelIndex&  ) = 0;
    virtual void delObjectsUnion( QWidget* , const QModelIndex&  ) {}
    virtual const QModelIndex addObject( QWidget* , const QModelIndex& index ) { return index; }
    virtual const QModelIndex addObjects( QWidget* , const QModelIndex& index  ) { return index; }
    virtual const QModelIndex cloneObject( QWidget* , const QModelIndex& index  ) { return index;  }
    virtual void resetObject( QWidget* , const QModelIndex&  );

    virtual std::string helpName( const QModelIndex& index ) const
    {
        return lineFromIndex( index )->getHelpName();
    }

    virtual std::string  getFieldPath( const QModelIndex& index ) const
    {
        return lineFromIndex(index)->get_path();
    }

    virtual std::string  getFieldData( const QModelIndex& index ) const
    {
        return lineFromIndex(index)->dump();
    }
    virtual void  setFieldData( const QModelIndex& index, const std::string& data );

    void resetModelData()
    {
        beginResetModel();
        endResetModel();
    }

protected:

    /// Return internal data link
    virtual jsonio17::JsonBase& current_data() const = 0;
    virtual jsonio17::JsonBase* lineFromIndex(const QModelIndex& index) const = 0;

    virtual bool set_value_via_type( jsonio17::JsonBase* object, const std::string& add_key,
                                     jsonio17::JsonBase::Type add_type, const QVariant& add_value );

};


} // namespace jsonui17

