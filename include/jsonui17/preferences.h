#pragma once

#include <QObject>
#include "jsonio17/io_settings.h"
#include "jsonio17/dbconnect.h"


namespace jsonui17 {

// Default logger for jsonui17 library
extern std::shared_ptr<spdlog::logger> ui_logger;

void helpWin( const std::string& name, const std::string& item );

class UIPreferencesPrivate;

/// \class Preferences - monitoring preferences into JSONIO
class UIPreferences: public QObject
{
    Q_OBJECT

    /// Constructor
    UIPreferences();

    friend UIPreferences& uiSettings();
    friend class PreferencesJSONUIPrivate;

signals:

    /// Changed Data Base
    void dbChanged();
    /// Changed thrift schemas Dir
    void schemaChanged();
    /// Signal to update "View" menu
    void viewMenuChanged();
    /// Signal to update editors model
    void modelChanged();
    /// Signal to update models tableview
    void tableChanged();
    /// Error editing settings
    void errorSettings( std::string );

public slots:

    /// Run update Preferences Dialog
    void CmSettingth();
    /// On/off extern flag to show schema comments into editor
    void CmShowComments( bool checked );
    /// On/off extern flag to show enum names into editor
    void CmDisplayEnums( bool checked );
    /// On/off extern flag to edit system data
    void CmEditID( bool checked );
    /// On/off extern flag to keep data fields expanded
    void CmEditExpanded( bool checked );
    /// Define new input/output directory
    void CmSetUserDir( QString dirPath );

public:

    /// Destructor
    ~UIPreferences();


    /// Default resourse Database name
    static std::string resourcesDatabaseName;
    /// Jsonui section name in resource file
    static std::string jsonui_section_name;

    /// Current work Database
    const jsonio17::DataBase& database() const;
    const std::shared_ptr<jsonio17::DataBase>& dbclient() const;

    /// Current resourse Database
    const jsonio17::DataBase &resources_database() const;
    const std::shared_ptr<jsonio17::DataBase>& dbresources() const;

private:

    QScopedPointer<UIPreferencesPrivate> impl_ptr;
    inline UIPreferencesPrivate* impl_func()
              { return reinterpret_cast<UIPreferencesPrivate *>(impl_ptr.get()); }
    inline const UIPreferencesPrivate* impl_func() const
              { return reinterpret_cast<const UIPreferencesPrivate *>(impl_ptr.get()); }

};

/// Function to connect to only one Preferences object
extern UIPreferences& uiSettings();

inline std::string jsonui_section( const std::string& item )
{
    return   UIPreferences::jsonui_section_name+"."+item;
}

} // namespace jsonui17

