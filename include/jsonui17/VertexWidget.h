#pragma once

#include "jsonui17/BaseWidget.h"

namespace jsonui17 {

class VertexWidgetPrivate;

/// \class VertexWidget - window to work with Vertexes (TDBGraph)
class VertexWidget : public BaseWidget
{
    Q_OBJECT

    Q_DISABLE_COPY(VertexWidget)

signals:

    /// Emit signal, when delete record(s)
    void vertexDeleted();

    /// Emit signal, when loaded graph
    void graphLoaded();


public slots:

    void openRecordKey(  const std::string& key_document, bool reset_in_out_query = false  ) override;
    void openRecordKey(  const QModelIndex& index );
    void changeKeyList() override;
    void updateQuery();

    // File
    void CmNew();
    void CmClone();
    void CmExportJSON();
    void CmImportJSON();

    // Record
    void CmRead();
    void CmUpdate();
    void CmDelete();
    void CmDeleteSelect();
    void CmDisplaySearchResult();
    void CmSearchQuery();
    void CmBackupQueriedRecordstoFile();
    void CmRestoreMultipleRecordsfromFile();
    void CmBackupGraphtoFile();
    void CmRestoreGraphfromFile();
#ifndef IMPEX_OFF
    void CmImportFormat();
    void CmUpdateFormat();
    void CmExportFormat();
#endif

    // View
    void CmFollowInEdges();
    void CmFollowOutEdges();

public:

    explicit VertexWidget( const std::string& aschema_name, QWidget *parent = nullptr);
    ~VertexWidget();

    /// Update query
    void setQueryEdited( QueryWidget* queryW  ) override;

    std::string currentKey() const override;

private:

    inline VertexWidgetPrivate* impl_func()
              { return reinterpret_cast<VertexWidgetPrivate *>(impl_ptr.get()); }
    inline const VertexWidgetPrivate* impl_func() const
              { return reinterpret_cast<const VertexWidgetPrivate *>(impl_ptr.get()); }

    void closeEvent(QCloseEvent* e) override;
    bool event(QEvent * e) override;

    std::vector<std::string> select_keys_document(const char* title);

};

} // namespace jsonui17

