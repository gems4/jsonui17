#pragma once

#include <QMainWindow>
#include "jsonio17/dbquerybase.h"

namespace jsonui17 {

class BaseWidget;
class QueryWidget;
class BaseWidgetPrivate;

/// Description of function to call when trying close the window
using  OnCloseEvent_f = std::function<bool(BaseWidget*)>;

/// Description external function for opening vertex or edge document widget
using  ShowVertexEdge_f = std::function<void( bool is_vertex, const std::string& schema_name,
                          const std::string& document_key, const jsonio17::DBQueryBase& query )>;

/// \class BaseWidget base window to organize update/close events
class BaseWidget : public QMainWindow
{
    Q_OBJECT

    Q_DISABLE_COPY(BaseWidget)

public slots:

    /// Show new document on window
    virtual void openRecordKey(  const std::string& document_key, bool reset_in_out_query=false  ) = 0;
    /// Refresh  documents keys list
    virtual void changeKeyList() = 0;
    /// Change the state of internal content as edited
    virtual void objectChanged();

    /// Update menu after change preferences
    virtual void updateViewMenu();
    /// Change model ( show/hide comments)
    virtual void updateModel();
    /// Change table (Show Enum Names Instead of Values)
    virtual void updateTable();
    /// Update internal lists after change DB locations
    virtual void updateDB();

    // Help
    virtual void  CmHelpAbout();
    virtual void  CmHelpAuthors();
    virtual void  CmHelpLicense();
    virtual void  CmHelpContens();

public:

    explicit BaseWidget(  const std::string& schema_name, QWidget *parent = nullptr );

    virtual ~BaseWidget();

    /// Set close condition function to executing when trying close the window
    void setOnCloseEventFunction( OnCloseEvent_f close_function );

    /// Set function to open other window
    void setShowWidgetFunction( ShowVertexEdge_f show_function );

    /// Return current document key
    virtual std::string currentKey() const
    {
        return "";
    }

    /// Run query
    virtual void setQueryEdited( QueryWidget* query_window  ) = 0;

protected:

    QScopedPointer<BaseWidgetPrivate> const impl_ptr;
    inline BaseWidgetPrivate* impl_func()
              { return reinterpret_cast<BaseWidgetPrivate *>(impl_ptr.get()); }
    inline const BaseWidgetPrivate* impl_func() const
              { return reinterpret_cast<const BaseWidgetPrivate *>(impl_ptr.get()); }

    BaseWidget(BaseWidgetPrivate &dd, QWidget * parent);

    /// Build schema file extension
    virtual std::string file_schema_ext( const std::string& schema_name, const std::string& ext );
    /// Get schema name from extension
    virtual std::string schema_from_name( const std::string& file_name );

    void closeEvent(QCloseEvent* e);

};

} // namespace jsonui17


