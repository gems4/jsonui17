﻿#pragma once

#include <QMainWindow>

namespace jsonui17 {

void helpWin( const std::string& name, const std::string& item );

class HelpMainWindowPrivate;

/// Declaration of classes HelpMainWindow - Help system main dialog
class HelpMainWindow : public QMainWindow
{
    Q_OBJECT

    Q_DISABLE_COPY(HelpMainWindow)

signals:

    void updateHelpRecord( const QString& key, const QString& anchor );
    void updateDocPage( QString md_text );
    void updateDBClient();

    void forwardAvailable(bool);
    void backwardAvailable(bool);
    // void html(QString sHtml);

public slots:

    void resetDBClient();

protected slots:

    void helpAbout();
    void helpVersion();
    void helpOnHelp();

    // Record
    void CmEdit();
    void CmRead();
    void CmUpdate();
    void CmDeleteList();

    void htmlResultReady(const QString &html);

    void tocResultReady(const QString& toc);

public:

    /// Extern flag to edit docpages
    static bool editHelp;
    /// Extern help collection name ( default  "docpages" )
    static std::string help_collection_name;

    static HelpMainWindow* pDia;

    HelpMainWindow( QWidget* parent = nullptr );
    virtual ~HelpMainWindow();

    void showDocument( const std::string& keywd );
    void showHelp( const std::string& name, const std::string& item );

protected:

    QScopedPointer<HelpMainWindowPrivate> impl_ptr;
    inline HelpMainWindowPrivate* impl_func()
              { return reinterpret_cast<HelpMainWindowPrivate *>(impl_ptr.get()); }
    inline const HelpMainWindowPrivate* impl_func() const
              { return reinterpret_cast<const HelpMainWindowPrivate *>(impl_ptr.get()); }
    friend class HelpMainWindowPrivate;

    void current_key_change( const std::string& key );
    void generate_Html( const std::string& key );
    void add_url_link( int atype, const std::string& alink, const std::string& anchor);
};

} // namespace jsonui17


