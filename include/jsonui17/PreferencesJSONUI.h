#pragma once

#include <QDialog>
#include <QButtonGroup>
#include "jsonui17/preferences.h"

namespace Ui {
class PreferencesJSONUI;
}

namespace jsonui17 {

class PreferencesJSONUIPrivate;

/// \class  PreferencesJSONUI  dialog for insert/edit settings
class PreferencesJSONUI : public QDialog
{
    Q_OBJECT

    Q_DISABLE_COPY(PreferencesJSONUI)

signals:

    void dbdriveChanged();

public slots:

    void CmSave();
    void CmProjectDir();
    void CmSchemasDir();
    void CmResourcesDir();
    void CmHelp();

#if (QT_VERSION < QT_VERSION_CHECK(5, 15, 0))
    void dbbuttonClicked(QAbstractButton *button);
#endif

public:

    explicit PreferencesJSONUI( jsonio17::JsonioSettings& asettings, QWidget *parent = nullptr );
    ~PreferencesJSONUI();

private:

    QScopedPointer<PreferencesJSONUIPrivate> impl_ptr;
    inline PreferencesJSONUIPrivate* impl_func()
              { return reinterpret_cast<PreferencesJSONUIPrivate *>(impl_ptr.get()); }
    inline const PreferencesJSONUIPrivate* impl_func() const
              { return reinterpret_cast<const PreferencesJSONUIPrivate *>(impl_ptr.get()); }

};



} // namespace jsonui17


