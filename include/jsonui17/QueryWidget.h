#pragma once

#include <QMainWindow>
#include "jsonio17/dbschemadoc.h"

namespace jsonui17 {

jsonio17::DBSchemaDocument* newDBQueryClient( const jsonio17::DBQueryBase& query  );

class QueryWidgetPrivate;

/// \class QueryWidget window to insert/edit  database query
class QueryWidget : public QMainWindow
{
    Q_OBJECT

    Q_DISABLE_COPY(QueryWidget)

public slots:

    void objectChanged(){}

    // Files
    void CmSaveQuery();
    void CmLoadQuery();
    void CmExecute();
    void CmApplay()
    {
      CmExecute();
      close();
    }

    // Select part
    void CmSelectFields();
    void CmAddSelectFields();

    // Where part
    void CmWhereApply();
    void CmWhereRestore();
    void CmAddWhereFields();

    // Record
    void CmClone();
    void CmRead();
    void CmUpdate();
    void CmDeleteList();

    // Help
    void CmHelp();
    void CmHelpAbout();

public:

    /// Extern impex collection name ( default "queries" )
    static std::string query_collection_name;

    explicit QueryWidget( const char* title,  const std::vector<std::string>& schemaNames,
                          const jsonio17::DBQueryDef& query,   QWidget *parent );
    ~QueryWidget();

    /// Return query
    const jsonio17::DBQueryDef& getQuery();

    /// Return query schema
    std::string getSchema() const;

    /// Update query
    void updateQuery( const std::vector<std::string>& schema_names, const jsonio17::DBQueryDef& query );

protected:

    QScopedPointer<QueryWidgetPrivate> const impl_ptr;
    inline QueryWidgetPrivate* impl_func()
              { return reinterpret_cast<QueryWidgetPrivate *>(impl_ptr.get()); }
    inline const QueryWidgetPrivate* impl_func() const
              { return reinterpret_cast<const QueryWidgetPrivate *>(impl_ptr.get()); }

    friend class QueryView;
    bool select_line( std::string& fldpath, const jsonio17::FieldDef** flddata );

};

} // namespace jsonui17

