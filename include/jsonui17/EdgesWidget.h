#pragma once

#include "jsonui17/BaseWidget.h"

namespace jsonui17 {

class EdgesWidgetPrivate;

/// \class EdgesWidget - window to insert/update  Edges (TDBGraph)
class EdgesWidget : public BaseWidget
{
    Q_OBJECT

    Q_DISABLE_COPY(EdgesWidget)

signals:

    /// Emit signal, when delete recod(s)
    void edgeDeleted();

    /// Emit signal, when loaded graph
    void graphLoaded();

public slots:

    void openRecordKey(  const std::string& key_document, bool reset_in_out_query = false  ) override;
    void openRecordKey(  const QModelIndex& index, bool reset_in_out_query = false );
    void changeKeyList() override;
    void updateQuery();

    // File
    void CmNew();
    void CmClone();
    void CmExportJSON();
    void CmImportJSON();
    void CmIncomingQuery();
    void CmOutgoingQuery();

    // Record
    void CmRead();
    void CmUpdate();
    void CmDelete();
    void CmDeleteSelect();
    void CmDisplaySearchResult();
    void CmSearchQuery();
    void CmBackupQueriedRecordstoFile();
    void CmRestoreMultipleRecordsfromFile();
    void CmBackupGraphtoFile();
    void CmRestoreGraphfromFile();

    // View
    void CmFollowInVertex();
    void CmFollowOutVertex();

    void CmHelpContens() override;

public:

    explicit EdgesWidget( const std::string& aschema_name, const jsonio17::DBQueryBase& query, QWidget *parent = nullptr);
    ~EdgesWidget();

    /// Update query
    void setQueryEdited( QueryWidget* queryW  ) override;

    std::string currentKey() const override;

private:

    inline EdgesWidgetPrivate* impl_func()
    { return reinterpret_cast<EdgesWidgetPrivate *>(impl_ptr.get()); }
    inline const EdgesWidgetPrivate* impl_func() const
    { return reinterpret_cast<const EdgesWidgetPrivate *>(impl_ptr.get()); }

    void closeEvent(QCloseEvent* e) override;
    bool event(QEvent * e) override;

    std::vector<std::string> select_keys_document( const char* title );

};

} // namespace jsonui17

