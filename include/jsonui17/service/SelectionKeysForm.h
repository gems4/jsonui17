#pragma once

#include <memory>
#include <string>
#include <vector>
#include <QDialog>
#include <QWidget>


class WaitingSpinnerWidget;


namespace jsonui17 {

class QueryWidget;
class SelectionKeysFormPrivate;

class SelectionKeysForm : public QWidget
{
    Q_OBJECT

    Q_DISABLE_COPY(SelectionKeysForm)

signals:

    /// Start long database process
    void startProcess();

    /// Finish long database process
    void finishProcess();

    /// Exception when database command execution
    void isException( const QString& titl, const QString& msg );

    /// Signal emits when current schema changed
    void changedSchema( QString newSchemaName );

public slots:

    void selectAll();

    void clearSelection();

    void closeQuery();

    void changeSchema( QString newSchemaName );

    void resetQuery();

    void changeDBClientData(  QString query );

    void objectChanged() { }

public:

    explicit SelectionKeysForm(  const std::vector<std::string>& aVertexesList, QWidget *parent = nullptr);
    ~SelectionKeysForm();

    /// Define connection to database
    void setDBClient(const std::string& schemaName );

    /// Update query functions ( must be changed )
    void setQuery( QueryWidget* queryW  );

    /// Returns selected keys
    std::vector<std::string> allSelectedKeys() const;

    /// Return selected schema
    std::string vertexSchema() const;

private:

    QScopedPointer<SelectionKeysFormPrivate> const impl_ptr;
    inline SelectionKeysFormPrivate* impl_func()
              { return reinterpret_cast<SelectionKeysFormPrivate *>(impl_ptr.get()); }
    inline const SelectionKeysFormPrivate* impl_func() const
              { return reinterpret_cast<const SelectionKeysFormPrivate *>(impl_ptr.get()); }

};


class SelectionKeysDialog : public QDialog
{
    Q_OBJECT

public slots:

    /// Start long database process
    void startProcess();

    /// Finish long database process
    void finishProcess();

    /// Exception when database command execution
    void get_exception( const QString& titl, const QString& msg );

public:

    explicit SelectionKeysDialog(QWidget *parent = nullptr);
    ~SelectionKeysDialog();

    /// Returns selected keys
    std::vector<std::string> allSelectedKeys()
    {
      return form->allSelectedKeys();
    }

    /// Return selected schema
    std::string vertexSchema() const
    {
       return form->vertexSchema();
    }

private:

   QScopedPointer<SelectionKeysForm> form;
   WaitingSpinnerWidget *waitDialog = nullptr;

};

} // namespace jsonui17

