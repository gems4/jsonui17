#pragma once

#include <QDialog>
#include "jsonio17/schema.h"


namespace jsonui17 {


class SchemaSelectDialogPrivate;

/// \class SchemaSelectDialog class provides a dialog that allow users to select
/// items from thrift schema fields list
class SchemaSelectDialog : public QDialog
{
    Q_OBJECT

    Q_DISABLE_COPY(SchemaSelectDialog)

public slots:
    void objectChanged() {}
    void CmHelp();

public:

    /// Multiple select constructor
    /// Construct tree view of fields thrift schema
    /// \param schemaName.
    /// \param def_fieldsList - default selection
    SchemaSelectDialog( QWidget* parent, const char* title,
                        const std::string& schema_name,
                        const std::vector<std::string>& def_fields_list );

    /// Single select constructor.
    /// Construct tree view of fields thrift schema schemaName.
    SchemaSelectDialog( QWidget* parent, const char* title,
                        const std::string& schema_name );

    /// Destructor
    virtual ~SchemaSelectDialog();

    /// Returns single selection
    /// \returns empty string if nothing selected
    std::string selIndex( const jsonio17::FieldDef** flddata = nullptr );

    /// Returns selection array
    ///    array is empty if nothing is selected
    std::vector<std::string> allSelected();

    /// Returns selection array types
    ///    array is empty if nothing is selected
    std::vector<const jsonio17::FieldDef*> allSelectedTypes();

private:

    QScopedPointer<SchemaSelectDialogPrivate> const impl_ptr;
    inline SchemaSelectDialogPrivate* impl_func()
              { return reinterpret_cast<SchemaSelectDialogPrivate *>(impl_ptr.get()); }
    inline const SchemaSelectDialogPrivate* impl_func() const
              { return reinterpret_cast<const SchemaSelectDialogPrivate *>(impl_ptr.get()); }

};


} // namespace jsonui17

