#pragma once

#include <QVBoxLayout>
#include <QWidget>
#include "jsonui17/models/table_view.h"
#include "jsonui17/charts/graph_data.h"

namespace jsonui17 {

class CSVPagePrivate;

/// \class CSVPage implements widget to work with CSV format data.
class CSVPage : public QWidget
{

    Q_OBJECT

    Q_DISABLE_COPY(CSVPage)

signals:

    void updateGraphWindow();

public slots:

    /// Save graphic definition to internal cfg file
    void saveGraphData(jsonui17::ChartData*);
    void updateTable();
    void objectChanged();

    void CmCalc();
    void SelectRow();
    void SelectColumn();
    void SelectAll();
    void CutData();
    void ClearData();
    void CopyData();
    void CopyDataHeader();
    void PasteData();
    void PasteTransposedData();

    void toggleX();
    void toggleY();

public:

    explicit CSVPage( std::shared_ptr<AbstractContainer> datacontainer, int mode, QWidget* parent = nullptr);
    ~CSVPage();

    /// Save selected data to csv format file
    void saveSelectedToCSV( const std::string& file_name ) const;

    /// Save all table data to csv format file
    void saveAllToCSV( const std::string& file_name ) const;

    /// Connect 2d graphic for columns
    void addGraphic();

    /// Test if column can be used as abscissa or ordinate
    bool canBeToggled( const QModelIndex& index, bool as_abscissa );

    /// Change abscissa and ordinate lines
    void setXYaxis( int ndxX, std::vector<int> ndxY);

protected:

    friend class TableEditWidgetPrivate;
    QScopedPointer<CSVPagePrivate> const impl_ptr;
    inline CSVPagePrivate* impl_func()
              { return reinterpret_cast<CSVPagePrivate *>(impl_ptr.get()); }
    inline const CSVPagePrivate* impl_func() const
              { return reinterpret_cast<const CSVPagePrivate *>(impl_ptr.get()); }

};

} // namespace jsonui17

