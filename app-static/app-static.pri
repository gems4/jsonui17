#
# Application Static Libary Project for CuteMarkEd v0.11.3
#
# Github : https://github.com/cloose/CuteMarkEd
#

INCLUDEPATH += $$PWD

SOURCES += \
    $$JSONUI_APP_STATIC/template/htmltemplate.cpp \
    $$JSONUI_APP_STATIC/template/presentationtemplate.cpp \
    $$JSONUI_APP_STATIC/converter/discountmarkdownconverter.cpp \
    $$JSONUI_APP_STATIC/converter/revealmarkdownconverter.cpp

HEADERS += \
    $$JSONUI_APP_STATIC/template/template.h \
    $$JSONUI_APP_STATIC/template/htmltemplate.h \
    $$JSONUI_APP_STATIC/template/presentationtemplate.h \
    $$JSONUI_APP_STATIC/converter/markdownconverter.h \
    $$JSONUI_APP_STATIC/converter/markdowndocument.h \
    $$JSONUI_APP_STATIC/converter/discountmarkdownconverter.h \
    $$JSONUI_APP_STATIC/converter/revealmarkdownconverter.h




