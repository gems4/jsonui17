#!/bin/bash

#./install-dependencies.sh

BRANCH_JSON=master
BuildType=Release
InstallPrefix=/usr/local
workfolder=${PWD}
QT_PATH=$1
echo $QT_PATH

USE_QT6=OFF
if [[ $QT_PATH == *"Qt/6"*  ]]; then
  USE_QT6=ON
fi

mkdir -p build
cd build
cmake .. -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_BUILD_TYPE=$BuildType -DCMAKE_INSTALL_PREFIX=$InstallPrefix -DCMAKE_PREFIX_PATH=$QT_PATH -DJSONUI_USE_QT6=$USE_QT6
make 
sudo make install

if [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
   sudo ldconfig
fi
