# The path where cmake config files are installed
set(JSONUI_INSTALL_CONFIGDIR ${CMAKE_INSTALL_LIBDIR}/cmake/jsonui17)

install(EXPORT jsonui17Targets
    FILE jsonui17Targets.cmake
    NAMESPACE jsonui17::
    DESTINATION ${JSONUI_INSTALL_CONFIGDIR}
    COMPONENT cmake)

include(CMakePackageConfigHelpers)

write_basic_package_version_file(
    ${CMAKE_CURRENT_BINARY_DIR}/jsonui17ConfigVersion.cmake
    VERSION ${PROJECT_VERSION}
    COMPATIBILITY SameMajorVersion)

configure_package_config_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules/jsonui17Config.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/jsonui17Config.cmake
    INSTALL_DESTINATION ${JSONUI_INSTALL_CONFIGDIR}
    PATH_VARS JSONUI_INSTALL_CONFIGDIR)

install(FILES
    ${CMAKE_CURRENT_BINARY_DIR}/jsonui17Config.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/jsonui17ConfigVersion.cmake
    DESTINATION ${JSONUI_INSTALL_CONFIGDIR})
