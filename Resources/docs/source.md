### Dependent

- jsonArango
- jsonio17
- jsonimpex17


### build


```
> cmake .. -DCMAKE_BUILD_TYPE=Release -DREFRESH_THIRDPARTY=ON -DCMAKE_PREFIX_PATH=/home/sveta/Qt/5.15.2/gcc_64
> cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH=/home/sveta/Qt/5.15.2/gcc_64
> cmake .. -DCMAKE_PREFIX_PATH=/home/sveta/Qt/5.15.2/gcc_64
> cmake .. -DCMAKE_PREFIX_PATH=/home/sveta/Qt/6.1.0/gcc_64 -DJSONUI_USE_QT6=ON
> ./install.sh /home/sveta/Qt/5.15.2/gcc_64
> ./install.sh /home/sveta/Qt/6.1.0/gcc_64

```

## Main Files descriptions

## toDo

1. Done

Implemented json_model.h, cpp
             free_model
             schema_model 

(x) dbkeysmodel+ (x) dbdocumentmodel -> (x) VertexWidget+ (x) EdgeWidget

(x) FormatImpexWidget

(x) model_query+ (x) SchemaSelectDialog -> (x) QueryWidget

2. Done

try hide implementation, using -> 

// https://stackoverflow.com/questions/25250171/how-to-use-the-qts-pimpl-idiom
/*
 * https://habr.com/ru/post/76248/
 Возможность реализовать систему сигналов и слотов для приватного класса и спрятать их от внешнего использования
  с помощью Q_PRIVATE_SLOT макроса (тема для отдельной статьи).
 */
!!!! add impl Qt pattern

 - CalcDialog.h  - not use impl Qt pattern ( only UI )
 - SelectDialog.h  + 
 - TableEditWindow.h +
 - service/CSVPage.h +
 - QueryWidget.h +

 - BaseWidget.h +
 - JsonEditWidget.h +
 - VertexWidget.h +
 - EdgesWidget.h +
 - FormatImpexWidget.h +

 - HelpMainWindow.h \
 - PreferencesJSONUI.h +
 - preferences.h +


## Next steps --------------------------------------------------------------------------------


Testing TableEditWindow&graphic
        HelpWindow


To do:

- Add test for new from jsonio17 and jsonimpex17

- Add test for table_container_template.h

- Add test for 
void toJsonNode( jsonio17::JsonBase& object ) const;
void fromJsonNode( const jsonio17::JsonBase& object );
  modules charts and help
  
- Try implement filtering for SelectDialog, MatrixTableProxy.

- Add descriptions for files from include/jsonui17

- "Resource/data/schemas" only query, impex and help.
  ? test schemas from jsonio17
  
- Implementing saving graph data to jsonui17-config.json


//--------------------------------------------------------------------

Not used

FormatImportDialog not need
SelectionKeysForm not need
