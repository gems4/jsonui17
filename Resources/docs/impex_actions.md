## Describe some triks

### Work with documents (jsonio17) 

Class *DBDocumentBase* and inherited classes *DBSchemaDocument*, *DBVertexDocument*, *DBEdgeDocument*, and *DBJsonDocument* used to work with documents into ArangoDB collections. Documents are JSON-like objects. These objects can be nested (to any depth) and may contain lists. Each document has a unique primary key that identifies it within its collection.  Furthermore, each document is uniquely identified by its document handle across all collections in the same database.

The class contains CRUD and select operations.

But we can use it to overwrite in import mode. In this case, we need to download data to compare values. We compare the current data with the internally loaded values, and if all the values are the same, then we update the selected record instead of creating new ones. 

These functions could be executed only in selection mode. For setup and execute a query for the document used command ```void setQuery( const DBQueryDef& querydef ); ```.

The same table is used in Vertex and Edge Widgets for selection documents from a collection. 

Default query looks like:

```
FOR u IN elements
FILTER u._label == 'element' 
```

>If no return, ArangoDB generate ```RETURN u``` by default

Users can change query and values to compare by executing command *"Database/Search DB Using &Query Widget ..."*.

We can use for ArangoDB:

- [AQL queries](https://www.arangodb.com/docs/stable/aql/fundamentals.html)
- [Template queries](https://www.arangodb.com/docs/stable/http/simple-query.html) returns all documents of a collection matching a given example;

> Simple Queries: Idiomatic interface in arangosh to perform trivial queries. They are superseded by AQL queries, which can also be run in arangosh. AQL is a language on its own and way more powerful than Simple Queries could ever be. In fact, the (still supported) Simple Queries are translated internally to AQL, then the AQL query is optimized and run against the database in recent versions, because of better performance and reduced maintenance complexity.



###  Import scripts  (jsonimpex17)

Import of the following types of input files has been implemented (see [impex.thrift](impex.thrift) ):

 - Import of data records from text file in legacy JSON/YAML/XML (foreign keywords) *(FormatStructDataFile)*.

 - Import of each data record from text file organized in blocks of key-value pairs. Key-Value pairs may follow in arbitrary order in the input (imported) file *(FormatKeyValueFile)*.
 
 - import of data records to table format file (strict order of columns and rows) *(FormatTableFile)*.
 
 Most of the formats contain specific settings and regular expressions for extracting data from. But the next steps are similar for all. These steps described for the  block data in the file corresponding to one database document (a record) *(FormatBlock)*.
 
 1. Extract data for the next record.
 2. Clear document data and set default values
 3. Match read data to corresponding fields in the document, if need execute Lua ```field = function_of(field)``` script or/and conversion map to input data.
 4. As the last step execute  Lua script, if exist, for operation on data values in the full JSON object. JSON encode and decode was implemented in internal functions ```   rintable = JSON:decode( json_data ) ```, for user need only check and change values into rintable.
 
 
>  Might need to describe more detail parse scripts for key-value and table input formats.

Users can import data by executing command *"Database/Import DB Records from Foreign Format File ..."*.

###  Update scripts  (jsonimpex17)

As update scripts could be used the same scripts as for import. One difference: the fields into the match list which would not be updated, but used to detect records to be updated,  must be set up as ignored. 
Before executing the update command, the user must select the documents to update by executing the ```setQuery ()``` command and leaving only fields important for comparison into loaded values.
We suggest in most cases using the default AQL query ``` FOR u IN collection RETURN u ```,  if you do not need to update only a specific subset of collections. 
More important is to select a list of only fields used for comparison, defines as ignored, and present into impex script. This line will detect the record to be updated.

On GUI users can change query and values to compare by executing command *"Database/Search DB Using &Query Widget ..."*.

Users can update data by executing command *"Database/Update DB Records from Foreign Format File ..."*.

###  Export scripts  (jsonimpex17)

Implemented only for key-value and table formats.
 
 > Might need to describe more detailed generate scripts for key-value and table output formats.
 
Users can export data by executing command *"Database/Export DB Records to Foreign Format File ..."*.


### QueryWidget (jsonui17)

*QueryWidget* window to insert/edit  database query

From this window, the user can save, load, and delete query records from the database; change query data; load formats to/from JSON files.

Users can:

- change fields from which data will be collected during the search by executing the command *"Query/Select Fields to collect data from ..."* or *"Query/Add additional fields to collect data from ..."*;
- modify the selection filter by editing or generating from the *"Filter"* commands;
- to apply changes click the action *"Query/Execute Query"*.

### FormatImpexWidget (jsonui17)

*FormatImpexWidget* - widget to work with Foreign Import/Export File Format.

From this window, the user can:

- save, load, and delete Impex records from the database; 
- edit import/export formats data; 
- load formats to/from JSON files; 
- when loading data from the file need select input file and push *"File/Execute ..."* action. 


##### How to execute test example.
I recommend making test database 'test177' to protect changing important data.

1. Import VertexSubstance data dcomp-aqueous.Ag.2.csv by script 
master_substances.2.FormatTableFile.json (
command *"Database/Import DB Records from Foreign Format File ..."*).
2.  Backup data to compare update results( command *"Database/Backup Queried DB Records to File ..."*.

3. Change query and values to compare by executing command *"Database/Search DB Using &Query Widget ..."*.
Better left used query 
```  
FOR u IN substances
FILTER u._label == 'substance'  
```
, but important select only *_id, properties.symbol, properties.sourcetdb* fields which will detect records to update.

4. Update VertexSubstance data dcomp-aqueous-update.Ag.2.csv by script master_substances_update.2.FormatTableFile.json (
command *"Database/Update DB Records from Foreign Format File ..."*).
5.  Backup data to compare results( command *"Database/Backup Queried DB Records to File ..."*.
