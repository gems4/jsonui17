## JSONUI17 

This [Jsonio17](https://bitbucket.org/gems4/jsonio17)- and [Qt5](http://www.qt.io/)-based widget C++ library and API is aimed at implementing lightweight local GUI applications for editing/viewing the structured data kept in a NoSQL database (ArangoDB), with rendering documents to/from JSON, YAML, XML files, and with importing/exporting data from/to arbitrary foreign formats. A widget for graphical presentation of tabular data (csv format fields) is also available.

* Jsonui17 is written in C/C++ using open-source Qt5 and Jsonio17 codes.
* Version: currently 0.1.
* Will be distributed as is (no liability) under the terms of Lesser GPL v.3 license.

### How to clone (download) the Jsonui17 source code

* In your home directory, make a folder named e.g. ~/jsonui17.
* Cd into jsonui17 and clone this repository from  https://bitbucket.org/gems4/jsonui17.git using git or a preinstalled free git client e.g. SmartGit (the best way on Windows). 
* Alternatively on Mac OS X or linux, open a terminal and in the command line (do not forget a period) run:

```sh
git clone https://bitbucket.org/gems4/jsonui17.git . 

```

### How to build the Jsonui17 library on Linux or Mac OS

* Make sure that Qt5 and CMake are installed in your system. For (K)Ubuntu 18.04/20.04, this can be done using the following commands:

```sh
sudo apt-get install cmake
sudo apt-get install qt5-default qttools5-dev libqt5svg5 libqt5svg5-dev libqt5help5
```

  For MacOS, make sure that Homebrew is installed (see [Homebrew web site](http://brew.sh) and [Homebrew on Mac OSX El Capitan](http://digitizor.com/install-homebrew-osx-el-capitan/)


* Install Dependencies

In order to build the Jsonui17 library on (k)ubuntu linux 16.04/18.04 or MacOS, first execute the following: 

```sh
#!bash
cd ~/jsonui17
sudo ./install-dependencies.sh
```

* Install the Jsonui17 library

Then navigate to the directory where this README.md file is located and type in terminal:

```sh
cd ~/jsonui17
sudo ./install.sh
```

This will install Jsonui17 assuming the Qt5 toolkit available in the system (as in (K)Ubuntu). If Qt5 toolkit is installed locally in your user folder, use the following commands:

```sh
cd ~/jsonui17
sudo ./install.sh /home/<you>/Qt/5.12.2/gcc_64/
```

or on MacOS

```sh
cd ~/jsonui17
sudo ./install.sh /home/<you>/Qt/5.12.2/clang_64/
```

* After that, third-party libraries and their headers can be found in /usr/local/{include,lib}. 

* Install current version of ArangoDB server locally ( see [jsonArango](https://bitbucket.org/gems4/jsonarango/src/master/) ).


### How to use Jsonui17 (use cases) 

For information on using  APIs take a look into the /example directory.

