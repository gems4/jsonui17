
HEADERS += \
    $$JSONUI17_HEADERS_DIR/jsonui17/charts/chart_model.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/charts/chart_view.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/charts/graph_data.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/charts/GraphDialog.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/charts/LegendDialog.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/charts/SymbolDialog.h \
    $$JSONUI17_DIR/charts/markershapes.h \
    $$JSONUI17_DIR/charts/from_jsonio.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/HelpMainWindow.h \
    $$JSONUI17_DIR/helpview/HelpEditDialog.h \
    $$JSONUI17_DIR/helpview/HelpFileDialog.h \
    $$JSONUI17_DIR/helpview/helpBrowser.h \
    $$JSONUI17_DIR/helpview/helpdata.h \
    $$JSONUI17_DIR/helpview/helpgenerator.h \
    $$JSONUI17_DIR/helpview/HelpMainWindow_p.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/models/table_container.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/models/table_container_template.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/models/table_model.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/models/table_view.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/models/base_model.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/models/json_model.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/models/schema_model.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/models/json_view.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/models/csv_container.h \
    $$JSONUI17_DIR/models/query_model.h \
    $$JSONUI17_DIR/models/dbkeys_view.h \
    $$JSONUI17_DIR/models/dbquery_thread.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/service/CSVPage.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/service/waitingspinnerwidget.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/service/SchemaSelectDialog.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/service/SelectionKeysForm.h \
    $$JSONUI17_DIR/service/ModelLineDialog.h \
    $$JSONUI17_DIR/service/SchemaSelectDialog_p.h \
    $$JSONUI17_DIR/service/CSVPage_p.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/preferences.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/PreferencesJSONUI.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/FileDialogs.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/TableEditWindow.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/SelectDialog.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/CalcDialog.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/BaseWidget.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/JsonEditWidget.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/VertexWidget.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/EdgesWidget.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/FormatImpexWidget.h \
    $$JSONUI17_HEADERS_DIR/jsonui17/QueryWidget.h \
    $$JSONUI17_DIR/preferences_p.h \
    $$JSONUI17_DIR/BaseWidget_p.h \
    $$JSONUI17_DIR/JsonEditWidget_p.h \
    $$JSONUI17_DIR/VertexWidget_p.h \
    $$JSONUI17_DIR/EdgesWidget_p.h \
    $$JSONUI17_DIR/FormatImpexWidget_p.h

SOURCES += \
    $$JSONUI17_DIR/charts/chart_model.cpp \
    $$JSONUI17_DIR/charts/chart_view.cpp \
    $$JSONUI17_DIR/charts/graph_data.cpp \
    $$JSONUI17_DIR/charts/markershapes.cpp \
    $$JSONUI17_DIR/charts/GraphDialog.cpp  \
    $$JSONUI17_DIR/charts/LegendDialog.cpp \
    $$JSONUI17_DIR/charts/SymbolDialog.cpp \
    $$JSONUI17_DIR/helpview/HelpEditDialog.cpp \
    $$JSONUI17_DIR/helpview/HelpFileDialog.cpp \
    $$JSONUI17_DIR/helpview/HelpMainWindow.cpp \
    $$JSONUI17_DIR/helpview/helpBrowser.cpp \
    $$JSONUI17_DIR/helpview/helpdata.cpp \
    $$JSONUI17_DIR/helpview/helpgenerator.cpp \
    $$JSONUI17_DIR/models/table_model.cpp \
    $$JSONUI17_DIR/models/table_view.cpp \
    $$JSONUI17_DIR/models/json_model.cpp \
    $$JSONUI17_DIR/models/schema_model.cpp \
    $$JSONUI17_DIR/models/json_view.cpp \
    $$JSONUI17_DIR/models/csv_container.cpp \
    $$JSONUI17_DIR/models/query_model.cpp \
    $$JSONUI17_DIR/models/dbkeys_view.cpp \
    $$JSONUI17_DIR/models/dbquery_thread.cpp \
    $$JSONUI17_DIR/service/CSVPage.cpp \
    $$JSONUI17_DIR/service/waitingspinnerwidget.cpp \
    $$JSONUI17_DIR/service/ModelLineDialog.cpp \
    $$JSONUI17_DIR/service/SchemaSelectDialog.cpp \
    $$JSONUI17_DIR/service/SelectionKeysForm.cpp \
    $$JSONUI17_DIR/preferences.cpp \
    $$JSONUI17_DIR/PreferencesJSONUI.cpp \
    $$JSONUI17_DIR/FileDialogs.cpp \
    $$JSONUI17_DIR/TableEditWindow.cpp \
    $$JSONUI17_DIR/SelectDialog.cpp \
    $$JSONUI17_DIR/CalcDialog.cpp \
    $$JSONUI17_DIR/BaseWidget.cpp \
    $$JSONUI17_DIR/JsonEditWidget.cpp \
    $$JSONUI17_DIR/VertexWidget.cpp \
    $$JSONUI17_DIR/EdgesWidget.cpp \
    $$JSONUI17_DIR/FormatImpexWidget.cpp \
    $$JSONUI17_DIR/QueryWidget.cpp


FORMS += \
    $$JSONUI17_DIR/charts/GraphDialog4.ui \
    $$JSONUI17_DIR/charts/LegendDialog4.ui \
    $$JSONUI17_DIR/charts/SymbolDialog4.ui \
    $$JSONUI17_DIR/helpview/HelpEditDialog.ui \
    $$JSONUI17_DIR/helpview/HelpFileDialog.ui \
    $$JSONUI17_DIR/helpview/HelpMainWindow.ui \
    $$JSONUI17_DIR/service/ModelLineDialog.ui \
    $$JSONUI17_DIR/service/SchemaSelectDialog4.ui \
    $$JSONUI17_DIR/service/SelectionKeysForm.ui \
    $$JSONUI17_DIR/PreferencesJSONUI.ui \
    $$JSONUI17_DIR/TableEditWindow.ui \
    $$JSONUI17_DIR/SelectDialog4.ui \
    $$JSONUI17_DIR/CalcDialog4.ui \
    $$JSONUI17_DIR/JsonEditWidget.ui \
    $$JSONUI17_DIR/VertexWidget.ui \
    $$JSONUI17_DIR/EdgesWidget.ui \
    $$JSONUI17_DIR/FormatImpexWidget.ui \
    $$JSONUI17_DIR/QueryWidget.ui





