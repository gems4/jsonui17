#include <QFileDialog>
#include <QMessageBox>
#include "ui_PreferencesJSONUI.h"
#include "preferences_p.h"
#include "jsonui17/PreferencesJSONUI.h"
#include "jsonui17/HelpMainWindow.h"
#include "jsonarango/arangoconnect.h"

namespace jsonui17 {

/// Private \class  PreferencesJSONUI  dialog for insert/edit settings
class PreferencesJSONUIPrivate
{

#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    Q_DISABLE_COPY_MOVE(PreferencesJSONUIPrivate)
#endif

public:

    explicit PreferencesJSONUIPrivate( PreferencesJSONUI *qdialog, jsonio17::JsonioSettings& asettings );
    ~PreferencesJSONUIPrivate();

    void db_state_changed(int state);
    void update_lists();
    // true database changed
    bool cm_save();

private:

    PreferencesJSONUI * const  base_ptr;
    inline PreferencesJSONUI* base_func() { return static_cast<PreferencesJSONUI *>(base_ptr); }
    inline const PreferencesJSONUI* base_func() const { return static_cast<const PreferencesJSONUI *>(base_ptr); }
    friend class PreferencesJSONUI;

    QScopedPointer<Ui::PreferencesJSONUI> qUi;
    QScopedPointer<QButtonGroup> smbGroup;

    /// Link to JsonioSettings - storing preferences to JSONIO
    jsonio17::JsonioSettings& jsonio_settings;
    /// Local, remote or thrift db connection on start
    int old_db_connection;

};

PreferencesJSONUIPrivate::PreferencesJSONUIPrivate( PreferencesJSONUI *qdialog, jsonio17::JsonioSettings& asettings):
    base_ptr( qdialog ),
    qUi( new Ui::PreferencesJSONUI ),
    jsonio_settings(asettings)
{
    auto q = base_func();
    qUi->setupUi(q);

    smbGroup.reset( new QButtonGroup());
    smbGroup->setExclusive(true);
    smbGroup->addButton( qUi->useLocal, 0);
    smbGroup->addButton( qUi->useRemote, 1);
    {
        qUi->resourcesEdit->setText( jsonio_settings.value<std::string>( jsonio17::common_section( "ResourcesDirectory" ), "" ).c_str() );
        qUi->schemasEdit->setText( jsonio_settings.value<std::string>( jsonio17::common_section( "SchemasDirectory" ), "" ).c_str() );
        qUi->usersEdit->setText( jsonio_settings.value<std::string>( jsonio17::common_section( "WorkDirectoryPath" ), "." ).c_str() );

        qUi->commentBox->setChecked( jsonio_settings.value( jsonui_section( "ShowComments" ), false) );
        qUi->enumBox->setChecked( jsonio_settings.value( jsonui_section( "ShowEnumNames" ), false) );
        qUi->idBox->setChecked( jsonio_settings.value( jsonui_section( "CanEdit_id" ), false) );
        qUi->expandedBox->setChecked( jsonio_settings.value( jsonui_section( "KeepExpanded" ), true) );

        qUi->localUrl->setText( jsonio_settings.value( jsonio17::arangodb_section( "ArangoDBLocal.DB_URL" ),
                                                      std::string( arangocpp::ArangoDBConnection::local_server_endpoint )).c_str() );
        update_lists();
        qUi->localDBName->setEditText( jsonio_settings.value( jsonio17::arangodb_section( "ArangoDBLocal.DBName" ),
                                                             std::string( arangocpp::ArangoDBConnection::local_server_database )).c_str() );
        qUi->localUser->setEditText( jsonio_settings.value( jsonio17::arangodb_section( "ArangoDBLocal.DBUser" ),
                                                           std::string( arangocpp::ArangoDBConnection::local_server_username )).c_str() );
        qUi->localPassword->setText( jsonio_settings.value( jsonio17::arangodb_section( "ArangoDBLocal.DBUserPassword" ),
                                                           std::string( arangocpp::ArangoDBConnection::local_server_password )).c_str() );
        qUi->localRO->setChecked( jsonio_settings.value( jsonio17::arangodb_section( "ArangoDBLocal.DBAccess" ), std::string("rw"))== "ro" );
        qUi->newDatabase->setChecked( jsonio_settings.value( jsonio17::arangodb_section( "ArangoDBLocal.DBCreate" ), false) );

        qUi->remoteUrl->setText( jsonio_settings.value( jsonio17::arangodb_section( "ArangoDBRemote.DB_URL" ),
                                                       std::string( arangocpp::ArangoDBConnection::remote_server_endpoint)).c_str() );
        qUi->remoteDBName->setText( jsonio_settings.value( jsonio17::arangodb_section( "ArangoDBRemote.DBName" ),
                                                          std::string( arangocpp::ArangoDBConnection::remote_server_database)).c_str() );
        qUi->remoteUser->setText( jsonio_settings.value( jsonio17::arangodb_section( "ArangoDBRemote.DBUser" ),
                                                        std::string( arangocpp::ArangoDBConnection::remote_server_username)).c_str() );
        qUi->remotePassword->setText( jsonio_settings.value( jsonio17::arangodb_section( "ArangoDBRemote.DBUserPassword" ),
                                                            std::string( arangocpp::ArangoDBConnection::remote_server_password)).c_str() );
        qUi->remoteRO->setChecked( jsonio_settings.value( jsonio17::arangodb_section( "ArangoDBRemote.DBAccess" ), std::string("ro")) == "ro" );

        old_db_connection = uiSettings().impl_func()->get_current_db_connection();
        smbGroup->button( old_db_connection )->setChecked( true );
    }

    db_state_changed( old_db_connection );

    QObject::connect( qUi->buttonBox, &QDialogButtonBox::accepted, q, &PreferencesJSONUI::CmSave);
    QObject::connect( qUi->buttonBox, &QDialogButtonBox::helpRequested, q, &PreferencesJSONUI::CmHelp);
    QObject::connect( qUi->usersButton, &QToolButton::clicked, q, &PreferencesJSONUI::CmProjectDir);
    QObject::connect( qUi->schemasButton, &QToolButton::clicked, q, &PreferencesJSONUI::CmSchemasDir);
    QObject::connect( qUi->resourcesButton, &QToolButton::clicked, q, &PreferencesJSONUI::CmResourcesDir);
#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    QObject::connect( smbGroup.get(), &QButtonGroup::idClicked, [&](int state) { db_state_changed(state); } );
#else
    QObject::connect( smbGroup.get(), SIGNAL(buttonClicked(QAbstractButton*)), q, SLOT(dbbuttonClicked(QAbstractButton *)) );
#endif
    QObject::connect( qUi->localUrl, &QLineEdit::editingFinished, [&]() { update_lists(); } );
}

PreferencesJSONUIPrivate::~PreferencesJSONUIPrivate()
{ }

#if (QT_VERSION < QT_VERSION_CHECK(5, 15, 0))
    void PreferencesJSONUI::dbbuttonClicked(QAbstractButton *button)
    {
      auto d = impl_func();
      int state = d->smbGroup->id(button);
      d->db_state_changed(state);
    }
#endif

void PreferencesJSONUIPrivate::db_state_changed(int state)
{
    qUi->localUrl->setEnabled(state==0);
    qUi->localDBName->setEnabled(state==0);
    qUi->localUser->setEnabled(state==0);
    qUi->localPassword->setEnabled(state==0);
    qUi->newDatabase->setEnabled(state==0);
    qUi->localRO->setEnabled(state==0);

    qUi->remoteUrl->setEnabled(state==1);
    qUi->remoteDBName->setEnabled(state==1);
    qUi->remoteUser->setEnabled(state==1);
    qUi->remotePassword->setEnabled(state==1);
    qUi->remoteRO->setEnabled(state==1);
}

void PreferencesJSONUIPrivate::update_lists()
{
    try {  // could be not exist local ArangoDB or illegal root password
        auto aroot_client = uiSettings().impl_func()->get_root_client();
        auto users = aroot_client->userNames();
        qUi->localUser->clear();
        for( const auto& user : users )
            qUi->localUser->addItem(user.c_str());

        auto dbnames = aroot_client->databaseNames();
        qUi->localDBName->clear();
        for( const auto& name : dbnames )
            qUi->localDBName->addItem(name.c_str());
    }
    catch(std::exception& e)
    {
        ui_logger->error("Error connection to localhost: {}", e.what());
    }
}

bool PreferencesJSONUIPrivate::cm_save()
{
        // save current settings
        jsonio_settings.setValue( jsonio17::common_section( "ResourcesDirectory" ), qUi->resourcesEdit->text().toStdString() );
        jsonio_settings.setValue( jsonio17::common_section( "SchemasDirectory" ), qUi->schemasEdit->text().toStdString() );
        jsonio_settings.setValue( jsonui_section( "ShowComments" ),  qUi->commentBox->isChecked() );
        jsonio_settings.setValue( jsonui_section( "ShowEnumNames" ),  qUi->enumBox->isChecked() );
        jsonio_settings.setValue( jsonui_section( "CanEdit_id" ),  qUi->idBox->isChecked() );
        jsonio_settings.setValue( jsonui_section( "KeepExpanded" ),  qUi->expandedBox->isChecked() );
        jsonio_settings.setValue( jsonui_section( "CanEditDocPages" ), HelpMainWindow::editHelp );

        // setup global values
        jsonio_settings.setUserDir( qUi->usersEdit->text().toStdString() );
        uiSettings().impl_func()->update_ui_settings();

        std::string localDatabase = qUi->localDBName->currentText().toStdString();
        std::string localUser = qUi->localUser->currentText().toStdString();
        std::string localPassword = qUi->localPassword->text().toStdString();
        bool createlocalDB = qUi->newDatabase->isChecked();
        int newDBConnection = smbGroup->checkedId();

        // do not change if exeption up

        jsonio_settings.setValue( jsonio17::arangodb_section( "ArangoDBLocal.DB_URL" ),  qUi->localUrl->text().toStdString() );
        jsonio_settings.setValue( jsonio17::arangodb_section( "ArangoDBLocal.DBName" ),  localDatabase );
        jsonio_settings.setValue( jsonio17::arangodb_section( "ArangoDBLocal.DBUser" ),  localUser );
        jsonio_settings.setValue( jsonio17::arangodb_section( "ArangoDBLocal.DBUserPassword" ),  localPassword );
        std::string localUserAccess = ( qUi->localRO->isChecked()? "ro":"rw");
        jsonio_settings.setValue( jsonio17::arangodb_section( "ArangoDBLocal.DBAccess" ), localUserAccess );
        jsonio_settings.setValue( jsonio17::arangodb_section( "ArangoDBLocal.DBCreate" ), createlocalDB );

        if( newDBConnection == 0  && createlocalDB )
        {
            // root url must be the same as  LocalArangoDBUrl
            uiSettings().impl_func()->get_root_client()->createDatabase( localDatabase, { arangocpp::ArangoDBUser(localUser, localPassword) });
        }

        jsonio_settings.setValue( jsonio17::arangodb_section( "ArangoDBRemote.DB_URL" ),   qUi->remoteUrl->text().toStdString() );
        jsonio_settings.setValue( jsonio17::arangodb_section( "ArangoDBRemote.DBName" ),   qUi->remoteDBName->text().toStdString() );
        jsonio_settings.setValue( jsonio17::arangodb_section( "ArangoDBRemote.DBUser" ),   qUi->remoteUser->text().toStdString() );
        jsonio_settings.setValue( jsonio17::arangodb_section( "ArangoDBRemote.DBUserPassword" ), qUi->remotePassword->text().toStdString() );
        std::string remoteUserAccess = ( qUi->remoteRO->isChecked()? "ro":"rw");
        jsonio_settings.setValue( jsonio17::arangodb_section( "ArangoDBRemote.DBAccess" ),   remoteUserAccess );

        uiSettings().impl_func()->set_current_db_connection( newDBConnection );
        // test to reset db driver => close all windows (closeAll) or only (updateDB)
        if( ( newDBConnection != old_db_connection ) ||        // Move to new Driver type
                uiSettings().impl_func()->test_current_driver( old_db_connection ) )   // Move to new dbname or user
        {
            uiSettings().impl_func()->update_database();
            return true;
        }
     return false;
}


//----------PreferencesJSONUI-------------------------------------------------------------------


PreferencesJSONUI::PreferencesJSONUI(jsonio17::JsonioSettings &asettings, QWidget *parent):
    QDialog(parent),
    impl_ptr( new PreferencesJSONUIPrivate( this, asettings ))
{ }

PreferencesJSONUI::~PreferencesJSONUI()
{ }

void PreferencesJSONUI::CmSave()
{
    try {  // could be not exist local ArangoDB or illegal root password
        auto d = impl_func();
        if( d->cm_save() )
        {
            emit dbdriveChanged();
        }
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "Preferences update", (std::string("Error connection to localhost: \n")+e.what()).c_str() );
    }
    accept();
}

void PreferencesJSONUI::CmProjectDir()
{
    auto d = impl_func();
    QString dir = QFileDialog::getExistingDirectory(this, "Select Project Directory",
                                                    "",  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks );
    d->qUi->usersEdit->setText( dir );
}

void PreferencesJSONUI::CmSchemasDir()
{
    auto d = impl_func();
    QString dir = QFileDialog::getExistingDirectory(this, "Select Schemas Directory",
                                                    "",  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks );
    d->qUi->schemasEdit->setText( dir );
}

void PreferencesJSONUI::CmResourcesDir()
{
    auto d = impl_func();
    QString dir = QFileDialog::getExistingDirectory(this, "Select Resource Directory",
                                                    "",  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks );
    d->qUi->resourcesEdit->setText( dir );
}

void PreferencesJSONUI::CmHelp()
{
    helpWin( "PreferencesJsonui", "" );
}

} // namespace jsonui17

// ------------------------ end of PreferencesJSONUI.cpp ------------------------------------------
