#pragma once

#include <memory>
#include "jsonui17/FormatImpexWidget.h"
#include "BaseWidget_p.h"
#include "jsonio17/dbschemadoc.h"
#include "jsonui17/models/json_view.h"

namespace Ui {
class FormatImpexWidget;
}

namespace jsonui17 {

/// \class FormatImpexWidget - Widget to work with Foreign Import/Export File Format
class FormatImpexWidgetPrivate : public BaseWidgetPrivate
{

    Q_OBJECT

#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    Q_DISABLE_COPY_MOVE(FormatImpexWidgetPrivate)
#endif

public:

    explicit FormatImpexWidgetPrivate( FormatImpexWidget::MODE_ mode, const std::string& run_schema_name );
    ~FormatImpexWidgetPrivate();

    /// Setup widget data
    void init();
    void cm_new();
    void cm_clone();
    void edit_id( bool editid );
    void run_script();

    void cm_read( const std::string& record_key )
    {
        dbimpex->readDocument( record_key );
        fromJsonNode( dbimpex->loaded_data() );
        set_content_state( false );
    }

    void cm_update()
    {
        if( dbimpex.get() == nullptr )
            return;
        current_record = toJson();
        dbimpex->updateFromJson( current_record, true );
        set_content_state( false );
    }

    void cm_delete_list( const std::vector<std::string>& keys)
    {
        for( const auto& key: keys)
            dbimpex->deleteDocument(key);
        // dbimpex->removeByKeys( keys );
    }

    void schema_changed(const QString & text)
    {
        current_schema_name = text.toStdString();
        std::string curscript = json_tree->saveToJson();
        json_tree->updateModelData( curscript, current_schema_name );
        json_tree->expandToDepth(0);
    }

private:

    inline FormatImpexWidget* base_func() { return static_cast<FormatImpexWidget *>(base_ptr); }
    inline const FormatImpexWidget* base_func() const { return static_cast<const FormatImpexWidget *>(base_ptr); }
    friend class FormatImpexWidget;

    QScopedPointer<Ui::FormatImpexWidget> qUi;
    /// tree view
    QScopedPointer<JsonView>   json_tree;

    // Internal data
    FormatImpexWidget::MODE_ use_mode = FormatImpexWidget::editMode;
    std::string parent_schema_name = "";

    std::shared_ptr<jsonio17::DBSchemaDocument> dbimpex;
    std::string current_record = "";

    // Work functions
    ExecuteImpex_f run_funct;

    /// Set up menu commands
    void set_actions();
    /// Set up current json data to view model
    bool reset_json( const std::string& json_string, const std::string& schema_name );
    /// Setup/ update database connection
    void reset_DB_client();
    std::string toJson() const;
    void fromJsonNode( const jsonio17::JsonBase& object );

    // update after change preferences
    void updt_view_menu() override;
    void updt_model() override;
    void updt_table() override;
    void updt_db() override;
};

} // namespace jsonui17

