#pragma once

#include "jsonui17/preferences.h"

namespace arangocpp {
class ArangoDBRootClient;
}

namespace jsonui17 {

/// Private \class Preferences - monitoring preferences into JSONIO
class UIPreferencesPrivate
{

#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    Q_DISABLE_COPY_MOVE(UIPreferencesPrivate)
#endif
    friend class PreferencesJSONUIPrivate;

public:

    /// Constructor
    explicit UIPreferencesPrivate();

    /// Constructor
    virtual ~UIPreferencesPrivate();

    /// Current work Database
    const jsonio17::DataBase& database() const
    {
        return *work_database.get();
    }

    /// Current resourse Database
    const jsonio17::DataBase& resources_database() const
    {
        return *resourse_database.get();
    }

protected:

    friend class UIPreferences;

    /// Link to JsonioSettings - storing preferences to JSONIO
    jsonio17::JsonioSettings& jsonio_settings;
    /// Link to jsonui settings in configuration data
    jsonio17::SectionSettings jsonui_group;

    /// Current work Database
    std::shared_ptr<jsonio17::DataBase> work_database;
    /// Current resourse Database
    std::shared_ptr<jsonio17::DataBase> resourse_database;
    /// Current ArangoDB root client
    std::shared_ptr<arangocpp::ArangoDBRootClient> root_client;

    /// Get current root connections
    arangocpp::ArangoDBRootClient* get_root_client();

    void update_ui_settings()
    {
        set_def_values();
    }

    /// Link new Driver
    void update_database();
    /// Test changes in current driver data
    bool test_current_driver( int db_connection );
    /// Update "CurrentDBConnection" value.
    void set_current_db_connection( int db_connection );
    /// Get "CurrentDBConnection" index
    int get_current_db_connection();

    /// Read/Init main settings
    void set_def_values();
    ///  Create empty collections if they are not present (for all vertexes and edges)
    void create_collection_if_no_exist( jsonio17::AbstractDBDriver* db_driver);
    /// Link new resource Driver
    void update_resource_database();
    /// Create resource Database
    void create_local_database_if_no_exist( const std::string& database_name );
    bool test_create_local_database( const std::string& database_name, const std::string& user );

};

} // namespace jsonui17

