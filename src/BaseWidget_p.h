#pragma once

#include "jsonui17/BaseWidget.h"

namespace jsonui17 {


/// Private \class BaseWidget base window to organize update/close events
class BaseWidgetPrivate : public QObject
{

    Q_OBJECT

#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    Q_DISABLE_COPY_MOVE(BaseWidgetPrivate)
#endif

public slots:

    /// Change the state of internal content as edited
    virtual void objectChanged()
    {
      set_content_state(true);
    }

public:

    explicit BaseWidgetPrivate( const std::string& schema_name ):
        QObject(nullptr), current_schema_name( schema_name )
    {
        on_close_function = [](QMainWindow*){ return true; };
    }

    virtual ~BaseWidgetPrivate() {}


    /// Return the state of internal content
    bool content_changed() const
    {
        return contents_changed;
    }

    /// Change the state of internal content
    virtual void set_content_state( bool state )
    {
        contents_changed = state;
    }

protected:

    BaseWidget * base_ptr;
    inline BaseWidget* base_func() { return static_cast<BaseWidget *>(base_ptr); }
    inline const BaseWidget* base_func() const { return static_cast<const BaseWidget *>(base_ptr); }
    friend class BaseWidget;


    /// Current documents schema name ( could be empty for FreeJson data )
    std::string current_schema_name = "";
    /// Current state of internal content ( changed or not )
    bool contents_changed = false;

    /// The function to call when trying close the window
    OnCloseEvent_f on_close_function;
    /// The external function for opening vertex or edge document widget
    ShowVertexEdge_f show_document_widget_function;

    // update after change preferences
    virtual void updt_view_menu() {};
    virtual void updt_model() {};
    virtual void updt_table() {};
    virtual void updt_db() {};

};

} // namespace jsonui17


