#include <QMessageBox>
#include <QLineEdit>
#include <QHeaderView>
#include "ui_FormatImpexWidget.h"
#include "FormatImpexWidget_p.h"
#include "jsonui17/SelectDialog.h"
#include "jsonui17/FileDialogs.h"
#include "jsonui17/preferences.h"
#include "jsonui17/models/schema_model.h"
#ifndef IMPEX_OFF
#include "jsonimpex17/impex_generator.h"
#include "jsonimpex17/yaml_xml2file.h"
#endif


namespace jsonui17 {

static jsonio17::DBSchemaDocument* newDBImpexClientUI(const jsonio17::DBQueryBase& query)
{
#ifndef IMPEX_OFF
    return jsonio17::newDBImpexClient(uiSettings().resources_database(), query);
#else
    return  jsonio17::DBSchemaDocument::newSchemaDocumentQuery(uiSettings().resources_database(),  "ImpexFormat",
                                                               "impexdefs",  query );
#endif
}

//---------FormatImpexWidget------------------------------------------------------------------------------------------


FormatImpexWidget::FormatImpexWidget( MODE_ mode, const std::string& run_schema_name,  QWidget *parent ) :
    BaseWidget(*new FormatImpexWidgetPrivate(mode, run_schema_name), parent)
{
    auto d = impl_func();
    d->init();
    // set up default data
    CmNew();
}

FormatImpexWidget::~FormatImpexWidget()
{}

void FormatImpexWidget::setExecuteFunction( ExecuteImpex_f afunc )
{
    auto d = impl_func();
    d->run_funct = afunc;
}

// Menu commands -----------------------------------------------------------

//void FormatImpexWidget::CmEditID( bool editid )
//{
//    Q_D(FormatImpexWidget);
//    d->edit_id(!editid);
//}


/// Set default bson record
void FormatImpexWidget::CmNew()
{
    try {
        auto d = impl_func();
        d->cm_new();
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

// Clone json record (clear _id)
void FormatImpexWidget::CmClone()
{
    try {
        auto d = impl_func();
        d->cm_clone();
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read new record from DB
void FormatImpexWidget::CmRead()
{
    try {
        auto d = impl_func();
        if( d->dbimpex.get() == nullptr )
            return;
        // Select key to load
        auto reckey = SelectKeyFrom( d->dbimpex.get(), this, "Please, select a record to read/view" );
        if( !reckey.empty() )
            d->cm_read(reckey);
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Save current record to DB file
void FormatImpexWidget::CmUpdate()
{
    try {
        auto d = impl_func();
        d->cm_update();
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Delete list of documents from DB
void FormatImpexWidget::CmDelete()
{
    try
    {
        auto d = impl_func();
        if( d->dbimpex.get() == nullptr )
            return;

        auto keys = SelectKeysFrom( d->dbimpex.get(), this, "Please, select records to be deleted" );
        if( !keys.empty() )
            d->cm_delete_list(keys);
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read format only from json/yaml file fileName
void FormatImpexWidget::CmImportJSON()
{
    try {
        auto d = impl_func();
        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                             "Please, select a file with format", domFilters  ))
        {
#ifndef IMPEX_OFF
            jsonio17::JsonYamlXMLFile file( fileName );
#else
            jsonio17::JsonFile file( fileName );
#endif
            d->current_record =  file.load_json();
            if( !d->reset_json( d->current_record, schema_from_name( fileName )) )
                jsonio17::JSONIO_THROW( "impex", 12, " try to read another schema format file "+ fileName );
            d->set_content_state( true );
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to json file fileName
void FormatImpexWidget::CmExportJSON()
{
    try {
        const auto d = impl_func();
        std::string fileName = file_schema_ext( d->current_schema_name, "json" );
        if(  ChooseFileSave( this, fileName,
                             "Please, select a file to write the format", domFilters, fileName  ))
        {
#ifndef IMPEX_OFF
            jsonio17::JsonYamlXMLFile file( fileName );
            file.set_type( jsonio17::TxtFile::Json );
#else
            jsonio17::JsonFile file( fileName );
#endif
            file.save( d->json_tree->current_object() );
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void FormatImpexWidget::CmSelectDataFile()
{
    try {
        auto d = impl_func();
        std::string fileName = d->qUi->datafileEdit->text().toStdString();
        if(  ChooseFileOpen( this, fileName,
                             "Select Input File Path", textFilters  ))
        {
            d->qUi->datafileEdit->setText( fileName.c_str() );
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void FormatImpexWidget::CmRunScript()
{
#ifndef IMPEX_OFF
    try {
        auto d = impl_func();
        d->run_script();
        close();
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
#endif
}

//----FormatImpexWidgetPrivate------------------------------------------------------------------------------------------

FormatImpexWidgetPrivate::FormatImpexWidgetPrivate( FormatImpexWidget::MODE_ mode, const std::string& run_schema_name ):
    BaseWidgetPrivate( "" ),
    qUi(nullptr), json_tree(nullptr),
    use_mode( mode ), parent_schema_name( run_schema_name ), current_record("")
{}

FormatImpexWidgetPrivate::~FormatImpexWidgetPrivate()
{}

void FormatImpexWidgetPrivate::init()
{
    qUi.reset(new Ui::FormatImpexWidget);

    auto q = base_func();
    qUi->setupUi(q);
    qUi->splitter->setStretchFactor(0, 1);
    qUi->splitter->setStretchFactor(1, 2);

    q->setAttribute(Qt::WA_DeleteOnClose); // automatically delete itself when window is closed

    QString title = qApp->applicationName()+" Structured Data Editor and Database Browser";
    q->setWindowTitle(title);

    //set up main parameters
    const jsonio17::StructDef*  strct_def = jsonio17::ioSettings().Schema().getStruct( "FormatStructDataFile" );
    if( strct_def != nullptr )
        qUi->impexschema->addItem("FormatStructDataFile");
    strct_def = jsonio17::ioSettings().Schema().getStruct( "FormatKeyValueFile" );
    if( strct_def != nullptr )
        qUi->impexschema->addItem("FormatKeyValueFile");
    strct_def = jsonio17::ioSettings().Schema().getStruct( "FormatTableFile" );
    if( strct_def != nullptr )
        qUi->impexschema->addItem("FormatTableFile");
    strct_def = jsonio17::ioSettings().Schema().getStruct( "FormatTextFile" );
    if( strct_def != nullptr )
        qUi->impexschema->addItem("FormatTextFile");
    jsonio17::JSONIO_THROW_IF( qUi->impexschema->count()<1, "impex", 11, " no impex formats defined" );

    current_schema_name = qUi->impexschema->currentText().toStdString();
    run_funct = [](std::string, std::string, std::string, std::string, bool ) {};

    //define schema checkbox
    auto vertex_names = jsonio17::DataBase::getVertexesList();
    for( const auto& vname: vertex_names )
        qUi->schemaBox->addItem( vname.c_str() );
    vertex_names = jsonio17::DataBase::getEdgesList();
    for( const auto& ename: vertex_names )
        qUi->schemaBox->addItem( ename.c_str() );
    if( !parent_schema_name.empty() )
        qUi->schemaBox->setCurrentText( parent_schema_name.c_str() );

    if( use_mode )
        qUi->direcBox->setCurrentIndex(use_mode-1);

    // define edit tree view
    QStringList    header_data;
    header_data << "key" << "value" << "comment"  ;
    json_tree.reset( new JsonView(  qUi->bsonWidget ) );
    auto schema_model = new JsonSchemaModel(  current_record, current_schema_name, header_data, q/*qUi->centralWidget*/ );
    JsonSchemaDelegate* deleg_schema = new JsonSchemaDelegate( q );
    json_tree->setModel( schema_model );
    json_tree->setItemDelegate( deleg_schema );
    json_tree->setColumnWidth( 0, 250 );
    json_tree->expandToDepth( 0 );
    qUi->gridLayout_3->addWidget( json_tree.get(), 1, 0, 1, 2 );

    // define menu
    set_actions();
    reset_DB_client();
}

//  Connect all actions
void FormatImpexWidgetPrivate::set_actions()
{
    auto q = base_func();
    QObject::connect( qUi->impexschema, &QComboBox::currentTextChanged, [&](const QString& text) { schema_changed(text); } );
    QObject::QObject::connect( qUi->datafileButton, &QToolButton::clicked, q, &FormatImpexWidget::CmSelectDataFile);

    // File
    QObject::connect( qUi->actionNew , &QAction::triggered, q, &FormatImpexWidget::CmNew);
    QObject::connect( qUi->actionClone, &QAction::triggered, q, &FormatImpexWidget::CmClone);
    QObject::connect( qUi->actionE_xit, &QAction::triggered, q, &FormatImpexWidget::close);
    QObject::connect( qUi->actionExport_File, &QAction::triggered, q, &FormatImpexWidget::CmExportJSON);
    QObject::connect( qUi->actionImport_File, &QAction::triggered, q, &FormatImpexWidget::CmImportJSON);
    QObject::connect( qUi->actionSelect_Data_File, &QAction::triggered, q, &FormatImpexWidget::CmSelectDataFile);
#ifndef IMPEX_OFF
    QObject::connect( qUi->action_Run_script, &QAction::triggered, q, &FormatImpexWidget::CmRunScript);
#else
    qUi->action_Run_script->setDisabled(true);
#endif

    // Edit
    QObject::connect(qUi->actionAdd_one_field, &QAction::triggered, json_tree.get(), &JsonView::CmAddObject);
    QObject::connect(qUi->action_Clone_Current, &QAction::triggered, json_tree.get(), &JsonView::CmCloneObject);
    QObject::connect(qUi->action_Delete_field, &QAction::triggered, json_tree.get(), &JsonView::CmDelObject);
    QObject::connect(qUi->action_Delete_fields, &QAction::triggered, json_tree.get(), &JsonView::CmDelObjects);
    QObject::connect(qUi->actionRemove_Alternatives_Union, &QAction::triggered, json_tree.get(), &JsonView::CmDelObjectsUnion);
    QObject::connect(qUi->action_Add_fields, &QAction::triggered, json_tree.get(), &JsonView::CmAddObjects);
    QObject::connect(qUi->actionReset_Data_to_Defaults, &QAction::triggered, json_tree.get(), &JsonView::CmResetObject);
    QObject::connect(qUi->action_Resize_array, &QAction::triggered, json_tree.get(), &JsonView::CmResizeArray);
    QObject::connect(qUi->action_Calculator, &QAction::triggered, json_tree.get(), &JsonView::CmCalc);
    QObject::connect(qUi->actionCopy_Field_Path, &QAction::triggered, json_tree.get(), &JsonView::CopyFieldPath);
    QObject::connect(qUi->actionCopy_Field, &QAction::triggered, json_tree.get(), &JsonView::CopyField);
    QObject::connect(qUi->actionPaste_Field, &QAction::triggered, json_tree.get(), &JsonView::PasteField);

    // Help
    QObject::connect( qUi->action_Help_About, &QAction::triggered, q, &FormatImpexWidget::CmHelpAbout);
    QObject::connect( qUi->actionContents, &QAction::triggered, q, &FormatImpexWidget::CmHelpContens);
    QObject::connect( qUi->actionAuthors, &QAction::triggered, q, &FormatImpexWidget::CmHelpAuthors);
    QObject::connect( qUi->actionLicense, &QAction::triggered, q, &FormatImpexWidget::CmHelpLicense);

    // View
    QObject::connect( qUi->action_Show_comments, &QAction::toggled, &uiSettings(), &UIPreferences::CmShowComments);
    QObject::connect( qUi->action_Display_enums, &QAction::toggled, &uiSettings(), &UIPreferences::CmDisplayEnums);
    //QObject::connect( qUi->action_Edit_id, &QAction::toggled, &uiSettings(), &UIPreferences::CmEditID);
    QObject::connect( qUi->actionKeep_Data_Fields_Expanded, &QAction::toggled, &uiSettings(), &UIPreferences::CmEditExpanded);

    QObject::connect( qUi->action_Show_comments, SIGNAL(toggled(bool)), q, SLOT(updateModel()));
    QObject::connect( qUi->action_Display_enums, SIGNAL(toggled(bool)), q, SLOT(updateTable()));
    QObject::connect( qUi->action_Edit_id, &QAction::toggled, [&](bool editid) { edit_id(editid); } );
    edit_id( JsonSchemaModel::editID );
    updt_view_menu();

    QObject::connect( qUi->action_Update, &QAction::triggered, q, &FormatImpexWidget::CmUpdate);
    QObject::connect( qUi->action_Delete, &QAction::triggered, q, &FormatImpexWidget::CmDelete);
    QObject::connect( qUi->action_Read, &QAction::triggered, q, &FormatImpexWidget::CmRead);

    if( use_mode == FormatImpexWidget::editMode )
    {
        qUi->runToolBar->hide();
        qUi->action_Run_script->setDisabled(true);
        qUi->actionSelect_Data_File->setDisabled(true);
        qUi->datafileButton->setDisabled(true);
        qUi->datafileEdit->setDisabled(true);
        qUi->overwriteBox->setDisabled(true);
    }
    else
    {
        qUi->direcBox->setEnabled(false);
        qUi->schemaBox->setEnabled(false);
    }

}

bool FormatImpexWidgetPrivate::reset_json( const std::string& json_string, const std::string& schema_name )
{
    // test legal schema name
    if( !current_schema_name.empty() && !schema_name.empty() && schema_name != current_schema_name )
        return false;
    json_tree->updateModelData( json_string, current_schema_name );
    json_tree->update();
    return true;
}

// set data to json
std::string FormatImpexWidgetPrivate::toJson() const
{
    auto  impex_data = jsonio17::JsonFree::object();
    impex_data["_id"]  =  qUi->keyNoEdit->text().toStdString();
    impex_data["name"]  =  qUi->nameEdit->text().toStdString();
    impex_data["impexschema"]  = qUi->impexschema->currentText().toStdString();
    impex_data["schema"]  =  qUi->schemaBox->currentText().toStdString();
    impex_data["comment"]  =   qUi->descEdit->text().toStdString();
    impex_data["direction"]  =  qUi->direcBox->currentIndex();
    impex_data["impex"] = json_tree->saveToJson();
    return impex_data.dump();
}

void FormatImpexWidgetPrivate::fromJsonNode( const jsonio17::JsonBase& object )
{
    // setup data from json
    std::string getdata;

    object.get_value_via_path<std::string>( "_id", getdata, "" );
    qUi->keyNoEdit->setText( getdata.c_str() );
    object.get_value_via_path<std::string>( "name", getdata, "new_impex" );
    qUi->nameEdit->setText(getdata.c_str());
    object.get_value_via_path<std::string>( "impexschema", getdata, "" );
    qUi->impexschema->setCurrentText( getdata.c_str() );
    object.get_value_via_path<std::string>( "schema", getdata, "" );
    qUi->schemaBox->setCurrentText( getdata.c_str() );
    object.get_value_via_path<std::string>( "comment", getdata, "" );
    qUi->descEdit->setText( getdata.c_str() );

    int direc;
    object.get_value_via_path( "direction", direc, 0 );
    qUi->direcBox->setCurrentIndex(direc);

    // read impex
    object.get_value_via_path<std::string>( "impex", current_record, "");
    reset_json(current_record, "");

    // not used now
    object.get_value_via_path<std::string>( "format", getdata, "" );
    object.get_value_via_path<std::string>( "extension", getdata, "" );
}

void FormatImpexWidgetPrivate::reset_DB_client()
{
    try{
        std::string queryString = "";
        if( use_mode!=FormatImpexWidget::editMode && !parent_schema_name.empty() )
        {
            queryString = "{ \"schema\": \"";
            queryString += parent_schema_name;
            queryString += "\", ";
            switch( use_mode )
            {
            case  FormatImpexWidget::runModeImport:
            default:
                queryString += " \"direction\": 0 }";
                break;
            case FormatImpexWidget::runModeExport:
                queryString += " \"direction\": 1 }";
                break;
            case FormatImpexWidget::runModeUpdate:
                queryString += " \"direction\": 2 }";
                break;
            }
        }

        jsonio17::DBQueryBase qrdata(queryString, jsonio17::DBQueryBase::qTemplate );
        jsonio17::DBSchemaDocument* newClient = newDBImpexClientUI(qrdata);
        if( newClient == nullptr )
            dbimpex.reset();
        else
            dbimpex.reset( newClient );

    }
    catch(std::exception& e)
    {
        ui_logger->error("Error when resetting FormatImpex DB client {}: {}", current_schema_name, e.what());
        throw;
    }
}

/// Change current View menu selections
void FormatImpexWidgetPrivate::updt_view_menu()
{
    qUi->action_Show_comments->setChecked( JsonSchemaModel::showComments );
    qUi->action_Display_enums->setChecked( JsonSchemaModel::useEnumNames);
    qUi->action_Edit_id->setChecked( JsonSchemaModel::editID );
    qUi->actionKeep_Data_Fields_Expanded->setChecked( JsonView::expandedFields );
}

/// Change model ( show/hide comments)
void FormatImpexWidgetPrivate::updt_model()
{
    if( !current_schema_name.empty() )
    {
        json_tree->resetModel();;
    }
}

/// Change table (Show Enum Names Instead of Values)
void FormatImpexWidgetPrivate::updt_table()
{
    json_tree->hide();
    json_tree->show();
}

/// Update after change DB locations
void FormatImpexWidgetPrivate::updt_db()
{
    //    done it at collection level for all documents
    //    if( dbimpex.get() )
    //        dbimpex->updateQuery();
}

void FormatImpexWidgetPrivate::cm_new()
{
    qUi->keyNoEdit->setText("");
    qUi->nameEdit->setText("new_impex_name");
    qUi->descEdit->setText("");
    reset_json( "", "" );
}

void FormatImpexWidgetPrivate::cm_clone()
{
    qUi->keyNoEdit->setText("");
}

void FormatImpexWidgetPrivate::edit_id(bool editid)
{
    qUi->keyNoEdit->setReadOnly(!editid);
}

void FormatImpexWidgetPrivate::run_script()
{
#ifndef IMPEX_OFF
    //int mode = qUi->direcBox->currentIndex();
    //auto inputIEFile = std::make_shared<jsonio17::ImpexFormatFile>(
    //            static_cast<jsonio17::ImpexFormatFile::ImpexMode>(mode),
    //            current_schema_name, json_tree->current_object() );
    std::string datafile = qUi->datafileEdit->text().toStdString();
    jsonio17::JSONIO_THROW_IF(datafile.empty(), "impex", 13, " undefined file with data to import");
    bool overwrite = qUi->overwriteBox->isChecked();
    std::string impex_data_name = qUi->schemaBox->currentText().toStdString();
    json_tree->getValueViaPath( "label", impex_data_name, impex_data_name);
    run_funct( impex_data_name, current_schema_name, json_tree->current_object().dump(true), datafile, overwrite );
#endif
}


} // namespace jsonui17

