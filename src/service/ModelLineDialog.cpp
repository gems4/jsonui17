#include <QSpinBox>
#include "ModelLineDialog.h"
#include "ui_ModelLineDialog.h"
#include "jsonui17/models/json_model.h"
#include "jsonui17/models/schema_model.h"
#include "jsonui17/preferences.h"

namespace jsonui17 {

QVariant SizeModel::data( const QModelIndex& index, int role ) const
{
    if( !index.isValid() )
        return QVariant();

    switch( role )
    {
    case Qt::DisplayRole:
    case Qt::EditRole:
        return QVariant( static_cast< unsigned int >( sizes_data[index.row()] ));
    default: break;
    }
    return QVariant();
}

bool SizeModel::setData( const QModelIndex &index, const QVariant& value, int role)
{
    if( index.isValid() && ( role == Qt::EditRole ) )
    {
        sizes_data[ index.row() ] = value.toUInt();
        return true;
    }
    return false;
}

void SizeModel::resetData()
{
    beginResetModel();
    endResetModel();
}

QWidget *SizeDelegate::createEditor( QWidget *parent, const QStyleOptionViewItem&,
                                     const QModelIndex&) const
{
    QSpinBox *spin = 	new QSpinBox( parent );
    spin->setMinimum(0);
    return spin;
}

//==================================================

ModelLineDialog::ModelLineDialog( const JsonBaseModel* fmodel,
                                  const QModelIndex& index, const QModelIndex& parentIndex,
                                  const std::string& field_name,  QWidget *parentWidget):
    QDialog( parentWidget ), ui(new Ui::ModelLineDialog),
    field_model(fmodel), resize_array_mode( !field_name.empty() ), array_sizes()
{
    ui->setupUi(this);

    size_model = new SizeModel( array_sizes, this);
    ui->sizeView->setModel(size_model);
    ui->sizeView->horizontalHeader()->hide();
    SizeDelegate *sizedlg = new SizeDelegate(this);
    ui->sizeView->setItemDelegate(sizedlg);

    if( dynamic_cast<const JsonSchemaModel*>(field_model) )
        set_schema_json_model( field_name, index, parentIndex );
    else
        if( dynamic_cast<const JsonFreeModel*>(field_model) )
            set_free_json_model( field_name, index );
}

ModelLineDialog::~ModelLineDialog()
{
    delete ui;
}

void ModelLineDialog::setDatabyType( int )
{
    auto jsontype = objectType();
    set_editor_by_type( jsontype );
}

void ModelLineDialog::set_schema_json_model( const std::string& field_name, const QModelIndex& field_index,
                                             const QModelIndex& parentIndex )
{
    auto parent_item =  dynamic_cast<const jsonio17::JsonSchema*>( field_model->lineFromIndex(parentIndex) );
    field_top_schema_name = parent_item->getStructName();
    ui->typeBox->setEnabled(false);

    if( resize_array_mode )
    {
        ui->nameBox->addItem( field_name.c_str() );
        ui->nameBox->setCurrentText( field_name.c_str() );
        ui->nameBox->setEnabled(false);

        // get old table sizes
        auto itline = field_model->lineFromIndex( field_index );
        auto schemaline = dynamic_cast<const jsonio17::JsonSchema*>(itline);
        size_t level = 0;
        auto field_data = schemaline->fieldDescription( level );
        reset_schema_size_model( level, field_data, itline );
        ui->labelSize->show();
        ui->sizeView->show();
        ui->labelDef->setText("Element default");
    }
    else
    {
        // set up names box
        auto nonames = parent_item->getNoUsedKeys();
        for(size_t ii=0; ii<nonames.size(); ii++ )
            ui->nameBox->addItem( nonames[ii].c_str() );

        QObject::connect( ui->nameBox, &QComboBox::currentTextChanged,
                          this, &ModelLineDialog::setDatabyName);

        if( ui->nameBox->count()<1 )
            return;
        ui->nameBox->setCurrentIndex(0);
    }
    setDatabyName( ui->nameBox->currentText() );
}


void ModelLineDialog::set_free_json_model( const std::string& field_name, const QModelIndex& index )
{
    ui->docLine->setText("Free format json object");

    if( resize_array_mode )
    {
        ui->nameBox->addItem( field_name.c_str() );
        ui->nameBox->setCurrentText( field_name.c_str() );
        ui->nameBox->setEnabled(false);

        // get old table sizes
        auto itline = field_model->lineFromIndex(index);
        while( itline && itline->isArray() )
        {
            array_sizes.push_back(itline->size());
            itline = itline->getChild(0);
        }
        size_model->resetData();
        ui->labelDef->setText(" Element default ");
        ui->labelType->setText(" Element type ");

        if( itline && itline->size() > 0 )
            set_editor_by_type( itline->child(0).type() );
        else
            set_editor_by_type( jsonio17::JsonBase::String );
    }
    else
    {
        ui->labelSize->hide();
        ui->sizeView->hide();
        ui->nameBox->setEditable(true);
        ui->defEdit->setText("");
    }

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
    QObject::connect( ui->typeBox, &QComboBox::currentIndexChanged,
                      this, &ModelLineDialog::setDatabyType);
#else
    QObject::connect( ui->typeBox, SIGNAL(currentIndexChanged(int)),
                      this, SLOT(setDatabyType(int)));
#endif
}

void ModelLineDialog::setDatabyName(const QString& text)
{
    std::string new_object = text.toStdString();

    auto schema_def = jsonio17::ioSettings().Schema().getStruct( field_top_schema_name );
    if( !schema_def )
        return;
    auto field_def = schema_def->getField( new_object );
    if( !field_def )
        return;

    ui->docLine->setText( field_def->description().c_str() );
    ui->defEdit->setText( field_def->defaultValue().c_str() );
    auto ajson_type = jsonio17::JsonSchema::fieldtype2basetype( field_def->type(0) );
    set_editor_by_type( jsonio17::JsonSchema::fieldtype2basetype( field_def->elemType() ));
    ui->typeBox->setCurrentText( jsonio17::JsonBase::typeName( ajson_type ) );

    if( !resize_array_mode )
    {
        reset_schema_size_model( 0, field_def, nullptr );
        if( array_sizes.size() > 0 )
        {
            ui->labelSize->show();
            ui->sizeView->show();
            ui->labelDef->setText("Element default");
        }
        else
        {
            ui->labelSize->hide();
            ui->sizeView->hide();
            ui->labelDef->setText("Default");
        }
    }
}

void ModelLineDialog::reset_schema_size_model( size_t level, const jsonio17::FieldDef *field_def, const jsonio17::JsonBase *object )
{
    array_sizes.clear();
    auto itline = object;

    while( field_def->type(level) == jsonio17::FieldDef::T_SET ||
           field_def->type(level) == jsonio17::FieldDef::T_LIST ||
           field_def->type(level) == jsonio17::FieldDef::T_MAP )
    {
        if( itline )
        {
            array_sizes.push_back(itline->size());
            itline = itline->getChild(0);
        }
        else
            array_sizes.push_back(0);
        if(field_def->type(level) == jsonio17::FieldDef::T_MAP )
            level++;
        level++;
    }
    size_model->resetData();
}


void ModelLineDialog::set_editor_by_type( jsonio17::JsonBase::Type jstype )
{
    ui->defEdit->setEnabled( true );
    switch( jstype  )
    {
    case jsonio17::JsonBase::Object:
    case jsonio17::JsonBase::Array:
        //    ui->defEdit->setEnabled(false);
        //    break;
    case jsonio17::JsonBase::Null:
    case jsonio17::JsonBase::Bool:
    case jsonio17::JsonBase::String:
        ui->defEdit->setValidator( nullptr );
        ui->defEdit->setText( "" );
        break;
    case jsonio17::JsonBase::Int:
        ui->defEdit->setValidator(new QIntValidator(ui->defEdit));
        ui->defEdit->setText( "0" );
        break;
    case jsonio17::JsonBase::Double:
        ui->defEdit->setValidator(new QDoubleValidator(ui->defEdit));
        ui->defEdit->setText( "0.0" );
        break;
    }
}

jsonio17::JsonBase::Type ModelLineDialog::objectType() const
{
    jsonio17::JsonBase::Type new_type = jsonio17::JsonBase::Null;
    switch( ui->typeBox->currentIndex()  )
    {
    case 0:  new_type = jsonio17::JsonBase::String;
        break;
    case 1:  new_type = jsonio17::JsonBase::Double;
        break;
    case 2:  new_type = jsonio17::JsonBase::Int;
        break;
    case 3:  new_type = jsonio17::JsonBase::Bool;
        break;
    case 4:  new_type = jsonio17::JsonBase::Object;
        break;
    case 5:  new_type = jsonio17::JsonBase::Array;
        break;
    }
    return new_type;
}

std::string ModelLineDialog::objectName() const
{
    return ui->nameBox->currentText().toStdString();
}

QVariant ModelLineDialog::objectValue() const
{
    bool ok;
    QVariant value;
    auto jstype = objectType();
    switch( jstype  )
    {
    case jsonio17::JsonBase::Null:
        break;
    case jsonio17::JsonBase::Bool:
        value = ( ui->defEdit->text() == "true" ? true : false );
        break;
    case jsonio17::JsonBase::String:
        value = ui->defEdit->text();
        break;
    case jsonio17::JsonBase::Int:
        value = ui->defEdit->text().toInt(&ok);
        if( !ok )
            value = 0;
        break;
    case jsonio17::JsonBase::Double:
        value = ui->defEdit->text().toDouble(&ok);
        if( !ok )
            value = 0.0;
        break;
    case jsonio17::JsonBase::Object:
        value = ui->defEdit->text();
        break;
    case jsonio17::JsonBase::Array:
        value = ui->defEdit->text();
        break;
    }
    return value;
}

} // namespace jsonui17
