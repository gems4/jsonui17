#include <QVBoxLayout>
#include <QMessageBox>
#include <QComboBox>
#include <QThread>
#include "jsonui17/service/SelectionKeysForm.h"
#include "ui_SelectionKeysForm.h"
#include "jsonui17/service/waitingspinnerwidget.h"
#include "jsonui17/preferences.h"
#include "../models/dbkeys_view.h"
#include "../models/dbquery_thread.h"

namespace jsonui17 {

class SelectionKeysFormPrivate
{

#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    Q_DISABLE_COPY_MOVE(SelectionKeysFormPrivate)
#endif


public:

    explicit SelectionKeysFormPrivate(  SelectionKeysForm *qdialog, const std::vector<std::string>& aVertexesList ):
        base_ptr( qdialog ),
        qUi( new Ui::SelectionKeysForm )
    {
        auto q = base_func();

        qUi->setupUi(q);
        qUi->splitter->setStretchFactor(0, 1);
        qUi->splitter->setStretchFactor(1, 4);

        //define schema checkbox
        for( size_t ii=0; ii<aVertexesList.size(); ii++ )
            qUi->schemaBox->addItem(aVertexesList[ii].c_str());

        if( aVertexesList.size()<2 )
            qUi->schemaBox->setDisabled(true);

        // define key table
        db_keys_table.reset( new DBKeysTable( qUi->widget_2, []( const QModelIndex& ){} ));
        db_keys_table->setSelectionMode(QAbstractItemView::MultiSelection);
       // db_keys_table->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        qUi->verticalLayout_2->addWidget(db_keys_table.get());

        //std::string curSchemaName = "VertexElement";
        //resetDBClient( curSchemaName );
    }

    ~SelectionKeysFormPrivate()
    {
        db_thread.quit();
        db_thread.wait();
    }

    /// Define connection to database
    void set_db_client(const std::string& schema_name );

    /// Returns selected keys
    std::vector<std::string> all_selected_keys() const
    {
        std::vector<std::string> selectedIds;
        QModelIndexList selection = db_keys_table->selectionModel()->selection().indexes();

        for(int i=0; i< selection.count(); i++)
        {
            QModelIndex index = selection.at(i);
            if(index.column() == 0 )
                selectedIds.push_back( index.data().toString().toStdString() );
        }

        return selectedIds;
    }

    void select_all()
    {
        db_keys_table->selectAll();
    }

    void clear_selection()
    {
        db_keys_table->clearSelection();
    }

    void close_query()
    {
        if( db_keys )
            db_keys->Close();
    }

    void reset_query()
    {
        std::vector<std::string> schemalst;
        schemalst.push_back(qUi->schemaBox->currentText().toStdString());
        db_keys->showQuery( "Vertex Query Widget", schemalst );
    }

private:

    SelectionKeysForm * const  base_ptr;
    inline SelectionKeysForm* base_func() { return static_cast<SelectionKeysForm *>(base_ptr); }
    inline const SelectionKeysForm* base_func() const { return static_cast<const SelectionKeysForm *>(base_ptr); }
    friend class SelectionKeysForm;

    QScopedPointer<Ui::SelectionKeysForm> qUi;
    /// Database keys list model
    std::shared_ptr<DBKeysObject> db_keys;
    /// Keys list table
    QScopedPointer<DBKeysTable> db_keys_table;

    // database API
    /// Database connection object
    DBVertexObject* db_request = nullptr;
    /// Thead moved db_request to
    QThread db_thread;
};

void SelectionKeysFormPrivate::set_db_client(const std::string& schema_name )
{
    qRegisterMetaType<std::shared_ptr<jsonio17::DBQueryDef> >("std::shared_ptr<jsonio17::DBQueryDef>");

    try{

        auto q = base_func();
        qUi->schemaBox->setCurrentText(schema_name.c_str());
        db_keys.reset( new DBKeysObject( nullptr, db_keys_table.get(), nullptr ) );
        db_request = new DBVertexObject(  schema_name  );

        QObject::connect( db_keys.get(), &DBKeysObject::updateQuery, db_request, &DBVertexObject::dbChangeQuery );
        QObject::connect( db_request, &DBVertexObject::changedDBClient, db_keys.get(), &DBKeysObject::changeDBClient );
        QObject::connect( db_request, &DBVertexObject::changedModel,  db_keys.get(), &DBKeysObject::resetModel );

        // link from GUI

        QObject::connect( q, &SelectionKeysForm::changedSchema, db_request, &DBVertexObject::dbResetClient );
        QObject::connect( qUi->schemaBox, &QComboBox::currentTextChanged, q, &SelectionKeysForm::changeSchema );
        QObject::connect( qUi->queryButton, &QPushButton::clicked, q, &SelectionKeysForm::resetQuery );

        // link to GUI

        QObject::connect( db_request, &DBVertexObject::changedQueryString, q, &SelectionKeysForm::changeDBClientData );
        QObject::connect( db_request, &DBVertexObject::finished, q, &SelectionKeysForm::finishProcess );
        QObject::connect( db_request, &DBVertexObject::isException, q, &SelectionKeysForm::isException );

        // thread functions

        db_request->moveToThread(&db_thread);
        QObject::connect(&db_thread, &QThread::finished, db_request, &QObject::deleteLater);
        db_thread.start();

        q->changeSchema(schema_name.c_str());
    }
    catch(std::exception& e)
    {
        ui_logger->error("Error when resetting DB client {}: {}", schema_name, e.what());
        throw;
    }
}



/*
{
   "properties.class_": {"2":"SC_AQSOLUTE"}
}
*/

SelectionKeysDialog::SelectionKeysDialog(QWidget *parent):
    QDialog(parent)
{
    auto vertexNames = jsonio17::DataBase::getVertexesList();

    QVBoxLayout *mainLayout = new QVBoxLayout;
    form.reset( new SelectionKeysForm(vertexNames, this) );
    mainLayout->addWidget(form.get());
    setLayout(mainLayout);

    waitDialog = new WaitingSpinnerWidget(this, true/*Qt::ApplicationModal*/, true);
    connect( form.get(), &SelectionKeysForm::startProcess, this, &SelectionKeysDialog::startProcess );
    connect( form.get(), &SelectionKeysForm::finishProcess, this, &SelectionKeysDialog::finishProcess );
    connect( form.get(), &SelectionKeysForm::isException, this, &SelectionKeysDialog::get_exception );

    std::string curSchemaName = "VertexElement";
    form->setDBClient( curSchemaName );
}

SelectionKeysDialog::~SelectionKeysDialog()
{
    if( waitDialog )
        delete waitDialog;
}


/// Start long database process
void SelectionKeysDialog::startProcess()
{
    waitDialog->start();
}

/// Finish long database process
void SelectionKeysDialog::finishProcess()
{
    waitDialog->stop();
}

// Exception when database command execution
void SelectionKeysDialog::get_exception( const QString& titl, const QString& msg )
{
    QMessageBox::critical( this, titl, msg );
}

//------------------------------------------------------

SelectionKeysForm::SelectionKeysForm(const std::vector<std::string> &aVertexesList, QWidget *parent) :
    QWidget(parent),
    impl_ptr( new SelectionKeysFormPrivate( this, aVertexesList ))
{}

SelectionKeysForm::~SelectionKeysForm()
{ }

void SelectionKeysForm::setDBClient(const std::string& schemaName )
{
    //qRegisterMetaType<std::shared_ptr<jsonio17::DBQueryDef> >("std::shared_ptr<jsonio17::DBQueryDef>");

    auto d = impl_func();
    d->set_db_client(schemaName);
}

std::vector<std::string> SelectionKeysForm::allSelectedKeys() const
{
    auto d = impl_func();
    return d->all_selected_keys();
}

void jsonui17::SelectionKeysForm::selectAll()
{
    auto d = impl_func();
    d->select_all();
}

void jsonui17::SelectionKeysForm::clearSelection()
{
    auto d = impl_func();
    d->clear_selection();
}

void jsonui17::SelectionKeysForm::closeQuery()
{
    auto d = impl_func();
    d->close_query();
}

void SelectionKeysForm::changeSchema(QString newSchemaName)
{
    closeQuery();
    emit startProcess();
    emit changedSchema(newSchemaName);
}

/// Update db document query
void SelectionKeysForm::resetQuery()
{
    try {
        auto d = impl_func();
        d->reset_query();
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "CmSearchQuery", e.what() );
    }
}

void SelectionKeysForm::setQuery( QueryWidget* queryW  )
{
    emit startProcess();
    auto d = impl_func();
    d->db_keys->setOneQuery(queryW);
}

std::string SelectionKeysForm::vertexSchema() const
{
    auto d = impl_func();
    return d->qUi->schemaBox->currentText().toStdString();
}

void SelectionKeysForm::changeDBClientData(  QString query )
{
    auto d = impl_func();
    d->qUi->queryEdit->setText( query );
}


} // namespace jsonui17
