
#include "jsonui17/service/CSVPage.h"
#include "jsonui17/models/csv_container.h"

namespace jsonui17 {

/// \class CSVPage implements widget to work with CSV format data.
class CSVPagePrivate
{

#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    Q_DISABLE_COPY_MOVE(CSVPagePrivate)
#endif

public:

    explicit CSVPagePrivate( CSVPage *qdialog, std::shared_ptr<AbstractContainer> datacontainer, int mode ):
        base_ptr( qdialog ), data_CSV(datacontainer), chart_data(nullptr)
    {
        auto q = base_func();
        // set up window
        box_layout.reset( new QVBoxLayout(q) );
        box_layout->setObjectName( QStringLiteral("verticalLayout") );

        if( data_CSV.get() == nullptr )
            data_CSV.reset( new CSVContainer() );

        model_CSV = new MatrixModel( data_CSV, q );
        table_CSV.reset( new MatrixTableProxy( q, mode ) );
        table_CSV->setModel( model_CSV );

        MatrixDelegate *deleg = new MatrixDelegate( q );
        table_CSV->setItemDelegate(deleg);
        box_layout->addWidget( table_CSV.get() );
    }

    ~CSVPagePrivate()
    {}

    void update_table()
    {
        model_CSV->resetMatrixData();
    }
    void object_changed()
    {
        contents_changed = true;
    }
    void calc()
    {
        table_CSV->runCalc();
    }
    void select_row()
    {
        table_CSV->selRow();
    }
    void select_column()
    {
        table_CSV->selColumn();
    }
    void select_all()
    {
        table_CSV->selectAll();
    }
    void cut_data()
    {
        table_CSV->cutData();
    }
    void clear_data()
    {
        table_CSV->clearData();
    }
    void copy_data()
    {
        table_CSV->copyData();
    }
    void copy_data_header()
    {
        table_CSV->copyDataHeader();
    }
    void paste_data()
    {
        table_CSV->pasteData();
    }
    void paste_transposed_data()
    {
        table_CSV->pasteTransposedData();
    }

    /// Save selected data to csv format file
    void save_selected_toCSV( const std::string& file_name ) const
    {
        table_CSV->saveSelectedToCSV(file_name);
    }
    /// Save all table data to csv format file
    void save_all_toCSV( const std::string& file_name ) const
    {
        table_CSV->saveAllToCSV(file_name);
    }
    QModelIndex current_index() const
    {
        return table_CSV->currentIndex();
    }

    void add_graphic();
    void set_axis(int ndxX, std::vector<int> ndxY);
    bool can_be_toggled( int section, bool as_abscissa ) const;
    void toggle_X( int ncolmn );
    void toggle_Y( int ncolmn );

protected:

    friend class TableEditWidgetPrivate;
    CSVPage * const  base_ptr;
    inline CSVPage* base_func() { return static_cast<CSVPage *>(base_ptr); }
    inline const CSVPage* base_func() const { return static_cast<const CSVPage *>(base_ptr); }
    friend class CSVPage;

    /// Widget layout
    QScopedPointer<QVBoxLayout>  box_layout;
    /// CSV table data container
    std::shared_ptr<AbstractContainer> data_CSV;
    /// Model for working with CSV data
    MatrixModel*  model_CSV = nullptr;
    /// Table for editing CSV data
    QScopedPointer<MatrixTableProxy> table_CSV;

    bool contents_changed = false;

    // graph charts information

    /// Abscissa columns list
    std::vector<int> xColumns;
    /// Ordinate columns list
    std::vector<int> yColumns;
    /// Descriptions of model extracting graph data
    std::vector<std::shared_ptr<ChartDataModel>> chart_models;
    /// Description of 2D plotting widget
    std::shared_ptr<ChartData> chart_data;

};


} // namespace jsonui17
