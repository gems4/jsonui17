#pragma once

#include <QAbstractItemModel>
#include "jsonui17/service/SchemaSelectDialog.h"

namespace Ui {
class SchemaSelectDialogData;
}

namespace jsonui17 {


/// \class SelectSchemaLine represents an item in a tree view, and contains several columns of data.
/// Each items data based on thrift field definition
class SelectSchemaLine
{

    friend class SelectSchemaModel;

public:

    /// The constructor is used to record the item's parent and the data associated with each column.
    SelectSchemaLine( const jsonio17::FieldDef* afld_def, SelectSchemaLine* theparent );
    ~SelectSchemaLine();

    /// Set up default template of values for map or array
    void add_value( const char* next );

    /// Add path to list of fields
    void add_link( std::vector<std::string>& fields_list, const std::string& path = "" );

    /// Number of levels for array or map
    int levels() const
    {
      return  arr_levels;
    }

    /// Field description to line
    const jsonio17::FieldDef* fld_desc() const
    {
      return fld_thrift;
    }

protected:

    // internal data
    /// Type of field ( 1- array, 2- map, 0 - other)
    int type;
    /// Field description to line
    const jsonio17::FieldDef* fld_thrift;

    /// Number of levels for array or map
    int arr_levels;
    /// Default value of field
    QString def_value;
    /// Values into columns (key, value, comment)
    QVector<QString> fld_value;

    size_t ndx_in_parent;
    SelectSchemaLine *parent;
    std::vector<std::unique_ptr<SelectSchemaLine>> children;
};


/// \class SelectSchemaModel
/// class for represents the data set and is responsible for fetching the data
/// is needed for viewing and for selecting.
/// Each items data based on thrift field definition for current thrift schema
class SelectSchemaModel: public QAbstractItemModel
{
    Q_OBJECT

public:

    /// The constructor for model based on schemaName thrift schema
    SelectSchemaModel( const std::string& theschema_name,  const QStringList& header_lst,   QObject* parent = nullptr );
    ~SelectSchemaModel();

    QModelIndex index(int row, int column, const QModelIndex& parent) const;
    QModelIndex parent(const QModelIndex& child) const;
    int rowCount ( const QModelIndex& parent ) const;     //ok
    int columnCount ( const QModelIndex& parent  ) const; // ok
    QVariant data ( const QModelIndex& index, int role ) const;
    bool setData ( const QModelIndex& index, const QVariant & value, int role );
    QVariant headerData ( int section, Qt::Orientation orientation, int role ) const;
    Qt::ItemFlags flags ( const QModelIndex& index ) const;

    /// Special function for access to internal item data
    SelectSchemaLine* lineFromIndex(const QModelIndex& index) const;

protected:

    QStringList header_data;
    std::string schema_name;
    const jsonio17::StructDef* struct_def = nullptr;
    std::shared_ptr<SelectSchemaLine> rootNode;

    QString get_description( const QModelIndex& index ) const;

    // Read internal data from json schema
    void struct2model( const jsonio17::StructDef* struct_def, SelectSchemaLine* parent );
    void field2model( const jsonio17::FieldDef* field_def, SelectSchemaLine* parent );
    void list2model( size_t level, const jsonio17::FieldDef* field_def, SelectSchemaLine* parent );
};

/// \class SchemaSelectDialogPrivate class provides a dialog that allow users to select
/// items from thrift schema fields list
class SchemaSelectDialogPrivate
{

#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    Q_DISABLE_COPY_MOVE(SchemaSelectDialogPrivate)
#endif

public:


    /// Constructor
    explicit SchemaSelectDialogPrivate( SchemaSelectDialog *qdialog,
                                        bool selection_type,
                                        const std::string& schema_name,
                                        const std::vector<std::string>& def_fields_list );

    std::vector<std::string> all_selected();


    std::string sel_index( const jsonio17::FieldDef** flddata );


private:

    SchemaSelectDialog * const  base_ptr;
    inline SchemaSelectDialog* base_func() { return static_cast<SchemaSelectDialog *>(base_ptr); }
    inline const SchemaSelectDialog* base_func() const { return static_cast<const SchemaSelectDialog *>(base_ptr); }
    friend class SchemaSelectDialog;

    QScopedPointer<Ui::SchemaSelectDialogData> qUi;

    bool multi_selection;
    std::string schema_name;
    std::vector<std::string> def_fields;

    std::vector<const jsonio17::FieldDef*> arr_types;
    SelectSchemaModel *model_schema = nullptr;

    void setup_window();
    void define_multi_selection();
    void select_with_children( const QModelIndex& parIndex,  const QModelIndexList& selIndexes,
                               std::vector<std::string>& arr, std::vector<const jsonio17::FieldDef*>& field_def_list );
    bool select_field(  const QModelIndex& parIndex, std::queue<std::string> names );

};

} // namespace jsonui17

