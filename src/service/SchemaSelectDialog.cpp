
#include <QPushButton>
#include "ui_SchemaSelectDialog4.h"
#include "jsonui17/preferences.h"
#include "jsonui17/models/schema_model.h"
#include "SchemaSelectDialog_p.h"

namespace jsonui17 {

// Select constructor
SchemaSelectDialog::SchemaSelectDialog( QWidget* parent, const char* title,
                                        const std::string& schemaName,
                                        const std::vector<std::string>& adef_fieldsList ):
    QDialog(parent),
    impl_ptr( new SchemaSelectDialogPrivate( this, true, schemaName, adef_fieldsList ) )
{
    setWindowTitle(title);
}


SchemaSelectDialog::SchemaSelectDialog( QWidget* parent, const char* title,
                                        const std::string& schemaName ):
    QDialog(parent),
    impl_ptr( new SchemaSelectDialogPrivate( this, false, schemaName, {} ) )
{
    setWindowTitle(title);
}


// Destructor
SchemaSelectDialog::~SchemaSelectDialog()
{}

std::vector<std::string> SchemaSelectDialog::allSelected()
{
    if( result() )
    {
        auto d = impl_func();
        return d->all_selected();
    }
    return {};
}

std::string SchemaSelectDialog::selIndex( const jsonio17::FieldDef** flddata )
{
    auto d = impl_func();
    return d->sel_index(flddata);
}

std::vector<const jsonio17::FieldDef *> SchemaSelectDialog::allSelectedTypes()
{
   auto d = impl_func();
   return d->arr_types;
}

void SchemaSelectDialog::CmHelp()
{
    helpWin( "SchemaSelectJsonui", "" );
}


//------------------------------------------------------------------------

SelectSchemaLine::SelectSchemaLine( const jsonio17::FieldDef* afld_def, SelectSchemaLine* theparent ):
     type(0), fld_thrift(afld_def), arr_levels(0), def_value(""), ndx_in_parent(0), parent(theparent)
{
    if( parent )
    {
        ndx_in_parent = parent->children.size();
        parent->children.push_back( std::unique_ptr<SelectSchemaLine>(this) );
    }

    if( fld_thrift )
    {
        fld_value.append(fld_thrift->name().c_str());
        fld_value.append("");
        fld_value.append(fld_thrift->description().c_str());
    }
    else
    {
        fld_value.append("root");
        fld_value.append("");
        fld_value.append("");
    }
}

SelectSchemaLine::~SelectSchemaLine()
{
    children.clear();
}

void SelectSchemaLine::add_value(const char *next)
{
    if( def_value.isEmpty() )
        def_value = next;
    else
        def_value += QString(".")+next;
    arr_levels++;
    fld_value[1] = def_value;
}

void SelectSchemaLine::add_link( std::vector<std::string>& fieldsList, const std::string& path )
{
    if( !parent )
    {
        fieldsList.push_back(path);
        return;
    }

    std::string new_path = fld_value[0].toStdString();
    if( type > 0  && ( !fld_value[1].isEmpty() && children.size() > 0 ) )
    {
        std::string fielndx =  fld_value[1].toStdString();
        std::queue<std::string> names = jsonio17::split( fielndx, ";" );
        std::string curname;
        while(  !names.empty() )
        {
            if( !names.front().empty() )
            {  curname = new_path + "." + names.front();
                if( !path.empty() )
                    curname += "."+ path;
                parent->add_link( fieldsList, curname );
            }
            names.pop();
        }
    }
    else
    {
        if( !path.empty())
            new_path += "."+ path;
        parent->add_link( fieldsList, new_path );
    }
}

//--------------------------------------------------------------------------------------
// class SelectSchemaModel
// class for represents the data set and is responsible for fetching the data
// is needed for viewing and for selecting.
// Selecting data from ThriftSchema object
//---------------------------------------------------------------------------------------

SelectSchemaModel::SelectSchemaModel( const std::string& theschema_name,  const QStringList& header_lst,   QObject* parent ):
    QAbstractItemModel(parent), header_data(header_lst),  schema_name(theschema_name)
{
    if( !schema_name.empty() )
        struct_def =  jsonio17::ioSettings().Schema().getStruct(schema_name);
    jsonio17::JSONIO_THROW_IF(struct_def == nullptr, "querywindow", 11, " undefined struct definition " + schema_name);
    // set up model data
    rootNode = std::make_shared<SelectSchemaLine>( nullptr,  nullptr );
    struct2model(struct_def,  rootNode.get() );

    //connect( this, SIGNAL(dataChanged( const QModelIndex&, const QModelIndex& )),
    //          parent,  SLOT(objectChanged()) );
}

SelectSchemaModel::~SelectSchemaModel()
{ }

SelectSchemaLine *SelectSchemaModel::lineFromIndex(const QModelIndex &index) const
{
    if (index.isValid()) {
        return static_cast<SelectSchemaLine *>(index.internalPointer());
    } else {
        return rootNode.get();
    }
}

QModelIndex SelectSchemaModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!rootNode)
        return QModelIndex();
    SelectSchemaLine *parentItem = lineFromIndex( parent );
    return createIndex(row, column, parentItem->children[row].get());
}

QModelIndex SelectSchemaModel::parent(const QModelIndex& child) const
{
    if (!child.isValid())
        return QModelIndex();

    SelectSchemaLine *childItem = lineFromIndex(child);
    SelectSchemaLine *parentItem = childItem->parent;
    if (parentItem == rootNode.get() )
        return QModelIndex();
    return createIndex( parentItem->ndx_in_parent, 0, parentItem);
}


int SelectSchemaModel::rowCount( const QModelIndex& parent ) const
{
    if (!rootNode)
        return 0;
    if (parent.column() > 0)
        return 0;
    SelectSchemaLine *parentItem = lineFromIndex( parent );
    return parentItem->children.size();
}

int SelectSchemaModel::columnCount( const QModelIndex& /*parent*/ ) const
{
    if( JsonSchemaModel::showComments  )
        return 3;
    return 2;
}

Qt::ItemFlags SelectSchemaModel::flags( const QModelIndex& index ) const
{
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    SelectSchemaLine *item = lineFromIndex( index );
    if( index.column() == 1 && (item->type > 0 ) )
    {
        flags |= Qt::ItemIsEditable;
        return flags;
    }
    else
        return (flags & ~Qt::ItemIsEditable);
}

QVariant SelectSchemaModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
    if( role == Qt::DisplayRole  && orientation == Qt::Horizontal && section<header_data.size())
        return header_data[section];
    return QVariant();
}

QString SelectSchemaModel::get_description( const QModelIndex& index ) const
{
    SelectSchemaLine *item = lineFromIndex( index );
    return item->fld_value[2];
}

QVariant SelectSchemaModel::data( const QModelIndex& index, int role ) const
{
    if(!index.isValid())
        return QVariant();

    switch( role )
    {
    case Qt::DisplayRole:
    case Qt::EditRole:
    {
        SelectSchemaLine *item = lineFromIndex( index );
        if( index.column() < item->fld_value.size()  )
            return item->fld_value[index.column()];
    }
        break;
    case Qt::ToolTipRole:
    case Qt::StatusTipRole:
        return  get_description( index );
    case Qt::ForegroundRole:
    {
        if ( index.column() == 0   )
            return QVariant( QColor( Qt::darkCyan ) );
    }
        break;
    default: break;
    }
    return QVariant();
}

bool SelectSchemaModel::setData( const QModelIndex& index, const QVariant& value, int role)
{
    if( index.isValid() && ( role == Qt::EditRole ) )
    {
        SelectSchemaLine *line =lineFromIndex(index);
        if(  index.column() <  line->fld_value.size() )
        {
            line->fld_value[index.column()] = value.toString();
        }
        return true;
    }
    return false;
}

// Set up struct without bson data
void SelectSchemaModel::struct2model( const jsonio17::StructDef* struct_def, SelectSchemaLine* parent )
{
    for( auto itfld = struct_def->cbegin(); itfld<struct_def->cend(); itfld++ )
    {
        field2model( itfld->get(), parent );
    }
}

void SelectSchemaModel::field2model( const jsonio17::FieldDef* field_def, SelectSchemaLine* parent )
{
    // add line to tree view
    SelectSchemaLine *line = new SelectSchemaLine( field_def, parent );
    // add levels for objects and arrays
    list2model( 0, field_def, line );
}

// Read array
void SelectSchemaModel::list2model( size_t level, const jsonio17::FieldDef* field_def, SelectSchemaLine* line )
{
    // add levels for objects and arrays
    switch( field_def->type(level) )
    {
    // main constructions
    case jsonio17::FieldDef::T_STRUCT:
    {
        // structure
        if( field_def->className().empty() )
            jsonio17::JSONIO_THROW( "querywindow", 12, " undefined struct name into schema " + field_def->name() );
        auto struct_def2 =  jsonio17::ioSettings().Schema().getStruct( field_def->className() );
        if( struct_def2 == nullptr )
            jsonio17::JSONIO_THROW( "querywindow", 14, " undefined struct definition " + field_def->className() );
        struct2model( struct_def2,  line );
    }
        break;
    case jsonio17::FieldDef::T_MAP:
    {  // map
        line->type = 2;
        line->add_value("key");
        list2model( level+2, field_def, line );
    }
        break;
    case jsonio17::FieldDef::T_LIST:
    case jsonio17::FieldDef::T_SET:
    {   // !! next level
        if( line->type == 0 )
            line->type = 1;
        line->add_value("0");
        list2model( level+1, field_def, line );
    }
        break;
    default:     break;
    }
}

//====================================================================

SchemaSelectDialogPrivate::SchemaSelectDialogPrivate( SchemaSelectDialog *qdialog,
                                                      bool selection_type,
                                                      const std::string &schema_name,
                                                      const std::vector<std::string> &def_fields_list):
    base_ptr( qdialog ),
    qUi( new Ui::SchemaSelectDialogData ),
    multi_selection(selection_type), schema_name(schema_name), def_fields(def_fields_list)
{
    setup_window();
    if( multi_selection )
        define_multi_selection();
}

void SchemaSelectDialogPrivate::setup_window()
{
    auto q = base_func();
    qUi->setupUi(q);

    // set up model
    QStringList aHeaderData;
    aHeaderData << "key" << "indexes" << "comment"  ;
    model_schema = new SelectSchemaModel( schema_name, aHeaderData, q );
    qUi->schemaView->setModel(model_schema);
    qUi->schemaView->setSelectionBehavior(QAbstractItemView::SelectRows);
    qUi->schemaView->setEditTriggers( QAbstractItemView::AnyKeyPressed );
    qUi->schemaView->header()->resizeSection(0, 250);
    qUi->schemaView->expandToDepth(0);

    if( multi_selection )
    {
        QPushButton * btn = qUi->buttonBox->addButton(QDialogButtonBox::Reset);
        QObject::connect( btn, &QPushButton::clicked, qUi->schemaView, &QTreeView::clearSelection);
        btn = qUi->buttonBox->addButton(QDialogButtonBox::RestoreDefaults);
        QObject::connect( btn, &QPushButton::clicked, [&]() {
            qUi->schemaView->clearSelection();
            define_multi_selection();
        } );
    }
    else
    {
        qUi->schemaView->setSelectionMode(QAbstractItemView::SingleSelection);
    }

    qUi->schemaView->setFocus();
    QObject::connect( qUi->buttonBox, &QDialogButtonBox::helpRequested, q, &SchemaSelectDialog::CmHelp);
}

// Select field by fieldpath
bool SchemaSelectDialogPrivate::select_field( const QModelIndex& parIndex, std::queue<std::string> names )
{
    SelectSchemaLine* item =  model_schema->lineFromIndex(parIndex);
    for( int ii=0; ii<item->levels(); ii++ )  //skip array and map names
        if( !names.empty() )
            names.pop();

    // select current line
    if( names.empty() )
    {
        qUi->schemaView->selectionModel()->select( parIndex,
                QItemSelectionModel::Rows|QItemSelectionModel::Select );
        return true;
    }

    // test children
    std::string fname = names.front();
    names.pop();

    QModelIndex item_index;
    for(int rw = 0; rw < model_schema->rowCount( parIndex ); rw++ )
    {
        item_index = model_schema->index( rw, 0, parIndex);
        std::string fldname = model_schema->data( item_index, Qt::DisplayRole ).toString().toStdString();
        if( fldname == fname )
            return select_field( item_index, names );
    }

    return false;  // not found
}


void SchemaSelectDialogPrivate::define_multi_selection()
{
    qUi->schemaView->setSelectionMode(QAbstractItemView::MultiSelection);
    SelectSchemaModel* themodel =dynamic_cast<SelectSchemaModel*>( qUi->schemaView->model());
    if( !themodel )
        return;

    for( size_t jj=0; jj< def_fields.size(); jj++ )
    {
        std::queue<std::string> names = jsonio17::split(def_fields[jj], ".");
        select_field( qUi->schemaView->rootIndex(), names );
    }
}

void SchemaSelectDialogPrivate::select_with_children( const QModelIndex& parIndex, const QModelIndexList& selIndexes,
                                               std::vector<std::string>& arr, std::vector<const jsonio17::FieldDef*>& field_def_list )
{
    QModelIndex index;
    for(int rw = 0; rw < model_schema->rowCount( parIndex ); rw++ )
    {
        index = model_schema->index( rw, 0, parIndex);
        if( selIndexes.contains( index ) )
        {
            SelectSchemaLine* item =  model_schema->lineFromIndex(index);
            item->add_link(arr);
            field_def_list.push_back( item->fld_desc() );
        }
        select_with_children( index, selIndexes, arr, field_def_list );
    }
}

std::vector<std::string> SchemaSelectDialogPrivate::all_selected()
{
    std::vector<std::string> arr;
    QModelIndexList selection = qUi->schemaView->selectionModel()->selectedRows();
    // Multiple rows can be selected (order of  structure)
    select_with_children(  qUi->schemaView->rootIndex(), selection, arr, arr_types );
    return arr;
}

std::string SchemaSelectDialogPrivate::sel_index(const jsonio17::FieldDef **flddata)
{
    QModelIndex index = qUi->schemaView->currentIndex();

    if (index.isValid())
    {
        const SelectSchemaModel* themodel =
                dynamic_cast<const SelectSchemaModel*>( index.model());
        std::vector<std::string> selected;
        if( themodel )
        {
            SelectSchemaLine* item =  themodel->lineFromIndex(index);
            item->add_link( selected );
            if( flddata )
                *flddata = item->fld_desc();
            return selected[0];
        }
    }
    return "";
}


} // namespace jsonui17


