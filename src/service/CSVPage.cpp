#include "CSVPage_p.h"
#include "jsonio17/jsondump.h"
#include "jsonio17/txt2file.h"

namespace jsonui17 {

//-------CSVPage-----------------------------------------------------------------------------

CSVPage::CSVPage( std::shared_ptr<AbstractContainer> datacontainer, int mode, QWidget *parent):
    QWidget(parent),
    impl_ptr( new CSVPagePrivate( this, datacontainer, mode ) )
{ }

CSVPage::~CSVPage()
{ }

void CSVPage::saveSelectedToCSV(const std::string &file_name) const
{
    auto d = impl_func();
    d->save_selected_toCSV(file_name);
}

void CSVPage::saveAllToCSV(const std::string &file_name) const
{
    auto d = impl_func();
    d->save_all_toCSV(file_name);
}

void CSVPage::updateTable()
{
    auto d = impl_func();
    d->update_table();
    emit updateGraphWindow();
}

void CSVPage::objectChanged()
{
    auto d = impl_func();
    d->object_changed();
}

void CSVPage::CmCalc()
{
    auto d = impl_func();
    d->calc();
}

void CSVPage::SelectRow()
{
    auto d = impl_func();
    d->select_row();
}

void CSVPage::SelectColumn()
{
    auto d = impl_func();
    d->select_column();
}

void CSVPage::SelectAll()
{
    auto d = impl_func();
    d->select_all();
}

void CSVPage::CutData()
{
    auto d = impl_func();
    d->cut_data();
}

void CSVPage::ClearData()
{
    auto d = impl_func();
    d->clear_data();
}

void CSVPage::CopyData()
{
    auto d = impl_func();
    d->copy_data();
}

void CSVPage::CopyDataHeader()
{
    auto d = impl_func();
    d->copy_data_header();
}

void CSVPage::PasteData()
{
    auto d = impl_func();
    d->paste_data();
}

void CSVPage::PasteTransposedData()
{
    auto d = impl_func();
    d->paste_transposed_data();
}

// Working with graphic charts -------------------------------------

void CSVPage::addGraphic()
{
    auto d = impl_func();
    d->add_graphic();
}

// Save graphic definition to internal cfg file
void CSVPage::saveGraphData( ChartData* grdat )
{
    jsonio17::JsonFile file( std::string("graph_cfg.json"));
    auto jsFree =  jsonio17::JsonFree::object();
    grdat->toJsonNode( jsFree );
    file.save( jsFree );
}

bool CSVPage::canBeToggled( const QModelIndex& index, bool as_abscissa )
{
    auto d = impl_func();
    if( !index.isValid() )
        return false;
    return d->can_be_toggled( index.column(), as_abscissa );
}

void CSVPage::setXYaxis(int ndxX, std::vector<int> ndxY)
{
    auto d = impl_func();
    d->set_axis(ndxX, ndxY);
    emit updateGraphWindow();
}

void CSVPage::toggleX()
{
    auto d = impl_func();
    QModelIndex index = d->current_index();
    if(!canBeToggled( index, true ))
        return;

    d->toggle_X( index.column() );
    emit updateGraphWindow();
}

void CSVPage::toggleY()
{
    auto d = impl_func();
    QModelIndex index = d->current_index();
    if(!canBeToggled( index, false ))
        return;

    d->toggle_Y( index.column() );
    emit updateGraphWindow();
}

//-----CSVPagePrivate-------------------------------------------------

void CSVPagePrivate::toggle_X(int ncolmn)
{
    auto it = std::find( xColumns.begin(), xColumns.end(), ncolmn );
    if ( it != xColumns.end() )
        xColumns.erase( it );
    else
        xColumns.push_back( ncolmn );

    CSVContainer* csvdata = dynamic_cast<CSVContainer*>(data_CSV.get());
    if( csvdata)
        csvdata->setXColumns(xColumns);

    chart_models[0]->setXColumns( xColumns );
}

void CSVPagePrivate::toggle_Y( int ncolmn )
{
    auto it = std::find( yColumns.begin(), yColumns.end(), ncolmn );
    if ( it != yColumns.end() )
        yColumns.erase( it );
    else
        yColumns.push_back( ncolmn );

    CSVContainer* csvdata = dynamic_cast<CSVContainer*>(data_CSV.get());
    if( csvdata)
        csvdata->setYColumns(yColumns);

    chart_models[0]->setYColumns( yColumns, true );
}


bool CSVPagePrivate::can_be_toggled( int section, bool as_abscissa ) const
{
    if( section<0 || section>=data_CSV->columnCount() )
        return false;

    // not number
    int dtype = data_CSV->getType( 0, section );
    if( dtype != AbstractContainer::ftNumeric && dtype != AbstractContainer::ftDouble  )
        return false;

    if( as_abscissa ) // selected as Y
    {
        auto it = std::find( yColumns.begin(), yColumns.end(), section );
        if ( it != yColumns.end() )
            return false;
    }
    else // selected as X
    {
        auto it = std::find( xColumns.begin(), xColumns.end(), section );
        if ( it != xColumns.end() )
            return false;
    }
    return true;
}

void CSVPagePrivate::set_axis(int ndxX, std::vector<int> ndxY)
{
    xColumns.clear();
    if( can_be_toggled( ndxX, true ) )
        xColumns.push_back( ndxX );

    yColumns.clear();
    for( auto ncolmn: ndxY )
        if( can_be_toggled( ncolmn, false ) )
            yColumns.push_back( ncolmn );

    CSVContainer* csvdata = dynamic_cast<CSVContainer*>(data_CSV.get());
    if( csvdata)
    {
        csvdata->setXColumns(xColumns);
        csvdata->setYColumns(yColumns);
    }

    chart_models[0]->setXColumns( xColumns );
    chart_models[0]->setYColumns( yColumns, true );
}

void CSVPagePrivate::add_graphic()
{
    auto q = base_func();
    chart_models.push_back( std::shared_ptr<ChartDataModel>( new ChartDataModel( model_CSV, q )) );
    chart_data = std::make_shared<ChartData>( chart_models, "Graph for window", "x", "y" );

    // !!! Must be read internal cfg
    jsonio17::JsonFile file(  std::string("graph_cfg.json"));
    if( file.exist() )
    {
        auto jsFree =  jsonio17::JsonFree::object();
        file.load( jsFree );
        chart_data->fromJsonNode( jsFree );
        xColumns = chart_models[0]->XColumns();
        yColumns = chart_models[0]->YColumns();
        CSVContainer* csvdata = dynamic_cast<CSVContainer*>(chart_data.get());
        if( csvdata)
        {
            csvdata->setXColumns(xColumns);
            csvdata->setYColumns(yColumns);
        }
    }
}


} // namespace jsonui17
