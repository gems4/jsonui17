#pragma once

#include <QDialog>
#include <QAbstractItemModel>
#include <QItemDelegate>
#include "jsonio17/jsonbase.h"

namespace Ui {
class ModelLineDialog;
}

namespace jsonio17 {
class FieldDef;
}

namespace jsonui17 {

class JsonBaseModel;

///  \class SizeModel represents the data sizes and is responsible for fetchin
///  the data is neaded for viewing and for writing back any changes.
class SizeModel: public QAbstractTableModel
{
    Q_OBJECT

public:

    SizeModel( std::vector<std::size_t>&  sizes,   QObject* parent = nullptr ):
        QAbstractTableModel(parent), sizes_data(sizes)
    {}
    ~SizeModel() {}

    int rowCount ( const QModelIndex&  ) const
    { return static_cast<int>(sizes_data.size());  }
    int columnCount ( const QModelIndex&   ) const
    { return 1;  }
    QVariant data ( const QModelIndex& index, int role ) const;
    bool setData ( const QModelIndex& index, const QVariant& value, int role );
    Qt::ItemFlags flags( const QModelIndex& ) const
    {
        return   Qt::ItemIsEditable | Qt::ItemIsEnabled ;
    }
    void resetData();

protected:

    std::vector<std::size_t>&  sizes_data;

};

/// \class SizeDelegate individual items in views are rendered and edited using delegates
class SizeDelegate: public QItemDelegate
{
    Q_OBJECT

public:

    SizeDelegate( QObject * parent = nullptr ):
        QItemDelegate( parent )
    {}
    QWidget *createEditor( QWidget *parent,
                           const QStyleOptionViewItem &option,
                           const QModelIndex &index) const;
};


/// \class ModelLineDialog provides a dialog that allow users to difine json object
class ModelLineDialog : public QDialog
{
    Q_OBJECT

protected slots:

    void setDatabyName( const QString& text );
    void setDatabyType( int ndx );

public:

    explicit ModelLineDialog( const JsonBaseModel* model,
                              const QModelIndex& index, const QModelIndex& parentIndex,
                              const std::string& field_name,  QWidget *parentWidget = nullptr);
    ~ModelLineDialog();

    /// Get object name
    std::string objectName() const;

    /// Get object type
    jsonio17::JsonBase::Type objectType() const;

    /// Get default value of object
    QVariant objectValue() const;

    /// Get object size (if object is array type)
    const std::vector<std::size_t>& arraySizes() const
    {  return array_sizes;   }

private:

    Ui::ModelLineDialog *ui;
    SizeModel* size_model = nullptr;

    /// Field model
    const JsonBaseModel* field_model;
    /// Field schema name
    std::string field_top_schema_name;
    /// Run dialog to resize array mode
    bool resize_array_mode;

    std::vector<std::size_t>  array_sizes;

    void set_editor_by_type( jsonio17::JsonBase::Type jsontype );
    void set_free_json_model( const std::string& field_name, const QModelIndex& field_index );
    void set_schema_json_model( const std::string& field_name, const QModelIndex& field_index, const QModelIndex& parentIndex );

    void reset_schema_size_model( size_t level, const  jsonio17::FieldDef* field_def, const jsonio17::JsonBase* object );
};

} // namespace jsonui17

