#include <QLabel>
#include <QMessageBox>
#include <QFileInfo>
#include "ui_HelpMainWindow.h"
#include "HelpMainWindow_p.h"
#include "helpgenerator.h"
#include "helpdata.h"
#include "helpBrowser.h"
#include "HelpEditDialog.h"
#include "jsonio17/jsonfree.h"
#include "jsonui17/SelectDialog.h"
#include "jsonui17/preferences.h"


namespace jsonui17 {

extern const char *_GEMIPM_version_stamp;

bool HelpMainWindow::editHelp = false;
std::string HelpMainWindow::help_collection_name = "docpages";

void helpWin( const std::string& name, const std::string& item )
{
    if( HelpMainWindow::pDia )
    {
        HelpMainWindow::pDia->showHelp( name, item );
        HelpMainWindow::pDia->show();
        HelpMainWindow::pDia->raise();
    }
}


//-------------------------------------------------------

HelpMainWindow* HelpMainWindow::pDia = nullptr;


HelpMainWindow::HelpMainWindow(  QWidget* parent):
    QMainWindow( parent ), impl_ptr( new HelpMainWindowPrivate( this ))
{
    std::string titl = " Help Viewer ";
    setWindowTitle( titl.c_str() );
    pDia = this;
    impl_func()->init();
}

HelpMainWindow::~HelpMainWindow()
{ }

void HelpMainWindow::showHelp(const std::string& name, const std::string& item )
{
    auto d = impl_func();
    std::string keywd =  help_collection_name+"/"+name+";md";// name+":md:0:";
    ui_logger->debug(" show help  '{}' '{}' keywd {}", name, item, keywd);
    d->anchorItem = item.c_str();
    current_key_change( keywd );
}

// must be key: "docpages/<name>;md"
void HelpMainWindow::showDocument( const std::string& keywd )
{
    auto d = impl_func();
    ui_logger->debug(" show help {}", keywd);
    d->anchorItem = "";
    current_key_change( keywd );
}

void HelpMainWindow::resetDBClient()
{
    auto d = impl_func();
    d->history.clear();
    emit updateDBClient();
    d->other_thread_dbhelp.reset();
}

// Slots ----------------------------------------------


void HelpMainWindow::helpVersion()
{
    QMessageBox::information(this,
                         #ifdef __unix
                         #ifdef __APPLE__
                             tr("Title"), ("GEMS4.2 (MacOS X >10.6 64 clang)\n\n")+
                         #else
                             "GEMS4.2 (Linux 32/64 gcc4.7 Qt5)",
                         #endif
                         #else
                             "GEMS4.2 (Windows 7 MinGW 32 gcc4.7",
                         #endif
                             tr("\nThis is GEM-Selektor code package\n\n")+
                             //   _GEMS_version_stamp  +  "\n\nusing " +
                             //   _GEMIPM_version_stamp  +
                              "\n\n\nFor GEMS R&D community\n\n"
                                     "(c) 2013, GEMS Development Team\n\n"
                                     "          PSI-UH-ETHZ"  );
}

void HelpMainWindow::helpAbout()
{
    showHelp( "helpAbout", "" );
}

void HelpMainWindow::helpOnHelp()
{
    showHelp( "helpOnHelp", "" );
}


void HelpMainWindow::CmEdit()
{
    auto d = impl_func();
    if( !d->help_ready_to_edit() )
        return;

    try {
        auto old_text = d->md_text;
        HelpEditDialog dlg( d->other_thread_dbhelp.get(), d->md_text, this);
        QObject::connect( &dlg, &HelpEditDialog::updateMarkdown, this, &HelpMainWindow::updateDocPage);

        if( dlg.exec() )
        {
            d->qUi->actionCreate_Update_Docpage_into_DB->setEnabled(true);
        }
        else
        {
            emit updateDocPage( old_text );
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }

}

void HelpMainWindow::CmRead()
{
    auto d = impl_func();
    if( !d->help_ready_to_edit() )
        return;

    try{
        std::vector<std::string> aKeyList;
        std::vector<jsonio17::values_t> aValList;
        d->other_thread_dbhelp->currentQueryResult().getKeysValues( aKeyList, aValList, { "type" }, { "0" } );
        if( aKeyList.empty() )
            return;

        SelectDialog selDlg( false, this, "Please, select a record to edit",
                             aValList, {}, MatrixTable::tbNoMenu|MatrixTable::tbSort  );
        if( !selDlg.exec() )
            return;
        size_t selNdx =  selDlg.getSelectedIndex();
        showDocument( aKeyList[selNdx].c_str() );
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch( std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void HelpMainWindow::CmUpdate()
{
    auto d = impl_func();
    if( !d->help_ready_to_edit() )
        return;

    try {
        DocPagesData data;
        data.setMarkdownRecord( d->adressLine->text().toStdString(), d->md_text.toStdString() );
        auto object = jsonio17::JsonFree::object();
        data.toJsonNode( object );
        std::string jsondocpage = object.toString(true);
        d->other_thread_dbhelp->updateFromJson( jsondocpage, true );
        d->qUi->actionCreate_Update_Docpage_into_DB->setEnabled(false);
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch( std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void HelpMainWindow::CmDeleteList()
{
    auto d = impl_func();
    if( !d->help_ready_to_edit() )
        return;

    try {
       auto keys = SelectKeysFrom( d->other_thread_dbhelp.get(), this, "Please, select records to be deleted" );
       if( !keys.empty() )
       {
           for( const auto& key: keys)
               d->other_thread_dbhelp->deleteDocument(key);
           // other_thread_dbhelp->removeByKeys( keys );
           emit updateDBClient(); // to update images
       }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void HelpMainWindow::htmlResultReady(const QString &html)
{
    auto d = impl_func();
    ui_logger->trace(" html\n {}", html.toStdString());
    if( d && d->helpView )
        d->helpView->setHtmlData( html );
}

void HelpMainWindow::tocResultReady(const QString &toc)
{
    auto d = impl_func();
    ui_logger->trace(" toc\n {}", toc.toStdString());
    if( d->tocView )
        d->tocView->setHtml(toc);
}

void HelpMainWindow::generate_Html( const std::string& key )
{
    auto d = impl_func();

    // generate HTML from record
    emit updateHelpRecord( QString(key.c_str()), d->anchorItem );
#ifndef USE_MARKDOWN
    //qUi->helpView->setUrl(QUrl("qrc:/scripts/index.html"));
    //pageHelp->toHtml([this](const QString& result) mutable {emit html(result);});
#endif

    if( HelpMainWindow::editHelp )
        d->qUi->actionCreate_Update_Docpage_into_DB->setEnabled(false);
}

void HelpMainWindow::current_key_change( const std::string& key )
{
    auto d = impl_func();

    if( HelpMainWindow::editHelp &&  d->qUi->actionCreate_Update_Docpage_into_DB->isEnabled())
    {
        QString msg = QString("Confirm saving changes to %1 document?").arg( d->adressLine->text() );
        auto reply = QMessageBox::question( this, "Save docpage", msg,
                                            QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes)
            CmUpdate();
    }

    generate_Html( key );
    add_url_link( History::DBRecord, key, d->anchorItem.toStdString() );
}

void HelpMainWindow::add_url_link( int atype, const std::string& alink, const std::string& anchor )
{
    auto d = impl_func();
    d->history.addLink( atype, alink, anchor );
    d->show_address( alink.c_str() );
    emit backwardAvailable( d->history.canGoBack());
    emit forwardAvailable( d->history.canGoForward() );
}


//------------------------------------------------

HelpMainWindowPrivate::HelpMainWindowPrivate( HelpMainWindow *qdialog ):
    base_ptr( qdialog ),
    qUi( new Ui::HelpWindowData ),
    other_thread_dbhelp(nullptr)
{
    auto q = base_func();
    qUi->setupUi(q);
    qUi->indexWidget->hide();

    //  qUi->splitter->setStretchFactor(0, 1);
    qUi->splitter->setStretchFactor(1, 500);

    helpView.reset( new HelpBrowser(qUi->helpWidget) );
    helpView->setObjectName("helpView");
    qUi->verticalLayout_2->addWidget( helpView.get() );

#ifdef USE_MARKDOWN

    tocView.reset( new QTextBrowser(qUi->tocWidget)) ;
    tocView->setObjectName("tocView");

    qUi->verticalLayout->addWidget(tocView.get());
    QObject::connect( tocView.get(), &QTextBrowser::anchorClicked, [&](const QUrl& url) { toc_link_clicked( url ); } );
    qUi->splitter->setStretchFactor(2, 1);

#endif

    QObject::connect( helpView.get(), &HelpBrowser::newLink, [&, q](const QUrl& url) { q->current_key_change( key_from_url(url.url()) ); });
    QObject::connect( helpView.get(), &HelpBrowser::loadFinished, [&](bool ok) { scroll_to_anchor(ok); });

}


HelpMainWindowPrivate::~HelpMainWindowPrivate()
{
    generatorThread.quit();
    generatorThread.wait();
}

void HelpMainWindowPrivate::init()
{
    set_actions();
    setup_Html_generator();
}

void HelpMainWindowPrivate::set_actions()
{
    auto q = base_func();

    QObject::connect( qUi->actionHome, &QAction::triggered, [&]() { action_home(); } );
    QObject::connect( qUi->actionBack, &QAction::triggered, [&]() { action_back(); } );
    QObject::connect( qUi->actionForward, &QAction::triggered, [&]() { action_forward(); } );
    QObject::connect( q, &HelpMainWindow::forwardAvailable, qUi->actionForward, &QAction::setEnabled);
    QObject::connect( q, &HelpMainWindow::backwardAvailable, qUi->actionBack, &QAction::setEnabled);
    qUi->actionForward->setEnabled(false);
    qUi->actionBack->setEnabled(false);

    QObject::connect( qUi->action_Find, &QAction::triggered, [&]() { action_find_next(); } );
    QObject::connect( qUi->actionFind_Next, &QAction::triggered, [&]() { action_find_next(); } );
    QObject::connect( qUi->actionFind_Previous, &QAction::triggered, [&]() { action_find_previous(); } );

    QObject::connect( qUi->actionZoom_In, &QAction::triggered, [&]() { action_zoomin(); } );
    QObject::connect( qUi->actionZoom_Out, &QAction::triggered, [&]() { action_zoomout(); } );

    QObject::connect( qUi->action_About, &QAction::triggered, q, &HelpMainWindow::helpAbout);
    QObject::connect( qUi->actionVersion, &QAction::triggered, q, &HelpMainWindow::helpVersion);
    QObject::connect( qUi->actionHelp_on_Help, &QAction::triggered, q, &HelpMainWindow::helpOnHelp);
    QObject::connect( qUi->action_Print, &QAction::triggered, helpView.get(), &HelpBrowser::helpPrint);

    // Database
    if( HelpMainWindow::editHelp )
    {
        QObject::connect( qUi->action_Read_another_Docpage_from_DB, &QAction::triggered, q, &HelpMainWindow::CmRead);
        QObject::connect( qUi->action_Edit_DocPage,&QAction::triggered, q, &HelpMainWindow::CmEdit);
        QObject::connect( qUi->actionCreate_Update_Docpage_into_DB, &QAction::triggered, q, &HelpMainWindow::CmUpdate);
        QObject::connect( qUi->action_Delete_Docpages_from_DB, &QAction::triggered, q, &HelpMainWindow::CmDeleteList);
    }
    else
    {
       qUi->menu_Database->menuAction()->setVisible(false);
    }

    QLabel *label_2 = new QLabel(qUi->toolFind);
    label_2->setText("Find:");
    qUi->toolFind->addWidget( label_2 );

    findLine.reset( new QLineEdit( qUi->toolFind ));
    findLine->setEnabled( true );
    findLine->setFocusPolicy( Qt::ClickFocus );
    qUi->toolFind->addWidget( findLine.get() );
    qUi->toolFind->addAction(qUi->actionFind_Previous);
    qUi->toolFind->addAction(qUi->actionFind_Next);

    if( HelpMainWindow::editHelp )
    {
        QLabel *label = new QLabel(qUi->toolAddress);
        label->setText("Address:");
        qUi->toolAddress->addWidget( label );

        adressLine.reset( new QLineEdit( qUi->toolAddress ) );
        adressLine->setEnabled( true );
        adressLine->setFocusPolicy( Qt::ClickFocus );
        adressLine->setReadOnly( true );
        adressLine->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        qUi->toolAddress->addWidget( adressLine.get() );
    }

}

void HelpMainWindowPrivate::setup_Html_generator()
{
    auto q = base_func();

    HelpGenerator2 *worker = new HelpGenerator2;
    worker->moveToThread(&generatorThread);
    QObject::connect(&generatorThread, &QThread::finished, worker, &QObject::deleteLater);

    QObject::connect( q, &HelpMainWindow::updateHelpRecord, worker, &HelpGenerator2::setHelpRecord);
    QObject::connect( q, &HelpMainWindow::updateDBClient, worker, &HelpGenerator2::resetDBClient);
    emit q->updateDBClient();

    if( HelpMainWindow::editHelp )
    {
        QObject::connect( worker, &HelpGenerator2::textChanged, [&](const QString &newtext) {md_text = newtext;} );
        QObject::connect( q, &HelpMainWindow::updateDocPage, worker, &HelpGenerator2::setText );
    }

#ifdef USE_MARKDOWN
    QObject::connect(worker, &HelpGenerator2::htmlResultReady, q, &HelpMainWindow::htmlResultReady );
    QObject::connect(worker, &HelpGenerator2::tocResultReady, q, &HelpMainWindow::tocResultReady );
#else

    //QObject::connect(this, SIGNAL(html(QString)), this, SLOT(handleHtml(QString)));
    //QObject::connect( helpView, SIGNAL( htmlLoadFinished() ), [&](bool ok) { scroll_to_anchor(ok); } );
    helpView->setWebChannel(worker);
    q->htmlResultReady( html_template() );
    //qUi->helpView->setUrl(QUrl("qrc:/scripts/index.html"));

#endif
    generatorThread.start();
}

void HelpMainWindowPrivate::scroll_to_anchor( bool ok)
{
    ui_logger->trace("scroll_to_anchor =", anchorItem.toStdString());
    if( !ok || anchorItem.isEmpty() )
        return;
    helpView->scrollToAnchor(anchorItem);
}

void HelpMainWindowPrivate::toc_link_clicked(const QUrl& url)
{
    QString anchor = url.toString();
    anchor = anchor.remove("#");
    anchorItem = anchor;
    scroll_to_anchor(true);
}

void HelpMainWindowPrivate::action_home()
{
    auto q = base_func();
    q->showHelp( "start", "" );
}

void HelpMainWindowPrivate::action_back()
{
    if( history.canGoBack() )
    {
        history.back();
        change_url_link();
    }
}

void HelpMainWindowPrivate::action_forward()
{
    if( history.canGoForward() )
    {
        history.forward();
        change_url_link();
    }
}

void HelpMainWindowPrivate::action_zoomin()
{
    helpView->zoomIn();
    if(tocView )
        tocView->zoomIn();
}

void HelpMainWindowPrivate::action_zoomout()
{
    helpView->zoomOut();
    if(tocView )
        tocView->zoomOut();
}


void HelpMainWindowPrivate::action_find_next()
{
    if( !findLine )
        return;

    helpView->actFind( findLine->text(), qUi->action_Case_sensetiv->isChecked() );
}

void HelpMainWindowPrivate::action_find_previous()
{
    if( !findLine )
        return;

    helpView->actFind( findLine->text(), qUi->action_Case_sensetiv->isChecked(), true );
}

void HelpMainWindowPrivate::show_address( const QString& name )
{
    if( HelpMainWindow::editHelp )
    {
        adressLine->setText( name );
    }
}

bool HelpMainWindowPrivate::help_ready_to_edit()
{
    if( !HelpMainWindow::editHelp )
        return false;

    if( other_thread_dbhelp.get() == nullptr )
    {
        other_thread_dbhelp.reset( newDBHelpClient( jsonio17::DBQueryBase(jsonio17::DBQueryBase::qAll) ));
    }
    return other_thread_dbhelp.get() != nullptr;
}

QString HelpMainWindowPrivate::html_template()
{
    QString htmlTemplate;
    QFile f(":/scripts/index.html");
    if (f.open(QIODevice::ReadOnly | QIODevice::Text)) {
        htmlTemplate = f.readAll();
    }
    return htmlTemplate;
}

std::string HelpMainWindowPrivate::key_from_url( const QString& urllink )
{
    anchorItem = urllink.section('#', 1, 1);
    QString  filePath= urllink.section('#', 0, 0);
    DocPagesData adata;
    adata.setFileInfo( filePath, false );
    return adata.recordKey();
}

void HelpMainWindowPrivate::change_url_link()
{
    auto q = base_func();
    if( tocView )
        tocView->setHtml("");
    switch( history.type() )
    {
    case History::DBRecord:

        anchorItem = history.anchor().c_str();
        q->generate_Html( history.link() );
        break;
    case History::ExternLink:
        helpView->loadHistory( history.link().c_str() );
        break;
    }
    show_address( history.link().c_str() );
    emit q->forwardAvailable( history.canGoForward() );
    emit q->backwardAvailable( history.canGoBack() );
}

//void HelpMainWindow::handleHtml(QString sHtml)
//{
//    std::cout <<"myhtml :"<< sHtml.toStdString() << std::endl;
//}


} // namespace jsonui17


