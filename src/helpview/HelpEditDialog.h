#pragma once

#include <QDialog>
#include "jsonio17/dbschemadoc.h"

namespace Ui {
class HelpEditDialog;
}

namespace jsonui17 {

/// Declaration of HelpEditDialog widget - dialog for editing help pages in markdown format
class HelpEditDialog : public QDialog
{
    Q_OBJECT

signals:

    /// Reset current md text
    void updateMarkdown(  QString new_mdtext );

protected slots:

    void applyChanges();
    void previewChanges();

    void insertImage();
    void addImage();
    void insertDocpage();
    void addDocPage();

    void showContextMenu( const QPoint &pt );

public:

    explicit HelpEditDialog( jsonio17::DBSchemaDocument* database, const QString& mdtext, QWidget *parent = nullptr );
    ~HelpEditDialog();

private:

    Ui::HelpEditDialog *ui;
    jsonio17::DBSchemaDocument *help_client;

    void imageLink( const std::string key );
    void docLink( const std::string key );
    std::string saveFiletoDB( int fileType, const QString &fileName, const QString &filePath );
};

} // namespace jsonui17

