#include <QFileInfo>
#include "helpdata.h"
#include "jsonio17/jsonbase.h"
#include "jsonui17/HelpMainWindow.h"
#include "jsonui17/preferences.h"

namespace jsonui17 {

std::string DocPagesData::fileName() const
{
    std::string fname;
    switch( type )
    {
    case Image : fname += "images/";
        break;
    case Markdown:
    default:     break;
    }
    fname += name+"."+ext;
    return fname;
}

std::string DocPagesData::recordKey() const
{
    return HelpMainWindow::help_collection_name+"/"+name+";"+ext;
}

// must be key: "docpages/<name>;<ext>"
// for all markdown must be md
//(*.markdown *.md *.mdown)
void DocPagesData::setRecordKey( const std::string& akey )
{
    auto pos_ext = akey.find_last_of(';');
    auto name_start = akey.find_first_of('/');

    jsonio17::JSONIO_THROW_IF( name_start == std::string::npos, "HelpView", 10, " illegal record key: " + akey  );

    if( pos_ext != std::string::npos )
    {
        name = akey.substr(name_start+1, pos_ext-name_start-1);
        ext = akey.substr(pos_ext+1);
    }
    else
    {
        name = akey.substr( name_start+1 );
        ext = "";
    }

    if( ext == "md" || ext == "markdown" || ext == "mdown" || ext.empty() )
        type = 0;
    else
        type = 1;    // image file

    markdown = "";
    image = "";
    // lastModif =  QDateTime::currentDateTime(););
}

void DocPagesData::setMarkdownRecord( const std::string &akey, const std::string &mdtext )
{
    setRecordKey( akey );
    markdown = mdtext;
}

// set data to bson
void DocPagesData::toJsonNode( jsonio17::JsonBase& object ) const
{
    object.clear();
    object.set_value_via_path( "name", name );
    object.set_value_via_path( "ext", ext );
    object.set_value_via_path( "type", type );
    object.set_value_via_path( "markdown", markdown );
    object.set_value_via_path( "image", image );
}

void DocPagesData::fromJsonNode( const jsonio17::JsonBase& object )
{
    object.get_value_via_path<std::string>( "name", name, "undefined" );
    object.get_value_via_path<std::string>( "ext", ext, "md" );
    object.get_value_via_path<int>( "type", type,  Markdown );
    object.get_value_via_path<std::string>( "markdown", markdown, "" );
    object.get_value_via_path<std::string>( "image", image, "" );
}

void DocPagesData::setFileInfo( const QString& filePath, bool testModif )
{
    QFileInfo finfo(filePath);

    name = finfo.baseName().toStdString(); // baseName
    ext = finfo.suffix().toStdString();
    if( filePath.contains("images/"))
        type = Image;
    else
        type = Markdown;

    markdown = "";
    image = "";
    if( testModif )
        lastModif =  finfo.lastModified();
}

void DocPagesData::readFile( int atype, const QString& filePath )
{
    QFile infile(filePath);
    type = atype;

    switch (type)
    {
    case Image:     // read binary image
        if ( infile.open( QFile::ReadOnly ) )
        {
            QByteArray barr = infile.readAll();
            QByteArray barr_64 = barr.toBase64();
            image = barr_64.data();
        }
        break;
    case Markdown:  // read md text file
    default:
        if( infile.open( QFile::ReadOnly | QFile::Text ) )
        {
            QString content = infile.readAll();
            markdown = content.toStdString();
        }
        break;
    }
    ui_logger->debug("DocPages read file {}.{} {}", name, ext, image.size());
    ui_logger->trace("DocPages read file {}", markdown);
}

void DocPagesData::saveFile( const QString& helpDir )
{
    QString fName = helpDir+"/"+fileName().c_str();
    QFile file(fName);

    switch (type)
    {
    case Image:     // read binary image
        if( !image.empty())
        {
            file.open( QIODevice::WriteOnly );
            QByteArray imgbase64 = image.c_str();
            file.write( QByteArray::fromBase64(imgbase64) );
        }
        break;
    case Markdown:  // read md text file
    default:
        if( !markdown.empty() )
        {
            file.open( QIODevice::WriteOnly | QIODevice::Text );
            file.write( markdown.c_str() );
        }
        break;
    }
}


void DocPagesData::deleteFile( const QString& helpDir )
{
    QString fName = helpDir+"/"+fileName().c_str();
    QFile file(fName);
    file.remove();
}


} // namespace jsonui17
