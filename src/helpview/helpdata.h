#pragma once

#include <string>
#include <QString>
#include <QDateTime>

namespace jsonio17 {
class JsonBase;
}

namespace jsonui17 {


/// Declarations of DocPagesData class description of  Help record
/// ( Markdown or image file )
struct DocPagesData {

    /// Types of Help files
    enum DocFileTypes {
        /// Markdown text file
        Markdown,
        /// Image binary file
        Image
    };

    /* id of this help record or 0 if unknown */
    //std::string _id;

    /// Name of file (used as key field)
    std::string name;
    /// Extension of file (used as key field)
    std::string ext;
    /// Type of content (used as key field)
    int type;
    /// markdown content
    std::string markdown;
    /// image content
    std::string image;

    // work data

    /// Last modified
    QDateTime lastModif;

    std::string fileName() const;

    // must be key: "docpages/<name>;md"
    std::string recordKey() const;
    void setRecordKey( const std::string& akey );
    void setMarkdownRecord( const std::string& akey, const std::string& mdtext );

    void toJsonNode( jsonio17::JsonBase& object ) const;
    void fromJsonNode( const jsonio17::JsonBase& object );

    void setFileInfo( const QString& fileName, bool testModif = true );
    void readFile( int atype, const QString& fileName );
    void saveFile( const QString& helpDir);
    void deleteFile( const QString& helpDir );

};

} // namespace jsonui17


