
#include <QFileDialog>
#include <QFileInfo>
#include "ui_HelpFileDialog.h"
#include "HelpFileDialog.h"
#include "helpdata.h"
#include "jsonui17/FileDialogs.h"

namespace jsonui17 {

HelpFileDialog::HelpFileDialog( int type, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HelpFileDialog)
{
    ui->setupUi(this);

    switch( type )
    {
    case DocPagesData::Markdown:
        setWindowTitle("Insert new Markdown File");
        filters =  tr("Markdown Files (*.markdown *.md *.mdown);;All Files (*)");
        break;
    case DocPagesData::Image:
        setWindowTitle("Select Image File");
        filters =  imageFilters()+";;All Files (*)";
        break;
    }
}

HelpFileDialog::~HelpFileDialog()
{
    delete ui;
}


void HelpFileDialog::SelectFile()
{
    std::string copyfile= ui->file->text().toStdString();
    if(  ChooseFileOpen( this, copyfile, "Select Original File ", filters ) )
        ui->file->setText( copyfile.c_str() );
}

QString HelpFileDialog::linkName()
{
    QString newFile =  ui->name->text();
    if( newFile.isEmpty() )
    {
      QString originalFile =  sourceFile();
      QFileInfo finfo(originalFile);
      newFile = finfo.baseName();
    }
    return newFile;
}

QString HelpFileDialog::sourceFile()
{
    return ui->file->text();
}

} // namespace jsonui17
