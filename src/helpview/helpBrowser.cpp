
#include <QtPrintSupport/QPrintDialog>
#include <QMessageBox>
#include <QDesktopServices>
#include <QUrl>
#include "helpgenerator.h"
#include "helpBrowser.h"
#include "jsonui17/preferences.h"

#ifndef NO_QWEBENGINE
#include <QtWebEngineWidgets>
#else
extern "C" {
#ifdef _MSC_VER
#include <Windows.h>
#endif
#include <mkdio.h>
}
#include <QCoreApplication>
#endif

namespace jsonui17 {

#ifndef NO_QWEBENGINE

void MDDocument::setText(const QString &text)
{
    if (text == m_text)
        return;
    m_text = text;
    emit textChanged(m_text);
}

bool PreviewPage::acceptNavigationRequest( const QUrl &url,
                                           QWebEnginePage::NavigationType /*type*/,
                                           bool /*isMainFrame*/)
{
    if( url.isLocalFile() )
    {
        emit newLink( url );
        return false;
    }
    // Only allow qrc:/scripts/index.html.
    if( url.scheme() == QString("qrc")  || url.scheme() == QString("data"))
        return true;

    QDesktopServices::openUrl(url);
    return false;
}


HelpBrowser::HelpBrowser(QWidget *parent): QWebEngineView(parent)
{
    baseUrl = QUrl::fromLocalFile( qApp->applicationDirPath() + "/" );
    ui_logger->debug("baseUrl {}", baseUrl.toString().toStdString());

    pageHelp = new PreviewPage(parent);
    setPage( pageHelp );
    connect( page(), SIGNAL( newLink(QUrl) ), this, SIGNAL( newLink(QUrl) ) );
}

HelpBrowser::~HelpBrowser()
{
    delete pageHelp;
}

void HelpBrowser::setWebChannel(HelpGenerator2 *worker)
{
    QWebChannel *channel = new QWebChannel(this);
    channel->registerObject(QStringLiteral("content"), &m_content);
    pageHelp->setWebChannel(channel);
    setUrl(QUrl("qrc:/scripts/index.html"));
    QObject::connect(worker, &HelpGenerator2::textChanged, &m_content, &MDDocument::setText);
    connect(worker, &HelpGenerator2::htmlResultReady, this, &HelpBrowser::htmlResultReady);
}


void HelpBrowser::htmlResultReady(const QString& html)
{
    ui_logger->info("htmlResultReady {}", html.toStdString());
    emit loadFinished(true);
}

void HelpBrowser::scrollToAnchor( const QString& anchorItem )
{
    ui_logger->debug("anchorItem = {}", anchorItem.toStdString());
    QString javascript = QString("location.hash = '#%1';").arg(anchorItem);
#ifndef USE_MARKDOWN
    javascript = javascript.toCaseFolded();
#endif
    ui_logger->trace("javascript = {}", javascript.toStdString());
    page()->runJavaScript( javascript );
}


void HelpBrowser::helpPrint()
{
    QPrintDialog dlg(  &printer, parentWidget() );
    dlg.setWindowTitle(tr("Print Help Page"));
    if ( hasSelection() )
        dlg.setOption(QAbstractPrintDialog::PrintSelection);
    if( dlg.exec() ) {
#if (QT_VERSION > QT_VERSION_CHECK(6, 2, 0))
        print( &printer ) ;
#else
        page()->print( &printer, [](const QVariant &result){ qDebug() << result; return result; } ) ;
#endif
    }
}

void HelpBrowser::actFind( const QString& findText, bool caseSensitively, bool findBackward )
{
    QWebEnginePage::FindFlags flg;

    if( findBackward )
        flg |= QWebEnginePage::FindBackward;
    if( caseSensitively )
        flg |= QWebEnginePage::FindCaseSensitively;

    //--if(ui->action_words_beginnings->isChecked() )
    //--    ; flg |=QWebEnginePage::FindAtWordBeginningsOnly;

#if (QT_VERSION > QT_VERSION_CHECK(6, 2, 0))
    page()->findText( findText, flg, [this](const QWebEngineFindTextResult &/*result*/) {
        //qInfo() << result.activeMatch() << "of" << result.numberOfMatches() << "matches";
    });
#else
    page()->findText( findText, flg, [this](bool found) {
        if (!found) QMessageBox::information(this, QString(), QStringLiteral("No occurrences found"));
    });
#endif

}

#else

//----------------------------------------------------------------------


HelpBrowser::HelpBrowser(QWidget *parent): QTextBrowser(parent)
{
    setOpenExternalLinks(true);
    setOpenLinks(false);
    setSearchPaths( { qApp->applicationDirPath()} );
    connect( this, SIGNAL(anchorClicked(QUrl) ), this, SLOT( setSource(QUrl) ) );
    //connect( this, SIGNAL(anchorClicked(const QUrl &link) ), this, SIGNAL( newLink(QUrl) ) );
    //connect( this, SIGNAL( textChanged() ), this, SIGNAL( loadFinished() ) );
}

HelpBrowser::~HelpBrowser()
{

}

void HelpBrowser::setSource(const QUrl &url)
{
    ui_logger->info("setSource {}", url.toString().toStdString());
    if( url.isRelative() )
    {
        emit newLink( url );
        return;
    }
    // Only allow qrc:/scripts/index.html.
    if( url.scheme() == QString("qrc")  || url.scheme() == QString("data"))
        return;

    QDesktopServices::openUrl(url);
}

void HelpBrowser::setWebChannel(HelpGenerator2 *worker)
{
    connect(worker, &HelpGenerator2::textChanged, this, &HelpBrowser::htmlResultReady);
}

void HelpBrowser::htmlResultReady(const QString& markdown )
{
    ui_logger->trace("markdownResultReady {}", markdown.toStdString());
    auto html = convertDocument( markdown );
    ui_logger->trace("htmlResultReady {}", html.toStdString());
    setHtml(html);
    emit loadFinished(true);
}


void HelpBrowser::helpPrint()
{
    QPrinter printer;
    QPrintDialog dlg(  &printer, parentWidget() );
    dlg.setWindowTitle(tr("Print Help Page"));
    if ( textCursor().hasSelection() )
        dlg.setOption(QAbstractPrintDialog::PrintSelection);
    if( dlg.exec() )
        print( &printer ) ;
}

void HelpBrowser::actFind( const QString& findText, bool caseSensitively, bool findBackward )
{
    QTextDocument::FindFlags flg;

    if( findBackward )
        flg |= QTextDocument::FindBackward;
    if( caseSensitively )
        flg |= QTextDocument::FindCaseSensitively;

    //--if(ui->action_words_beginnings->isChecked() )
    //--    ; flg |=QTextDocument::FindWholeWords;

    find( findText, flg);
}



void HelpBrowser::loadHistory(const QString &history)
{
    setSource( QUrl(history) );
}


unsigned int translateConverterOptions()
{
    //#ifndef __unix
    //    unsigned int converterOptions = 0;
    //#else
    unsigned int converterOptions = MKD_TOC | MKD_NOSTYLE;

    //    converterOptions |= MKD_NODLDISCOUNT;

    // autolink
    //if (options.testFlag(MarkdownConverter::AutolinkOption)) {
    converterOptions |= MKD_AUTOLINK;
    //}

    /* strikethrough
    if (options.testFlag(MarkdownConverter::NoStrikethroughOption)) {
        converterOptions |= MKD_NOSTRIKETHROUGH;
    }

    // alphabetic lists
    if (options.testFlag(MarkdownConverter::NoAlphaListOption)) {
        converterOptions |= MKD_NOALPHALIST;
    }

    // definition lists
    if (options.testFlag(MarkdownConverter::NoDefinitionListOption)) {
        converterOptions |= MKD_NODLIST;
    }*/

    // SmartyPants
    //if (options.testFlag(MarkdownConverter::NoSmartypantsOption)) {
    converterOptions |= MKD_NOPANTS;
    //}

    // Footnotes
    //if (options.testFlag(MarkdownConverter::ExtraFootnoteOption)) {
    converterOptions |= MKD_EXTRA_FOOTNOTE;
    //}

    /* Superscript
    if (options.testFlag(MarkdownConverter::NoSuperscriptOption)) {
        converterOptions |= MKD_NOSUPERSCRIPT;
    }*/
    //#endif
    return converterOptions;
}


QString HelpBrowser::convertDocument(const QString &text )
{
    QString html;

    //#ifndef __unix
    //   html = text;   //temporally not convert
    //#else
    MMIOT *doc = nullptr;

    if (text.length() > 0)
    {
        QString markdownText(text);

        // text has to always end with a line break,
        // otherwise characters are missing in HTML
        if (!markdownText.endsWith('\n')) {
            markdownText.append('\n');
        }

        unsigned int converterOptions = translateConverterOptions();

        QByteArray utf8Data = markdownText.toUtf8();
        doc = mkd_string(utf8Data, utf8Data.length(), converterOptions);

        mkd_compile(doc, converterOptions);
        char *out;
        mkd_document(doc, &out);

        html = QString::fromUtf8(out);
        mkd_cleanup(doc);

    }
    //#endif

    return html;
}
#endif

} // namespace jsonui17

