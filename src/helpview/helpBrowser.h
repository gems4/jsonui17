#pragma once

#include <QtPrintSupport/QPrinter>

#ifndef NO_QWEBENGINE
#include <QWebEngineView>
#else
#include <QTextBrowser>
#endif

namespace jsonui17 {

class HelpGenerator2;

#ifndef NO_QWEBENGINE

class MDDocument : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString text MEMBER m_text NOTIFY textChanged FINAL)
public:
    explicit MDDocument(QObject *parent = nullptr) : QObject(parent) {}
public slots:
    void setText(const QString &text);

signals:
    void textChanged(const QString &text);

private:
    QString m_text;

};


class PreviewPage : public QWebEnginePage
{
    Q_OBJECT

signals:
    void newLink(const QUrl& url);

public:
    explicit PreviewPage(QObject *parent = nullptr) : QWebEnginePage(parent) {}

protected:
    bool acceptNavigationRequest(const QUrl &url, NavigationType type, bool isMainFrame);
};


/// Declaration of HelpBrowser class provides an object to view help documents
class HelpBrowser : public QWebEngineView
{

    Q_OBJECT

signals:
    void newLink(const QUrl& url);
    //void htmlLoadFinished();

public slots:

    void htmlResultReady(const QString& html);
    void helpPrint();

public:

    HelpBrowser(QWidget *parent = Q_NULLPTR);
    virtual ~HelpBrowser();

    void setWebChannel(HelpGenerator2 *worker);

    void setHtmlData(const QString &html )
    {
        pageHelp->setHtml( html, baseUrl );
        //setContent(html.toUtf8(), "text/html;charset=UTF-8", baseUrl);
    }

    void scrollToAnchor( const QString& anchorItem );

    void zoomIn()
    {
        setZoomFactor(zoomFactor()*1.1);
    }

    void zoomOut()
    {
        setZoomFactor(zoomFactor()*0.9);
    }

    void actFind( const QString& findText, bool caseSensitively, bool findBackward = false );

    void loadHistory(const QString &history)
    {
        load( QUrl(history) );
    }

protected:

    QUrl baseUrl;
    QPrinter printer;
    PreviewPage *pageHelp = nullptr;
    MDDocument m_content;

};

#else

/// Declaration of HelpBrowser class provides an object to view help documents
class HelpBrowser : public QTextBrowser
{

    Q_OBJECT

signals:
    void newLink(const QUrl& url);
    void loadFinished(bool ok=true);

public slots:

    void setSource(const QUrl &url);
    void htmlResultReady(const QString& html);
    void helpPrint();

public:

    HelpBrowser(QWidget *parent = Q_NULLPTR);
    virtual ~HelpBrowser();

    void setWebChannel(HelpGenerator2 *worker);

    void actFind( const QString& findText, bool caseSensitively, bool findBackward = false );
    void loadHistory(const QString &history);
    void setHtmlData(const QString &html )
    {
        setHtml(html);
        emit loadFinished();
    }


    // from base class
    // void scrollToAnchor( const QString& anchorItem );
    // void zoomIn()
    // void zoomOut()


protected:

    QString convertDocument(const QString &mdtexts);

};

#endif

} // namespace jsonui17

