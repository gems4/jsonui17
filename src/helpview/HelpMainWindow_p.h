#pragma once

#include <vector>
#include <string>
#include <memory>
#include <QTextBrowser>
#include <QLineEdit>
#include <QThread>

// http://doc.qt.io/qt-5/qtwebenginewidgets-qtwebkitportingguide.html
#include "jsonui17/HelpMainWindow.h"
#include "helpBrowser.h"

namespace Ui {
class HelpWindowData;
}

namespace jsonio17 {
class DBSchemaDocument;
}

namespace jsonui17 {

class HelpBrowser;

class History
{
    std::vector<std::string> links;
    std::vector<std::string> anchors;
    std::vector<int> types;
    std::size_t current = 0;

public:

    enum LINK_TIPES { DBRecord, ExternLink, Undefined };

    bool canGoBack() const
    {
        return current > 1;
    }

    bool canGoForward() const
    {
        return current < links.size();
    }

    void back()
    {
        if( canGoBack() )
            current--;
    }

    void forward()
    {
        if( canGoForward() )
            current++;
    }

    int type() const
    {
        if( current > 0 )
            return types[current-1];
        return Undefined;
    }

    std::string link() const
    {
        if( current  > 0 )
            return links[current-1];
        return "";
    }

    std::string anchor() const
    {
        if( current  > 0 )
            return anchors[current-1];
        return "";
    }


    void addLink( int atype, const std::string& alink, const std::string& anchor )
    {
        types.resize(current);
        links.resize(current);
        types.push_back(atype);
        links.push_back(alink);
        anchors.push_back(anchor);
        current++;
    }

    void clear()
    {
        types.clear();
        links.clear();
        current = 0;
    }

};


/// Private Declaration of classes HelpMainWindow - Help system main dialog
class HelpMainWindowPrivate
{

 #if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    Q_DISABLE_COPY_MOVE(HelpMainWindowPrivate)
 #endif

public:

    HelpMainWindowPrivate( HelpMainWindow *qdialog );
    virtual ~HelpMainWindowPrivate();

    void init();
    void scroll_to_anchor(bool ok);
    void toc_link_clicked(const QUrl &url);

    void action_home();
    void action_back();
    void action_forward();

    void action_zoomin();
    void action_zoomout();
    void action_find_next();
    void action_find_previous();

protected:

    HelpMainWindow * const  base_ptr;
    inline HelpMainWindow* base_func() { return static_cast<HelpMainWindow *>(base_ptr); }
    inline const HelpMainWindow* base_func() const { return static_cast<const HelpMainWindow *>(base_ptr); }
    friend class HelpMainWindow;

    /// Menu description
    QScopedPointer<Ui::HelpWindowData> qUi;

    QScopedPointer<HelpBrowser> helpView;
    QScopedPointer<QTextBrowser> tocView;
    QScopedPointer<QLineEdit> adressLine;
    QScopedPointer<QLineEdit> findLine;

    QString anchorItem = "";
    History history;

    QThread generatorThread;

    /// internal help data connection
    std::shared_ptr<jsonio17::DBSchemaDocument> other_thread_dbhelp;
    /// Text written using the Markdown (https://en.wikipedia.org/wiki/Markdown) syntax.
    QString md_text;

    // internal functions
    void set_actions();
    void setup_Html_generator();

    void show_address( const QString& name );
//    void current_key_change( const std::string& key );
//    void generate_Html( const std::string& key );
//    void add_url_link( int atype, const std::string& alink, const std::string& anchor);
    void change_url_link();
    std::string key_from_url( const QString& urllink );
    QString html_template();
    bool help_ready_to_edit();

};

} // namespace jsonui17


