#include <QSettings>
#include <QDir>
#include "helpgenerator.h"
#include "helpdata.h"
#include "jsonui17/preferences.h"
#include "jsonio17/jsondump.h"
#include "jsonui17/HelpMainWindow.h"

#ifdef USE_MARKDOWN

#include <converter/markdownconverter.h>
#include <template/template.h>
#include <converter/markdowndocument.h>
#include <converter/discountmarkdownconverter.h>
#include <converter/revealmarkdownconverter.h>

#endif

namespace jsonui17 {

/// could be changed to read only, but change part for loading images
/// it will be more quickly
jsonio17::DBSchemaDocument* newDBHelpClient()
{
    jsonio17::DBSchemaDocument* newClient =  jsonio17::DBSchemaDocument::newSchemaDocument(
                uiSettings().resources_database(),  "DocPages", HelpMainWindow::help_collection_name );
    return newClient;
}

jsonio17::DBSchemaDocument* newDBHelpClient( const jsonio17::DBQueryBase& query  )
{
    jsonio17::DBSchemaDocument* newClient =  jsonio17::DBSchemaDocument::newSchemaDocumentQuery(
                uiSettings().resources_database(),  "DocPages", HelpMainWindow::help_collection_name,  query );
    return newClient;
}

//-----------------------------------------------------
//  TreadGeneratorPrivate
//-----------------------------------------------------

#ifdef USE_MARKDOWN

class HelpGenerator2Private
{

public:

    enum MarkdownConvert { CDiscountMarkdownConverter, CRevealMarkdownConverter };

    explicit HelpGenerator2Private( ):
        document(nullptr), converter(nullptr), markdownConverter(CDiscountMarkdownConverter)
    {
        setMarkdownConverter(  markdownConverter );
        setCodeHighlightingStyle("default");
     }

    ~HelpGenerator2Private()
    {
        delete document;
        delete converter;
    }

    // main functions
    void setMarkdownConverter( MarkdownConvert aconverter );
    void setCodeHighlightingStyle(const QString &style)
    {
      converter->templateRenderer()->setCodeHighlightingStyle(style);
    }

    QString generateHtmlFromMarkdown()
    {
        if( !document )
            return QString();
        QString html = converter->templateRenderer()->render(converter->renderAsHtml(document), renderOptions());
        return html;
    }

    QString generateTableOfContents()
    {
        if( !document )
            return QString();
        QString toc = converter->renderAsTableOfContents(document);
        //QString styledToc = QString("<html><head>\n<style type=\"text/css\">ul { list-style-type: none; padding: 0; margin-left: 1em; } a { text-decoration: none; }</style>\n</head><body>%1</body></html>").arg(toc);
        QString styledToc = QString("<html><head>\n<style type=\"text/css\">ul li {  text-align: left; list-style-type: none; padding: 0; margin-left: -2em; } a { text-decoration: none; }</style>\n</head><body>%1</body></html>").arg(toc);
        return styledToc;
    }

    void setMarkdownText(const QString &text)
    {
        //if( !text.isEmpty() )
        {
            delete document;
            // set new markdown document
            document = converter->createDocument(text, converterOptions());
        }
    }


    // addition function
    QString exportHtml(const QString &styleSheet, const QString &highlightingScript, const QString &highlightStyle);

private:

    MarkdownDocument *document;
    MarkdownConverter *converter;
    MarkdownConvert markdownConverter =  CDiscountMarkdownConverter;

    MarkdownConverter::ConverterOptions converterOptions() const;
    Template::RenderOptions renderOptions() const;

};


MarkdownConverter::ConverterOptions HelpGenerator2Private::converterOptions() const
{
    MarkdownConverter::ConverterOptions parserOptionFlags(
         MarkdownConverter::TableOfContentsOption | MarkdownConverter::NoStyleOption);

    // autolink
    parserOptionFlags |= MarkdownConverter::AutolinkOption;
    // strikethrough
    //?parserOptionFlags |= MarkdownConverter::NoStrikethroughOption;
    // alphabetic lists
    //?parserOptionFlags |= MarkdownConverter::NoAlphaListOption;
    // definition lists
    //?parserOptionFlags |= MarkdownConverter::NoDefinitionListOption;
     // SmartyPants
    parserOptionFlags |= MarkdownConverter::NoSmartypantsOption;
    // Footnotes
    parserOptionFlags |= MarkdownConverter::ExtraFootnoteOption;
    // Superscript
    //?parserOptionFlags |= MarkdownConverter::NoSuperscriptOption;

    return parserOptionFlags;
}

Template::RenderOptions HelpGenerator2Private::renderOptions() const
{
    Template::RenderOptions renderOptionFlags;

    // math support
    ///renderOptionFlags |= Template::MathSupport;

    // inline math support
    renderOptionFlags |= Template::MathInlineSupport;

    // diagram support
    renderOptionFlags |= Template::DiagramSupport;

    // code highlighting
    renderOptionFlags |= Template::CodeHighlighting;

    return renderOptionFlags;
}

QString HelpGenerator2Private::exportHtml( const QString &styleSheet,
                  const QString &highlightingScript, const QString &highlightStyle )
{
    if (!document) return QString();

    QString header;
    if( !styleSheet.isEmpty() )
        header += QString("\n<style>%1</style>").arg(styleSheet);

    if( !highlightingScript.isEmpty() && !highlightStyle.isEmpty() )
    {
        header += QString("\n<style>%1</style>").arg(highlightStyle);
        header += QString("\n<script>%1</script>").arg(highlightingScript);
        header += "\n<script>hljs.initHighlightingOnLoad();</script>";
    }

    return converter->templateRenderer()->exportAsHtml(header, converter->renderAsHtml(document), renderOptions());
}


void HelpGenerator2Private::setMarkdownConverter(  MarkdownConvert aconverter )
{
    QString style;

    if( markdownConverter == aconverter  && converter )
        return;

    markdownConverter = aconverter;

    if (converter)
    {
        style = converter->templateRenderer()->codeHighlightingStyle();
        delete converter;
    }

    switch (markdownConverter) {
    case  CRevealMarkdownConverter:
        converter = new RevealMarkdownConverter();
        converter->templateRenderer()->setCodeHighlightingStyle(style);
        break;

    case  CDiscountMarkdownConverter:
    //default:
        converter = new DiscountMarkdownConverter();
        converter->templateRenderer()->setCodeHighlightingStyle(style);
        break;
    }
}

#else

class HelpGenerator2Private
{
public:
    explicit HelpGenerator2Private( )  {}
    ~HelpGenerator2Private() {}
};

#endif

//-----------------------------------------------------
//  HelpGenerator2
//-----------------------------------------------------


HelpGenerator2::HelpGenerator2( QObject *parent ):
    QObject(parent), m_dbhelp(nullptr), pdata( new HelpGenerator2Private() )
{
    // move to other thread - must be run after allocator
    // resetDBClient();
}

HelpGenerator2::~HelpGenerator2()
{}

void HelpGenerator2::setHelpRecord( const QString& key, const QString& anchor )
{
    m_key = key;
    m_anchor = anchor;
    QString code = readRecord( key );
    ui_logger->trace("Oldhtml {}", code.toStdString());
    setText( code );
}

void HelpGenerator2::setText(const QString &text)
{

    m_text = text;
    emit textChanged( m_text );

#ifdef USE_MARKDOWN

    pdata->setMarkdownText(m_text);

    const QString html = pdata->generateHtmlFromMarkdown();
    m_htmltext = html;
    ui_logger->trace("Oldhtml {}", m_htmltext.toStdString());
    emit htmlResultReady(html);

    QString toc = pdata->generateTableOfContents();
    emit tocResultReady(toc);

#endif

}

// ---------------------- db ------------

QString HelpGenerator2::readRecord( const QString& s_key )
{
  if( m_dbhelp.get() == nullptr )
      return "";

  if( s_key.isEmpty() )
      return "";
  try
   {
      std::string key = s_key.toStdString();
      if( m_dbhelp->existsDocument( key ) )
      {
          std::string markdown;
          m_dbhelp->readDocument( key );
          m_dbhelp->getValueViaPath<std::string>( "markdown", markdown, "" );
          return QString(markdown.c_str());
      }
   }
  catch(...) {}

  return "";
}

void HelpGenerator2::resetDBClient()
{
    try{
        jsonio17::DBSchemaDocument* newClient =  newDBHelpClient();
        if( newClient == nullptr )
            m_dbhelp.reset();
        else
            m_dbhelp.reset( newClient );
        copyImageRecordsToFileStructure(".");
    }
    catch(std::exception& e)
    {
        ui_logger->error("Error when resetting help DB client: {}", e.what());
        // throw;
    }
}

void HelpGenerator2::saveRecordToFile( const QString& s_key, const QString& helpDir )
{
   if( m_dbhelp.get() == nullptr )
      return;

   std::string key = s_key.toStdString();
   if( m_dbhelp->existsDocument( key ) )
   {
       m_dbhelp->readDocument( key );
       DocPagesData data;
       data.fromJsonNode( m_dbhelp->loaded_data() );
       data.saveFile( helpDir );
   }
}

void HelpGenerator2::copyImageRecordsToFileStructure( const QString& dirPath )
{
    if( m_dbhelp.get() == nullptr )
      return;

    ui_logger->info("Start copyImageRecordsToFileStructure {}", dirPath.toStdString());
    QDir  dir(dirPath);
    dir.mkpath("images");
    clearDir( dirPath+"/images");

    jsonio17::DBQueryBase queryImages("{ \"type\": 1 }", jsonio17::DBQueryBase::qTemplate );

    jsonio17::SetReaded_f setfnc = [dirPath]( const std::string& jsondata )
    {
        auto object =  jsonio17::json::loads( jsondata );
        DocPagesData data;
        data.fromJsonNode( object );
        data.saveFile(dirPath);
    };

    m_dbhelp->selectQuery( queryImages, setfnc );
    ui_logger->info("Finish copyImageRecordsToFileStructure");
}

void HelpGenerator2::clearDir( const QString& dirPath )
{
    QDir dir( dirPath );
    dir.setFilter( QDir::NoDotAndDotDot | QDir::Files );
    foreach( QString dirItem, dir.entryList() )
        dir.remove( dirItem );
}


} // namespace jsonui17
