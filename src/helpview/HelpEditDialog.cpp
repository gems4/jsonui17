
#include <QPushButton>
#include <QMessageBox>
#include <QMenu>

#include "HelpEditDialog.h"
#include "ui_HelpEditDialog.h"
#include "helpdata.h"
#include "HelpFileDialog.h"
#include "jsonio17/jsonfree.h"
#include "jsonui17/SelectDialog.h"

namespace jsonui17 {

HelpEditDialog::HelpEditDialog( jsonio17::DBSchemaDocument *database, const QString &mdtext, QWidget *parent ):
    QDialog(parent), ui(new Ui::HelpEditDialog), help_client(database)
{
    ui->setupUi(this);

    ui->editor->setPlainText(mdtext);

    QObject::connect( ui->previewButton, &QPushButton::clicked, this, &HelpEditDialog::previewChanges);
    QObject::connect( ui->undoButton, &QPushButton::clicked, ui->editor, &QPlainTextEdit::undo);
    QObject::connect( ui->redoButton, &QPushButton::clicked, ui->editor, &QPlainTextEdit::redo);
    QObject::connect( ui->editor, &QPlainTextEdit::undoAvailable, ui->undoButton, &QPushButton::setEnabled);
    QObject::connect( ui->editor, &QPlainTextEdit::redoAvailable, ui->redoButton, &QPushButton::setEnabled);

    QObject::connect( ui->insertImgButton, &QPushButton::clicked, this, &HelpEditDialog::insertImage);
    QObject::connect( ui->addmgButton, &QPushButton::clicked, this, &HelpEditDialog::addImage);
    QObject::connect( ui->insertDocButton, &QPushButton::clicked, this, &HelpEditDialog::insertDocpage);
    QObject::connect( ui->addDocButton, &QPushButton::clicked, this, &HelpEditDialog::addDocPage);

    connect(ui->buttons->button(QDialogButtonBox::Apply), &QPushButton::clicked, this, &HelpEditDialog::applyChanges);
    connect(ui->buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);

    ui->editor->setContextMenuPolicy(Qt::CustomContextMenu);
    connect( ui->editor, &QPlainTextEdit::customContextMenuRequested,
             this, &HelpEditDialog::showContextMenu);
}

HelpEditDialog::~HelpEditDialog()
{
    delete ui;
}

void HelpEditDialog::previewChanges()
{
    emit updateMarkdown( ui->editor->toPlainText() );
}

void HelpEditDialog::applyChanges()
{
    previewChanges();
    accept();
}


void HelpEditDialog::insertImage()
{
    try{
        std::vector<std::string> aKeyList;
        std::vector<jsonio17::values_t> aValList;
        help_client->currentQueryResult().getKeysValues( aKeyList, aValList, { "type" }, { "1" } );
        if( aKeyList.empty() )
            return;

        SelectDialog selDlg( false, this, "Please, select an image record to link",
                             aValList, {}, MatrixTable::tbNoMenu|MatrixTable::tbSort  );
        if( !selDlg.exec() )
            return;
        size_t selNdx =  selDlg.getSelectedIndex();
        imageLink( aKeyList[selNdx] );
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void HelpEditDialog::addImage()
{
    try{
        HelpFileDialog addDlg( DocPagesData::Image, this );

        if( addDlg.exec() )
        {
            QString initFile = addDlg.sourceFile();
            QString nameFile = addDlg.linkName();
            if( initFile.isEmpty() || nameFile.isEmpty() )
                return;

            // save to DB
            std::string newkey = saveFiletoDB( DocPagesData::Image, nameFile, initFile);
            // copy to images
            help_client->readDocument( newkey );
            DocPagesData data;
            data.fromJsonNode( help_client->loaded_data() );
            data.saveFile(".");
            // add link
            imageLink( newkey );
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch( std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void HelpEditDialog::insertDocpage()
{
    try{
        std::vector<std::string> aKeyList;
        std::vector<jsonio17::values_t> aValList;
        help_client->currentQueryResult().getKeysValues( aKeyList, aValList, { "type" }, { "0" } );
        if( aKeyList.empty() )
            return;

        SelectDialog selDlg( false, this, "Please, select a help page to link",
                             aValList, {}, MatrixTable::tbNoMenu|MatrixTable::tbSort  );
        if( !selDlg.exec() )
            return;
        size_t selNdx =  selDlg.getSelectedIndex();
        docLink( aKeyList[selNdx] );
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void HelpEditDialog::addDocPage()
{
    try{
        HelpFileDialog addDlg( DocPagesData::Markdown, this );

        if( addDlg.exec() )
        {
            QString initFile = addDlg.sourceFile();
            QString nameFile = addDlg.linkName();
            if( nameFile.isEmpty() )
                return;

            // save to DB
            std::string newkey = saveFiletoDB( DocPagesData::Markdown, nameFile, initFile);
            // add link
            docLink( newkey );
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch( std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }}


void HelpEditDialog::imageLink( const std::string key )
{
    DocPagesData  data;
    data.setRecordKey( key );
    QString link = QString(" ![%1](images/%1.%2) ").arg(data.name.c_str(),data.ext.c_str());
    ui->editor->insertPlainText(link);
}


void HelpEditDialog::docLink( const std::string key )
{
    DocPagesData  data;
    data.setRecordKey( key );
    QString link = QString(" [%1](%1.%2) ").arg(data.name.c_str(),data.ext.c_str());
    ui->editor->insertPlainText(link);
}

std::string HelpEditDialog::saveFiletoDB( int fileType, const QString& fileName, const QString& filePath )
{
    DocPagesData data;
    if( !filePath.isEmpty() )
    {
        data.setFileInfo(filePath);
        data.readFile( fileType, filePath );
    }
    else
    {
        data.ext = "md";
        data.type = DocPagesData::Markdown;
    }
    data.name = fileName.toStdString();

    // save changes
    auto object = jsonio17::JsonFree::object();
    data.toJsonNode( object );
    std::string jsondocpage = object.toString(true);
    auto keyhandler = help_client->createFromJson( jsondocpage, true );
    return keyhandler;
}

void HelpEditDialog::showContextMenu(const QPoint &pt)
{
    QMenu *menu = ui->editor->createStandardContextMenu();
    menu->addSeparator();

    auto act =  new QAction(tr("&Insert Image"), this);
    act->setShortcut(tr("Ctrl+I"));
    act->setStatusTip(tr("Insert existing image link to document."));
    connect(act, &QAction::triggered, this, &HelpEditDialog::insertImage);
    menu->addAction(act);

    act =  new QAction(tr("&Add Image"), this);
    act->setShortcut(tr("Ctrl+A"));
    act->setStatusTip(tr("Add new image to database and insert link to document."));
    connect(act, &QAction::triggered, this, &HelpEditDialog::addImage);
    menu->addAction(act);

    act =  new QAction(tr("Insert &Docpage"), this);
    act->setShortcut(tr("Ctrl+D"));
    act->setStatusTip(tr("Insert existing docpage link to document."));
    connect(act, &QAction::triggered, this, &HelpEditDialog::insertDocpage);
    menu->addAction(act);

    act =  new QAction(tr("Add Docpage"), this);
    //act->setShortcut(tr("Ctrl+I"));
    act->setStatusTip(tr("Add new docpage to DB and insert link to document."));
    connect(act, &QAction::triggered, this, &HelpEditDialog::addDocPage);
    menu->addAction(act);

    menu->exec(ui->editor->mapToGlobal(pt));
    delete menu;
}

} // namespace jsonui17
