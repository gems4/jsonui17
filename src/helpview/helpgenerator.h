#pragma once

#include <QObject>
#include <QString>
#include <QtCore/QMutex>
#include <QtCore/QThread>
#include <QtCore/QQueue>
#include <QtCore/QWaitCondition>

#include "jsonio17/dbschemadoc.h"

// patch -p 1 < cutemarked.patch

namespace jsonui17 {

/// Allocate Help DB Schema
jsonio17::DBSchemaDocument* newDBHelpClient();
jsonio17::DBSchemaDocument* newDBHelpClient( const jsonio17::DBQueryBase& query );

class HelpGenerator2Private;

/// \class HelpGenerator is used for extract data from Help database and
/// generate Help HTML from internal Markdown format
class HelpGenerator2: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString text MEMBER m_text NOTIFY textChanged FINAL)
    //Q_PROPERTY(QString htmltext MEMBER m_htmltext NOTIFY htmlResultReady FINAL)
    //Q_PROPERTY(QString anchor MEMBER m_anchor FINAL)

signals:
    void textChanged(const QString &text);
    void htmlResultReady(const QString& html);
    void tocResultReady(const QString& toc);

public slots:

    /// Set markdown text from record
    void setHelpRecord( const QString& key, const QString& anchor );

    /// Reset/change Database
    void resetDBClient();

    /// Set markdown text
    void setText(const QString &text);

public:

    explicit HelpGenerator2(QObject *parent = nullptr);
    virtual ~HelpGenerator2();

    /// Set html text
    //void setHtmlText(const QString &htmltext);

private:

    /// internal help data connection
    std::shared_ptr<jsonio17::DBSchemaDocument> m_dbhelp;
    /// Path to help images data
    QString m_docPagesImagePath = "/images";
    /// Key of readed record
    QString m_key;
    /// Text written using the Markdown (https://en.wikipedia.org/wiki/Markdown) syntax.
    QString m_text;
    /// Text written using the Markdown (https://en.wikipedia.org/wiki/Markdown) syntax.
    QString m_anchor;
    /// Html format help page
    QString m_htmltext;

    // CuteMarlEd data
    std::shared_ptr<HelpGenerator2Private> pdata = nullptr;

    // work with DB ------------------------------------

    /// Read record from database
    QString readRecord( const QString& key );

    /// Save record to file
    void saveRecordToFile( const QString& key, const QString& helpDir );

    /// Download image records to dirPath
    void copyImageRecordsToFileStructure( const QString& dirPath );

    /// Clear directory
    void clearDir( const QString& dirPath );

};


} // namespace jsonui17

