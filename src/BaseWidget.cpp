#include <QCloseEvent>
#include "BaseWidget_p.h"
#include "jsonui17/preferences.h"

namespace jsonui17 {

std::string BaseWidget::file_schema_ext( const std::string& schema_name, const std::string& ext )
{
    std::string ret = "";
    if( !schema_name.empty())
    {
        ret += "*.";
        ret += schema_name;
        ret += ".";//".schema.";
        ret += ext;
    }
    return ret;
}

std::string BaseWidget::schema_from_name( const std::string& file_name )
{
    std::string ret = "";
    size_t pose = file_name.find_last_of(".");
    if( pose != std::string::npos)
    {
        size_t posb = file_name.find_last_of(".", pose-1 );
        if( posb != std::string::npos )
            ret = file_name.substr(posb+1, pose-posb-1 );
    }
    return ret;
}


BaseWidget::BaseWidget(const std::string &schema_name, QWidget *parent):
    QMainWindow(parent), impl_ptr( new BaseWidgetPrivate( schema_name ))
{
    impl_ptr->base_ptr = this;
}

BaseWidget::BaseWidget(BaseWidgetPrivate &dd, QWidget *parent):
    QMainWindow(parent), impl_ptr(&dd)
{
    impl_ptr->base_ptr = this;
}

BaseWidget::~BaseWidget() {}

void BaseWidget::setOnCloseEventFunction(OnCloseEvent_f close_function)
{
    auto d = impl_func();
    d->on_close_function = close_function;
}

void BaseWidget::setShowWidgetFunction(ShowVertexEdge_f show_function)
{
    auto d = impl_func();
    d->show_document_widget_function = show_function;
}

void BaseWidget::objectChanged()
{
    auto d = impl_func();
    d->set_content_state(true);
}

void BaseWidget::updateViewMenu()
{
    auto d = impl_func();
    d->updt_view_menu();
}

void BaseWidget::updateModel()
{
    auto d = impl_func();
    d->updt_model();
}

void BaseWidget::updateTable()
{
    auto d = impl_func();
    d->updt_table();
}

void BaseWidget::updateDB()
{
    auto d = impl_func();
    d->updt_db();
}

void BaseWidget::CmHelpContens()
{
    const auto d = impl_func();
    helpWin( d->current_schema_name, "" );
}


void BaseWidget::CmHelpAbout()
{
    helpWin( "AboutJsonui", "" );
}

void BaseWidget::CmHelpAuthors()
{
    helpWin( "AuthorsJsonui", "" );
}

void BaseWidget::CmHelpLicense()
{
    helpWin( "LicenseJsonui", "" );
}

void BaseWidget::closeEvent(QCloseEvent* e)
{
    const auto d = impl_func();
    if( !d->on_close_function(this) )
        e->ignore();
    else
        QWidget::closeEvent(e);
}

} // namespace jsonui17
