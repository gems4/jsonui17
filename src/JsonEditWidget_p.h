#pragma once

#include "jsonui17/JsonEditWidget.h"
#include "BaseWidget_p.h"
#include "jsonui17/models/json_view.h"
#include "jsonio17/dbschemadoc.h"

namespace Ui {
class JsonEditWidget;
}

namespace jsonui17 {

/// Private \class JsonEditWidget - window to edit/create free format json or
/// schema based json
class JsonEditWidgetPrivate : public BaseWidgetPrivate
{
    Q_OBJECT

#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    Q_DISABLE_COPY_MOVE(JsonEditWidgetPrivate)
#endif

public:

    explicit JsonEditWidgetPrivate( const std::string& aschema_name = "",
                                    const std::string& collect_name = "" );
    ~JsonEditWidgetPrivate();

    /// Setup widget data
    void init();

    /// Extract current key from editor data
    std::string current_key()
    {
        current_record = json_tree->saveToJson();
        return dbcollection->recFromJson( current_record, false );
    }

    /// Set empty or default json document
    void cm_new()
    {
        reset_json( "", "");
    }

    /// Clone json document (clear _id, _rev ... )
    void cm_clone()
    {
        json_tree->setOid("");
        json_tree->setValueViaPath( "_rev", std::string("") );
        json_tree->hide();
        json_tree->show();
    }

    /// Save current document to database as new
    void create_insert()
    {
        if( dbcollection.get() == nullptr )
            return;
        std::string key = current_key();
        std::string rid = dbcollection->createDocument( key );
        json_tree->updateModelData( dbcollection->getJson(), current_schema_name );
    }

    /// Read new document from database
    void cm_read( const std::string& record_key )
    {

        dbcollection->readDocument( record_key );
        reset_json( dbcollection->getJson(true), "" );
        set_content_state( false );
    }

    /// Save current document to database
    void cm_update()
    {
        if( dbcollection.get() == nullptr )
            return;
        std::string key = current_key();
        dbcollection->updateDocument( key );
        set_content_state( false );
    }

    /// Delete current document from database
    void cm_delete( const std::string& record_key )
    {
        dbcollection->deleteDocument( record_key );
        set_content_state( false );
    }

private:

    inline JsonEditWidget* base_func() { return static_cast<JsonEditWidget *>(base_ptr); }
    inline const JsonEditWidget* base_func() const { return static_cast<const JsonEditWidget *>(base_ptr); }
    friend class JsonEditWidget;

    QScopedPointer<Ui::JsonEditWidget> qUi;
    /// tree view
    QScopedPointer<JsonView> json_tree;

    // document collection data
    std::string current_collection_name = "";
    std::shared_ptr<jsonio17::DBDocumentBase> dbcollection;
    std::string current_record;

    /// Set up menu commands
    void set_actions();
    /// Set up current json data to view model
    bool reset_json( const std::string& json_string, const std::string& schema_name )
    {
        // test legal schema name
        if( !current_schema_name.empty() && !schema_name.empty() && schema_name != current_schema_name )
            return false;
        current_record =  json_string;
        json_tree->updateModelData( json_string, current_schema_name );
        json_tree->update();
        return true;
    }
    /// Setup/ update database connection
    void reset_DB_client();

    // update after change preferences
    void updt_view_menu() override;
    void updt_model() override;
    void updt_table() override;
    void updt_db() override;
};

} // namespace jsonui17

