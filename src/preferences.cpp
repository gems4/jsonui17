#include <QMessageBox>
#include "jsonui17/models/schema_model.h"
#include "jsonui17/models/json_view.h"
#include "jsonui17/PreferencesJSONUI.h"
#include "jsonui17/HelpMainWindow.h"
#include "jsonarango/arangoconnect.h"
#include "jsonio17/dbdriverarango.h"
#include "preferences_p.h"
#include <spdlog/sinks/stdout_color_sinks.h>

namespace jsonui17 {

std::string UIPreferences::resourcesDatabaseName = "resources";
std::string UIPreferences::jsonui_section_name = "jsonui";
// Thread-safe logger to stdout with colors
std::shared_ptr<spdlog::logger> ui_logger = spdlog::stdout_color_mt("jsonui17");

UIPreferences& uiSettings()
{
    static  UIPreferences data;
    return data;
}


UIPreferences::UIPreferences():
    impl_ptr( new UIPreferencesPrivate )
{ }

UIPreferences::~UIPreferences()
{ }

void UIPreferences::CmSettingth()
{
    try {
        auto d = impl_func();
        // define new preferences
        PreferencesJSONUI dlg( d->jsonio_settings );
        // signal to reset database
        QObject::connect( &dlg, &PreferencesJSONUI::dbdriveChanged, this, &UIPreferences::dbChanged);

        if( !dlg.exec() )
            return;

        if( d->jsonio_settings.updateSchemaDir() )
            emit schemaChanged();

        emit viewMenuChanged();
        emit modelChanged();
        emit tableChanged();

    }
    catch(jsonio17::jsonio_exception& e)
    {
        ui_logger->error("jsonio exception when update settings {}", e.what());
        emit errorSettings( e.what() );
    }
    catch(std::exception& e)
    {
        ui_logger->error("std exception when update settings {}", e.what());
        emit errorSettings( e.what() );
    }
}

void UIPreferences::CmShowComments( bool checked )
{
    auto d = impl_func();
    JsonSchemaModel::showComments = checked;
    d->jsonui_group.setValue("ShowComments",  JsonSchemaModel::showComments );
    emit viewMenuChanged();
    emit modelChanged();
}

void UIPreferences::CmDisplayEnums( bool checked )
{
    auto d = impl_func();
    JsonSchemaModel::useEnumNames = checked;
    d->jsonui_group.setValue("ShowEnumNames",  JsonSchemaModel::useEnumNames );
    emit viewMenuChanged();
    emit tableChanged();
}

void UIPreferences::CmEditID( bool checked )
{
    auto d = impl_func();
    JsonSchemaModel::editID = checked;
    d->jsonui_group.setValue("CanEdit_id",  JsonSchemaModel::editID );
    emit viewMenuChanged();
}

void UIPreferences::CmEditExpanded( bool checked )
{
    auto d = impl_func();
    JsonView::expandedFields = checked;
    d->jsonui_group.setValue("KeepExpanded",  JsonView::expandedFields );
    // signal to update menu all
    emit viewMenuChanged();
}

void UIPreferences::CmSetUserDir( QString dirPath)
{
    auto d = impl_func();
    d->jsonio_settings.setUserDir( dirPath.toStdString() );
}

const jsonio17::DataBase &UIPreferences::database() const
{
    auto d = impl_func();
    return d->database();
}

const jsonio17::DataBase &UIPreferences::resources_database() const
{
    auto d = impl_func();
    return d->resources_database();
}

const std::shared_ptr<jsonio17::DataBase> &UIPreferences::dbclient() const
{
    auto d = impl_func();
    return d->work_database;
}

const std::shared_ptr<jsonio17::DataBase> &UIPreferences::dbresources() const
{
    auto d = impl_func();
    return d->resourse_database;
}


//---------UIPreferencesPrivate-----------------------------------------------------

UIPreferencesPrivate::UIPreferencesPrivate():
    jsonio_settings( jsonio17::ioSettings() ), jsonui_group( jsonio_settings.section( UIPreferences::jsonui_section_name ) ),
    work_database( nullptr ), resourse_database( nullptr )
{
    jsonio_settings.get_logger("jsonimpex17");
    jsonio_settings.get_logger("jsonui17");
    set_def_values();
    update_database();
}

UIPreferencesPrivate::~UIPreferencesPrivate()
{

}

void UIPreferencesPrivate::set_def_values()
{
    // load main programm settingth
    JsonSchemaModel::showComments = jsonui_group.value("ShowComments", JsonSchemaModel::showComments);
    JsonSchemaModel::useEnumNames = jsonui_group.value("ShowEnumNames", JsonSchemaModel::useEnumNames);
    JsonSchemaModel::editID = jsonui_group.value("CanEdit_id", JsonSchemaModel::editID);
    JsonView::expandedFields = jsonui_group.value("KeepExpanded", JsonView::expandedFields);

    HelpMainWindow::editHelp = jsonui_group.value( "CanEditDocPages", HelpMainWindow::editHelp );
}

//  Create empty collections if they are not present (for all vertexes and edges)
void UIPreferencesPrivate::create_collection_if_no_exist( jsonio17::AbstractDBDriver* db_driver )
{
    try{

        for( const auto& vertexCol: jsonio17::DataBase::usedVertexCollections() )
            db_driver->create_collection( vertexCol.second, "vertex");

        for( const auto& edgeCol: jsonio17::DataBase::usedEdgeCollections() )
            db_driver->create_collection( edgeCol.second, "edge");

    }
    catch( std::exception& e )
    {
        ui_logger->error("Exception when creating empty collections, if they do not exist {}", e.what());
    }
}

bool UIPreferencesPrivate::test_create_local_database( const std::string& database_name, const std::string& user )
{
    try {
        // could be not exist local ArangoDB or illegal root password
        auto rootclient = get_root_client();
        if( !rootclient->existDatabase( database_name ) )
        {
            QString msg = QString("The database %1 does not exist.\n Do you wants to create it?").arg(database_name.c_str());
            auto reply = QMessageBox::question( 0, "Create database", msg, QMessageBox::Yes|QMessageBox::No);
            if (reply == QMessageBox::Yes)
                return true;
            jsonio17::JSONIO_THROW( "UIPreferences", 10, " the database "+database_name+" does not exist."  );
        }
        else
        {
            Q_UNUSED(user)
            // Could be add grantUserToDataBase ????
            // Grant or revoke user access to a database ( "rw", "ro" or "none").
            //void grantUserToDataBase(const std::string& dbname, const std::string& username, const std::string& grand = "rw" );
        }
    }
    catch( std::exception& e )
    {
        ui_logger->error("Exception when test local database {}", e.what());
    }
    return false;
}

void UIPreferencesPrivate::create_local_database_if_no_exist( const std::string& database_name )
{
    try {
        // could be not exist local ArangoDB or illegal root password
        auto localConnect = jsonio17::getFromSettings( jsonio_settings.section( jsonio17::arangodb_section("ArangoDBLocal") ), false  );
        root_client->createDatabase( database_name, { localConnect.user });
    }
    catch(std::exception& e)
    {
        ui_logger->error("Exception when creating local database, if they do not exist {}", e.what());
    }
}

// Update "CurrentDBConnection" value.
void UIPreferencesPrivate::set_current_db_connection( int ndx )
{
    std::string dbConnection = "ArangoDBLocal";
    switch( ndx )
    {
    default:
    case 0: // localDB
        dbConnection = "ArangoDBLocal";
        jsonio_settings.setValue( jsonio17::arangodb_section( "UseArangoDBInstance" ), "ArangoDBLocal");
        break;
    case 1: // remoteDB
        dbConnection = "ArangoDBRemote";
        jsonio_settings.setValue( jsonio17::arangodb_section( "UseArangoDBInstance" ), "ArangoDBRemote");
        break;
    case 2: // localDB
        dbConnection = "ThriftDBSocket";
        break;
    }
    jsonui_group.setValue("CurrentDBConnection", dbConnection );
}


// Get "CurrentDBConnection" index
int UIPreferencesPrivate::get_current_db_connection( )
{
    auto dbconnection = jsonui_group.value<std::string>("CurrentDBConnection", "ArangoDBLocal" );
    if( dbconnection == "ArangoDBRemote" )
        return 1;
#ifndef IMPEX_OFF
    if( dbconnection == "ThriftDBSocket" )
        return 2;
#endif
    return 0;
}

void UIPreferencesPrivate::update_resource_database()
{
    std::shared_ptr<jsonio17::AbstractDBDriver> dbDriver;
    int dbConnection = get_current_db_connection();

    switch( dbConnection )
    {
    default:
    case 0: // localDB
    {
        create_local_database_if_no_exist( UIPreferences::resourcesDatabaseName );
        auto localConnect = jsonio17::getFromSettings( jsonio_settings.section( jsonio17::arangodb_section("ArangoDBLocal") ), false  );
        localConnect.databaseName = UIPreferences::resourcesDatabaseName;
        dbDriver.reset( new jsonio17::ArangoDBClient( localConnect ));
        //dbDriver->createCollection( "impexdefs", "vertex");
        //dbDriver->createCollection( "queries", "vertex");
        //dbDriver->createCollection( HelpMainWindow::help_collection_name, "vertex");
    }
        break;
    case 1: // remoteDB
    {
        auto remoteConnect = jsonio17::getFromSettings( jsonio_settings.section( jsonio17::arangodb_section("ArangoDBRemote") ), false  );
        remoteConnect.databaseName = UIPreferences::resourcesDatabaseName;
        dbDriver.reset( new jsonio17::ArangoDBClient( remoteConnect ));
    }
        break;
//#ifndef IMPEX_OFF
//    case 2: // ThriftClient
//    {
//        std::string host =  jsonio_settings.value( jsonio17::jsonimpex_section( "ThriftDBSocketHost" ), jsonio17::defHost );
//        int port = jsonio_settings.value( jsonio17::jsonimpex_section( "ThriftDBSocketPort" ), jsonio17::defPort );
//        dbDriver.reset( new jsonio17::ThriftClient( host, port ));
//    }
//        break;
//#endif
    }

    if( resourse_database.get() == nullptr )
        resourse_database.reset( new jsonio17::DataBase( dbDriver) );
    else
        resourse_database->updateDriver( dbDriver );
}


void UIPreferencesPrivate::update_database()
{

    std::shared_ptr<jsonio17::AbstractDBDriver> dbDriver;
    int dbConnection = get_current_db_connection();

    switch( dbConnection )
    {
    default:
    case 0: // localDB
    {
        auto localConnect = jsonio17::getFromSettings( jsonio_settings.section( jsonio17::arangodb_section("ArangoDBLocal") ), false  );
        dbDriver.reset( new jsonio17::ArangoDBClient( localConnect ));
        if( test_create_local_database( localConnect.databaseName, localConnect.user.name ) )
            create_local_database_if_no_exist( localConnect.databaseName );
        create_collection_if_no_exist( dbDriver.get() );
    }
        break;
    case 1: // remoteDB
    {
        auto remoteConnect = jsonio17::getFromSettings( jsonio_settings.section( jsonio17::arangodb_section("ArangoDBRemote") ), false  );
        dbDriver.reset( new jsonio17::ArangoDBClient( remoteConnect ));
    }
        break;
//#ifndef IMPEX_OFF
//    case 2: // ThriftClient
//    {
//        std::string host =  jsonio_settings.value( jsonio17::jsonimpex_section( "ThriftDBSocketHost" ), jsonio17::defHost );
//        int port = jsonio_settings.value( jsonio17::jsonimpex_section( "ThriftDBSocketPort" ), jsonio17::defPort );
//        dbDriver.reset( new jsonio17::ThriftClient( host, port ));
//    }
//        break;
//#endif
    }

    if( work_database.get() == nullptr )
        work_database.reset( new jsonio17::DataBase(dbDriver) );
    else
        work_database->updateDriver(dbDriver);

    update_resource_database();
}


bool UIPreferencesPrivate::test_current_driver( int db_connection )
{

    if( work_database.get() == nullptr )
        return true;

    switch( db_connection )
    {
    default:
    case 0: // localDB
    {
        auto localConnect = jsonio17::getFromSettings( jsonio_settings.section( jsonio17::arangodb_section("ArangoDBLocal") ), false  );
        jsonio17::ArangoDBClient* client = dynamic_cast<jsonio17::ArangoDBClient*>( work_database->theDriver() );
        return ( !client || localConnect != client->connect_data() );
    }
    case 1: // remoteDB
    {
        auto remoteConnect = jsonio17::getFromSettings( jsonio_settings.section( jsonio17::arangodb_section("ArangoDBRemote") ), false  );
        jsonio17::ArangoDBClient* client = dynamic_cast<jsonio17::ArangoDBClient*>(work_database->theDriver());
        return ( !client || remoteConnect != client->connect_data() );
    }
//#ifndef IMPEX_OFF
//    case 2: // ThriftClient
//    {
//        std::string host =  jsonio_settings.value( jsonio17::jsonimpex_section( "ThriftDBSocketHost" ), jsonio17::defHost );
//        int port = jsonio_settings.value( jsonio17::jsonimpex_section( "ThriftDBSocketPort" ), jsonio17::defPort );
//        jsonio17::ThriftClient* client = dynamic_cast<jsonio17::ThriftClient*>(work_database->theDriver());
//        return ( !client || host != client->host() || port != client->port()  );
//    }
//#endif
    }
    //return false;
}

arangocpp::ArangoDBRootClient *UIPreferencesPrivate::get_root_client()
{
    auto rootConnect = jsonio17::getFromSettings( jsonio_settings.section( jsonio17::arangodb_section("ArangoDBLocal") ), true  );
    root_client.reset(  new arangocpp::ArangoDBRootClient( rootConnect ) );
    return  root_client.get();
}


} // namespace jsonui17
