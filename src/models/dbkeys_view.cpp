#include <QKeyEvent>
#include <QHeaderView>
#include "dbkeys_view.h"
#include "jsonui17/QueryWidget.h"
#include "jsonui17/TableEditWindow.h"
#include "jsonui17/BaseWidget.h"

namespace jsonui17 {



void DBKeyContainer::resetData()
{
    col_heads.clear();
    matrix.clear();
    if( db_client==nullptr )
        return;

    const std::vector<std::string>& selected_fields = db_client->currentQueryResult().query().fields();
    const jsonio17::key_values_table_t& val_tabl = db_client->currentQueryResult().queryResult();

    col_heads.push_back( "key" );
    for( const auto& field: selected_fields )
        col_heads.push_back( field.c_str() );

    for( const auto& line:  val_tabl )
    {
        QVector<QVariant> vec;
        matrix.push_back( vec );
        matrix.last().push_back( line.first.c_str() );
        for( const auto& val_col: line.second )
            matrix.last().push_back( val_col.c_str() );
    }
}


//--------------------------------------------------------------------------

DBKeysObject::DBKeysObject( const jsonio17::DBVertexDocument* adb_client,  DBKeysTable *akeys_table, BaseWidget *aparent ):
    table_view(akeys_table),   query_window(nullptr), result_window(nullptr), parent_window( aparent )
{
    define_keys_table();
    changeDBClient( adb_client );
}

DBKeysObject::~DBKeysObject()
{
    Close();
}


// Close addition windows
void DBKeysObject::Close()
{
    if( query_window )
        query_window->close();
    if( result_window )
        result_window->close();
}

void DBKeysObject::changeDBClient( const jsonio17::DBVertexDocument* adb_client  )
{
    parent_dbclient = adb_client;
    reset_keys_table();
    // if( query_window )  ??? could be done into dbcollection::reset
    //    query_window->reset_DB_client();
}

void DBKeysObject::resetModel( QString key )
{
    // reset model data
    model_view->resetMatrixData();

    if( result_window )
        result_window->updateTable();

    auto id_key = key.toStdString();
    if( parent_window && id_key.empty() )
        id_key = parent_window->currentKey();

    movetoKey( id_key );
}


void DBKeysObject::showQuery( const char* title, const std::vector<std::string>& schemalst )
{
    if( parent_dbclient == nullptr )
        return;

    const jsonio17::DBQueryDef& query = parent_dbclient->currentQueryResult().query();
    if( !query_window )
    {
        query_window.reset( new QueryWidget( title, schemalst, query, parent_window ) );
    }
    else
    {
        query_window->updateQuery( schemalst, query );
        query_window->raise();
    }
    query_window->show();
}

void DBKeysObject::setOneQuery( QueryWidget* query_widget )
{
    if( query_widget != query_window.get() )
        return;
    auto query_def = std::make_shared<jsonio17::DBQueryDef>( query_widget->getQuery() );
    emit updateQuery( query_widget->getSchema().c_str(), std::move(query_def)  );
}

void DBKeysObject::showResult( const char* title )
{
    if( !result_window )
    {
        result_window.reset( new TableEditWidget( title, dbkeys, MatrixTable::tbShow  ) );
    }
    else
    {
        result_window->updateTable();
        result_window->raise();
    }
    result_window->show();
}

void DBKeysObject::define_keys_table()
{
    //table->horizontalHeader()->hide();
    table_view->setSelectionBehavior(QAbstractItemView::SelectRows);
    table_view->horizontalHeader()->setSectionResizeMode( QHeaderView::Interactive );
    table_view->setEditTriggers( QAbstractItemView::AnyKeyPressed );
    table_view->setSortingEnabled(true);
    disconnect( table_view, &DBKeysTable::customContextMenuRequested,
                table_view, &DBKeysTable::slotPopupContextMenu);
}

void DBKeysObject::reset_keys_table()
{
    if( parent_dbclient == nullptr)
        return;
    dbkeys.reset( new DBKeyContainer( "select", parent_dbclient ) );
    if( model_view )
      delete model_view;
    model_view = new MatrixModel( dbkeys, parent() );
    table_view->setModel( model_view );
    table_view->hideColumn(0); // !!!! do not hide for Edges
}

} // namespace jsonui17

