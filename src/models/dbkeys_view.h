#pragma once

#include "jsonui17/models/table_view.h"
#include "jsonio17/dbedgedoc.h"


namespace jsonui17 {

class QueryWidget;
class TableEditWidget;
class BaseWidget;

/// Class for key table data container
class DBKeyContainer : public AbstractContainer
{

public:

    DBKeyContainer( const char * aname, const jsonio17::DBSchemaDocument* adbclient ):
        AbstractContainer(aname), db_client(adbclient)
    {
        resetData();
    }
    virtual ~DBKeyContainer() {}

    int rowCount() const override
    {
        return matrix.size();
    }

    int columnCount() const override
    {
        return col_heads.size();
    }

    QVariant data( int line, int column ) const override
    {
        return matrix.at( line ).at( column );
    }

    bool setData( int line, int column, const QVariant &value ) override
    {
        Q_UNUSED( line );
        Q_UNUSED( column );
        Q_UNUSED( value );
        //matrix[line].replace(column, value);
        return true;
    }

    QString headerData ( int section ) const override
    {
        return col_heads[section];
    }

    bool  isEditable( int line, int column ) const override
    {
        Q_UNUSED( line );
        Q_UNUSED( column );
        return false;
    }

    Data_Types getType( int line, int column ) const override
    {
        Q_UNUSED( line );
        Q_UNUSED( column );
        return ftString;
    }

    QString getToolTip( int line, int column ) const override
    {
        Q_UNUSED( line );
        return col_heads[column];
    }

    void resetData() override;

protected:

    const jsonio17::DBSchemaDocument *db_client = nullptr;
    QVector< QString > col_heads;
    QVector< QVector<QVariant> > matrix;

};



//---------------------------------------------------

//http://qaru.site/questions/1690066/how-do-i-populate-a-qtableview-with-asynchronous-fetched-data
/// \class DBKeysObject object to work with database keys/fields table
class DBKeysObject : public QObject
{
    Q_OBJECT

signals:

    /// signal when update query
    void updateQuery( QString schema, std::shared_ptr<jsonio17::DBQueryDef> query_def );

public slots:

    /// Update db document
    void changeDBClient( const jsonio17::DBVertexDocument* adb_client );

    /// Update keys model data
    void resetModel( QString id_key );

    /// Update key
    void movetoKey( const std::string& id_key )
    {
        if(table_view) {
            table_view->move_to_key(id_key);
        }
    }

public:

    DBKeysObject( const jsonio17::DBVertexDocument* adb_client,
                  DBKeysTable *akeys_table, BaseWidget *aparent );
    ~DBKeysObject();

    /// Show query window
    void showQuery( const char* title, const std::vector<std::string>& schemalst );

    /// Update keys model for changed query
    void setOneQuery( QueryWidget* query_widget );

    /// Show table window with database keys/fields table
    void showResult( const char* title );

    /// Close addition windows
    void Close();

protected:

    /// DB keys table data container
    std::shared_ptr<DBKeyContainer> dbkeys;
    /// Table model
    MatrixModel* model_view = nullptr;
    /// Extern link to table view
    DBKeysTable* const table_view = nullptr;

    /// Keys query window
    std::shared_ptr<QueryWidget> query_window;

    /// Keys list demo window
    std::shared_ptr<TableEditWidget> result_window;

    BaseWidget* parent_window;
    const jsonio17::DBSchemaDocument* parent_dbclient;

    /// Reset keys table model
    void reset_keys_table();
    /// Set up keys table
    void define_keys_table();
};


} // namespace jsonui17


