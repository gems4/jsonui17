#pragma once

#include <QAbstractItemModel>
#include <QTreeView>
#include <QItemDelegate>
#include "jsonio17/dbquerybase.h"
#include "jsonio17/schema.h"
#include "jsonio17/jsondetail.h"

namespace jsonui17 {

class AQLline;

struct SchemaFieldData
{
    /// Thrift type of field
    jsonio17::FieldDef::FieldType field_type = jsonio17::FieldDef::T_STRING;
    /// Name of enum
    std::string class_name = "";
};

/// \class QueryLine represents a query item in a tree view.
/// Item data defines one query object
class AQLline
{

    friend class QueryModel;
    friend class QueryDelegate;

public:

    enum fieldType
    { Top, Field, LogicalTwo, LogicalNot, Operator };
    //{ tTop, tField, tConditionAdd, tConditionOne, tList, tObject, tString };

    AQLline( size_t andx, const std::string& akey, fieldType atype, AQLline* parentline );
    virtual ~AQLline();

    /// Return json string with template query
    std::string toTemplate();
    /// Return string with AQL query
    std::string toAQL(const std::string& collection);

protected:

    /// Type of field
    fieldType type;
    /// Name of field
    std::string keyname;
    /// Value of field
    std::string value;
    ///Internal data from thrift schema
    SchemaFieldData field_data;

    size_t ndx_in_parent;
    AQLline *parent;
    std::vector<std::shared_ptr<AQLline>> children;


    fieldType type_from_key( const std::string& akey );
    void query2dom( jsonio17::JsonBase& object );
    void value2dom( jsonio17::JsonBase& object );
    std::string query2AQL();
    std::string value2AQL( const std::string& the_operator );
    bool remove_child(AQLline *child);

    /// Return json object
    virtual void toDom( jsonio17::JsonBase& object ) const;
    /// Set json dom object
    virtual void fromDom( const jsonio17::JsonBase& object );
    /// Json value type
    jsonio17::JsonBase::Type value_type();

    /// Get Value from string
    /// If field is not type T, the false will be returned.
    template <class T>
    bool get_value( T& val  )
    {
        if( jsonio17::is<T>( val, value ) )
            return true;
        return false;
    }

};


/// \class QueryModel
/// class for represents the data set and is responsible for fetching the data
/// is needed for viewing and for writing back any changes.
/// Reading/writing data from/to query object
class QueryModel: public QAbstractItemModel
{
    Q_OBJECT

    friend class QueryDelegate;
    friend class QueryView;

public:

    QueryModel( QObject* parent = nullptr );
    ~QueryModel(){}

    /// Make query string from internal model data
    jsonio17::DBQueryBase getQuery( jsonio17::DBQueryBase::QType query_type, const std::string& collection );
    /// Make FILTER json object from internal model data
    std::string getFILTER();
    /// Set up internal model data from FILTER json string
    void setFILTER( const std::string& filter_generator );

protected:

    QStringList hdData;
    std::unique_ptr<AQLline> rootNode;

    QModelIndex index(int row, int column, const QModelIndex& parent) const override;
    QModelIndex parent(const QModelIndex& child) const override;
    int rowCount ( const QModelIndex& parent ) const override;
    int columnCount ( const QModelIndex& parent  ) const override;
    QVariant data ( const QModelIndex& index, int role ) const override;
    bool setData ( const QModelIndex& index, const QVariant& value, int role ) override;
    QVariant headerData ( int section, Qt::Orientation orientation, int role ) const override;
    Qt::ItemFlags flags ( const QModelIndex& index ) const override;

    void clearModelData();
    bool canLogical( const QModelIndex& index, bool AQL_mode ) const;
    bool canOperator( const QModelIndex& index, bool AQL_mode ) const;
    bool canField( const QModelIndex& index, bool AQL_mode ) const;

    const QModelIndex addObject( const std::string& akey, bool isoperator, const QModelIndex& index );
    const QModelIndex addField( const std::string& flds_name,
                                const jsonio17::FieldDef* fld_data, const QModelIndex& index );
    void delObject( const QModelIndex& index );

    AQLline* lineFromIndex(const QModelIndex& index) const;

};

/// \class QueryDelegate
/// individual items in views are rendered and edited using delegates
class QueryDelegate: public QItemDelegate
{
    Q_OBJECT

public:

    QueryDelegate( QObject * parent = nullptr ):
        QItemDelegate( parent )
    {}
    QWidget *createEditor( QWidget *parent,
                           const QStyleOptionViewItem &option,
                           const QModelIndex &index) const;
protected:
    QWidget *enums_to_combobox(QWidget *parent, const jsonio17::EnumDef *enumdef) const;
};

///  \class QueryView implements a tree view of query structure
/// that displays items from a TQueryModel model.
class QueryView: public QTreeView
{
    Q_OBJECT

protected slots:
    void slotPopupContextMenu(const QPoint& pos);
    void changeCurrent( int section );

public slots:

    void CmAddObjects( const std::vector<std::string>& fields_list,
                       const std::vector<const jsonio17::FieldDef*>& fields_types );
    void CmAddField();

    void CmAddAnd()
    {  addObject( "AND", false );  }
    void CmAddOr()
    {  addObject( "OR", false );  }
    void CmAddNot()
    {  addObject( "NOT", false );  }
    void CmAddIn()
    {  addObject( "IN" );  }
    void CmAddNin()
    {  addObject( "NOT IN" );  }
    void CmAddLike()
    {  addObject( "LIKE" );  }
    void CmAddRegexp()
    {  addObject( "=~" );  }
    void CmAddNotRegexp()
    {  addObject( "!~" );  }
    void CmAddGt()
    {  addObject( ">" );  }
    void CmAddGte()
    {  addObject( ">=" );  }
    void CmAddLt()
    {  addObject( "<" );  }
    void CmAddLte()
    {  addObject( "<=" );  }
    void CmAddEq()
    {  addObject( "==" );  }
    void CmAddNotEq()
    {  addObject( "!=" );  }

    void CmDelLine();

    bool AQLmode( bool checked)
    {
        if( checked != AQL_mode )
        {
            ClearAll();
            AQL_mode = checked;
            return true;
        }
        return false;
    }

public:

    QueryView( QWidget * parent = nullptr );

    void ClearAll()
    {
        QueryModel* themodel =  view_model();
        if( themodel)
            themodel->clearModelData();
    }

private:

    bool AQL_mode;
    void keyPressEvent(QKeyEvent* e) override;

    void addObject( const std::string& akey, bool isoperator = true );
    QMenu* contextMenu( const QModelIndex &index );

    QueryModel* view_model()
    {
        return dynamic_cast<QueryModel*>( model() );
    }
};

} // namespace jsonui17


