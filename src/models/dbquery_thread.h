#pragma once

#include <QObject>
#include "jsonio17/dbedgedoc.h"

namespace jsonui17 {

/// \class DBVertexObject object to work with database document (vertex)
class DBVertexObject: public QObject
{
    Q_OBJECT

signals:

    /// signal when db document changed
    void changedDBClient( const jsonio17::DBVertexDocument* adbgraph );

    /// signal when db document query changed
    void changedQueryString( QString query_string );

    /// To update keys model data
    void changedModel( QString key_document );

    /// Finished process
    void finished();

    /// Finished with exception
    void isException( const QString& titl, const QString& msg );

    /// Update oid
    void updatedOid( QString oid );

    /// After open new document
    void openedDocument(  QString json_document  );

    /// Signal of deleting documents
    /// Possible delete edges too ( reload all open edges )
    void deletedDocument();

    /// Signal of loading graph documents
    /// Load records to different collections (reload all open )
    void loadedGraph();

public slots:

    /// Reset current DB client
    virtual void dbResetClient( QString aschema_name );

    /// Update query
    virtual void dbChangeQuery( QString aschema_name, std::shared_ptr<jsonio17::DBQueryDef> querydef );
    /// Reload query data
    virtual void dbReloadQuery();

    /// Open document
    virtual void dbReadDocument( QString key_document );

    /// Save json Document to database
    virtual void dbUpdate( QString json_document );

    /// Delete keyDocument document from database
    virtual void dbDelete( QString key_document );

    /// Delete documents from database
    virtual void dbDeleteList( std::vector<std::string> keys_document );

    /// Read multiple records from file fileName
    virtual void dbRestoreMultipleRecordsfromFile( QString file_name );

    /// Write multiple records to file fileName
    virtual void dbBackupQueriedRecordstoFile( QString file_name, std::vector<std::string> keys_document);

#ifndef IMPEX_OFF

    /// Read json records from format file fileName
    void dbImportFormat( std::string impex_format_type, std::string impex_format_string,
                         QString file_name, bool overwrite  );

    /// Read json records from format file fileName
    void dbUpdateFormat( std::string impex_format_type, std::string impex_format_string,
                         QString file_name, bool overwrite  );

    /// Write json record to format file fileName
    void dbExportFormat( std::string impex_format_type, std::string impex_format_string,
                         QString file_name, std::vector<std::string> keys_document );
#endif

    /// Write graph to file fileName
    void dbBackupGraphtoFile( QString file_name, std::vector<std::string> keys_document );

    /// Read multiple records from file fileName
    void dbRestoreGraphfromFile( QString file_name );


public:

    DBVertexObject( const std::string& aschema_name,
                    const jsonio17::DBQueryBase& query = jsonio17::DBQueryBase::emptyQuery() ):
        document_schema_name(aschema_name), dbdocument(nullptr),
        current_query(query),    is_default_query( !query.empty() )
    {
        // now in main thread
        //dbResetClient( _schemaName );
    }

    ~DBVertexObject()
    { }

protected:

    /// Current schema name
    std::string document_schema_name;

    /// Current link to Arango Database
    std::shared_ptr<jsonio17::DBVertexDocument> dbdocument;

    /// Current document query
    jsonio17::DBQueryBase current_query;

    /// Use default query for collection
    bool is_default_query = false;

};

/// \class DBEdgeObject object to work with database edge document
class DBEdgeObject : public DBVertexObject
{
    Q_OBJECT

signals:

    /// Fixed incoming query
    void fixIncoming();

    /// signal when incoming document changed
    void changedIncomingDBClient( const jsonio17::DBVertexDocument* adbgraph );

    /// To update Incoming keys model data
    void changedIncomingModel( QString key_document );

    /// Fixed Outgoing query
    void fixOutgoing();

    /// signal when Outgoing document changed
    void changedOutgoingDBClient(  const jsonio17::DBVertexDocument* adbgraph );

    /// To update Outgoing keys model data
    void changedOutgoingModel( QString key_document );

    /// Reset TypeBox
    void resetTypeBox( QString aschema_name );

public slots:

    /// Reset current DB client
    virtual void dbResetClient(  QString aschema_name );

    /// Reset current DB schema
    virtual void dbResetSchema(  QString aschema_name );

    /// Open document
    virtual void dbReadDocument(  QString key_document  );

    /// Reload query data
    virtual void dbReloadQuery();

    /// Update Incoming  query
    void dbChangeIncomingQuery( QString aschema_name, std::shared_ptr<jsonio17::DBQueryDef> querydef );

    /// Update Outgoing  query
    void dbChangeOutgoingQuery( QString aschema_name, std::shared_ptr<jsonio17::DBQueryDef> querydef );

    /// Read multiple records from file fileName
    virtual void dbRestoreMultipleRecordsfromFile( QString file_name );

public:

    DBEdgeObject( const std::string& aschemaname,
                  const jsonio17::DBQueryBase& query = jsonio17::DBQueryBase(jsonio17::DBQueryBase::qUndef) ):
        DBVertexObject( aschemaname, query ), change_schema_mode(is_default_query)
    {
        // now in main thread
        //dbResetClient( _schemaName );
    }

    ~DBEdgeObject()
    { }

private:

    /// Edge change schema mode
    bool change_schema_mode = false;

    // Incoming vertexes list data

    /// Current incoming vertexes query
    jsonio17::DBQueryBase query_incoming;
    /// Database connection incoming vertexes object
    std::shared_ptr<jsonio17::DBVertexDocument> dbincoming;

    // Outgoing vertexes list data

    /// Current outgoing vertexes query
    jsonio17::DBQueryBase query_outgoing;
    /// Database connection outgoing vertexes object
    std::shared_ptr<jsonio17::DBVertexDocument> dboutgoing;

    /// Update query
    bool change_query_to_id( jsonio17::DBVertexDocument* dbvertex, const std::string& aid_key );

    /// Open incoming and outgoing vertex documents
    void reset_incoming_outgoing_dbclients();
};


} // namespace jsonui17
