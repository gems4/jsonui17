#include "dbquery_thread.h"
#include "jsonio17/traversal.h"
#include "jsonui17/preferences.h"
#ifndef IMPEX_OFF
#include "jsonimpex17/yaml_xml2file.h"
#include "jsonimpex17/impex_generator.h"
#endif


namespace jsonui17 {

std::string extractStringField( const std::string& field_key, const std::string& jsondata );

std::string extractStringField( const std::string& field_key, const std::string& jsondata )
{
    std::string regstr =  std::string(".*\"")+field_key+"\"\\s*:\\s*\"([^\"]*)\".*";
    std::string query_data = jsondata;

    jsonio17::replace_all( query_data, "\'", '\"');
    return jsonio17::regexp_extract_string(regstr, query_data);
}

// Reset current DB client
void DBVertexObject::dbResetClient( QString aschema_name )
{
    try{
        document_schema_name = aschema_name.toStdString();
        jsonio17::DBVertexDocument* newClient = jsonio17::DBVertexDocument::newVertexDocumentQuery(
                    uiSettings().database(), document_schema_name, current_query );

        // no schema
        if( newClient == nullptr  )
        {
            dbdocument.reset();
            emit changedDBClient( nullptr );
        }
        else
        {
            dbdocument.reset( newClient );
            // update search tables ( wait untill currentQueryResult mutex )
            emit changedQueryString( dbdocument->currentQueryResult().condition().queryString().c_str() );
            emit changedDBClient( dbdocument.get() );
            dbReadDocument( dbdocument->currentQueryResult().getFirstKey().c_str()  );
        }
    }
    catch(std::exception& e)
    {
        emit isException( "DBVertexObject::dbResetClient ", e.what() );
    }
    emit finished();
}

void DBVertexObject::dbChangeQuery( QString aschema_name, std::shared_ptr<jsonio17::DBQueryDef> querydef )
{
    if( dbdocument.get() == nullptr )
    {
        emit finished();
        return;
    }
    auto oldquery = dbdocument->currentQueryResult().query();
    try
    {
        dbdocument->resetSchema( aschema_name.toStdString(), true );
        // reset internal query data
        dbdocument->setQuery( *querydef );
        current_query = *querydef->condition();
    }
    catch(std::exception& e)
    {
        dbdocument->setQuery( oldquery );
        emit isException( "DBVertexObject::dbChangeQuery ", e.what() );
    }
    // update search tables ( wait untill currentQueryResult mutex )
    emit changedQueryString( dbdocument->currentQueryResult().condition().queryString().c_str() );
    emit changedModel("");
    emit finished();
}

void DBVertexObject::dbReloadQuery()
{
    try
    {
        if( dbdocument.get() != nullptr )
        {
            dbdocument->updateQuery();
            // update search tables ( wait untill currentQueryResult mutex )
            emit changedQueryString( dbdocument->currentQueryResult().condition().queryString().c_str() );
            emit changedModel("");
        }
    }
    catch(std::exception& e)
    {
        emit isException( "DBVertexObject::dbReloadQuery ", e.what() );
    }
    emit finished();
}

void DBVertexObject::dbReadDocument(  QString key_document  )
{
    if( dbdocument.get() == nullptr || key_document.isEmpty() )
        return;

    try{
        ui_logger->debug("dbReadDocument {}", key_document.toStdString());
        dbdocument->readDocument( key_document.toStdString() );
        std::string jsonDocument = dbdocument->getJson();
        emit openedDocument(  jsonDocument.c_str()  );
    }
    catch( jsonio17::jsonio_exception& e)
    {
        emit isException( "DBVertexObject::dbReadDocument ", e.what() );
    }
    catch(std::exception& e)
    {
        emit isException( "DBVertexObject::std::exception", e.what() );
    }
}


// Save current record to DB file
void DBVertexObject::dbUpdate( QString json_document )
{
    try
    {
        if( dbdocument.get() == nullptr )
            return;

        std::string  key = dbdocument->recFromJson( json_document.toStdString(), false );
        if( !dbdocument->existsDocument( key ))
        {
            key = dbdocument->createDocument( key );
            emit updatedOid(key.c_str());
        }
        else
        {
            dbdocument->updateDocument( key );
        }
        emit changedModel( key.c_str() );
    }
    catch(jsonio17::jsonio_exception& e)
    {
        emit isException(  "DBVertexObject::dbUpdate", e.what() );
    }
    catch(std::exception& e)
    {
        emit isException(  "DBVertexObject::std::exception", e.what() );
    }
}


// Delete current record from DB
void DBVertexObject::dbDelete( QString key_document )
{
    try
    {
        if( dbdocument.get() == nullptr || key_document.isEmpty())
            return;
        dbdocument->deleteDocument( key_document.toStdString() );
        emit changedModel("");
        emit deletedDocument();
    }
    catch(jsonio17::jsonio_exception& e)
    {
        emit isException( "DBVertexObject::dbDelete ", e.what() );
    }
    catch(std::exception& e)
    {
        emit isException( "DBVertexObject::std::exception ", e.what() );
    }
}

// Delete records from List
void DBVertexObject::dbDeleteList( std::vector<std::string> keys_document )
{
    try
    {
        if( dbdocument.get() && !keys_document.empty() )
        {
            for( size_t ii=0; ii<keys_document.size(); ii++ )
                dbdocument->deleteDocument( keys_document[ii] );
            emit changedModel("");
            emit deletedDocument();
        }
    }
    catch(jsonio17::jsonio_exception& e)
    {
        emit isException( "DBVertexObject::dbDeleteList", e.what() );
    }
    catch(std::exception& e)
    {
        emit isException( "DBVertexObject::std::exception", e.what() );
    }
    emit finished();
}

// Read multiple records from file fileName
void DBVertexObject::dbRestoreMultipleRecordsfromFile( QString file_name )
{
    if( dbdocument.get() == nullptr )
    {
        emit finished();
        return;
    }

    try{
        std::string curRecord;
#ifndef IMPEX_OFF
        jsonio17::JsonYamlXMLArrayFile file( file_name.toStdString() );
#else
        jsonio17::JsonArrayFile file( file_name.toStdString() );
#endif
        file.Open( jsonio17::TxtFile::ReadOnly );
        while( file.loadNext( curRecord ) ) {
            dbdocument->updateFromJson( curRecord, false );
        }
        file.Close();
    }
    catch( jsonio17::jsonio_exception& e)
    {
        emit isException( "DBVertexObject::dbRestoreMultipleRecordsfromFile ", e.what() );
    }
    catch(std::exception& e)
    {
        emit isException( "DBVertexObject::std::exception ", e.what() );
    }
    emit changedModel("");
    emit finished();
}

// Write multiple records to file fileName
void DBVertexObject::dbBackupQueriedRecordstoFile( QString file_name, std::vector<std::string> keys_document)
{
    if( dbdocument.get() == nullptr)
    {
        emit finished();
        return;
     }

    try {
#ifndef IMPEX_OFF
        jsonio17::JsonYamlXMLArrayFile file( file_name.toStdString() );
#else
        jsonio17::JsonArrayFile file( file_name.toStdString() );
#endif
        file.Open( jsonio17::TxtFile::WriteOnly );
        for( const auto& doc_key: keys_document )
        {
            dbdocument->readDocument( doc_key );
            file.saveNext( dbdocument->loaded_data() );
        }
        file.Close();
    }
    catch(jsonio17::jsonio_exception& e)
    {
        emit isException(  "DBVertexObject::dbBackupQueriedRecordstoFile ", e.what() );
    }
    catch(std::exception& e)
    {
        emit isException(  "DBVertexObject::std::exception ", e.what() );
    }
    emit finished();
}

// Read multiple records from file fileName
void DBVertexObject::dbRestoreGraphfromFile( QString file_name )
{
    try{
        jsonio17::GraphTraversal travel( uiSettings().database() );
        travel.restoreGraphFromFile( file_name.toStdString() );
    }
    catch(jsonio17::jsonio_exception& e)
    {
        emit isException( "DBVertexObject::dbRestoreGraphfromFile ", e.what() );
    }
    catch(std::exception& e)
    {
        emit isException( "DBVertexObject::std::exception ", e.what() );
    }
    emit changedModel("");
    emit loadedGraph();
    emit finished();
}

// Write multiple records to file fileName
void DBVertexObject::dbBackupGraphtoFile(QString file_name, std::vector<std::string> keys_document)
{
    if( keys_document.empty() )
    {
        emit finished();
        return;
    }

    try {

        auto file = std::make_shared<jsonio17::JsonArrayFile>( file_name.toStdString() );
        jsonio17::GraphTraversal travel( uiSettings().database() );

        file->Open( jsonio17::TxtFile::WriteOnly );
        jsonio17::GraphElement_f afunc =  [file]( bool , const std::string& data )
        {
           file->saveNext(data);
        };
        travel.Traversal( true, keys_document, afunc );
        file->Close();
    }
    catch(jsonio17::jsonio_exception& e)
    {
        emit isException( "DBVertexObject::dbBackupGraphtoFile ", e.what() );
    }
    catch(std::exception& e)
    {
        emit isException( "DBVertexObject::std::exception ", e.what() );
    }
    emit finished();
}

#ifndef IMPEX_OFF

// Read json records from format file fileName
void DBVertexObject::dbImportFormat( std::string impex_format_type, std::string impex_format_string,
                                     QString file_name, bool overwrite )
{
    if( dbdocument.get() == nullptr )
    {
        emit finished();
        return;
    }

    try{
        jsonio17::FormatImpexGenerator worker(uiSettings().dbclient(), overwrite, jsonio17::FormatImpexGenerator::String);
        worker.importFileToDatabase(impex_format_type, impex_format_string, file_name.toStdString(), dbdocument.get());
    }
    catch( jsonio17::jsonio_exception& e)
    {
        ui_logger->error("jsonio exception DBVertexObject::dbImportFormat {}", e.what());
        emit isException( "DBVertexObject::dbImportFormat ", e.what() );
    }
    catch(std::exception& e)
    {
        ui_logger->error("std exception DBVertexObject::dbImportFormat {}", e.what());
        emit isException( "DBVertexObject::std::exception ", e.what() );
    }
    emit changedModel("");
    emit finished();
}

// Read json records from format file fileName
void DBVertexObject::dbUpdateFormat( std::string impex_format_type, std::string impex_format_string,
                                     QString file_name, bool overwrite  )
{
    if( dbdocument.get() == nullptr /*|| !overwrite*/ )
    {
        emit finished();
        return;
    }

    try{
        jsonio17::FormatImpexGenerator worker(uiSettings().dbclient(), overwrite, jsonio17::FormatImpexGenerator::String);
        worker.updateFileToDatabase(impex_format_type, impex_format_string, file_name.toStdString(), dbdocument.get());
    }
    catch( jsonio17::jsonio_exception& e)
    {
        ui_logger->error("jsonio exception DBVertexObject::dbUpdateFormat {}", e.what());
        emit isException( "DBVertexObject::dbUpdateFormat ", e.what() );
    }
    catch(std::exception& e)
    {
        ui_logger->error("std exception DBVertexObject::dbUpdateFormat {}", e.what());
        emit isException( "DBVertexObject::std::exception ", e.what() );
    }
    emit changedModel("");
    emit finished();
}


// Write json record to format file fileName
void DBVertexObject::dbExportFormat( std::string impex_format_type, std::string impex_format_string,
                                     QString file_name, std::vector<std::string> keys_document )
{
    try {
        if( dbdocument.get() || !keys_document.empty() )
        {
            jsonio17::FormatImpexGenerator worker(uiSettings().dbclient(), true, jsonio17::FormatImpexGenerator::String);
            worker.exportFileFromDatabase( keys_document, impex_format_type, impex_format_string, file_name.toStdString(), dbdocument.get());
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        emit isException( "DBVertexObject::dbExportFormat ", e.what() );
    }
    catch(std::exception& e)
    {
        emit isException( "DBVertexObject::std::exception ", e.what() );
    }
    emit finished();
}

#endif


// DBEdgeObject model ----------------------------------------

// Reset current DB client
void DBEdgeObject::dbResetClient(  QString aschema_name )
{
    try{

        document_schema_name = aschema_name.toStdString();
        jsonio17::DBEdgeDocument* newClient = jsonio17::DBEdgeDocument::newEdgeDocumentQuery(
                    uiSettings().database(), document_schema_name, current_query );
        newClient->setMode(change_schema_mode);

        // no schema
        if( newClient == nullptr  )
        {
            dbdocument.reset();
            emit changedDBClient( nullptr );
        }
        else
        {
            dbdocument.reset( newClient );
            reset_incoming_outgoing_dbclients();
            // update search tables ( wait untill currentQueryResult mutex )
            emit changedQueryString( dbdocument->currentQueryResult().condition().queryString().c_str() );
            emit changedDBClient( dbdocument.get() );
            dbReadDocument(  dbdocument->currentQueryResult().getFirstKey().c_str()  );
        }
    }
    catch(std::exception& e)
    {
        emit isException( "DBEdgeObject::resetDBClient ", e.what() );
    }
    emit finished();
}


void DBEdgeObject::dbResetSchema(  QString aschema_name )
{
    try{
        document_schema_name = aschema_name.toStdString();

        if( !is_default_query )
        {
            dbdocument->resetSchema( document_schema_name, true );
            dbdocument->updateQuery();
            // update search tables ( wait untill currentQueryResult mutex )
            emit changedQueryString( dbdocument->currentQueryResult().condition().queryString().c_str() );
            emit changedModel("");
            dbReadDocument( dbdocument->currentQueryResult().getFirstKey().c_str() );
        }
        else
            dbdocument->resetSchema( document_schema_name, false );
    }
    catch(std::exception& e)
    {
        emit isException( "DBEdgeObject::dbResetSchema ", e.what() );
    }
    emit finished();
}

void DBEdgeObject::dbReadDocument(  QString key_document  )
{
    if( dbdocument.get() == nullptr || key_document.isEmpty())
        return;

    try{
        dbdocument->readDocument( key_document.toStdString() );

        if( is_default_query )
        {
            std::string new_schema = dbdocument->getSchemaName();
            if( new_schema != document_schema_name )
            {
                document_schema_name = new_schema;
                emit resetTypeBox( document_schema_name.c_str() );
            }
        }
        std::string inV, outV;
        // update incoming lists
        dbdocument->getValueViaPath<std::string>("_to", inV, "");
        if( !inV.empty() && ( !is_default_query || query_incoming.empty() ) )
        {
            if( change_query_to_id( dbincoming.get(), inV ) )
            {
                //  wait untill query_result_mutex mutex
                dbincoming->currentQueryResult();
                emit changedIncomingModel( inV.c_str() );
            }
        }
        // update outgoing lists
        dbdocument->getValueViaPath<std::string>("_from", outV, "" );
        if( !outV.empty() && ( !is_default_query || query_outgoing.empty() ) )
        {
            if( change_query_to_id( dboutgoing.get(), outV ) )
            {
                //  wait untill query_result_mutex mutex
                dboutgoing->currentQueryResult();
                emit changedOutgoingModel( outV.c_str() );
             }
        }
        std::string jsonDocument = dbdocument->getJson();
        emit openedDocument(  jsonDocument.c_str()  );
    }
    catch( jsonio17::jsonio_exception& e)
    {
        emit isException( "DBEdgeObject::dbReadDocument ", e.what() );
    }
    catch(std::exception& e)
    {
        emit isException( "DBEdgeObject::std::exception ", e.what() );
    }
}

// ??????????? must be test
// Read multiple records from file fileName
void DBEdgeObject::dbRestoreMultipleRecordsfromFile( QString file_name )
{
    if( dbdocument.get() == nullptr )
        return;

    dbdocument->setMode(true);
    DBVertexObject::dbRestoreMultipleRecordsfromFile( file_name );
    dbdocument->setMode(change_schema_mode);
    std::string new_schema = dbdocument->getSchemaName();
    if( new_schema != document_schema_name )
        dbdocument->resetSchema( document_schema_name, false);
}

//----------------------------------------------------------------------

void DBEdgeObject::reset_incoming_outgoing_dbclients()
{
    auto vertexlist = jsonio17::DataBase::getVertexesList();
    if( vertexlist.empty() )
        return;

    query_incoming = jsonio17::DBQueryBase::emptyQuery();
    query_outgoing = jsonio17::DBQueryBase::emptyQuery();
    std::string inVertex = vertexlist[0], outVertex = vertexlist[0];

    jsonio17::DBVertexDocument* vertexdoc = jsonio17::DBVertexDocument::newVertexDocumentQuery(
                uiSettings().database(), inVertex, query_incoming );
    if( vertexdoc == nullptr )
        return;
    vertexdoc->setMode(true);

    if( is_default_query )  // set up fixed query
    {
        std::string inV_val = extractStringField( "_to", current_query.queryString() );
        if( !inV_val.empty() )
        {
            query_incoming = vertexdoc->idQuery( inV_val );
            std::string newschema = vertexdoc->extractSchemaFromId( inV_val );
            if( !newschema.empty() )
            {
                inVertex = newschema;
                ui_logger->info(" ##########  {}", inVertex);
            }
            emit fixIncoming();
        }

        std::string outV_val = extractStringField( "_from", current_query.queryString() );
        if( !outV_val.empty() )
        {
            query_outgoing = vertexdoc->idQuery( outV_val );
            std::string newschema = vertexdoc->extractSchemaFromId( outV_val );
            if( !newschema.empty() )
            {
                outVertex = newschema;
                ui_logger->info(" ##########  {}", outVertex);
            }
            emit fixOutgoing();
        }
        vertexdoc->resetSchema( inVertex, true );
        vertexdoc->setQuery( query_incoming );
    }
    dbincoming.reset( vertexdoc );

    jsonio17::DBVertexDocument* outClient = jsonio17::DBVertexDocument::newVertexDocumentQuery(
                uiSettings().database(), outVertex, query_outgoing );
    dboutgoing.reset( outClient );
    dboutgoing->setMode(true);

    emit changedIncomingDBClient( dbincoming.get() );
    emit changedOutgoingDBClient( dboutgoing.get() );
}


bool DBEdgeObject::change_query_to_id( jsonio17::DBVertexDocument* dbvertex, const std::string& aid_key )
{
    if( dbvertex == nullptr )
        return false;

    std::string schemaName = dbvertex->extractSchemaFromId( aid_key );
    if( !schemaName.empty() && schemaName != dbvertex->getSchemaName() )
    {
        dbvertex->resetSchema(schemaName, true );
        if( !is_default_query )
        {
            dbvertex->updateQuery();
            return true;
        }
    }
    if( is_default_query &&  !schemaName.empty() )
    {
        auto qStr = dbvertex->idQuery( aid_key );
        dbvertex->setQuery( qStr, {} );
        return true;
    }
    return false;
}

void DBEdgeObject::dbChangeOutgoingQuery( QString aschema_name, std::shared_ptr<jsonio17::DBQueryDef> querydef )
{
    if( dboutgoing.get() == nullptr )
    {
        emit finished();
        return;
    }
    auto oldquery = dboutgoing->currentQueryResult().query();
    try
    {
        dboutgoing->resetSchema( aschema_name.toStdString(), true );
        // reset internal query data
        dboutgoing->setQuery( *querydef );
        query_outgoing = *querydef->condition();
    }
    catch(std::exception& e)
    {
        dboutgoing->setQuery( oldquery );
        emit isException( "DBEdgeObject::dbChangeOutgoingQuery ", e.what() );
    }
    // update search tables
    //  wait untill query_result_mutex mutex
    dboutgoing->currentQueryResult();
    emit changedOutgoingModel("");
    emit finished();
}

void DBEdgeObject::dbChangeIncomingQuery( QString aschema_name, std::shared_ptr<jsonio17::DBQueryDef> querydef )
{
    if( dbincoming.get() == nullptr )
    {
        emit finished();
        return;
    }
    auto oldquery = dbincoming->currentQueryResult().query();
    try
    {
        dbincoming->resetSchema( aschema_name.toStdString(), true );
        // reset internal query data
        dbincoming->setQuery( *querydef );
        query_incoming = *querydef->condition();
    }
    catch(std::exception& e)
    {
        dbincoming->setQuery( oldquery );
        emit isException( " DBEdgeObject::dbChangeIncomingQuery ", e.what() );
    }
    // update search tables
    //  wait untill query_result_mutex mutex
    dbincoming->currentQueryResult();
    emit changedIncomingModel("");
    emit finished();
}

void DBEdgeObject::dbReloadQuery()
{
    try
    {
        if( dbincoming.get() != nullptr )
        {
            dbincoming->updateQuery();
            //  wait untill query_result_mutex mutex
            dbincoming->currentQueryResult();
            emit changedIncomingModel("");
        }
        if( dboutgoing.get() != nullptr )
        {
            dboutgoing->updateQuery();
            //  wait untill query_result_mutex mutex
            dboutgoing->currentQueryResult();
            emit changedOutgoingModel("");
        }
        if( dbdocument.get() != nullptr )
        {
            dbdocument->updateQuery();
            // update search tables ( wait untill currentQueryResult mutex )
            emit changedQueryString( dbdocument->currentQueryResult().condition().queryString().c_str() );
            emit changedModel("");
        }
    }
    catch(std::exception& e)
    {
        emit isException( "DBEdgeObject::dbReloadQuery ", e.what() );
    }
    emit finished();
}

} // namespace jsonui17
