#include <QMenu>
#include <QFile>
#include <QApplication>
#include <QClipboard>
#include <QHeaderView>
#include <QKeyEvent>
#include <QLineEdit>
#include <QTextStream>
#include "jsonio17/exceptions.h"
#include "jsonui17/models/table_view.h"
#include "jsonui17/CalcDialog.h"
#include "jsonui17/preferences.h"

namespace jsonui17 {

/// Internal data for selection
class Selection
{
public:

    int N1;
    int N2;
    int M1;
    int M2;

    Selection(int n1, int n2, int m1, int m2 ):
        N1(n1), N2(n2), M1(m1), M2(m2)
    {}

    Selection(const Selection& sel):
        N1(sel.N1), N2(sel.N2), M1(sel.M1), M2(sel.M2)
    {}

};

//-------------------------------------------------------------------------------------
// TMatrixDelegate -  individuak items in views are rendered and edited using delegates
//-------------------------------------------------------------------------------------

// Editing QTreeView for objects in System page
QWidget *MatrixDelegate::createEditor( QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    int type = index.model()->data(index, MatrixModel::TypeRole ).toInt();
    switch( type  )
    {
    case AbstractContainer::ftNumeric:
    {
        QLineEdit *lineEdit = new QLineEdit(parent);
        lineEdit->setValidator( new QIntValidator );
        return lineEdit;
    }
    case AbstractContainer::ftDouble:
    {
        QLineEdit *lineEdit = new QLineEdit(parent);
        lineEdit->setValidator( new QDoubleValidator );
        return lineEdit;
    }
    case AbstractContainer::ftString:
    {
        QLineEdit *lineEdit = new QLineEdit(parent);
        return lineEdit;
    }
    default:
        return QAbstractItemDelegate::createEditor( parent, option,  index );
    }
}

//-------------------------------------------------------------------------------------
// class TMatrixTable implements a table view that displays items from a model.
//-------------------------------------------------------------------------------------

MatrixTable::MatrixTable( QWidget * parent, int mode  ):
    QTableView( parent ), mode_using(mode)
{

    verticalHeader()->setSectionResizeMode( QHeaderView::ResizeToContents );
    horizontalHeader()->setSectionResizeMode( QHeaderView::ResizeToContents );

    //setFocusPolicy(Qt::WheelFocus/*Qt::StrongFocus*/);
    //setTabKeyNavigation( false );

    setVerticalScrollMode( QAbstractItemView::ScrollPerItem );
    setHorizontalScrollMode( QAbstractItemView::ScrollPerItem );
    setEditTriggers( QAbstractItemView::DoubleClicked|QAbstractItemView::AnyKeyPressed );
    setSortingEnabled( mode_using & tbSort );

    setContextMenuPolicy( Qt::CustomContextMenu );
    if( !( mode_using & tbNoMenu ) )
        connect( this, &MatrixTable::customContextMenuRequested, this, &MatrixTable::slotPopupContextMenu);
}

void MatrixTable::makePopupContextMenu(QMenu *menu, QModelIndex index )
{
    QAction* act;

    if( mode_using & tbEdit )
    {
        int type_ = model()->data(index, MatrixModel::TypeRole ).toInt();
        if( type_ == AbstractContainer::ftNumeric || type_ == AbstractContainer::ftDouble  )
        {
            act =  new QAction(tr("&Calculator"), this);
            act->setShortcut(tr("F8"));
            act->setStatusTip(tr("Use calculator  to the specified cells"));
            connect(act, &QAction::triggered, this, &MatrixTable::runCalc);
            menu->addAction(act);
            menu->addSeparator();
        }
    }

    act =  new QAction(tr("Select &row"), this);
    act->setShortcut(tr("Ctrl+R"));
    act->setStatusTip(tr("Select current row"));
    connect(act, &QAction::triggered, this, &MatrixTable::selRow);
    menu->addAction(act);

    act =  new QAction(tr("Select co&lumn"), this);
    act->setShortcut(tr("Ctrl+L"));
    act->setStatusTip(tr("Select current column"));
    connect(act, &QAction::triggered, this, &MatrixTable::selColumn);
    menu->addAction(act);

    act =  new QAction(tr("Select &all"), this);
    act->setShortcut(tr("Ctrl+A"));
    act->setStatusTip(tr("Select all cells"));
    connect(act, &QAction::triggered, this, &MatrixTable::selectAll);
    menu->addAction(act);

    menu->addSeparator();

    act =  new QAction(tr("&Copy"), this);
    act->setShortcut(tr("Ctrl+C"));
    act->setStatusTip(tr("Copy selected cells"));
    connect(act, &QAction::triggered, this, &MatrixTable::copyData);
    menu->addAction(act);

    act =  new QAction(tr("Copy wit&h names"), this);
    act->setShortcut(tr("Ctrl+H"));
    act->setStatusTip(tr("Copy selected header&cells"));
    connect(act, &QAction::triggered, this, &MatrixTable::copyDataHeader);
    menu->addAction(act);

    if( mode_using & tbEdit )
    {
        menu->addSeparator();

        act =  new QAction(tr("C&ut"), this);
        act->setShortcut(tr("Ctrl+U"));
        act->setStatusTip(tr("Cut selected cells"));
        connect(act, &QAction::triggered, this, &MatrixTable::cutData);
        menu->addAction(act);

        act =  new QAction(tr("&Paste"), this);
        act->setShortcut(tr("Ctrl+V"));
        act->setStatusTip(tr("Paste to selected area"));
        connect(act, &QAction::triggered, this, &MatrixTable::pasteData);
        menu->addAction(act);

        act =  new QAction(tr("Paste &transposed"), this);
        act->setShortcut(tr("Ctrl+T"));
        act->setStatusTip(tr("Paste transposed to selected area"));
        connect(act, &QAction::triggered, this, &MatrixTable::pasteTransposedData);
        menu->addAction(act);

        act =  new QAction(tr("&Clear"), this);
        act->setShortcut(tr("Ctrl+Del"));
        act->setStatusTip(tr("Clear selected cells"));
        connect(act, &QAction::triggered, this, &MatrixTable::clearData);
        menu->addAction(act);
    }
}

void MatrixTable::slotPopupContextMenu(const QPoint &pos)
{
    if( mode_using & tbNoMenu )
        return;

    QModelIndex index = indexAt( pos );
    std::shared_ptr<QMenu> menu( new QMenu(this) );
    makePopupContextMenu( menu.get(), index );
    menu->exec( viewport()->mapToGlobal(pos) );
}

void MatrixTable::keyPressEvent(QKeyEvent* e)
{
    if( mode_using & tbNoMenu )
    {
        QTableView::keyPressEvent(e);
        return;
    }

    if ( e->modifiers() & Qt::ControlModifier )
    {
        switch ( e->key() )
        {
        case Qt::Key_R:
            selRow();
            return;
        case Qt::Key_L:
            selColumn();
            return;
        case Qt::Key_A:
            selectAll();
            return;
        case Qt::Key_U:
            cutData();
            return;
        case Qt::Key_C:
            copyData();
            return;
        case Qt::Key_H:
            copyDataHeader();
            return;
        case Qt::Key_V:
            pasteData();
            return;
        case Qt::Key_T:
            pasteTransposedData();
            return;
        case Qt::Key_Delete:
             clearData();
            return;
        }
    }
    switch( e->key() )
    {
    case Qt::Key_F8:
        runCalc();
        return;
    }
    QTableView::keyPressEvent(e);
}


// Calculator on F8 pressed on data field
void MatrixTable::runCalc()
{
    if( !(mode_using & tbEdit) )
        return;

    QString res;
    CalcDialog calc( window() );
    if( calc.exec() )
    {
        foreach( QModelIndex ndx,  selectedIndexes()  )
        {
            res = calc.computeFunctionString( ndx.data(Qt::EditRole).toDouble() );
            model()->setData(ndx, res,  Qt::EditRole);
        }
    }
}

void MatrixTable::selRow()
{
    selectionModel()->select( currentIndex(), QItemSelectionModel::Rows|QItemSelectionModel::ClearAndSelect );
}

void MatrixTable::selColumn()
{
    selectionModel()->select( currentIndex(), QItemSelectionModel::Columns|QItemSelectionModel::ClearAndSelect );
}

void MatrixTable::cutData()
{
    if( !(mode_using & tbEdit) )
        return;
    copyData();
    clearData();
}

void MatrixTable::clearData()
{
    if( !(mode_using & tbEdit) )
        return;
    foreach( QModelIndex ndx,  selectedIndexes()  )
        model()->setData(ndx, ""/*emptiness.c_str()*/,  Qt::EditRole);
}

void MatrixTable::copyData()
{
    Selection sel = get_selection_range();
    QString clipText = create_string( sel, splitCol );
    QApplication::clipboard()->setText( clipText/*, QClipboard::Clipboard*/ );
}

void MatrixTable::copyDataHeader()
{
    Selection sel = get_selection_range();
    QString clipText = create_header(splitCol);
    clipText += create_string( sel, splitCol );
    QApplication::clipboard()->setText( clipText/*, QClipboard::Clipboard*/ );
}

// Save selected data to csv format file
void MatrixTable::saveSelectedToCSV( const std::string& file_name )
{
    Selection sel = get_selection_range();
    QString clipText = create_header(',');
    clipText += create_string( sel, ',' );

    // save data to file
    QFile outFile( file_name.c_str() );
    outFile.open(QIODevice::WriteOnly | QIODevice::Text);

    if( !outFile.isOpen())
    {
        ui_logger->error("{} unable to open for output", file_name);
    }
    else
    {
        QTextStream outStream( &outFile );
        outStream << clipText;
        outFile.close();
    }
}

void MatrixTable::pasteData()
{
    if( !(mode_using & tbEdit) )
        return;
    Selection sel = get_selection_range( true );
    paste_into_area( sel, false, splitCol);
}

void MatrixTable::pasteTransposedData()
{
    if( !(mode_using & tbEdit) )
        return;
    Selection sel = get_selection_range( true );
    paste_into_area( sel, true, splitCol );
}

//------------------------------------------------------------------------

QString MatrixTable::create_header( char split_col_symb )
{
    QString cText;
    QString clipText;
    int col;
    bool frst = true;
    for( col = 0; col < model()->columnCount( rootIndex() ); col++ )
    {
        if( selectionModel()->columnIntersectsSelection( col,  rootIndex() ) )
        {
            if( !frst )
                clipText += split_col_symb;
            frst = false;
            cText = model()->headerData( col, Qt::Horizontal, Qt::DisplayRole ).toString();
            if( cText.isEmpty() /* == emptiness.c_str()*/ )
                cText = "  ";//"\r";
            clipText += cText;
        }
    }
    if( !frst )
        clipText += splitRow;
    return clipText;
}


QString MatrixTable::create_string( Selection& sel, char split_col_symb )
{
    int ii, jj;
    QModelIndex index = currentIndex();
    QString cText;
    QString clipText;

    for(  ii=sel.N1; ii<=sel.N2; ii++ )
    {
        if( ii > sel.N1 )
            clipText += splitRow;

        for( jj=sel.M1; jj<=sel.M2; jj++ )
        {
            QModelIndex wIndex = 	index.sibling( ii, jj );
            // selected all region if( selmodel->isSelected( wIndex ) )
            if( jj > sel.M1 )
                clipText += split_col_symb;
            cText = wIndex.data(Qt::EditRole).toString();
            if( cText.isEmpty() /*cText == emptiness.c_str()*/ )
                cText = "  ";//"\r";
            clipText += cText;
        }
    }
    return clipText;
}

Selection MatrixTable::get_selection_range( bool to_paste )
{
    QModelIndex index = currentIndex();
    if(  !index.isValid() )
        return Selection( 0, 0, 0, 0 );

    if( !to_paste  && !selectionModel()->hasSelection()  )
        return Selection( index.row(), index.row(), index.column(), index.column() );

    int N1=-1, N2=0, M1=-1, M2=0;
    foreach( QModelIndex ndx,  selectedIndexes()  )
    {
        if( N1 == -1 || M1 == -1)
        {
            N1 = ndx.row();
            M1 = ndx.column();
        }
        if( N1 > ndx.row() ) N1 = ndx.row();
        if( N2 < ndx.row() ) N2 = ndx.row();
        if( M1 > ndx.column() ) M1 = ndx.column();
        if( M2 < ndx.column() ) M2 = ndx.column();
    }

    // only one selected => all for end of table
    if( to_paste && ( !selectionModel()->hasSelection() || ( N1==N2 && M1==M2 ) ) )
        return Selection( index.row(), index.model()->rowCount( index )-1,
                          index.column(), index.model()->columnCount( index )-1 );

    return Selection( N1, N2, M1, M2 );
}

void  MatrixTable::set_from_string( char splitrow, const QString& str, Selection sel, bool transpose )
{
    if( str.isEmpty() )
        return;

    QModelIndex wIndex;
#if (QT_VERSION < QT_VERSION_CHECK(5, 15, 0))
    const QStringList rows = str.split(splitrow, QString::KeepEmptyParts);
#else
    const QStringList rows = str.split(splitrow, Qt::KeepEmptyParts);
#endif

    int ii, jj;
    int rowNum = sel.N1;
    const int nLimit = (transpose) ? (sel.N1 + sel.M2-sel.M1) : sel.N2;

    for( int it = 0; it < rows.count() && rowNum <= nLimit; it++, rowNum++)
    {
#if (QT_VERSION < QT_VERSION_CHECK(5, 15, 0))
        const QStringList cells = rows[it].split(splitCol, QString::KeepEmptyParts);
#else
        const QStringList cells = rows[it].split(splitCol, Qt::KeepEmptyParts);
#endif
        int cellNum = sel.M1;
        const int mLimit = (transpose) ? (sel.M1 + sel.N2-sel.N1) : sel.M2;
        for( int cellIt = 0;  cellIt < cells.count() && cellNum <= mLimit; cellIt++, cellNum++)
        {
            QString str_ = cells[ cellIt ];
            str_ = str_.trimmed();  // strip
            if( transpose )
            {
                ii = (cellNum-sel.M1)+sel.N1;
                jj = (rowNum-sel.N1)+sel.M1;
            }
            else
            {
                ii = rowNum;
                jj = cellNum;
            }
            wIndex = currentIndex().sibling( ii, jj );
            model()->setData(wIndex, str_, Qt::EditRole);
        }
    }
}

void MatrixTable::paste_into_area( Selection& sel, bool transpose, char split_col_symb )
{
    QString clipboard = QApplication::clipboard()->text(QClipboard::Clipboard);
    char splitrow = splitRow;

    int lastCR = clipboard.lastIndexOf(splitrow);
    if( lastCR < 0 )
    {
#ifdef __APPLE__
        splitrow = '\n';
#else
        splitrow = '\r';
#endif
        lastCR = clipboard.lastIndexOf(splitrow);
    }
    if( lastCR == clipboard.length() - 1 )
        clipboard.remove(lastCR, 1);
    QString undoString;

    const QStringList rows = clipboard.split(splitrow);
    const int clipN = rows.count();
    const bool largerN = transpose ? (clipN > (sel.M2 - sel.M1 + 1)) : (clipN > (sel.N2 - sel.N1 +1 ));

    int rowNum = sel.N1;
    bool largerM = false;
    for(int it = 0; it < rows.count(); it++, rowNum++)
    {
        int clipM = rows[it].count(split_col_symb) + 1;
        largerM = transpose ? (clipM > (sel.N2 - sel.N1 + 1 )) : (clipM > (sel.M2 - sel.M1 + 1));
        if( largerM )
            break;
    }
    if( largerN || largerM )
    {
        ui_logger->warn("Object paste:  pasting contents has larger dimensions then the object!");
    }
    undoString = create_string( sel, splitCol );
    set_from_string(splitrow, clipboard, sel, transpose);
}

// Get header data
QVector<QString> MatrixTable::headerData() const
{
    QVector< QString > headerD;
    for(int col = 0; col < model()->columnCount(); col++ )
        headerD.push_back(model()->headerData( col, Qt::Horizontal, Qt::DisplayRole ).toString());
    return headerD;
}

// Get table data
QVector<QVector<QVariant>> MatrixTable::tableValues() const
{
    QVector< QVector<QVariant> > valuesD;
    for( int  row =0; row< model()->rowCount(); row++)
    {
        QVector<QVariant> rowData;
        for(int col = 0; col < model()->columnCount(); col++ )
            rowData.push_back( model()->data( model()->index(row, col) ) );
        valuesD.push_back(rowData);
    }
    return valuesD;
}


// -----------------------------------------------

void MatrixTableProxy::setModel(QAbstractItemModel *model)
{
    if( mode_using & MatrixTable::tbSort )
    {
        SortFilterProxyModel *proxyModel = new SortFilterProxyModel();
        proxyModel->setSourceModel( model );
        MatrixTable::setModel(proxyModel);
    }
    else
    {
        MatrixTable::setModel(model);
    }
}

QModelIndex MatrixTableProxy::sourceModelIndex(const QModelIndex &proxyIndex) const
{
    if( mode_using & MatrixTable::tbSort)
    {
        SortFilterProxyModel *proxyModel = dynamic_cast<SortFilterProxyModel *>(model());
        if( proxyModel )
        {
            return proxyModel->mapToSource(proxyIndex);
        }
    }
    return proxyIndex;
}


void MatrixTableProxy::setCurrentRow( int row )
{
    if( model()->rowCount() <= 0 )
        return;
    if(  row > model()->rowCount()  )
        row = 0;

    if( mode_using & MatrixTable::tbSort)
    {
        SortFilterProxyModel *proxyModel = dynamic_cast<SortFilterProxyModel *>(model());
        if( proxyModel )
        {
            setCurrentIndex(  proxyModel->mapFromSource( proxyModel->sourceModel()->index(row,0) ) );
            return;
        }
    }
    setCurrentIndex( model()->index(row,0) );
}

std::size_t MatrixTableProxy::getCurrentRow() const
{
    auto selndx = selectedRow();
    jsonio17::JSONIO_THROW_IF( selndx<0, "models", 21, "undefined single selection" );
    return static_cast<size_t>(selndx);
}

int MatrixTableProxy::selectedRow() const
{
    if( model()->rowCount() <= 0 || !currentIndex().isValid() )
        return -1;

    if( mode_using & MatrixTable::tbSort )
    {
        SortFilterProxyModel *proxyModel = dynamic_cast<SortFilterProxyModel *>(model());
        if( proxyModel )
            return proxyModel->mapToSource(currentIndex()).row();
    }
    return currentIndex().row();
}

void MatrixTableProxy::selectRows(const std::set<std::size_t> &rows)
{
    SortFilterProxyModel *proxyModel = nullptr;
    auto tmodel = model();

    if( mode_using & MatrixTable::tbSort )
    {
        proxyModel = dynamic_cast<SortFilterProxyModel *>(tmodel);
        if( proxyModel )
            tmodel =  proxyModel->sourceModel();
    }

    QItemSelection selitems;
    int startRow=-1, endRow=-1;
    for( int row: rows )
    {
        if( row >= tmodel->rowCount() )
            break;
        if( startRow == -1 )
            startRow = endRow = row;
        else
            if( endRow+1 == row )
                endRow++;
            else
            {
                selitems.merge( QItemSelection(tmodel->index(startRow, 0), tmodel->index(endRow, 0)), QItemSelectionModel::Select);
                startRow = endRow = row;
            }
    }
    if(startRow != -1 )
        selitems.merge(QItemSelection(tmodel->index(startRow, 0), tmodel->index(endRow, 0)), QItemSelectionModel::Select );

    if( ( mode_using & MatrixTable::tbSort ) && proxyModel )
    {
        selitems =  proxyModel->mapSelectionFromSource(selitems);
    }
    selectionModel()->select(selitems, QItemSelectionModel::Rows|QItemSelectionModel::ClearAndSelect );
}

std::set<std::size_t> MatrixTableProxy::allSelectedRows()
{
    std::set<std::size_t> arr;
    QItemSelection selitems = selectionModel()->selection();
    if( mode_using & MatrixTable::tbSort)
    {
        SortFilterProxyModel *proxyModel = dynamic_cast<SortFilterProxyModel *>(model());
        if( proxyModel )
            selitems =  proxyModel->mapSelectionToSource(selitems);
    }
    QModelIndexList selection = selitems.indexes();

    // Multiple rows can be selected
    for(int i=0; i< selection.count(); i++)
    {
        QModelIndex index = selection.at(i);
        if( index.column() == 0 )
            arr.insert( index.row() );
    }
    return arr;
}


void MatrixUniqueSelection::slotSelectionChange(const QItemSelection & selected, const QItemSelection &/*deselected*/)
{
    bool equal;
    std::set<int> testedrows;
    std::set<int> rows;
    auto indexes = selected.indexes();
    for( auto ind: indexes )
        rows.insert( ind.row() );

    QModelIndexList selection = selectionModel()->selectedRows();

    for( auto therow: rows )
    {
        for(int i=0; i< selection.count(); i++)
        {
            QModelIndex index = selection.at(i);
            if( index.row() == therow || testedrows.find(index.row())!= testedrows.end() )
                continue;

            equal = true;
            for( auto colmn: column_to_be_unique )
            {
                if( index.sibling( index.row(),colmn).data() != index.sibling( therow, colmn).data()    )
                {
                    equal = false;
                    break;
               }
            }
            if( equal )
                selectionModel()->select(index, QItemSelectionModel::Rows|QItemSelectionModel::Deselect);
        }
        testedrows.insert(therow);
    }
}

//-----------------------------------------------------------------

void DBKeysTable::keyPressEvent(QKeyEvent* e)
{
    MatrixTable::keyPressEvent(e);
    switch( e->key() )
    {
    case Qt::Key_Up:
    case Qt::Key_Down:
    case Qt::Key_PageUp:
    case Qt::Key_PageDown:
    {
        QModelIndex index = currentIndex();
        if (index.isValid())
        {
            open_function(index);
            setCurrentIndex(index);
        }
    }
        break;
    }
}

void DBKeysTable::CmNext()
{
    QModelIndex index = currentIndex();
    if( !index.isValid() )
    {
        if( model()->rowCount() > 0 )
            index = model()->index(0,0);
    }
    else
    {
        int row = index.row();
        row = std::min( row+1, model()->rowCount()-1 );
        index = index.sibling( row, index.column() );
    }
    if( index.isValid() )
    {
        open_function(index);
        setCurrentIndex(index);
    }
}

void DBKeysTable::CmPrevious()
{
    QModelIndex index = currentIndex();
    if( index.isValid() )
    {
        int row = index.row();
        row = std::max( row-1, 0);
        index = index.sibling(row, index.column());
        open_function( index );
        setCurrentIndex(index);
    }
}

void DBKeysTable::move_to_key( std::string id_key )
{
    if( id_key.empty()  )
        return;

    setColumnHidden( 0, false );  // ??? not hide to edges
    // search `item` text in model
    QModelIndexList Items =  model()->match(  model()->index(0, 0),
                                              Qt::DisplayRole, QString(id_key.c_str()));
    if( !Items.isEmpty() )
    {
        selectionModel()->select( Items.first(), QItemSelectionModel::ClearAndSelect |
                                        QItemSelectionModel::Rows );
        scrollTo( Items.first(), QAbstractItemView::EnsureVisible );
    }
    setColumnHidden( 0, true ); // ??? not hide to edges
}


} // namespace jsonui17

