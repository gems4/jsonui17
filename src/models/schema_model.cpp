#include <QLineEdit>
#include <QComboBox>
#include <QMessageBox>
#include "jsonui17/models/schema_model.h"
#include "jsonui17/SelectDialog.h"
#include "../service/ModelLineDialog.h"
#include "jsonui17/preferences.h"

namespace jsonui17 {

bool JsonSchemaModel::showComments = false;
bool JsonSchemaModel::useEnumNames = false;
bool JsonSchemaModel::editID = false;


JsonSchemaModel::JsonSchemaModel( const std::string& json_string, const std::string& schema_name,
                                  const QStringList& header_names,  QObject* parent ):
    JsonBaseModel(parent), header_data( header_names ),
    currentschema(schema_name),root_node( jsonio17::JsonSchema::object(currentschema))
{
    setupModelData( json_string, currentschema );
    connect( this, SIGNAL(dataChanged(const QModelIndex&, const QModelIndex&)),
             parent,  SLOT(objectChanged()) );
}

jsonio17::JsonBase* JsonSchemaModel::lineFromIndex(const QModelIndex &index ) const
{
    if( index.isValid() )  {
        return static_cast<jsonio17::JsonSchema*>( index.internalPointer() );
    }
    else {
        return const_cast<jsonio17::JsonSchema*>( &root_node );
    }
}

void JsonSchemaModel::setupModelData( const std::string& json_string, const std::string&  schema )
{
    try{
        beginResetModel();
        if( currentschema != schema )
        {
            currentschema = schema;
            root_node = jsonio17::JsonSchema::object(currentschema);
        }
        if( json_string.empty() )
            root_node.clear();
        else
            root_node.loads( json_string );
        endResetModel();
    }
    catch( ... )
    {
        endResetModel();
        throw;
    }
}

QModelIndex JsonSchemaModel::index(int row, int column, const QModelIndex &parent) const
{
    auto parentItem = lineFromIndex( parent );
    if( parentItem->size() > 0 )
        return createIndex( row, column, parentItem->getChild( row ) );
    else
        return QModelIndex();
}

QModelIndex JsonSchemaModel::parent(const QModelIndex& child) const
{
    if( !child.isValid() )
        return QModelIndex();

    auto childItem = lineFromIndex(child);
    auto parentItem = childItem->getParent();
    if( parentItem == &root_node )
        return QModelIndex();
    return createIndex( parentItem->getNdx(), 0, parentItem );
}

int JsonSchemaModel::rowCount( const QModelIndex& parent ) const
{
    if ( !parent.isValid() )
        return root_node.size();
    auto parentItem = lineFromIndex( parent );
    return parentItem->size();
}

int JsonSchemaModel::columnCount( const QModelIndex& parent ) const
{
    Q_UNUSED( parent );
    if( showComments )
        return 3;
    return 2;
}

Qt::ItemFlags JsonSchemaModel::flags( const QModelIndex& index ) const
{
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    auto item = lineFromIndex( index );

    if( !editID && item->getKey()[0] == '_' )
    {
        return (flags & ~Qt::ItemIsEditable);
    }

    if( index.column() == 1 &&  !( item->isObject() || item->isArray() || get_map_enumdef( index ) != nullptr ) )
    {
        flags |= Qt::ItemIsEditable;
        return flags;
    }

    if( index.column() == 0 && !item->isTop() && dynamic_cast<jsonio17::JsonSchema*>(item->getParent())->isMap() )
    {
        flags |= Qt::ItemIsEditable;
        return flags;
    }

    return (flags & ~Qt::ItemIsEditable);
}

QVariant JsonSchemaModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
    if( role == Qt::DisplayRole  && orientation == Qt::Horizontal && section<header_data.size())
        return header_data[section];
    return QVariant();
}


QVariant JsonSchemaModel::data( const QModelIndex& index, int role ) const
{
    if(!index.isValid())
        return QVariant();

    auto node = lineFromIndex( index );
    switch( role )
    {
    case Qt::DisplayRole:
        if( useEnumNames )
        {
            auto enumdef = get_i32_enumdef( index );
            if( enumdef != nullptr )
            {
                int idx;
                node->get_to(idx);
                std::string name = enumdef->value2name(idx);

                if( index.column() == 1 )
                    return  QString( name.c_str() );
                if( index.column() == 2 )
                    return  QString( enumdef->name2description(name).c_str() );
            }
        }
        if( index.column() == 2 )
        {
            auto mapenum = get_map_enumdef( index );
            if( mapenum != nullptr )
            {
                std::string name;
                node->get_to(name);
                return  QString( mapenum->name2description(name).c_str());
            }
        }
        return get_value( index.column(), node );
    case Qt::EditRole:
        return get_value( index.column(), node );
    case Qt::ToolTipRole:
    case Qt::StatusTipRole:
        return  QString( dynamic_cast<jsonio17::JsonSchema*>(node)->getFullDescription().c_str() );
    case Qt::ForegroundRole:
    {
        if( index.column() == 0 && !node->isTop() &&
                !dynamic_cast<jsonio17::JsonSchema*>(node->getParent())->isMap()  )
        {
            return QVariant( QColor( Qt::darkCyan ) );
        }
        //return QVariant( QColor( Qt::black ) );
    }
        break;
    default: break;
    }
    return QVariant();
}

bool JsonSchemaModel::setData( const QModelIndex& index, const QVariant& value, int role)
{
    if( index.isValid() && ( role == Qt::EditRole ) )
    {
        auto line = lineFromIndex( index );
        switch( index.column() )
        {
        case 0:
        {
            // ?????????????
            auto lineschema = dynamic_cast<jsonio17::JsonSchema*>(line);
            lineschema->setMapKey( value.toString().toStdString() );
        }
            break;
        case 1:
            set_value_via_type( line, "",  line->type(), value );
            break;
        default:
            break;
        }
        return true;
    }
    return false;
}

bool JsonSchemaModel::isCanBeResized(const QModelIndex &index) const
{
    auto item = dynamic_cast<jsonio17::JsonSchema*>(lineFromIndex(index));
    return  item->isArray() || item->isMap();
}

bool JsonSchemaModel::isCanBeAdd(const QModelIndex &index) const
{
    auto line = dynamic_cast<jsonio17::JsonSchema*>(lineFromIndex(index));
    if( line->isTop() || ( line->isStruct()  && line->size() < 1 ) )
        return true;
    auto parent_item = dynamic_cast<jsonio17::JsonSchema*>(lineFromIndex(parent(index)));
    return parent_item->isStruct();
}

bool jsonui17::JsonSchemaModel::isUnion(const QModelIndex &index) const
{
    auto item = dynamic_cast<jsonio17::JsonSchema*>(lineFromIndex(index));
    return  item->isUnion();
}

bool jsonui17::JsonSchemaModel::isCanBeRemoved(const QModelIndex &index) const
{
    auto line = lineFromIndex(index);
    return  !line->isTop() && line->getParent()->isObject();
}

bool jsonui17::JsonSchemaModel::isCanBeCloned(const QModelIndex &index) const
{
    auto line = lineFromIndex(index);
    if( line->isTop() )
        return false;
    auto parentIndex = parent(index);
    auto parent_item = dynamic_cast<jsonio17::JsonSchema*>(lineFromIndex(parentIndex));
    return  ( parent_item->isArray() || parent_item->isMap() );
}

void  JsonSchemaModel::setFieldData( const QModelIndex& index, const std::string& data)
{
    auto line =  lineFromIndex(index);
    if( !editID && line->getKey()[0] == '_' )
        return;
    try {
        beginResetModel();
        if( data.empty() )
            line->clear();
        else
            line->loads( data );
        endResetModel();
    }
    catch( ... )
    {
        endResetModel();
        throw;
    }
}

//*****************************************************************

void JsonSchemaModel::resizeArray( QWidget* parentWidget, const QModelIndex& index )
{
    auto line =  lineFromIndex(index);

    ModelLineDialog dlg( this, index, index, line->getKey(), parentWidget );
    if( !dlg.exec() )
        return;
    QVariant new_value = dlg.objectValue();
    auto new_sizes = dlg.arraySizes();

    // message to delete old data
    if(  rowCount( index ) )
    {
        beginRemoveRows( index.siblingAtColumn(0), 0, rowCount( index ) );
        endRemoveRows();
    }
    line->array_resize_xD( new_sizes, new_value.toString().toStdString() );
    // message to add lines
    if( new_sizes[0]>0 )
    {
        beginInsertRows( index.siblingAtColumn(0), 0, static_cast<int>(new_sizes[0])-1 );
        endInsertRows();
    }
}

void JsonSchemaModel::delObject( QWidget* parentWidget, const QModelIndex& index )
{
    auto line = lineFromIndex(index);
    size_t level = 0;
    auto field_data = dynamic_cast<jsonio17::JsonSchema*>(line)->fieldDescription( level );
    if( level==0 && field_data->required() == jsonio17::FieldDef::fld_required   )
    {
        QMessageBox::warning(parentWidget, "Data object to delete",
                             "Required data object cannot be deleted");
        return;
    }

    QString msg = QString("Confirm deletion of %1 data object?").arg( line->getKey().c_str() );
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question( parentWidget->window(), "Data object to delete", msg,
                                   QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No)
        return;

    beginRemoveRows( parent(index), index.row(), index.row());
    line->remove();
    endRemoveRows();
}

void JsonSchemaModel::delObjects( QWidget * parentWidget, const QModelIndex& cindex )
{
    QModelIndex parentIndex= parent(cindex);
    auto parent_item = lineFromIndex(parentIndex);

    // select fields to be deleted
    std::vector<std::string> sch_lst = parent_item->getUsedKeys();
    if( sch_lst.empty() )
        return;
    SelectDialog selDlg( true, parentWidget, "Please, select objects to be deleted", sch_lst );
    if( !selDlg.exec() )
        return;
    auto sel = selDlg.allSelected();
    if( sel.empty() )
        return;

    if( rowCount( parentIndex ) )
    {
        beginRemoveRows( parentIndex, 0, rowCount( parentIndex ) );
        endRemoveRows();
    }
    for( const auto& sel_ndx: sel )
    {
        auto child = parent_item->getChild( sch_lst[sel_ndx] );
        if( child )
            child->remove();
    }
    auto nchildr = parent_item->size();
    if( nchildr > 0 )
    {
        beginInsertRows( parentIndex, 0, nchildr-1 );
        endInsertRows();
    }
}

void JsonSchemaModel::delObjectsUnion( QWidget* parentWidget, const QModelIndex& index )
{
    auto line =  lineFromIndex(index);

    QString msg = QString("Confirm deletion of all alternatives to %1 data object union variant?").arg(
                line->getKey().c_str() );
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(parentWidget, "Data objects to delete", msg,
                                  QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No)
        return;

    // resizeline item
    auto parent_item = line->getParent();
    // remove rows after current
    size_t current_next = index.row()+1;
    if( current_next < parent_item->size()  )
    {
        beginRemoveRows( parent(index), current_next, parent_item->size()-1 );
        for(size_t ii=parent_item->size()-1; ii>=current_next; ii-- )
            parent_item->getChild(ii)->remove();
        endRemoveRows();
    }
    //remove rows before current
    current_next = index.row();
    if( current_next > 0  )
    {
        beginRemoveRows( parent(index), 0, index.row()-1 );
        while( parent_item->size() > 1 )
            parent_item->getChild(0)->remove();
        endRemoveRows();
    }
}

const QModelIndex JsonSchemaModel::addObjects( QWidget * parentWidget, const QModelIndex& cindex )
{
    QModelIndex parentIndex;
    auto line =  lineFromIndex(cindex);
    if( line->isObject() && line->size() < 1 )
    {
        parentIndex = cindex.siblingAtColumn(0);
    }
    else
    {
        parentIndex = parent(cindex).siblingAtColumn(0);
    }
    auto parent_item = dynamic_cast<jsonio17::JsonSchema*>( lineFromIndex(parentIndex) );

    // select fields to be added
    std::vector<std::string> sch_lst = parent_item->getNoUsedKeys();
    if( sch_lst.empty() )
        return cindex;
    SelectDialog selDlg( true, parentWidget, "Please, select objects to be added", sch_lst );
    if( !selDlg.exec() )
        return cindex;
    auto sel = selDlg.allSelected();
    if( sel.empty() )
        return cindex;

    auto schema_def = jsonio17::ioSettings().Schema().getStruct( parent_item->getStructName() );
    if( !schema_def )
        return cindex;

    if( rowCount( parentIndex ) )
    {
        beginRemoveRows( parentIndex, 0, rowCount( parentIndex ) );
        endRemoveRows();
    }

    for( const auto& sel_ndx: sel )
    {
        auto new_key = sch_lst[sel_ndx];
        auto field_def = schema_def->getField( new_key );
        if( !field_def )
            continue;

        set_value_via_type( lineFromIndex(parentIndex), sch_lst[sel_ndx],
                            jsonio17::JsonSchema::fieldtype2basetype( field_def->type(0) ), field_def->defaultValue().c_str() );
    }
    auto nchildr = parent_item->size();
    if( nchildr )
    {
        beginInsertRows( parentIndex, 0, nchildr-1 );
        endInsertRows();
    }
    return index( rowCount( parentIndex )-1, 0, parentIndex);
}

// add object to and of object
const QModelIndex JsonSchemaModel::addObject( QWidget* parentWidget, const QModelIndex& cindex )
{
    QModelIndex parentIndex;
    auto line =  lineFromIndex(cindex);
    if( line->isObject() && line->size() < 1 )
    {
        parentIndex = cindex.siblingAtColumn(0);
    }
    else
    {
        parentIndex = parent(cindex).siblingAtColumn(0);
    }

    auto parent_item =  lineFromIndex(parentIndex);
    // select fields to be added
    std::vector<std::string> sch_lst = dynamic_cast<jsonio17::JsonSchema*>(parent_item)->getNoUsedKeys();
    if( sch_lst.empty() )
        return cindex;

    ModelLineDialog dlg( this, cindex, parentIndex, "", parentWidget);
    if( !dlg.exec() )
        return cindex;

    std::string new_object_key = dlg.objectName();
    jsonio17::JsonBase::Type new_object_type = dlg.objectType();
    QVariant new_object_value = dlg.objectValue();
    auto new_sizes = dlg.arraySizes();

    if( rowCount( parentIndex ) )
    {
        beginRemoveRows( parentIndex, 0, rowCount( parentIndex ) );
        endRemoveRows();
    }
    set_value_via_type( parent_item, new_object_key, new_object_type, new_object_value );
    if( !new_sizes.empty() )
    {
        auto child =parent_item->getChild(new_object_key);
        if( child )
            child->array_resize_xD( new_sizes, new_object_value.toString().toStdString() );
    }
    auto nchildr = parent_item->size();
    if( nchildr )
    {
        beginInsertRows( parentIndex, 0, nchildr-1 );
        endInsertRows();
    }
    return index( rowCount( parentIndex )-1, 0, parentIndex);
}

// add object to and of object
const QModelIndex JsonSchemaModel::cloneObject( QWidget*, const QModelIndex& cindex )
{
    QModelIndex parentIndex = parent(cindex);
    auto line =  lineFromIndex(cindex);

    try{
        auto parent_item = lineFromIndex( parentIndex);
        auto row =  rowCount( parentIndex );
        auto data = line->dump() ;
        // fixed existent keys in map
        auto new_object_key = std::to_string(row);
        auto used_names = parent_item->getUsedKeys();
        while( std::find( used_names.begin(), used_names.end(), new_object_key) != used_names.end() )
             new_object_key += "0";

        beginInsertRows( parentIndex, row, row );
        if( set_value_via_type( parent_item,  new_object_key, line->type(), QString("0") ) &&  !data.empty() )
            parent_item->getChild(row)->loads( data );
        endInsertRows();
        return index( row, 0, parentIndex);
    }
    catch( ... )
    {
        endInsertRows();
        throw;
    }
}


//-------------------------------------------------------------------------------------

QString JsonSchemaModel::get_value( int column, const jsonio17::JsonBase *object ) const
{
    switch( column )
    {
    case 0: return  QString( object->getKey().c_str() );
    case 2: return  QString( object->getDescription().c_str() );
    case 1: return  QString( object->getFieldValue().c_str() );
    }
    return QString("");
}

const jsonio17::EnumDef* JsonSchemaModel::get_map_enumdef( const QModelIndex& index ) const
{
    if ( !index.isValid() ) // isTop
        return nullptr;

    auto parentIndex = parent( index );
    auto parent_item = dynamic_cast<jsonio17::JsonSchema*>( lineFromIndex(parentIndex) );

    if( parent_item->isMap() )
    {
        size_t level = 0;
        auto field_data = parent_item->fieldDescription( level );
        std::string enumName = field_data->className();
        if( !enumName.empty() )
            return jsonio17::ioSettings().Schema().getEnum( enumName );
    }
    return nullptr;
}

const jsonio17::EnumDef* JsonSchemaModel::get_i32_enumdef( const QModelIndex& index) const
{
    if ( !index.isValid() ) // isTop
        return nullptr;

    auto item = dynamic_cast<jsonio17::JsonSchema*>( lineFromIndex(index) );
    if( item->fieldType() != jsonio17::FieldDef::T_I32 )
        return nullptr;
    size_t level = 0;
    auto field_data = item->fieldDescription( level );
    std::string enumName = field_data->className();
    if(  !enumName.empty() )
        return jsonio17::ioSettings().Schema().getEnum( enumName );
    return nullptr;
}


//-------------------------------------------------------------------------------------
// JsonDomDelegate -  individuak items in views are rendered and edited using delegates
//-------------------------------------------------------------------------------------

QWidget *JsonSchemaDelegate::createEditor( QWidget *parent,  const QStyleOptionViewItem &option,
                                           const QModelIndex &index) const
{
    auto model = dynamic_cast<const JsonSchemaModel *>(index.model() );
    auto line =  model->lineFromIndex(index);
    auto node = dynamic_cast<jsonio17::JsonSchema*>(line);

    int type;
    // map key
    if( index.column() == 0 )
        type = node->fieldKeyType();
    else
        type = node->fieldType();

    // if top level map with class - new edit table
    if( index.column() == 0 )
    {
        auto enumdef = model->get_map_enumdef(index);
        if( enumdef != nullptr )
            return enums_to_combobox( parent, enumdef );
    }

    switch( type )
    {
    case jsonio17::FieldDef::T_BOOL:
    {
        QComboBox *accessComboBox = new QComboBox(parent);
        accessComboBox->addItem(tr("false"), tr("false"));
        accessComboBox->addItem(tr("true"), tr("true"));
        return accessComboBox;
    }
    case jsonio17::FieldDef::T_I08:
    case jsonio17::FieldDef::T_I16:
    case jsonio17::FieldDef::T_I32:
    {
        auto enumdef = model->get_i32_enumdef(index);
        if( enumdef != nullptr )
            return enums_to_combobox( parent, enumdef );
    }
        [[fallthrough]];
    case jsonio17::FieldDef::T_I64:
    case jsonio17::FieldDef::T_U64:
    {
        QLineEdit *lineEdit = new QLineEdit(parent);
        QIntValidator *ivalid = new QIntValidator(lineEdit);
        //ivalid->setBottom( static_cast<int>( node->minValue() ));
        //ivalid->setTop( static_cast<int>( node->maxValue() ));
        if( type == jsonio17::FieldDef::T_U64 )
            ivalid->setBottom(0);
        lineEdit->setValidator(ivalid);
        return lineEdit;
    }
    case jsonio17::FieldDef::T_DOUBLE:
    {
        QLineEdit *lineEdit = new QLineEdit(parent);
        QDoubleValidator *dvalid = new QDoubleValidator(lineEdit);
        dvalid->setBottom(node->minValue());
        dvalid->setTop(node->maxValue());
        lineEdit->setValidator(dvalid);
        return lineEdit;
    }
    case jsonio17::FieldDef::T_UTF8:
    case jsonio17::FieldDef::T_STRING:
    {
        QLineEdit *lineEdit = new QLineEdit(parent);
        return lineEdit;
    }
    }
    return QAbstractItemDelegate::createEditor( parent, option,  index );
}

void JsonSchemaDelegate::setEditorData( QWidget *editor,  const QModelIndex &index) const
{
    QComboBox *cellEdit = dynamic_cast<QComboBox*>(editor);
    if( cellEdit)
    {
        int idx = cellEdit->findData(index.data(Qt::EditRole));
        if ( idx != -1 )  // -1 for not found
            cellEdit->setCurrentIndex(idx);
    }
    else
        QItemDelegate::setEditorData(editor, index);
}

void JsonSchemaDelegate::setModelData(QWidget *editor,
                                      QAbstractItemModel *model,
                                      const QModelIndex &index) const
{
    QComboBox *cellEdit = dynamic_cast<QComboBox*>(editor);
    if( cellEdit )
    {
        model->setData(index, cellEdit->currentData(),  Qt::EditRole);
        if( index.column() == 0 )
            model->setData( index.sibling(index.row(), 1), cellEdit->currentText(),  Qt::EditRole);
    }
    else
        QItemDelegate::setModelData(editor, model, index);
}

QWidget* JsonSchemaDelegate::enums_to_combobox( QWidget *parent, const jsonio17::EnumDef * enumdef ) const
{
    auto lst = enumdef->all_names();
    QComboBox *accessComboBox = new QComboBox(parent);
    for( size_t ii=0; ii<lst.size(); ii++ )
    {
        std::string itname = lst[ii];
        accessComboBox->addItem(tr(itname.c_str()), enumdef->name2value(itname) );
        accessComboBox->setItemData( static_cast<int>(ii), enumdef->name2description(itname).c_str(), Qt::ToolTipRole);
    }
    return accessComboBox;
}

} // namespace jsonui17
