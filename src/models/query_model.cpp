#include <QKeyEvent>
#include <QComboBox>
#include <QMenu>
#include <QAction>
#include <QHeaderView>
#include <QLineEdit>
#include <QMessageBox>
#include "jsonio17/jsonschema.h"
#include "jsonio17/jsondump.h"
#include "jsonui17/preferences.h"
#include "jsonui17/QueryWidget.h"
#include "query_model.h"

namespace jsonui17 {

//--------------------------------------------------------------------------------------
//  class TQueryModel
//  class for represents the data set and is responsible for fetchin
//  the data is neaded for viewing and for writing back any changes.
//---------------------------------------------------------------------------------------

QueryModel::QueryModel( QObject* parent ):
    QAbstractItemModel(parent), rootNode(nullptr)
{
    // define edit tree view
    hdData << "key" << "value"  ;
    rootNode.reset( new AQLline(0, "root", AQLline::Top, nullptr ));
    connect( this, SIGNAL(dataChanged(const QModelIndex&, const QModelIndex&)),
             parent,  SLOT(objectChanged()) );
}

void QueryModel::clearModelData()
{
    beginResetModel();
    rootNode.reset( new AQLline(0, "root", AQLline::Top, nullptr ) );
    endResetModel();
}


AQLline *QueryModel::lineFromIndex(const QModelIndex &index) const
{
    if (index.isValid()) {
        return static_cast<AQLline *>(index.internalPointer());
    } else {
        return rootNode.get();
    }
}

QModelIndex QueryModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!rootNode)
        return QModelIndex();
    AQLline *parentItem = lineFromIndex( parent );
    if( parentItem->children.size() > 0 )
        return createIndex(row, column, parentItem->children[row].get());
    else
        return QModelIndex();
}

QModelIndex QueryModel::parent(const QModelIndex& child) const
{
    if (!child.isValid())
        return QModelIndex();

    AQLline *childItem = lineFromIndex(child);
    AQLline *parentItem = childItem->parent;
    if (parentItem == rootNode.get() )
        return QModelIndex();
    return createIndex(parentItem->ndx_in_parent, 0, parentItem);
}

int QueryModel::rowCount( const QModelIndex& parent ) const
{
    if (!rootNode)
        return 0;
    AQLline *parentItem = lineFromIndex( parent );
    return static_cast<int>(parentItem->children.size());
}	

int QueryModel::columnCount( const QModelIndex& /*parent*/ ) const
{
    return 2;
}	

Qt::ItemFlags QueryModel::flags( const QModelIndex& index ) const
{
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    AQLline *item = lineFromIndex( index );
    if( index.column() == 1 &&  item && item->type == AQLline::Field  )
    {
        flags |= Qt::ItemIsEditable;
        return flags;
    }
    else
        return (flags & ~Qt::ItemIsEditable);
}

QVariant QueryModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
    if( role == Qt::DisplayRole  && orientation == Qt::Horizontal )
        return hdData[section];
    return QVariant();
}

QVariant QueryModel::data( const QModelIndex& index, int role ) const
{
    if(!index.isValid())
        return QVariant();

    switch( role )
    {
    case Qt::DisplayRole:
    case Qt::EditRole:
    {
        AQLline *item = lineFromIndex( index );
        if( index.column()== 0 )
            return item->keyname.c_str();
        else
            if( index.column()== 1 )
                return item->value.c_str();
    }
        break;
    case Qt::ForegroundRole:
        if ( index.column() == 0 )
        {
            return QVariant( QColor( Qt::darkCyan ) );
        }
        break;
    default: break;
    }
    return QVariant();
}

bool QueryModel::setData( const QModelIndex& index, const QVariant& value_, int role)
{
    if( index.isValid() && ( role == Qt::EditRole ) )
    {
        if( index.column()== 1 )
        {
            AQLline *line =lineFromIndex(index);
            if( line )
                line->value = value_.toString().toStdString();
        }
        return true;
    }
    return false;
}

bool QueryModel::canLogical(const QModelIndex &index, bool AQL_mode ) const
{
    if( !AQL_mode )
        return false;
    auto item = lineFromIndex( index );
    return( item && ( ( item->children.empty() && item->type==AQLline::Top)   ||
                      item->type==AQLline::LogicalTwo ||
                      (item->children.empty() && item->type==AQLline::LogicalNot) ));
}

bool QueryModel::canOperator(const QModelIndex &index, bool AQL_mode ) const
{
    if( !AQL_mode )
        return false;
    auto item = lineFromIndex( index );
    return( item && ( ( item->children.empty() && item->type==AQLline::Top)
                      || item->type==AQLline::LogicalTwo
                      || ( item->children.empty() && item->type==AQLline::LogicalNot ) ));
}

bool QueryModel::canField(const QModelIndex &index, bool AQL_mode) const
{
    if( !AQL_mode )
        return true;
    auto item = lineFromIndex( index );
    return( item && item->children.empty() && item->type==AQLline::Operator );
}


const QModelIndex QueryModel::addField( const std::string& flds_name, const jsonio17::FieldDef* fld_data,
                                        const QModelIndex& parentIndex )
{
    AQLline *line =  lineFromIndex(parentIndex);
    if(!line)
        return parentIndex;

    int row = static_cast<int>(line->children.size());
    beginInsertRows( parentIndex, row, row );
    AQLline* newline = new AQLline( line->children.size(), flds_name, AQLline::Field, line );
    newline->field_data.field_type = fld_data->type();
    newline->field_data.class_name = fld_data->className();
    endInsertRows();
    return index( row, 0 , parentIndex);
}

const QModelIndex QueryModel::addObject( const std::string& akey, bool isoperator, const QModelIndex& parentIndex )
{
    AQLline *line =  lineFromIndex(parentIndex);
    if(!line)
        return parentIndex;

    int row = static_cast<int>(line->children.size());
    beginInsertRows( parentIndex, row, row );
    auto type = ( isoperator ? AQLline::Operator : ( akey=="NOT" ? AQLline::LogicalNot: AQLline::LogicalTwo ) );
    new AQLline(line->children.size(), akey, type, line );
    endInsertRows();
    return index( row, 0 , parentIndex);
}

void QueryModel::delObject( const QModelIndex& index )
{
    AQLline *line =  lineFromIndex(index);
    if(!line)
        return;

    const QModelIndex parentNdx = parent(index);
    AQLline *line_parent = line->parent;
    beginRemoveRows( parentNdx, index.row(), index.row());
    line_parent->remove_child( line );
    endRemoveRows();
}

jsonio17::DBQueryBase QueryModel::getQuery( jsonio17::DBQueryBase::QType query_type, const std::string& collection )
{
    std::string query_string;

    if( rootNode->children.size()<1 )
        return jsonio17::DBQueryBase(); // all

    if( query_type == jsonio17::DBQueryBase::qTemplate )
        query_string = rootNode->toTemplate();
    else if( query_type == jsonio17::DBQueryBase::qAQL )
        query_string = rootNode->toAQL(collection);
    else  query_type = jsonio17::DBQueryBase::qUndef;

    return jsonio17::DBQueryBase( query_string, query_type );
}

std::string QueryModel::getFILTER()
{
    auto  query_object = jsonio17::JsonFree::object();
    rootNode->toDom( query_object );
    return query_object.dump(true);
}

void QueryModel::setFILTER(const std::string &filter_generator)
{
    if( !filter_generator.empty() )
    {
        auto  query_object = jsonio17::json::loads(filter_generator);
        rootNode->fromDom( query_object );
    }
}


//-------------------------------------------------------------------------------------
// TQueryDelegate -  individual items in views are rendered and edited using delegates
//-------------------------------------------------------------------------------------

QWidget* QueryDelegate::enums_to_combobox( QWidget *parent, const jsonio17::EnumDef * enumdef ) const
{
    auto lst = enumdef->all_names();
    QComboBox *accessComboBox = new QComboBox(parent);
    for( size_t ii=0; ii<lst.size(); ii++ )
    {
        std::string itname = lst[ii];
        accessComboBox->addItem(tr(itname.c_str()), enumdef->name2value(itname) );
        accessComboBox->setItemData( static_cast<int>(ii), enumdef->name2description(itname).c_str(), Qt::ToolTipRole);
    }
    return accessComboBox;
}

// Editing QTreeView for objects in System page
QWidget *QueryDelegate::createEditor( QWidget *parent,
                                      const QStyleOptionViewItem &option,
                                      const QModelIndex &index) const
{
    const QueryModel *themodel = dynamic_cast< const QueryModel *>(index.model() );
    if( !themodel )
        return QAbstractItemDelegate::createEditor( parent, option,  index );

    AQLline *line =  themodel->lineFromIndex(index);
    if( index.column() > 0 )
    {
        switch( line->value_type() )
        {
        case jsonio17::JsonBase::Bool:
        {
            QComboBox *accessComboBox = new QComboBox(parent);
            accessComboBox->addItem(tr("false"), tr("false"));
            accessComboBox->addItem(tr("true"), tr("true"));
            return accessComboBox;
        }
        case jsonio17::JsonBase::Int:
        {
            std::string enumName = line->field_data.class_name;
            if( !enumName.empty() )
            {
                const jsonio17::EnumDef* enumdef = jsonio17::ioSettings().Schema().getEnum( enumName );
                if( enumdef != nullptr )
                    return enums_to_combobox( parent, enumdef );
            }
            QLineEdit *lineEdit = new QLineEdit(parent);
            QIntValidator *ivalid = new QIntValidator(lineEdit);
            if( line->field_data.field_type == jsonio17::FieldDef::T_U64 )
                ivalid->setBottom(0);
            lineEdit->setValidator(ivalid);
            return lineEdit;
        }
        case jsonio17::JsonBase::Double:
        {
            QLineEdit *lineEdit = new QLineEdit(parent);
            QDoubleValidator *dvalid = new QDoubleValidator(lineEdit);
            lineEdit->setValidator(dvalid);
            return lineEdit;
        }
        case jsonio17::JsonBase::String:
        default:
        {
            QLineEdit *lineEdit = new QLineEdit(parent);
            return lineEdit;
        }
        }
    }
    return QAbstractItemDelegate::createEditor( parent, option,  index );
}


//-------------------------------------------------------------------------------------
// class TQueryView implements a tree view that displays items from a model to query.
//-------------------------------------------------------------------------------------

QueryView::QueryView( QWidget * parent ):
    QTreeView( parent )
{
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    header()->setSectionsClickable(true);
    header()->setSectionResizeMode( /*QHeaderView::ResizeToContents*/QHeaderView::Interactive );

    setEditTriggers( QAbstractItemView::DoubleClicked|QAbstractItemView::AnyKeyPressed );
    setFocusPolicy(Qt::StrongFocus);
    setTabKeyNavigation( false );
    setContextMenuPolicy(Qt::CustomContextMenu);
    setSelectionMode( QAbstractItemView::SingleSelection );
    setSelectionBehavior( QAbstractItemView::SelectRows );
    setIndentation( 30 );

    connect( this, &QueryView::customContextMenuRequested,
             this, &QueryView::slotPopupContextMenu);
    connect( header(), &QHeaderView::sectionClicked,
             this, &QueryView::changeCurrent);
}

void QueryView::changeCurrent( int section )
{
    if( !currentIndex().isValid() )
    {
        QModelIndex index = model()->index( 0, section, rootIndex());
        setCurrentIndex(index);
    }
}

void QueryView::slotPopupContextMenu(const QPoint &pos)
{
    QModelIndex index = indexAt( pos );
    QMenu *menu = contextMenu( index);
    if( menu)
    {
        menu->exec( viewport()->mapToGlobal(pos) );
        delete menu;
    }
}

QMenu* QueryView::contextMenu(const QModelIndex &index)
{
    const QueryModel* themodel = view_model();

    if( !themodel )
        return nullptr;

    QMenu *menu = new QMenu(this);
    QAction* act;

    if(  themodel->canField(index, AQL_mode) )
    {
        act =  new QAction(tr("Add &Fieldpath"), this);
        act->setShortcut(tr("Ctr+F"));
        act->setStatusTip(tr("Add the Next Fieldpath"));
        connect(act, &QAction::triggered, this, &QueryView::CmAddField);
        menu->addAction(act);
        menu->addSeparator();
    }

    if(  themodel->canLogical(index, AQL_mode) )
    {
        act =  new QAction(tr("Add '&AND'"), this);
        act->setShortcut(tr("Ctr+A"));
        act->setStatusTip(tr("Add '&&' joined condition"));
        connect(act, &QAction::triggered, this, &QueryView::CmAddAnd);
        menu->addAction(act);

        act =  new QAction(tr("Add '&OR'"), this);
        act->setShortcut(tr("Ctr+O"));
        act->setStatusTip(tr("Add '||' joined condition"));
        connect(act, &QAction::triggered, this, &QueryView::CmAddOr);
        menu->addAction(act);

        act =  new QAction(tr("Add '&NOT'"), this);
        act->setShortcut(tr("Ctr+N"));
        act->setStatusTip(tr("Add '!' condition"));
        connect(act, &QAction::triggered, this, &QueryView::CmAddNot);
        menu->addAction(act);

        menu->addSeparator();
    }

    if(  themodel->canOperator(index, AQL_mode) )
    {
        act =  new QAction(tr("Add '==' operator"), this);
        act->setShortcut(tr("="));
        act->setStatusTip(tr("Value equality comparison operator"));
        connect(act, &QAction::triggered, this, &QueryView::CmAddEq);
        menu->addAction(act);

        act =  new QAction(tr("Add '!=' operator"), this);
        act->setShortcut(tr("!"));
        act->setStatusTip(tr("Value inequality comparison operator"));
        connect(act, &QAction::triggered, this, &QueryView::CmAddNotEq);
        menu->addAction(act);

        act =  new QAction(tr("Add '>' operator"), this);
        act->setShortcut(tr(">"));
        act->setStatusTip(tr("Value greater than"));
        connect(act, &QAction::triggered, this, &QueryView::CmAddGt);
        menu->addAction(act);

        act =  new QAction(tr("Add '>=' operator"), this);
        act->setShortcut(tr("Ctr+G"));
        act->setStatusTip(tr("Value greater than or equal to"));
        connect(act, &QAction::triggered, this, &QueryView::CmAddGte);
        menu->addAction(act);

        act =  new QAction(tr("Add '<' operator"), this);
        act->setShortcut(tr("<"));
        act->setStatusTip(tr("Value lesser than "));
        connect(act, &QAction::triggered, this, &QueryView::CmAddLt);
        menu->addAction(act);

        act =  new QAction(tr("Add '<=' operator"), this);
        act->setShortcut(tr("Ctr+E"));
        act->setStatusTip(tr(" Value lesser than or equal to"));
        connect(act, &QAction::triggered, this, &QueryView::CmAddLte);
        menu->addAction(act);

        menu->addSeparator();

        act =  new QAction(tr("Add 'IN'"), this);
        act->setShortcut(tr("Ctr+I"));
        act->setStatusTip(tr("Test if a value is contained in an array"));
        connect(act, &QAction::triggered, this, &QueryView::CmAddIn);
        menu->addAction(act);

        act =  new QAction(tr("Add 'NOT IN'"), this);
        act->setStatusTip(tr("Test if a value is not contained in an array"));
        connect(act, &QAction::triggered, this, &QueryView::CmAddNin);
        menu->addAction(act);

        act =  new QAction(tr("Add '&LIKE'"), this);
        act->setShortcut(tr("Ctr+L"));
        act->setStatusTip(tr("Tests if a string value matches a pattern"));
        connect(act, &QAction::triggered, this, &QueryView::CmAddLike);
        menu->addAction(act);

        act =  new QAction(tr("Add '=~'"), this);
        act->setShortcut(tr("Ctr+R"));
        act->setStatusTip(tr("Tests if a string value matches a regular expression"));
        connect(act, &QAction::triggered, this, &QueryView::CmAddRegexp);
        menu->addAction(act);

        menu->addSeparator();
        act =  new QAction(tr("Add '!~'"), this);
        act->setStatusTip(tr("Tests if a string value does not match a regular expression"));
        connect(act, &QAction::triggered, this, &QueryView::CmAddNotRegexp);
        menu->addAction(act);

        menu->addSeparator();
    }

    act =  new QAction(tr("Remove current line"), this);
    act->setShortcut(tr("F8"));
    act->setStatusTip(tr("Delete current line from object"));
    connect(act, &QAction::triggered, this, &QueryView::CmDelLine);
    menu->addAction(act);

    return menu;
}

void QueryView::keyPressEvent(QKeyEvent* e)
{
    if ( e->modifiers() & Qt::ControlModifier )
    {
        switch ( e->key() )
        {
        case Qt::Key_F:
            CmAddField();
            return;

        case Qt::Key_A:
            CmAddAnd();
            return;
        case Qt::Key_O:
            CmAddOr();
            return;
        case Qt::Key_N:
            CmAddNot();
            return;

        case Qt::Key_Equal:
            CmAddEq();
            return;
        case Qt::Key_Exclam:
            CmAddNotEq();
            return;
        case Qt::Key_Greater:
            CmAddGt();
            return;
        case Qt::Key_G:
            CmAddGte();
            return;
        case Qt::Key_Less:
            CmAddLt();
            return;
        case Qt::Key_E:
            CmAddLte();
            return;

        case Qt::Key_I:
            CmAddIn();
            return;
        case Qt::Key_L:
            CmAddLike();
            return;
        case Qt::Key_R:
            CmAddRegexp();
            return;
        }
    }

    switch( e->key() )
    {
    case Qt::Key_F8:
        CmDelLine();
        return;
    }
    QTreeView::keyPressEvent(e);
}

void QueryView::addObject( const std::string& akey, bool isoperator )
{
    QueryModel* themodel = view_model();

    try {
        QModelIndex index = currentIndex();
        if( !index.isValid())
            index = rootIndex();
        if( themodel && ( ( !isoperator  && themodel->canLogical(index, AQL_mode) ) ||
                          ( isoperator  && themodel->canOperator(index, AQL_mode) ) ))
        {
            QModelIndex ndx = themodel->addObject( akey, isoperator, index);
            selectionModel()->setCurrentIndex( ndx, QItemSelectionModel::ClearAndSelect );
        }
        else
            QMessageBox::information( window(), "Add to FILTER",
                                      "Could not execute action." );
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( window(), "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( window(), "std::exception", e.what() );
    }
}

void QueryView::CmAddField()
{
    QueryModel* themodel = view_model();

    try {
        QModelIndex index = currentIndex();
        if( !index.isValid() || !AQL_mode )
            index = rootIndex();
        if( themodel && themodel->canField(index, AQL_mode) )
        {
            QueryWidget* parent_dlg = dynamic_cast<QueryWidget*>(window());
            std::string fldpath;
            const jsonio17::FieldDef* flddata=nullptr;
            parent_dlg->select_line(fldpath, &flddata);

            QModelIndex ndx = themodel->addField( fldpath, flddata, index);
            selectionModel()->setCurrentIndex( ndx, QItemSelectionModel::ClearAndSelect );
        }
        else
            QMessageBox::information( window(), "Add to FILTER",
                                      "Could not execute action." );
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( window(), "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( window(), "std::exception", e.what() );
    }
}


void QueryView::CmAddObjects( const std::vector<std::string>& fields_list,
                              const std::vector<const jsonio17::FieldDef*>& fields_types )
{
    QueryModel* themodel = view_model();

    try {
        QModelIndex index = rootIndex();
        if( themodel && themodel->canField(index, AQL_mode) )
        {
            QModelIndex ndx = index;
            for(size_t ii=0; ii<fields_list.size(); ii++)
                ndx = themodel->addField( fields_list[ii], fields_types[ii], index);
            selectionModel()->setCurrentIndex( ndx, QItemSelectionModel::ClearAndSelect );
        }
        else
            QMessageBox::information( window(), "Add to FILTER", "Could not execute action." );
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( window(), "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( window(), "std::exception", e.what() );
    }
}

void QueryView::CmDelLine()
{
    QModelIndex index = currentIndex();
    auto themodel = dynamic_cast<const QueryModel*>(index.model());
    try {
        if(  themodel )
        {
            selectionModel()->clear();
            collapse(index);
            const_cast<QueryModel*>(themodel)->delObject( index );
        } else
            QMessageBox::information( window(), "Delete from Object",
                                      "Please, determine the key-value pair to be deleted" );
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( window(), "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( window(), "std::exception", e.what() );
    }
}


//-----------------------------------------------------------------

AQLline::AQLline( size_t andx, const std::string& akey, AQLline::fieldType atype, AQLline* parentline ):
    type( atype ), keyname( akey), value( "" ), ndx_in_parent( andx )
{
    //type = typeFromKey( keyname );
    if( !parentline )
        type = Top;
    parent = parentline;
    if(  parent )
    {
        parent->children.push_back( std::shared_ptr<AQLline>(this) );
        field_data = parent->field_data;
    }
}


AQLline::~AQLline()
{
    children.clear();
}

jsonio17::JsonBase::Type AQLline::value_type()
{
    auto jstype = jsonio17::JsonSchema::fieldtype2basetype( field_data.field_type );
    if( parent && parent->type == AQLline::Operator &&
            ( parent->keyname == "IN" || parent->keyname == "NOT IN" ) )
        jstype = jsonio17::JsonBase::Array;

    if( parent && parent->type == AQLline::Operator &&
            ( parent->keyname == "LIKE" || parent->keyname == "=~" || parent->keyname == "!~" ) )
        jstype = jsonio17::JsonBase::String;

    return jstype;
}

void AQLline::value2dom(  jsonio17::JsonBase& object )
{
    std::string add_key = keyname;
    auto jstype = value_type();

    switch( jstype )
    {
    case jsonio17::JsonBase::Null:
        object.set_value_via_path( keyname, "" );
        object.getChild(keyname)->set_null();
        break;
    case jsonio17::JsonBase::Bool:
        object.set_value_via_path( keyname,  ( value == "true"? true : false) );
        break;
    case jsonio17::JsonBase::Int:
    {
        int ivalue=0;
        if( !field_data.class_name.empty() )
        {
            const jsonio17::EnumDef* enumdef = jsonio17::ioSettings().Schema().getEnum( field_data.class_name );
            if( enumdef != nullptr )
                ivalue = enumdef->name2value( value );
            object.set_value_via_path( keyname, ivalue );
        }
        if( !get_value(ivalue) )
            object.set_value_via_path( keyname, ivalue );
    }
        break;
    case jsonio17::JsonBase::Double:
    {
        int dvalue;
        if( !get_value(dvalue) )
            object.set_value_via_path( keyname, dvalue );
    }
        break;
    case jsonio17::JsonBase::String:
        object.set_value_via_path( keyname, value );
        break;
    case jsonio17::JsonBase::Object:
        object.add_object_via_path( keyname ).loads(value);
        break;
    case jsonio17::JsonBase::Array:
        object.add_array_via_path( keyname ).loads(value);
        break;
    }
}

void AQLline::query2dom( jsonio17::JsonBase& object )
{
    switch (type)
    {
    // impotant datatypes
    case Top:
        object.clear();
        for(size_t ii1=0; ii1< children.size(); ii1++ )
            children[ii1]->query2dom( object );
        break;
    case Field:
        if(!value.empty())
        {
            value2dom( object );
        }
        break;
    default: break;
    }
}

std::string AQLline::toTemplate()
{
    auto  dom_data = jsonio17::JsonFree::object();
    query2dom( dom_data );
    return dom_data.dump();
}


std::string AQLline::value2AQL(  const std::string& the_operator  )
{
    auto jstype = value_type();
    std::string filter( "u." + keyname + " " + the_operator + " ");

    switch (jstype)
    {
    case jsonio17::JsonBase::String:
    {
        auto str = jsonio17::json::dump( value );
        //filter += "'"+str+"'";
        filter += str;
    }
        break;
    case jsonio17::JsonBase::Int:
    {
        int val=0;
        if( !field_data.class_name.empty() )
        {
            const jsonio17::EnumDef* enumdef = jsonio17::ioSettings().Schema().getEnum( field_data.class_name );
            if( enumdef != nullptr )
                val = enumdef->name2value( value );
            filter += std::to_string(val);
        }
        else
            filter += value;
    }
        break;
    default:
        filter += value;
        break;
    }
    filter += " ";
    return filter;
}


std::string AQLline::query2AQL()
{
    std::string filter;

    switch (type)
    {
    case Top:
        for(size_t ii=0; ii< children.size(); ii++ )
            filter += children[ii]->query2AQL();
        break;
    case LogicalTwo:
    {
        filter += "( ";
        for( size_t ii=0; ii< children.size(); ii++ )
        {
            if( ii > 0)
                filter += " "+ keyname +" ";
            filter += children[ii]->query2AQL();
        }
        filter += " )";
    }
        break;
    case LogicalNot:
        if( !children.empty())
        {
            filter += " "+ keyname + "  ";
            filter += children[0]->query2AQL();
            filter += " ";
        }
        break;
    case Operator:
        if( !children.empty() && children[0]->type == Field )
        {
            filter += " ";
            filter += children[0]->value2AQL( keyname );
            filter += " ";
        }
        break;
    case Field:
    {
        filter += value2AQL( "==" );
    }
        break;
    }
    return filter;
}

std::string AQLline::toAQL( const std::string& collection  )
{
    std::string aql_query("FOR u IN " );
    aql_query += collection;
    aql_query += "\nFILTER ";
    aql_query += query2AQL();
    aql_query += "\nRETURN u ";
    return aql_query;
}

void AQLline::toDom( jsonio17::JsonBase& object ) const
{
    object.set_value_via_path<int>( "type",  type);
    object.set_value_via_path( "keyname",  keyname);
    object.set_value_via_path( "value",  value );
    if( type == AQLline::Field )
    {
        object.set_value_via_path<int>( "thrifttype", field_data.field_type);
        object.set_value_via_path( "enumname",  field_data.class_name );
    }
    auto& arr = object.add_array_via_path( "children" );
    for( size_t ii=0; ii< children.size(); ii++ )
    {
        auto& obj = arr.add_object_via_path( std::to_string(ii) );
        children[ii]->toDom(obj);
    }
}

void AQLline::fromDom( const jsonio17::JsonBase& object )
{
    children.clear();
    int atype = Operator;
    object.get_value_via_path<int>( "type",  atype, atype );
    object.get_value_via_path<std::string>(  "keyname",  keyname, "" );
    object.get_value_via_path<std::string>( "value",  value, "" );
    type = static_cast<AQLline::fieldType>(atype);
    if( type == AQLline::Field )
    {
        object.get_value_via_path<int>( "thrifttype", atype, 11 );
        field_data.field_type = static_cast<jsonio17::FieldDef::FieldType>(atype);
        object.get_value_via_path<std::string>( "enumname",  field_data.class_name, ""  );
    }
    auto arr  = object.field( "children" );
    if(arr == nullptr)
        return;

    for(size_t ii=0; ii<arr->size(); ii++)
    {
        auto newline = new AQLline( children.size(), "undef", AQLline::Field, this );
        newline->fromDom( arr->child(ii) );
    }
}

bool AQLline::remove_child( AQLline* child )
{
    int thisndx = -1;
    for(std::size_t ii=0; ii< children.size(); ii++ )
    {
        if( children[ii].get() == child )
            thisndx = static_cast<int>(ii);
        if( thisndx >= 0 )
        {
            children[ii]->ndx_in_parent--;
        }
    }
    if( thisndx >= 0 )
    {
        children.erase(children.begin() + thisndx);
        return true;
    }
    return false;
}

} // namespace jsonui17

