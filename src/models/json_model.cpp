#include <QLineEdit>
#include <QComboBox>
#include <QMessageBox>
#include "jsonui17/models/json_model.h"
#include "jsonui17/SelectDialog.h"
#include "../service/ModelLineDialog.h"

namespace jsonui17 {


JsonFreeModel::JsonFreeModel( const std::string& json_string, const QStringList& header_names,  QObject* parent ):
    JsonBaseModel(parent), header_data( header_names ), root_node( jsonio17::JsonFree::object())
{
    setupModelData( json_string, "" );
    connect( this, SIGNAL( dataChanged( const QModelIndex&, const QModelIndex& ) ),
             parent,  SLOT( objectChanged()) );
}


jsonio17::JsonBase* JsonFreeModel::lineFromIndex(const QModelIndex &index) const
{
    if( index.isValid() )  {
        return static_cast<jsonio17::JsonFree*>( index.internalPointer() );
    }
    else {
        return const_cast<jsonio17::JsonFree*>( &root_node );
    }
}

void JsonFreeModel::setupModelData( const std::string& json_string, const std::string&   )
{
    try{
        beginResetModel();
        if( json_string.empty() )
            root_node.clear();
        else
            root_node.loads( json_string );
        endResetModel();
    }
    catch( ... )
    {
        endResetModel();
        throw;
    }
}

QModelIndex JsonFreeModel::index( int row, int column, const QModelIndex &parent ) const
{
    auto parentItem = lineFromIndex( parent );
    if( parentItem->size() > 0 )
        return createIndex( row, column, parentItem->getChild( row ) );
    else
        return QModelIndex();
}

QModelIndex JsonFreeModel::parent(const QModelIndex& child) const
{
    if( !child.isValid() )
        return QModelIndex();

    auto childItem = lineFromIndex(child);
    auto parentItem = childItem->getParent();
    if( parentItem == &root_node )
        return QModelIndex();
    return createIndex( parentItem->getNdx(), 0, parentItem );
}

int JsonFreeModel::rowCount( const QModelIndex& parent ) const
{
    if ( !parent.isValid() )
        return root_node.size();
    auto parentItem = lineFromIndex( parent );
    return parentItem->size();
}

int JsonFreeModel::columnCount( const QModelIndex& parent ) const
{
    Q_UNUSED( parent );
    return 2;
}

Qt::ItemFlags JsonFreeModel::flags( const QModelIndex& index ) const
{
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    auto item = lineFromIndex( index );
    if( index.column() == 1 && ( !item->isObject() && !item->isArray() ) )
    {
        flags |= Qt::ItemIsEditable;
        return flags;
    }
    else
        return (flags & ~Qt::ItemIsEditable);
}

QVariant JsonFreeModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
    if( role == Qt::DisplayRole  && orientation == Qt::Horizontal && section< header_data.size() )
        return header_data[section];
    return QVariant();
}

QVariant JsonFreeModel::data( const QModelIndex& index, int role ) const
{
    if( !index.isValid() )
        return QVariant();

    switch( role )
    {
    case Qt::DisplayRole:
    case Qt::EditRole:
        {
        auto item = lineFromIndex( index );
        if( index.column() == 0 )
            return item->getKey().c_str();
        else
            if( index.column() == 1 )
                return item->getFieldValue().c_str();
        }
        break;
    case Qt::ToolTipRole:
    case Qt::StatusTipRole:
        return lineFromIndex( index )->getDescription().c_str();
    case Qt::ForegroundRole:
        if( index.column() == 0 )
        {
            return QVariant( QColor( Qt::darkCyan ) );
        }
        break;
    default: break;
    }
    return QVariant();
}

bool JsonFreeModel::setData( const QModelIndex& index, const QVariant& value, int role )
{
    if( index.isValid() && ( role == Qt::EditRole ) )
    {
        if( index.column() == 1 )
        {
            auto line = lineFromIndex( index );
            set_value_via_type( line, "",  line->type(), value );
        }
        return true;
    }
    return false;
}

//*****************************************************************

void JsonFreeModel::resizeArray( QWidget* parentWidget, const QModelIndex& index )
{
    auto line =  lineFromIndex(index);

    ModelLineDialog dlg( this, index, index, line->getKey(), parentWidget);
    if( !dlg.exec() )
        return;
    QVariant new_value = dlg.objectValue();
    std::vector<size_t> new_sizes = dlg.arraySizes();

    // message to delete old data
    if(  rowCount( index ) )
    {
        beginRemoveRows( index.siblingAtColumn(0), 0, rowCount( index ) );
        endRemoveRows();
    }
    line->array_resize_xD( new_sizes, new_value.toString().toStdString() );
    // message to add lines
    if( new_sizes[0]>0 )
    {
        beginInsertRows( index.siblingAtColumn(0), 0, new_sizes[0]-1 );
        endInsertRows();
    }
}

void JsonFreeModel::delObject( QWidget* parentWidget, const QModelIndex& index )
{
    auto line =  lineFromIndex(index);
    QString msg = QString("Confirm deletion of %1 data object?").arg( line->getKey().c_str() );
    QMessageBox::StandardButton reply;

    reply = QMessageBox::question( parentWidget->window(), "Data object to delete", msg,
                                   QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No)
        return;

    beginRemoveRows( parent(index), index.row(), index.row() );
    line->remove();
    endRemoveRows();
}


void JsonFreeModel::delObjects( QWidget * parentWidget, const QModelIndex& cindex )
{
    QModelIndex parentIndex= parent(cindex);
    auto parent_item = lineFromIndex(parentIndex);

    // select fields to be added
    std::vector<std::size_t> sel;
    std::vector<std::string> sch_lst = parent_item->getUsedKeys();
    if( sch_lst.empty() )
        return;
    SelectDialog selDlg( true, parentWidget, "Please, select objects to be deleted", sch_lst );
    if( !selDlg.exec() )
        return;
    selDlg.getSelection( sel );

    for( std::size_t ii=sel.size(); ii>0; ii-- )
    {
        auto child = parent_item->getChild( sch_lst[sel[ii-1]] );
        int rowa = child->getNdx();
        try {
            beginRemoveRows( parentIndex, rowa, rowa);
            child->remove();
            endRemoveRows();
        }
        catch( ... )
        {
            endRemoveRows();
            throw;
        }
    }
}

const QModelIndex JsonFreeModel::addObjects( QWidget * parentWidget, const QModelIndex& cindex )
{
    return addObject( parentWidget, cindex );
}

// add object to and of object
const QModelIndex JsonFreeModel::addObject( QWidget* parentWidget, const QModelIndex& cindex )
{
    int row;
    QModelIndex parentIndex;
    auto line =  lineFromIndex(cindex);
    if( line->isObject() && line->size() < 1 )
    {
        parentIndex = cindex.siblingAtColumn(0);
        row = 0;
    }
    else
    {
        parentIndex = parent(cindex).siblingAtColumn(0);
        row =  rowCount( parentIndex );
    }

    ModelLineDialog dlg( this, cindex, parentIndex, "", parentWidget);
    if( !dlg.exec() )
        return cindex;

    std::string new_object_key = dlg.objectName();
    jsonio17::JsonBase::Type new_object_type = dlg.objectType();
    QVariant new_object_value = dlg.objectValue();
    if( new_object_key.empty() )
         jsonio17::JSONIO_THROW( "models", 24, " can't add empty key " );

    auto parent_item =  lineFromIndex(parentIndex);
    auto used_names = parent_item->getUsedKeys();
    if( std::find( used_names.begin(), used_names.end(), new_object_key) != used_names.end() )
         jsonio17::JSONIO_THROW( "models", 23, " can't add existing key "+ new_object_key );

    try{
        // start insert row
        beginInsertRows( parentIndex, row, row );
        set_value_via_type( parent_item, new_object_key, new_object_type, new_object_value );
        endInsertRows();
    }
    catch( ... )
    {
        endInsertRows();
        throw;
    }
    return index( row, 0, parentIndex);
}

// add object to and of object
const QModelIndex JsonFreeModel::cloneObject( QWidget*, const QModelIndex& cindex )
{
    QModelIndex parentIndex = parent(cindex);
    auto line =  lineFromIndex(cindex);

    auto parent =  lineFromIndex(parentIndex);
    if( !parent->isArray() )
        return cindex;
    int row =  rowCount( parentIndex );
    auto data = line->dump() ;
    try{
        beginInsertRows( parentIndex, row, row );
        if( set_value_via_type( parent,  std::to_string(row), line->type(), QString("0")/*jsonio17::JsonBase::String, QString()*/ ) &&  !data.empty() )
            parent->getChild(row)->loads( data ); // the exist name -> error
        endInsertRows();
        return index( row, 0, parentIndex);
    }
    catch( ... )
    {
        endInsertRows();
        throw;
    }
}

//-------------------------------------------------------------------------------------
// TJsonDomDelegate -  individuak items in views are rendered and edited using delegates
//-------------------------------------------------------------------------------------

QWidget *JsonFreeDelegate::createEditor( QWidget *parent, const QStyleOptionViewItem &option,
                                         const QModelIndex &index) const
{
    if( index.isValid() )
    {
        auto themodel = dynamic_cast<const JsonFreeModel*>( index.model() );
        auto line =  themodel->lineFromIndex(index);
        if( index.column() > 0 )
        {
            switch( line->type()  )
            {
            case jsonio17::JsonBase::Bool:
            {
                QComboBox *accessComboBox = new QComboBox(parent);
                accessComboBox->addItem(tr("false"));
                accessComboBox->addItem(tr("true"));
                return accessComboBox;
            }
            case jsonio17::JsonBase::Int:
            {
                QLineEdit *lineEdit = new QLineEdit(parent);
                //lineEdit->setValidator(new QIntValidator(lineEdit));
                return lineEdit;
            }
            case jsonio17::JsonBase::Double:
            {
                QLineEdit *lineEdit = new QLineEdit(parent);
                //lineEdit->setValidator(new QDoubleValidator(lineEdit));
                return lineEdit;
            }
            case jsonio17::JsonBase::Null:
            case jsonio17::JsonBase::String:
            {
                QLineEdit *lineEdit = new QLineEdit(parent);
                return lineEdit;
            }
                // main constructions
            case jsonio17::JsonBase::Object:
            case jsonio17::JsonBase::Array:
            default:
                jsonio17::JSONIO_THROW( "models", 22, " can't edit type "+ std::string( line->typeName() ) );
            }
        }
    }
    return QAbstractItemDelegate::createEditor( parent, option,  index );
}


} // namespace jsonui17
