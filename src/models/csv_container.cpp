#include "jsonui17/models/csv_container.h"

namespace jsonui17 {

void removeComments( QString& valCsv );

void removeComments( QString& valCsv )
{
    auto foundStart = valCsv.indexOf('#');
    int foundEnd;
    while( foundStart >= 0 )
    {
        foundEnd = valCsv.indexOf('\n');
        if( foundEnd > 0 )
            valCsv.remove( foundStart, foundEnd-foundStart+1 );
        else
        {
            valCsv.remove( foundStart, valCsv.size()-foundStart+1 );
            break;
        }
        foundStart = valCsv.indexOf('#', foundStart );
    }
}


// Set std::string to internal data
void CSVContainer::matrix_from_csv_string( const QString& value_csv )
{
    int ii, jj, nlines;

    // clear old data
    col_heads.clear();
    matrix.clear();
    col_double.clear();
    x_colms.clear();
    y_colms.clear();

    if( value_csv.isEmpty() )
        return;

#if (QT_VERSION < QT_VERSION_CHECK(5, 15, 0))
    // split rows
    const QStringList rows = value_csv.split('\n', QString::KeepEmptyParts);
    // get header
    QStringList cells = rows[0].split(',', QString::KeepEmptyParts);
#else
    // split rows
    const QStringList rows = value_csv.split('\n', Qt::KeepEmptyParts);
    // get header
    QStringList cells = rows[0].split(',', Qt::KeepEmptyParts);
#endif

    for( ii=0; ii< cells.count(); ii++ )
    {
        col_heads.push_back( cells[ii] );
        col_double.push_back( true );  //number
    }

    // get values
    bool ok;
    for( jj=1, nlines=0;  jj<rows.count(); jj++ )
    {
#if (QT_VERSION < QT_VERSION_CHECK(5, 15, 0))
        cells = rows[jj].split(',', QString::KeepEmptyParts);
#else
        cells = rows[jj].split(',', Qt::KeepEmptyParts);
#endif
        if( cells.count() < col_heads.size() )
            continue;
        QVector<QVariant> vec;
        matrix.push_back( vec );

        for( ii=0; ii< cells.count(); ii++ )
        {
            if( !cells.empty() ||  col_double[ii] )
            {
                cells[ii].toDouble(&ok);
                if( !ok )
                    col_double[ii] = false;
            }
            matrix[nlines].push_back( cells[ii] );
        }
        nlines++;
    }
}

QString CSVContainer::matrix_to_csv_string( )
{
    QString valCsv;

    if( col_heads.size() == 0 )
        return "";

    QVectorIterator<QString> head( col_heads );
    while( head.hasNext() )
    {
        valCsv += head.next();
        if(head.hasNext() )
            valCsv += ",";
    }
    valCsv += "\n";

    QVectorIterator<QVector<QVariant> > line( matrix );
    while (line.hasNext())
    {
        int col =0;
        QVectorIterator<QVariant> valC(line.next());
        while( valC.hasNext() )
        {
            QVariant val = valC.next();
            //if( col  >= numberStringColumns )
            //    valCsv += ValToString(val.toDouble(), 12);
            //  else
            valCsv += val.toString();
            if(valC.hasNext() )
                valCsv += ",";
            col++;
        }
        valCsv += "\n";
    }

    return valCsv;
}


} // namespace jsonui17
