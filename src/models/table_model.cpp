#include "jsonui17/models/table_model.h"

namespace jsonui17 {

QVariant MatrixModel::data( const QModelIndex& index, int role ) const
{
    if(!index.isValid())
        return QVariant();

    switch( role )
    {
    case TypeRole:
        return data_container->getType( index.row(), index.column() );
    case Qt::DisplayRole:
    case Qt::EditRole:
        return data_container->data( index.row(), index.column() );
    case Qt::ToolTipRole:
    case Qt::StatusTipRole:
        return data_container->getToolTip( index.row(), index.column() );
    case  Qt::ForegroundRole:
        if( data_container->useColor() )
            return data_container->getColor( index.row(), index.column()  );
        break;
    case Qt::TextAlignmentRole:
    {
        auto atype = data_container->getType( index.row(), index.column() );
        if( atype == AbstractContainer::ftNumeric || atype == AbstractContainer::ftDouble )
            return int(Qt::AlignRight | Qt::AlignVCenter);
        else
            return int(Qt::AlignLeft | Qt::AlignVCenter);
    }
    case  Qt::SizeHintRole:
    default: break;
    }
    return QVariant();
}

bool MatrixModel::setData( const QModelIndex &index, const QVariant& value, int role)
{
    if( index.isValid() && ( role == Qt::EditRole ) )
    {
        data_container->setData( index.row(), index.column(), value );
        emit dataChanged(index, index);
        return true;
    }
    return false;
}

QVariant MatrixModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
    switch( role )
    { case Qt::DisplayRole:
        if( orientation == Qt::Horizontal )
            return data_container->headerData( section );
        else
            return QVariant(section);

    case Qt::TextAlignmentRole:
        return int(Qt::AlignRight | Qt::AlignVCenter);
    default:
        break;
    }

    return QVariant();
}

Qt::ItemFlags MatrixModel::flags( const QModelIndex & index ) const
{
    Qt::ItemFlags flags = QAbstractTableModel::flags(index);
    if( data_container->isEditable( index.row(), index.column() )  )
    {
        flags |= Qt::ItemIsEditable;
        return flags;
    }
    return (flags & ~Qt::ItemIsEditable);
}

// internal part
void MatrixModel::resetMatrixData()
{
    beginResetModel();
    data_container->resetData();
    endResetModel();
}

//------------------------------------------------------------------------------

QVariant StringVectorModel::data(const QModelIndex &index, int role) const
{
    if( index.row() < 0 || index.row() >= internal_lst.size() )
        return QVariant();

    if( role == Qt::DisplayRole || role == Qt::EditRole )
        return internal_lst[ index.row() ];

    return QVariant();
}

bool StringVectorModel::setData( const QModelIndex &index, const QVariant &value, int role )
{
    if( index.row() >= 0 && index.row() < internal_lst.size()
        && (role == Qt::EditRole || role == Qt::DisplayRole))
    {
        internal_lst[ index.row() ] = value.toString();
        return true;
    }
    return false;
}

void StringVectorModel::setStringList( const jsonio17::values_t& stdvector )
{
    beginResetModel();
    from_values_t( stdvector );
    endResetModel();
}

void StringVectorModel::from_values_t( const jsonio17::values_t& stdvector )
{
    internal_lst.clear();
    for( const auto& row: stdvector )
        internal_lst.append(QString(row.c_str()));
}

jsonio17::values_t StringVectorModel::to_values_t() const
{
    jsonio17::values_t tmp;
    for( const auto& row: internal_lst )
        tmp.push_back(row.toStdString());
    return tmp;
}

bool SortFilterProxyModel::lessThan( const QModelIndex &left, const QModelIndex &right ) const
{
    int typeleft = left.model()->data( left, MatrixModel::TypeRole ).toInt();
    int typeright = right.model()->data( right, MatrixModel::TypeRole ).toInt();

    if( typeleft>0 && typeright>0 )
    {
        QVariant leftData = left.model()->data(left);
        QVariant rightData = right.model()->data(right);
        return leftData.toDouble() < rightData.toDouble();
    }
    else
        return QSortFilterProxyModel::lessThan( left, right);
}


} // namespace jsonui17

