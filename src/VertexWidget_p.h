#pragma once

#include <QThread>
#include "jsonui17/models/json_view.h"
#include "jsonui17/VertexWidget.h"
#include "BaseWidget_p.h"
#include "models/dbkeys_view.h"

namespace Ui {
class VertexWidget;
}

class WaitingSpinnerWidget;

namespace jsonui17 {

class DBVertexObject;

/// Private \class VertexWidget - window to work with Vertexes (TDBGraph)
class VertexWidgetPrivate : public BaseWidgetPrivate
{
    Q_OBJECT

#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    Q_DISABLE_COPY_MOVE(VertexWidgetPrivate)
#endif

signals:  // signals to DBVertexObject thread

    /// Reset current DB client
    void cmResetClient(  QString aschema_name );

    /// Reload query
    void cmReloadQuery();

    /// Open document
    void cmReadDocument(  QString key_document  );

    /// Save json Document to database
    void cmUpdate( QString json_document );

    /// Delete keyDocument document from database
    void cmDelete( QString key_document );

    /// Delete documents from database
    void cmDeleteList( std::vector<std::string> keys_document );

    /// Read multiple records from file file_name
    void cmRestoreMultipleRecordsfromFile( QString file_name );

    /// Write multiple records to file file_name
    void cmBackupQueriedRecordstoFile( QString file_name, std::vector<std::string> keys_document );

#ifndef IMPEX_OFF
    /// Read json records from format file file_name
    void cmImportFormat( std::string impex_format_type, std::string impex_format_string,
                         QString file_name, bool overwrite  );

    /// Read json records from format file file_name
    void cmUpdateFormat( std::string impex_format_type, std::string impex_format_string,
                         QString file_name, bool overwrite  );

    /// Write json record to format file file_name
    void cmExportFormat( std::string impex_format_type, std::string impex_format_string,
                         QString file_name, std::vector<std::string> keys_document );
#endif

    /// Write graph to file file_name
    void cmBackupGraphtoFile(QString file_name, std::vector<std::string> keys_document );

    /// Read multiple records from file file_name
    void cmRestoreGraphfromFile( QString file_name );

protected slots:

    /// Start long database process
    void start_process();

    /// Finish long database process
    void finish_process();

    /// Exception when database command execution
    void get_exception( const QString& title, const QString& msg );

    /// Update oid
    void update_oid( QString oid )
    {
        json_tree->setOid(oid.toStdString());
        if( oid.isEmpty() )
            json_tree->setValueViaPath("_rev", std::string(""));
        json_tree->hide();
        json_tree->show();
    }

    /// After open new document
    void open_document( QString json_document  );

    /// Update db document query
    void change_DB_query(  QString query );

    /// Possible delete edges
    void afterDeleteDocument()
    {
        auto q = base_func();
        set_content_state( false );
        emit q->vertexDeleted();
    }

    /// Load records to different collections
    void afterLoadGraph()
    {
        auto q = base_func();
        emit q->graphLoaded();
    }

public:

    explicit VertexWidgetPrivate( const std::string& aschema_name );
    ~VertexWidgetPrivate();

    /// Setup widget data
    void init();

    void cm_update()
    {
        current_record = json_tree->saveToJson();
        set_content_state( false );
        emit cmUpdate(current_record.c_str());
    }

    void cm_delete_select( const std::vector<std::string>& keys )
    {
        start_process();
        emit cmDeleteList( keys );
    }

    void cm_backup_records( const std::string& file_path, const std::vector<std::string>& keys )
    {
        start_process();
        emit cmBackupQueriedRecordstoFile( file_path.c_str(), keys );
    }

    void cm_restore_records( const std::string& file_path )
    {
        start_process();
        emit cmRestoreMultipleRecordsfromFile(file_path.c_str());
    }

    void cm_backup_graph( const std::string& file_path, const std::vector<std::string>& keys )
    {
        start_process();
        emit cmBackupGraphtoFile( file_path.c_str(), keys );
    }

    void cm_restore_graph( const std::string& file_path )
    {
        start_process();
        emit cmRestoreGraphfromFile(file_path.c_str());
    }

protected:

    inline VertexWidget* base_func() { return static_cast<VertexWidget *>(base_ptr); }
    inline const VertexWidget* base_func() const { return static_cast<const VertexWidget *>(base_ptr); }
    friend class VertexWidget;

    // widget data

    /// VertexWidget.ui description
    QScopedPointer<Ui::VertexWidget> qUi;

    /// Tree view editor widget
    QScopedPointer<JsonView>  json_tree;
    /// Internal proxy data
    std::string current_record = "";

    /// DB keys table data container
    std::shared_ptr<DBKeysObject> db_keys;
    /// Extern link to table view
    QScopedPointer<DBKeysTable> keys_table_view;

    // database API

    /// Database connection object
    DBVertexObject* db_request = nullptr;
    /// Thead moved db_request to
    QThread db_thread;
    /// Dialog when waiting db_rquest command ends
    WaitingSpinnerWidget* wait_dialog = nullptr;

    /// Set up menu commands
    void set_actions();
    /// Set up current json data to view model
    bool reset_json( const std::string& json_string, const std::string& schema_name );
    /// Setup/ update database connection
    void reset_DB_client( const std::string& schema_name );
    /// Get current record key from json view
    std::string get_key_from_dom() const;

    std::string key_from_index( const QModelIndex& index )
    {
        QModelIndex sel_index = index.sibling( index.row(), 0);
        return keys_table_view->model()->data(sel_index).toString().toStdString();
    }

    void keys_move_to( const std::string& id_key )
    {
        keys_table_view->move_to_key(id_key);
    }

    // update after change preferences
    void updt_view_menu() override;
    void updt_model() override;
    void updt_table() override;
    void updt_db() override;
};

} // namespace jsonui17

