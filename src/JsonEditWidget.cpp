#include <QMessageBox>
#include <QKeyEvent>
#include <QLineEdit>
#include <QHeaderView>
#include "ui_JsonEditWidget.h"
#include "JsonEditWidget_p.h"

#include "jsonui17/preferences.h"
#include "jsonui17/SelectDialog.h"
#include "jsonui17/FileDialogs.h"
#include "jsonui17/models/json_model.h"
#include "jsonui17/models/schema_model.h"
#include "jsonio17/dbjsondoc.h"
#ifndef IMPEX_OFF
#include "jsonimpex17/yaml_xml2file.h"
#else
#include "jsonio17/txt2file.h"
#endif

namespace jsonui17 {


JsonEditWidget::JsonEditWidget( const std::string& aschema_name, const std::string& collect_name, QWidget *parent) :
    BaseWidget(*new JsonEditWidgetPrivate(aschema_name, collect_name), parent)
{
    auto d = impl_func();
    d->init();
    // set up default data
    CmNew();
}

JsonEditWidget::~JsonEditWidget()
{}

// Menu commands -----------------------------------------------------------

// Set default json record
void JsonEditWidget::CmNew()
{
    try {
        auto d = impl_func();
        d->cm_new();
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

// Clone json record (clear _id)
void JsonEditWidget::CmClone()
{
    try {
        auto d = impl_func();
        d->cm_clone();
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

// Save current record to DB file as new
void JsonEditWidget::CmCreateInsert()
{
    try {
        auto d = impl_func();
        d->create_insert();
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


// Read new record from DB
void JsonEditWidget::CmRead()
{
    try {
        auto d = impl_func();
        if( d->dbcollection.get() == nullptr )
            return;

        // Select key to load
        auto reckey = SelectKeyFrom( d->dbcollection.get(), this, "Please, select a record to read/view" );
        if( !reckey.empty() )
            d->cm_read(reckey);
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

// Save current record to DB file
void JsonEditWidget::CmUpdate()
{
    try {
        auto d = impl_func();
        d->cm_update();
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

// Delete current record from DB
void JsonEditWidget::CmDelete()
{
    try {
        auto d = impl_func();
        if( d->dbcollection.get() == nullptr )
            return;
        // get current key
        std::string key = d->current_key();
        QString msg = QString("Confirm deletion of %1 record?").arg( key.c_str() );
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question( this, "Record to delete", msg,
                                       QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::No)
            return;
        d->cm_delete(key);
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read bson record from json file fileName
void JsonEditWidget::CmImportJSON()
{
    try {
        auto d = impl_func();
        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                             "Please, select a file with JSON object", jsonFilters  ))
        {
#ifndef IMPEX_OFF
            jsonio17::JsonYamlXMLFile file( fileName );
            file.set_type( jsonio17::TxtFile::Json );
#else
            jsonio17::JsonFile file( fileName );
#endif
            if( !d->reset_json( file.load_json(), ""/*schema_from_name(fileName)*/) )
                jsonio17::JSONIO_THROW( "editors", 10, "Try to read another schema format file "+ fileName );

            d->set_content_state( true );
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to json file fileName
void JsonEditWidget::CmExportJSON()
{
    try {
        const auto d = impl_func();
        std::string fileName = file_schema_ext( d->current_schema_name, "json" );
        if(  ChooseFileSave( this, fileName,
                             "Please, select a file to write the data", jsonFilters, fileName  ))
        {
#ifndef IMPEX_OFF
            jsonio17::JsonYamlXMLFile file( fileName );
            file.set_type( jsonio17::TxtFile::Json );
#else
           jsonio17:: JsonFile file( fileName );
#endif
            file.save( d->json_tree->current_object() );
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

#ifndef IMPEX_OFF

/// Read bson record from yaml file fileName
void JsonEditWidget::CmImportYAML()
{
    try {
        auto d = impl_func();

        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                             "Please, select a file with YAML data object", yamlFilters  ))
        {
            jsonio17::JsonYamlXMLFile file( fileName );
            file.set_type( jsonio17::TxtFile::Yaml );
            if( !d->reset_json( file.load_json(), ""/*schema_from_name(fileName)*/) )
                jsonio17::JSONIO_THROW( "editors", 11, "Try to read another schema format file "+ fileName );
            d->set_content_state( true );
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to YAML file fileName
void JsonEditWidget::CmExportYAML()
{
    try {
        const auto d = impl_func();
        std::string fileName = file_schema_ext( d->current_schema_name, "yaml" );
        if(  ChooseFileSave( this, fileName,
                             "Please, select a file to write the data", yamlFilters, fileName  ))
        {
            jsonio17::JsonYamlXMLFile file( fileName );
            file.set_type( jsonio17::TxtFile::Yaml );
            file.save( d->json_tree->current_object() );
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Read bson record from xml file fileName
void JsonEditWidget::CmImportXML()
{
    try {
        auto d = impl_func();

        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                             "Please, select a file with xml object to read", xmlFilters  ))
        {
            jsonio17::JsonYamlXMLFile file( fileName );
            file.set_type( jsonio17::TxtFile::XML );
            if( !d->reset_json( file.load_json(), ""/*schema_from_name(fileName)*/) )
                jsonio17::JSONIO_THROW( "editors", 12, "Try to read another schema format file "+ fileName );
            d->set_content_state( true );
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to file fileName
void JsonEditWidget::CmExportXML()
{
    try {
        const auto d = impl_func();
        std::string fileName = file_schema_ext( d->current_schema_name, "xml" );
        if(  ChooseFileSave( this, fileName,
                             "Please, select a file to write the data", xmlFilters, fileName  ))
        {
            jsonio17::JsonYamlXMLFile file( fileName );
            file.set_type( jsonio17::TxtFile::XML );
            file.save( d->json_tree->current_object() );
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

#endif


//--------JsonEditWidgetPrivate----------------------------------------------------------------------------------

JsonEditWidgetPrivate::JsonEditWidgetPrivate( const std::string& aschema_name, const std::string& collect_name ) :
    BaseWidgetPrivate( aschema_name ),
    qUi(nullptr), json_tree(nullptr),
    current_collection_name(collect_name), current_record()
{}

JsonEditWidgetPrivate::~JsonEditWidgetPrivate()
{}

void JsonEditWidgetPrivate::init()
{
    qUi.reset(new Ui::JsonEditWidget);

    auto q = base_func();
    qUi->setupUi(q);
    q->setAttribute(Qt::WA_DeleteOnClose); // automatically delete itself when window is closed

    QString title = qApp->applicationName()+" Structured Data Editor and Database Browser";
    q->setWindowTitle(title);

    // define edit tree view
    QItemDelegate* deleg_schema = nullptr;
    QStringList    header_data;
    JsonBaseModel*  json_model;
    header_data << "key" << "value"  ;
    json_tree.reset( new JsonView(  qUi->bsonWidget ) );
    if( current_schema_name.empty() )
    {
        json_model = new JsonFreeModel(  current_record, header_data, q );
        deleg_schema =  new JsonFreeDelegate();
    }
    else
    {
        header_data << "comment" ;
        json_model = new JsonSchemaModel(  current_record,  current_schema_name, header_data, q );
        deleg_schema = new JsonSchemaDelegate();
    }
    json_tree->setModel(json_model);
    json_tree->setItemDelegate(deleg_schema);
    json_tree->setColumnWidth( 0, 250 );
    json_tree->expandToDepth(0);
    qUi->verticalLayout->addWidget(json_tree.get());

    // define menu
    set_actions();
    reset_DB_client();
}

//  Connect all actions
void JsonEditWidgetPrivate::set_actions()
{
    auto q = base_func();
    // File
    QObject::connect( qUi->actionNew , &QAction::triggered, q, &JsonEditWidget::CmNew);
    QObject::connect( qUi->action_Clone_Structured_Data_Object , &QAction::triggered, q, &JsonEditWidget::CmClone);
    QObject::connect( qUi->actionE_xit, &QAction::triggered, q, &JsonEditWidget::close);
    QObject::connect( qUi->actionExport_Json_File, &QAction::triggered, q, &JsonEditWidget::CmExportJSON);
    QObject::connect( qUi->actionImport_Json_File, &QAction::triggered, q, &JsonEditWidget::CmImportJSON);
#ifndef IMPEX_OFF
    QObject::connect( qUi->action_Export_YAML_File, &QAction::triggered, q, &JsonEditWidget::CmExportYAML);
    QObject::connect( qUi->actionImport_YAML_File, &QAction::triggered, q, &JsonEditWidget::CmImportYAML);
    QObject::connect( qUi->actionExport_XML_File, &QAction::triggered, q, &JsonEditWidget::CmExportXML);
    QObject::connect( qUi->actionImport_XML_File, &QAction::triggered, q, &JsonEditWidget::CmImportXML);
#else
    qUi->action_Export_YAML_File->setVisible(false);
    qUi->actionImport_YAML_File->setVisible(false);
    qUi->actionExport_XML_File->setVisible(false);
    qUi->actionImport_XML_File->setVisible(false);
#endif

    // Edit
    QObject::connect(qUi->actionAdd_one_field, &QAction::triggered, json_tree.get(), &JsonView::CmAddObject);
    QObject::connect(qUi->action_Clone_Current, &QAction::triggered, json_tree.get(), &JsonView::CmCloneObject);
    QObject::connect(qUi->action_Delete_field, &QAction::triggered, json_tree.get(), &JsonView::CmDelObject);
    QObject::connect(qUi->action_Delete_fields, &QAction::triggered, json_tree.get(), &JsonView::CmDelObjects);
    if( !current_schema_name.empty() )
    {
        QObject::connect(qUi->actionRemove_Alternatives_Union, &QAction::triggered, json_tree.get(), &JsonView::CmDelObjectsUnion);
        QObject::connect(qUi->action_Add_fields, &QAction::triggered, json_tree.get(), &JsonView::CmAddObjects);
        QObject::connect(qUi->actionReset_Data_to_Defaults, &QAction::triggered, json_tree.get(), &JsonView::CmResetObject);
    }
    else
    {
        qUi->actionRemove_Alternatives_Union->setVisible(false);
        qUi->action_Add_fields->setVisible(false);
        qUi->actionReset_Data_to_Defaults->setVisible(false);
    }
    QObject::connect(qUi->action_Resize_array, &QAction::triggered, json_tree.get(), &JsonView::CmResizeArray);
    QObject::connect(qUi->action_Calculator, &QAction::triggered, json_tree.get(), &JsonView::CmCalc);
    QObject::connect(qUi->actionCopy_Field_Path, &QAction::triggered, json_tree.get(), &JsonView::CopyFieldPath);
    QObject::connect(qUi->actionCopy_Field, &QAction::triggered, json_tree.get(), &JsonView::CopyField);
    QObject::connect(qUi->actionPaste_Field, &QAction::triggered, json_tree.get(), &JsonView::PasteField);

    // Help
    QObject::connect( qUi->action_Help_About, &QAction::triggered, q, &JsonEditWidget::CmHelpAbout);
    QObject::connect( qUi->actionContents, &QAction::triggered, q, &JsonEditWidget::CmHelpContens);
    QObject::connect( qUi->actionAuthors, &QAction::triggered, q, &JsonEditWidget::CmHelpAuthors);
    QObject::connect( qUi->actionLicense, &QAction::triggered, q, &JsonEditWidget::CmHelpLicense);

    // View
    QObject::connect( qUi->action_Show_comments, &QAction::toggled, &uiSettings(), &UIPreferences::CmShowComments);
    QObject::connect( qUi->action_Display_enums, &QAction::toggled, &uiSettings(), &UIPreferences::CmDisplayEnums);
    QObject::connect( qUi->action_Edit_id, &QAction::toggled, &uiSettings(), &UIPreferences::CmEditID);
    QObject::connect( qUi->actionKeep_Data_Fields_Expanded, &QAction::toggled, &uiSettings(), &UIPreferences::CmEditExpanded);
    updt_view_menu();

    if( !current_collection_name.empty() )
    {
        QObject::connect( qUi->action_Update, &QAction::triggered, q, &JsonEditWidget::CmUpdate);
        QObject::connect( qUi->action_Create, &QAction::triggered, q, &JsonEditWidget::CmCreateInsert);
        QObject::connect( qUi->action_Delete, &QAction::triggered, q, &JsonEditWidget::CmDelete);
        QObject::connect( qUi->action_Read, &QAction::triggered, q, &JsonEditWidget::CmRead);
    }
    else
        qUi->menu_Database->menuAction()->setVisible(false);

    if( !current_schema_name.empty() )
    {
        QLineEdit* pLineTask = new QLineEdit( qUi->nameToolBar );
        pLineTask->setEnabled( true );
        pLineTask->setFocusPolicy( Qt::ClickFocus );
        pLineTask->setReadOnly( true );
        pLineTask->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        QString title =  current_schema_name.c_str();
        pLineTask->setText(title);
        qUi->nameToolBar->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        qUi->nameToolBar->addWidget( pLineTask ); // setStretchableWidget( pLine );
    }
    else
    {
        qUi->mainToolBar->hide();
    }
}

void JsonEditWidgetPrivate::reset_DB_client()
{
    if( current_collection_name.empty())
        return;

    try{
        jsonio17::DBDocumentBase* newClient;
        if( current_schema_name.empty() )
        {
            newClient = jsonio17::DBJsonDocument::newJsonDocumentQuery( uiSettings().database(),  current_collection_name,
                                                                        { "name" } );
        }
        else
        {
            newClient = jsonio17::DBSchemaDocument::newSchemaDocumentQuery( uiSettings().database(), current_schema_name,
                                                                            current_collection_name );
        }
        if( newClient == nullptr )
            dbcollection.reset();
        else
            dbcollection.reset( newClient );

    }
    catch(std::exception& e)
    {
        ui_logger->error("Error when resetting DB client {}: {}", current_schema_name, e.what());
        throw;
    }
}

void JsonEditWidgetPrivate::updt_view_menu()
{
    qUi->action_Show_comments->setChecked( JsonSchemaModel::showComments );
    qUi->action_Display_enums->setChecked( JsonSchemaModel::useEnumNames);
    qUi->action_Edit_id->setChecked( JsonSchemaModel::editID );
    qUi->actionKeep_Data_Fields_Expanded->setChecked( JsonView::expandedFields );
}

void JsonEditWidgetPrivate::updt_model()
{
    if( !current_schema_name.empty() )
    {
        json_tree->resetModel();
    }
}

void JsonEditWidgetPrivate::updt_table()
{
    json_tree->hide();
    json_tree->show();
}

void JsonEditWidgetPrivate::updt_db()
{
    //    done it at collection level for all documents
    //    if( dbcollection.get() )
    //        dbcollection->updateQuery();
}


} // namespace jsonui17

