
#include <algorithm>
#include <QFileInfo>
#include <QFileDialog>
#include <QHeaderView>
#include <QImageWriter>
#include "jsonui17/FileDialogs.h"
#include "jsonui17/preferences.h"

namespace jsonui17 {

const QString jsonFilters = "JSON files (*.json);;All files (*.*)";
const QString textFilters = "All files (*);;Text files (*.txt)";

#ifndef IMPEX_OFF
const QString domFilters =  "JSON files (*.json);;YAML files (*.yaml);;XML files (*.xml);;All files (*.*)";
const QString yamlFilters = "YAML files (*.yaml);;All files (*.*)";
const QString xmlFilters =  "XML files (*.xml);;All files (*.*)";
#else
const QString domFilters =  jsonFilters;
#endif

const QString csvFilters = "CSV files (*.csv);;All files (*)";

bool ChooseFileOpen( QWidget* par, std::string& file_path, const QString& title, const QString& filter )
{
    QString path;
    if( file_path.find('/') == std::string::npos )
        path  =  QString( ( jsonio17::ioSettings().userDir()+"/"+file_path ).c_str() );
    else
        path = file_path.c_str();

    QString filt;
    if( filter.isEmpty() )
        filt = "All files (*)";
    else
        filt = filter;

    QString fn = QFileDialog::getOpenFileName(  par, title,  path, filt, nullptr, QFileDialog::DontConfirmOverwrite );
#ifdef buildWIN32
    std::replace( fn.begin(), fn.end(), '/', '\\');
#endif
    if ( !fn.isEmpty() )
    {
        QFileInfo flinfo(fn);
        jsonio17::ioSettings().setUserDir( flinfo.dir().path().toStdString() );
        file_path = fn.toStdString();
        return true;
    }
    else
    {
        file_path = "";
        return false;
    }

}

bool ChooseFileSave( QWidget* par, std::string& file_path, const QString& title, const QString& filter,  const std::string& schemafilter )
{
    QString path;
    if( file_path.find('/') == std::string::npos )
        path  =  QString( ( jsonio17::ioSettings().userDir()+"/"+file_path ).c_str() );
    else
        path = file_path.c_str();

    //replace(path.begin(), path.end(),'\\', '/');

    QString filt;
    if( !schemafilter.empty() )
        filt += QString("Schema (%1)").arg( schemafilter.c_str() )+";;";
    if( filter.isEmpty() )
        filt += "All files (*.*)";
    else
        filt += filter;

    QString selectedFilter;
    QString fn = QFileDialog::getSaveFileName( par, title, path, filt, &selectedFilter, QFileDialog::DontConfirmOverwrite);

    if ( !fn.isEmpty() )
    {
        QFileInfo flinfo(fn);
        jsonio17::ioSettings().setUserDir( flinfo.dir().path().toStdString() );
        if( flinfo.suffix().isEmpty() ) // solwing for linux
        {
            int posb = selectedFilter.lastIndexOf(".");
            if( !schemafilter.empty() )
                posb = selectedFilter.indexOf(".");
            int pose = selectedFilter.indexOf(")", posb);
            if( posb > 0 && pose > posb )
            { QString ext = selectedFilter.mid(posb+1, pose-posb-1);
                fn += "."+ext;
            }
        }
        file_path = fn.toStdString();
        return true;
    }
    else
    {
        file_path  = "";
        return false;
    }
}

} // namespace jsonui17


