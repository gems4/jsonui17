#include <QMessageBox>
#include <QKeyEvent>
#include <QHeaderView>
#include <QLineEdit>
#include "ui_VertexWidget.h"
#include "VertexWidget_p.h"
#include "jsonui17/SelectDialog.h"
#include "jsonui17/FileDialogs.h"
#include "jsonui17/preferences.h"
#include "jsonui17/service/waitingspinnerwidget.h"
#include "jsonio17/dbvertexdoc.h"
#include "jsonui17/models/schema_model.h"
#include "models/dbkeys_view.h"
#include "models/dbquery_thread.h"

#ifndef IMPEX_OFF
#include "jsonui17/FormatImpexWidget.h"
#include "jsonimpex17/yaml_xml2file.h"
#endif

namespace jsonui17 {


VertexWidget::VertexWidget( const std::string& aschema_name, QWidget *parent ) :
  BaseWidget(*new VertexWidgetPrivate(aschema_name), parent)
{
  auto d = impl_func();
  d->init();
}

VertexWidget::~VertexWidget()
{}

void VertexWidget::closeEvent(QCloseEvent* e)
{
    auto d = impl_func();
    if( d->db_keys )
        d->db_keys->Close();

    BaseWidget::closeEvent(e);
}

bool VertexWidget::event(QEvent * e) // overloading event(QEvent*) method of QMainWindow
{
    switch(e->type())
    {
        case QEvent::WindowActivate:
            /// crash when show()
            /// problems reset model
            /// changeKeyList();
            break ;
        default: break;
    }
    return QMainWindow::event(e) ;
}

std::string VertexWidget::currentKey() const
{
    auto d = impl_func();
    return  d->get_key_from_dom();
}

void VertexWidget::setQueryEdited( QueryWidget* queryW  )
{
    auto d = impl_func();
    d->start_process();
    d->db_keys->setOneQuery( queryW );
}

void VertexWidget::openRecordKey( const QModelIndex& index )
{
    auto d = impl_func();
    std::string key_document = d->key_from_index(index);
    openRecordKey( key_document );
}

void VertexWidget::openRecordKey( const std::string& reckey, bool )
{
    auto d = impl_func();
    emit d->cmReadDocument( reckey.c_str() );
}

void jsonui17::VertexWidget::changeKeyList()
{
   auto d = impl_func();
   d->db_keys->resetModel( d->get_key_from_dom().c_str() );
}

void VertexWidget::updateQuery()
{
    auto d = impl_func();
    d->start_process();
    emit d->cmReloadQuery();
}

// Menu commands -----------------------------------------------------------

/// Set default bson record
void VertexWidget::CmNew()
{
    auto d = impl_func();
    d->open_document( "" );
}

/// Clone bson record (clear _id)
void VertexWidget::CmClone()
{
    auto d = impl_func();
    d->update_oid( "" );
}

/// Read document from json file fileName
void VertexWidget::CmImportJSON()
{
    try{
        auto d = impl_func();
        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                             "Please, select a file with JSON object", domFilters  ))
        {
#ifndef IMPEX_OFF
            jsonio17::JsonYamlXMLFile file( fileName );
#else
            jsonio17::JsonFile file( fileName );
#endif
            if( !d->reset_json( file.load_json(), schema_from_name( fileName )) )
                jsonio17::JSONIO_THROW( "editors", 20, "Try to read another schema format file "+ fileName );
            d->set_content_state( true );
        }
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write current document to json file fileName
void VertexWidget::CmExportJSON()
{
    try {
        const auto d = impl_func();
        std::string fileName = file_schema_ext( d->current_schema_name, "json");
        if(  ChooseFileSave( this, fileName,
                             "Please, select a file to write the data", domFilters, fileName  ))
        {
#ifndef IMPEX_OFF
            jsonio17::JsonYamlXMLFile file( fileName );
            file.set_type( jsonio17::TxtFile::Json );
#else
            jsonio17::JsonFile file( fileName );
#endif
            file.save( d->json_tree->current_object() );
        }
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Read new record from DB
void VertexWidget::CmRead()
{
    try  {
        const auto d = impl_func();
        auto aValList = d->keys_table_view->tableValues();
        if( aValList.empty() )
            return;

        SelectDialog selDlg( false, this, "Please, select a record to read/view", aValList );
        if( !selDlg.exec() )
            return;
        std::string reckey = aValList[selDlg.selIndex()][0].toString().toStdString();
        openRecordKey(  reckey  );
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Save current document to DB file
void VertexWidget::CmUpdate()
{
    try {
        auto d = impl_func();
        d->cm_update();
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Delete current document from DB
void VertexWidget::CmDelete()
{
    try {
        auto d = impl_func();
        auto key = d->get_key_from_dom();
        QString msg = QString("Confirm deletion of %1 record?").arg( key.c_str() );
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question( this, "Record to delete", msg,
                                       QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::No)
            return;

        emit d->cmDelete( key.c_str() );
        d->set_content_state( false );
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Delete list of documents
void VertexWidget::CmDeleteSelect()
{
    try {
        auto d = impl_func();
        d->cm_delete_select(select_keys_document("Please, select a records to delete."));
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void VertexWidget::CmDisplaySearchResult()
{
    try  {
        auto d = impl_func();
        d->db_keys->showResult("Vertex Query Result window");
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "CmDisplaySearchResult", e.what() );
    }
}

void VertexWidget::CmSearchQuery()
{
    try {
        auto d = impl_func();
        d->db_keys->showQuery( "Vertex Query Widget", { d->current_schema_name } );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "CmSearchQuery", e.what() );
    }
}

/// Write multiple records to file fileName
void VertexWidget::CmBackupQueriedRecordstoFile()
{
    try {
        auto d = impl_func();
        std::string fileName = file_schema_ext( d->current_schema_name, "json");
        if(  ChooseFileSave( this, fileName,
                             "Please, select a file to write (document) records", domFilters, fileName  ))
        {
            std::vector<std::string> keysDocument = select_keys_document("Please, select a records to backup.");
            d->cm_backup_records( fileName, keysDocument );
        }
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
   catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Read multiple records from file fileName
void VertexWidget::CmRestoreMultipleRecordsfromFile()
{
    try{
        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                             "Please, select a file with json object", domFilters  ))
        {
            auto d = impl_func();
            d->cm_restore_records(fileName);
        }
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write multiple records to file fileName
void VertexWidget::CmBackupGraphtoFile()
{
    try {
        auto d = impl_func();
        std::string fileName = file_schema_ext( d->current_schema_name, "json");
        if(  ChooseFileSave( this, fileName,
                             "Please, select a file to write (document) records", jsonFilters, fileName  ))
        {
            std::vector<std::string> keysDocument = select_keys_document("Please, select a records to backup graph.");
            d->cm_backup_graph( fileName, keysDocument );
        }
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Read multiple records from file fileName
void VertexWidget::CmRestoreGraphfromFile()
{
    try {
        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                             "Please, select a file with json object", jsonFilters  ))
        {
            auto d = impl_func();
            d->cm_restore_graph(fileName);
        }
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

#ifndef IMPEX_OFF

/// Read records from format file fileName
void VertexWidget::CmImportFormat()
{
    try{
        auto d = impl_func();
        // Select format&input file
        FormatImpexWidget* dlg = new FormatImpexWidget(
                    FormatImpexWidget::runModeImport, d->current_schema_name, this );

        ExecuteImpex_f execF = [&]( std::string impex_data_name,
                std::string impex_format_type, std::string impex_format_string,
                std::string dataFile, bool overwrite )
        {
            auto d = impl_func();
            if( !d->reset_json( "", impex_data_name ) )
                jsonio17::JSONIO_THROW( "editors", 21, "Try to read another schema format file "+ impex_data_name );

            d->start_process();
            emit d->cmImportFormat(impex_format_type, impex_format_string, dataFile.c_str(), overwrite);
        };

        dlg->setExecuteFunction(execF);
        dlg->show();
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read records from format file fileName
void VertexWidget::CmUpdateFormat()
{
    try{
        auto d = impl_func();

        QMessageBox::StandardButton reply;
        reply = QMessageBox::question( this, "Update Format",
                                       "Have you queried the documents to update and\n"
                                       "generated the table of values that define documents update?",
                                       QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::No)
            return;

        // Select format&input file
        FormatImpexWidget* dlg = new FormatImpexWidget(
                    FormatImpexWidget::runModeUpdate, d->current_schema_name, this );

        ExecuteImpex_f execF = [&]( std::string impex_data_name,
                std::string impex_format_type, std::string impex_format_string,
                std::string dataFile, bool overwrite )
        {
            auto d = impl_func();
            if( !d->reset_json( "", impex_data_name ) )
                jsonio17::JSONIO_THROW( "editors", 21, "Try to read another schema format file "+ impex_data_name );
            d->start_process();
            emit d->cmUpdateFormat(impex_format_type, impex_format_string, dataFile.c_str(), overwrite);
        };

        dlg->setExecuteFunction(execF);
        dlg->show();
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Write json record to format file fileName
void VertexWidget::CmExportFormat()
{
    auto d = impl_func();
    try {
        // Select format&export file
        FormatImpexWidget* dlg = new FormatImpexWidget(
                    FormatImpexWidget::runModeExport, d->current_schema_name, this );

        ExecuteImpex_f execF = [&]( std::string impex_data_name,
                std::string impex_format_type, std::string impex_format_string,
                std::string dataFile, bool /*overwrite*/ )
        {
            auto d = impl_func();
            if( !d->reset_json( "", impex_data_name ) )
                jsonio17::JSONIO_THROW( "editors", 22, "Try to read another schema format file "+ impex_data_name );

            std::vector<std::string> keysDocument = select_keys_document("Please, select a records to export.");
            d->start_process();
            emit d->cmExportFormat(impex_format_type, impex_format_string, dataFile.c_str(), keysDocument);
        };

        dlg->setExecuteFunction(execF);
        dlg->show();
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}
#endif

/// Show QObject::connect Edjes window
void VertexWidget::CmFollowInEdges()
{
    try {
        auto d = impl_func();
        std::string stroid;
        d->json_tree->getValueViaPath<std::string>( "_id", stroid, "" );

        std::shared_ptr<jsonio17::DBEdgeDocument> edges( jsonio17::documentAllEdges( uiSettings().database() ));
        if( !edges->existInEdges( stroid ) )
        {
            QMessageBox::StandardButton reply;
            reply = QMessageBox::question( this, "Follow Incoming Edges",
                                           "No Incoming Edges. Create new?",
                                           QMessageBox::Yes|QMessageBox::No);
            if (reply == QMessageBox::No)
                return;
        }
        d->show_document_widget_function( false, "", "",  edges->inEdgesQuery( stroid ));
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "Follow edges", e.what() );
    }
}

/// Show QObject::connect Edjes window
void VertexWidget::CmFollowOutEdges()
{
    try {
        auto d = impl_func();
        std::string stroid;
        d->json_tree->getValueViaPath<std::string>( "_id", stroid, "" );

        std::shared_ptr<jsonio17::DBEdgeDocument> edges( jsonio17::documentAllEdges( uiSettings().database() ));
        if( !edges->existOutEdges( stroid ) )
        {
            QMessageBox::StandardButton reply;
            reply = QMessageBox::question( this, "Follow Outgoing Edges",
                                           "No Outgoing Edges. Create new?",
                                           QMessageBox::Yes|QMessageBox::No);
            if (reply == QMessageBox::No)
                return;
        }
        d->show_document_widget_function( false, "", "",  edges->outEdgesQuery( stroid ));
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "Follow edges", e.what() );
    }
}

std::vector<std::string> VertexWidget::select_keys_document( const char* title )
{
    const auto d = impl_func();
    std::vector<std::string> keysDocument;

    auto aValList = d->keys_table_view->tableValues();
    if( aValList.empty() )
            return keysDocument;

    SelectDialog selDlg( true, this, title, aValList, {}, MatrixTable::tbNoMenu|MatrixTable::tbSort );
    if( selDlg.exec() )
    {
        auto selNdx = selDlg.allSelected();
        for( auto ndx:  selNdx )
            keysDocument.push_back( aValList[ndx][0].toString().toStdString() );
    }
    return keysDocument;
}


//------VertexWidgetPrivate-----------------------------------------------------------------------

VertexWidgetPrivate::VertexWidgetPrivate( const std::string& aschema_name ) :
    BaseWidgetPrivate( aschema_name ),
    qUi(nullptr), json_tree(nullptr)
{}

VertexWidgetPrivate::~VertexWidgetPrivate()
{
    db_thread.quit();
    db_thread.wait();

    delete wait_dialog;
}

void VertexWidgetPrivate::init()
{
    auto q = base_func();

    qUi.reset(new Ui::VertexWidget);
    qUi->setupUi(q);
    qUi->keySplitter->setStretchFactor(0, 2);
    qUi->keySplitter->setStretchFactor(1, 1);
    qUi->mainSplitter->setStretchFactor(0, 1);
    qUi->mainSplitter->setStretchFactor(1, 4);

    q->setAttribute(Qt::WA_DeleteOnClose); // automatically delete itself when window is closed
    QString title = qApp->applicationName()+" Structured Data Editor and Database Browser";
    q->setWindowTitle(title);

    // define key table
    keys_table_view.reset( new DBKeysTable( qUi->keylistWidget, [&]( const QModelIndex& index ){ base_func()->openRecordKey( index ); } ) );
    qUi->horizontalLayout->addWidget(keys_table_view.get());
    QObject::connect( keys_table_view.get(), SIGNAL(clicked(const QModelIndex&)), q, SLOT(openRecordKey(const QModelIndex&)));

     // define edit tree view
    QStringList aHeaderData;
    aHeaderData << "key" << "value" << "comment" ;
    json_tree.reset( new JsonView(q) );
    auto schema_model = new JsonSchemaModel(  current_record, current_schema_name, aHeaderData, this/*qUi->centralWidget*/ );
    JsonSchemaDelegate * deleg_schema = new JsonSchemaDelegate();
    json_tree->setModel(schema_model);
    json_tree->setItemDelegate(deleg_schema);
    json_tree->setColumnWidth( 0, 250 );
    json_tree->expandToDepth(2);
    qUi->verticalLayout->addWidget(json_tree.get());

    // define menu
    set_actions();
    wait_dialog = new WaitingSpinnerWidget(q, true/*Qt::ApplicationModal*/, true);
    // Open db QObject::connection and load keys table
    reset_DB_client( current_schema_name );
}

//  Connect all actions
void VertexWidgetPrivate::set_actions()
{
    auto q = base_func();
    // File
    QObject::connect( qUi->actionNew, &QAction::triggered, q, &VertexWidget::CmNew);
    QObject::connect( qUi->action_Clone_Structured_Data_Object, &QAction::triggered, q, &VertexWidget::CmClone);
    QObject::connect( qUi->actionE_xit, &QAction::triggered, q, &VertexWidget::close);
    QObject::connect( qUi->actionExport_Json_File, &QAction::triggered, q, &VertexWidget::CmExportJSON);
    QObject::connect( qUi->actionImport_Json_File, &QAction::triggered, q, &VertexWidget::CmImportJSON);

    // Edit
    QObject::connect(qUi->actionAdd_one_field, &QAction::triggered, json_tree.get(), &JsonView::CmAddObject);
    QObject::connect(qUi->action_Clone_Current, &QAction::triggered, json_tree.get(), &JsonView::CmCloneObject);
    QObject::connect(qUi->action_Delete_field, &QAction::triggered, json_tree.get(), &JsonView::CmDelObject);
    QObject::connect(qUi->action_Delete_fields, &QAction::triggered, json_tree.get(), &JsonView::CmDelObjects);
    QObject::connect(qUi->actionRemove_Alternatives_Union, &QAction::triggered, json_tree.get(), &JsonView::CmDelObjectsUnion);
    QObject::connect(qUi->action_Add_fields, &QAction::triggered, json_tree.get(), &JsonView::CmAddObjects);
    QObject::connect(qUi->actionReset_Data_to_Defaults, &QAction::triggered, json_tree.get(), &JsonView::CmResetObject);
    QObject::connect(qUi->action_Resize_array, &QAction::triggered, json_tree.get(), &JsonView::CmResizeArray);
    QObject::connect(qUi->action_Calculator, &QAction::triggered, json_tree.get(), &JsonView::CmCalc);
    QObject::connect(qUi->actionCopy_Field_Path, &QAction::triggered, json_tree.get(), &JsonView::CopyFieldPath);
    QObject::connect(qUi->actionCopy_Field, &QAction::triggered, json_tree.get(), &JsonView::CopyField);
    QObject::connect(qUi->actionPaste_Field, &QAction::triggered, json_tree.get(), &JsonView::PasteField);

    // Help
    QObject::connect( qUi->action_Help_About, &QAction::triggered, q, &VertexWidget::CmHelpAbout);
    QObject::connect( qUi->actionContents, &QAction::triggered, q, &VertexWidget::CmHelpContens);
    QObject::connect( qUi->actionAuthors, &QAction::triggered, q, &VertexWidget::CmHelpAuthors);
    QObject::connect( qUi->actionLicense, &QAction::triggered, q, &VertexWidget::CmHelpLicense);

    // View
    QObject::connect( qUi->action_Show_comments, &QAction::toggled, &uiSettings(), &UIPreferences::CmShowComments);
    QObject::connect( qUi->action_Display_enums, &QAction::toggled, &uiSettings(), &UIPreferences::CmDisplayEnums);
    QObject::connect( qUi->action_Edit_id, &QAction::toggled, &uiSettings(), &UIPreferences::CmEditID);
    QObject::connect( qUi->actionKeep_Data_Fields_Expanded, &QAction::toggled, &uiSettings(), &UIPreferences::CmEditExpanded);
    updt_view_menu();
    QObject::connect( qUi->actionFollow_Edges, &QAction::triggered, q, &VertexWidget::CmFollowOutEdges);
    QObject::connect( qUi->actionFollow_Edges_Incoming, &QAction::triggered, q, &VertexWidget::CmFollowInEdges);

    // Record
    QObject::connect( qUi->actionNext_Record, &QAction::triggered, keys_table_view.get(), &DBKeysTable::CmNext);
    QObject::connect( qUi->actionPrevious_Record, &QAction::triggered, keys_table_view.get(), &DBKeysTable::CmPrevious);
    QObject::connect( qUi->action_Update, &QAction::triggered, q, &VertexWidget::CmUpdate);
    //QObject::connect( qUi->action_Create, &QAction::triggered, q, &VertexWidget::CmCreateInsert);
    QObject::connect( qUi->action_Delete, &QAction::triggered, q, &VertexWidget::CmDelete);
    QObject::connect( qUi->action_Read, &QAction::triggered, q, &VertexWidget::CmRead);
#ifndef IMPEX_OFF
    QObject::connect( qUi->actionImport, &QAction::triggered, q, &VertexWidget::CmImportFormat);
    QObject::connect( qUi->actionUpdateFormat, &QAction::triggered, q, &VertexWidget::CmUpdateFormat);
    QObject::connect( qUi->actionExport, &QAction::triggered, q, &VertexWidget::CmExportFormat);
#else
    qUi->actionImport->setVisible(false);
    qUi->actionImportpdate->setVisible(false);
    qUi->actionExport->setVisible(false);
#endif
    QObject::connect( qUi->actionDeleteMultiple, &QAction::triggered, q, &VertexWidget::CmDeleteSelect);
    QObject::connect( qUi->actionSearch_Results, &QAction::triggered, q, &VertexWidget::CmDisplaySearchResult);
    QObject::connect( qUi->actionSearch, &QAction::triggered, q, &VertexWidget::CmSearchQuery);

    QObject::connect( qUi->actionBackup_Queried_Records_to_File, &QAction::triggered,
             q,  &VertexWidget::CmBackupQueriedRecordstoFile);
    QObject::connect( qUi->actionRestore_Multiple_Records_from_File, &QAction::triggered,
             q, &VertexWidget::CmRestoreMultipleRecordsfromFile);

    QObject::connect( qUi->actionBackup_Graph_to_File, &QAction::triggered,
             q,  &VertexWidget::CmBackupGraphtoFile);
    QObject::connect( qUi->actionRestore_Graph_from_File, &QAction::triggered,
             q, &VertexWidget::CmRestoreGraphfromFile);

    QLineEdit* pLineTask = new QLineEdit( qUi->nameToolBar );
    pLineTask->setEnabled( true );
    pLineTask->setFocusPolicy( Qt::ClickFocus );
    pLineTask->setReadOnly( true );
    pLineTask->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
    QString title =  current_schema_name.c_str();
    pLineTask->setText(title);
    qUi->nameToolBar->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
    qUi->nameToolBar->addWidget( pLineTask ); // setStretchableWidget( pLine );
}

void VertexWidgetPrivate::reset_DB_client(const std::string& schemaName )
{
    //qRegisterMetaType<QVector<QString> >("QVector<QString>");
    qRegisterMetaType<std::vector<std::string> >("std::vector<std::string>");
    qRegisterMetaType<std::shared_ptr<jsonio17::DBQueryDef> >("std::shared_ptr<jsonio17::DBQueryDef>");
    qRegisterMetaType<std::string>("std::string");

    try {

        auto q = base_func();
        db_keys.reset( new DBKeysObject( nullptr, keys_table_view.get(), q ) );
        db_request = new DBVertexObject(  current_schema_name  );

        QObject::connect( db_keys.get(), &DBKeysObject::updateQuery, db_request, &DBVertexObject::dbChangeQuery );
        QObject::connect( db_request, &DBVertexObject::changedDBClient, db_keys.get(), &DBKeysObject::changeDBClient );
        QObject::connect( db_request, &DBVertexObject::changedModel,  db_keys.get(), &DBKeysObject::resetModel );

        // link from GUI
        QObject::connect( this, &VertexWidgetPrivate::cmResetClient, db_request, &DBVertexObject::dbResetClient );
        //QObject::connect( this, &VertexWidget::cmChangeQuery, db_request, &DBVertexObject::dbChangeQuery );
        QObject::connect( this, &VertexWidgetPrivate::cmReadDocument, db_request, &DBVertexObject::dbReadDocument );
        QObject::connect( this, &VertexWidgetPrivate::cmUpdate, db_request, &DBVertexObject::dbUpdate );
        QObject::connect( this, &VertexWidgetPrivate::cmDelete, db_request, &DBVertexObject::dbDelete );
        QObject::connect( this, &VertexWidgetPrivate::cmDeleteList, db_request, &DBVertexObject::dbDeleteList );
        QObject::connect( this, &VertexWidgetPrivate::cmRestoreMultipleRecordsfromFile, db_request, &DBVertexObject::dbRestoreMultipleRecordsfromFile );
        QObject::connect( this, &VertexWidgetPrivate::cmBackupQueriedRecordstoFile, db_request, &DBVertexObject::dbBackupQueriedRecordstoFile );
#ifndef IMPEX_OFF
        QObject::connect( this, &VertexWidgetPrivate::cmImportFormat, db_request, &DBVertexObject::dbImportFormat );
        QObject::connect( this, &VertexWidgetPrivate::cmUpdateFormat, db_request, &DBVertexObject::dbUpdateFormat );
        QObject::connect( this, &VertexWidgetPrivate::cmExportFormat, db_request, &DBVertexObject::dbExportFormat );
#endif
        QObject::connect( this, &VertexWidgetPrivate::cmBackupGraphtoFile, db_request, &DBVertexObject::dbBackupGraphtoFile );
        QObject::connect( this, &VertexWidgetPrivate::cmRestoreGraphfromFile, db_request, &DBVertexObject::dbRestoreGraphfromFile );
        QObject::connect( this, &VertexWidgetPrivate::cmReloadQuery, db_request, &DBVertexObject::dbReloadQuery );

        // link to GUI

        QObject::connect( db_request, &DBVertexObject::changedQueryString, this, &VertexWidgetPrivate::change_DB_query );
        QObject::connect( db_request, &DBVertexObject::finished, this, &VertexWidgetPrivate::finish_process );
        QObject::connect( db_request, &DBVertexObject::isException, this, &VertexWidgetPrivate::get_exception );
        QObject::connect( db_request, &DBVertexObject::updatedOid, [&](QString oid) {  update_oid( oid ); } );

        QObject::connect( db_request, &DBVertexObject::openedDocument, this, &VertexWidgetPrivate::open_document );
        QObject::connect( db_request, &DBVertexObject::deletedDocument, this, &VertexWidgetPrivate::afterDeleteDocument );
        QObject::connect( db_request, &DBVertexObject::loadedGraph,   this, &VertexWidgetPrivate::afterLoadGraph );
        /*QObject::connect( db_request, &DBVertexObject::changedDBClient,
               [&](jsonio::TDBVertexDocument* newClient)
                     {  openRecordKey( newClient->lastQueryData()->getFirstKeyFromList().c_str() );
                     } );*/

        // thread functions
        db_request->moveToThread(&db_thread);
        QObject::connect(&db_thread, &QThread::finished, db_request, &QObject::deleteLater);
        db_thread.start();

        // might be execute signals it other thread
        start_process();
        emit cmResetClient( schemaName.c_str() );
    }
    catch(std::exception& e)
    {
        ui_logger->error("Error when resetting vertex DB client {}: {}", current_schema_name, e.what());
        throw;
    }
}

void VertexWidgetPrivate::open_document(QString json_document)
{
    auto q = base_func();
    try {
        reset_json( json_document.toStdString(), "" );
        keys_move_to( get_key_from_dom() );
        set_content_state( false );
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( q, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( q, "std::exception", e.what() );
    }
}

void VertexWidgetPrivate::change_DB_query(QString query)
{
    qUi->queryEdit->setText( query );
}

/// Start long database process
void VertexWidgetPrivate::start_process()
{
    // possible open Waiting Spinner
    ui_logger->info("VertexWidget start process");
    wait_dialog->start();
}

/// Finish long database process
void VertexWidgetPrivate::finish_process()
{
    // possible close Waiting Spinner
    wait_dialog->stop();
    ui_logger->info("VertexWidget finish process");
}

// Exception when database command execution
void VertexWidgetPrivate::get_exception( const QString& title, const QString& msg )
{
    auto q = base_func();
    QMessageBox::critical( q, title, msg );
}


bool VertexWidgetPrivate::reset_json( const std::string& json_string, const std::string& schema_name )
{
    // test legal schema name
    if( !current_schema_name.empty() && !schema_name.empty() && schema_name != current_schema_name )
        return false;
    current_record =  json_string;
    json_tree->updateModelData( json_string, current_schema_name );
    json_tree->update();
    return true;
}

std::string VertexWidgetPrivate::get_key_from_dom() const
{
    std::string key_str = "";
    json_tree->getValueViaPath<std::string>( "_id", key_str, ""  );
    return key_str;
}

void VertexWidgetPrivate::updt_view_menu()
{
    qUi->action_Show_comments->setChecked( JsonSchemaModel::showComments );
    qUi->action_Display_enums->setChecked( JsonSchemaModel::useEnumNames);
    qUi->action_Edit_id->setChecked( JsonSchemaModel::editID );
    qUi->actionKeep_Data_Fields_Expanded->setChecked( JsonView::expandedFields );
}

void VertexWidgetPrivate::updt_model()
{
    json_tree->resetModel();
}

void VertexWidgetPrivate::updt_table()
{
    json_tree->hide();
    json_tree->show();
}

void VertexWidgetPrivate::updt_db()
{
    db_keys->resetModel( "" );
    open_document("");
}

} // namespace jsonui17


