#include <QMessageBox>
#include <QKeyEvent>
#include <QHeaderView>
#include <QLineEdit>
#include "ui_EdgesWidget.h"
#include "EdgesWidget_p.h"
#include "jsonui17/SelectDialog.h"
#include "jsonui17/FileDialogs.h"
#include "jsonui17/preferences.h"
#include "jsonui17/service/waitingspinnerwidget.h"
#include "jsonio17/dbedgedoc.h"
#include "jsonui17/models/schema_model.h"
#include "models/dbkeys_view.h"
#include "models/dbquery_thread.h"
#ifndef IMPEX_OFF
#include "jsonimpex17/yaml_xml2file.h"
#endif

namespace jsonui17 {


EdgesWidget::EdgesWidget( const std::string& aschema_name, const jsonio17::DBQueryBase& query, QWidget *parent ) :
    BaseWidget(*new EdgesWidgetPrivate( aschema_name, query ), parent)
{
    auto d = impl_func();
    d->init();
}

EdgesWidget::~EdgesWidget()
{}

bool EdgesWidget::event(QEvent * e)
{
    switch(e->type())
    {
    case QEvent::WindowActivate :
        /// crash when show()
        /// problems reset model
        /// changeKeyList();
        break ;
    default: break;
    }

    return QMainWindow::event(e) ;
}

void EdgesWidget::closeEvent(QCloseEvent* e)
{
    auto d = impl_func();
    if( d->db_keys )
        d->db_keys->Close();
    if( d->incoming_keys )
        d->incoming_keys->Close();
    if( d->outgoing_keys )
        d->outgoing_keys->Close();

    BaseWidget::closeEvent(e);
}

std::string EdgesWidget::currentKey() const
{
    auto d = impl_func();
    return  d->get_key_from_dom();
}

void EdgesWidget::setQueryEdited( QueryWidget* queryW  )
{
    auto d = impl_func();
    d->start_process();
    d->db_keys->setOneQuery( queryW );
    d->incoming_keys->setOneQuery( queryW );
    d->outgoing_keys->setOneQuery( queryW );
}


void EdgesWidget::openRecordKey(  const QModelIndex& index, bool reset_in_out_query  )
{
    auto d = impl_func();
    std::string key_document = d->key_from_index(index);
    openRecordKey(  key_document, reset_in_out_query  );
}

void EdgesWidget::openRecordKey( const std::string& key_document, bool )
{
    auto d = impl_func();
    emit d->cmReadDocument( key_document.c_str() );
}

void EdgesWidget::changeKeyList()
{
    auto d = impl_func();
    d->change_keys_list();
}

void EdgesWidget::updateQuery()
{
    auto d = impl_func();
    d->start_process();
    emit d->cmReloadQuery();
}

// Menu commands -----------------------------------------------------------

void EdgesWidget::CmHelpContens()
{
    auto d = impl_func();
    helpWin( "Edges", d->current_schema_name );
}

/// Set default json record
void EdgesWidget::CmNew()
{
    auto d = impl_func();
    d->open_document( "" );
}

/// Clone json record (clear _id)
void EdgesWidget::CmClone()
{
    auto d = impl_func();
    d->update_oid( "" );
}

/// Read bson record from json file fileName
void EdgesWidget::CmImportJSON()
{
    try{
        auto d = impl_func();
        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                             "Please, select Structured Data file", domFilters  ))
        {
#ifndef IMPEX_OFF
            jsonio17::JsonYamlXMLFile file( fileName );
#else
            jsonio17::JsonFile file( fileName );
#endif
            if( !d->reset_json( file.load_json(), schema_from_name( fileName )) )
                jsonio17::JSONIO_THROW( "editors", 20, "Try to read another schema format file "+ fileName );
            d->set_content_state( true );
        }
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write document to json file fileName
void EdgesWidget::CmExportJSON()
{
    try {
        auto d = impl_func();
        std::string fileName = file_schema_ext( d->current_schema_name, "json");
        if(  ChooseFileSave( this, fileName,
                             "Please, select Structured Data file to write the data", domFilters, fileName  ))
        {
#ifndef IMPEX_OFF
            jsonio17::JsonYamlXMLFile file( fileName );
            file.set_type( jsonio17::TxtFile::Json );
#else
            jsonio17::JsonFile file( fileName );
#endif
            file.save( d->json_tree->current_object() );
        }
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Set up incoming query
void EdgesWidget::CmIncomingQuery()
{
    try {
        auto d = impl_func();
        d->incoming_keys->showQuery( "Incoming Query Widget", jsonio17::DataBase::getVertexesList() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "Incoming Query", e.what() );
    }
}

/// Set up incoming query
void EdgesWidget::CmOutgoingQuery()
{
    try {
        auto d = impl_func();
        d->outgoing_keys->showQuery( "Outgoing Query Widget", jsonio17::DataBase::getVertexesList() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "Outgoing Query", e.what() );
    }
}

/// Read new record from DB
void EdgesWidget::CmRead()
{
    try {
        const auto d = impl_func();
        auto aValList = d->db_keys_table->tableValues();
        if( aValList.empty() )
            return;

        SelectDialog selDlg( false, this, "Please, select a record to read/view", aValList );
        if( !selDlg.exec() )
            return;

        std::string reckey = aValList[selDlg.selIndex()][0].toString().toStdString();
        openRecordKey(  reckey  );
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Save current record to DB file
void EdgesWidget::CmUpdate()
{
    try {
        auto d = impl_func();
        d->cm_update();
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Delete current record from DB
void EdgesWidget::CmDelete()
{
    try {
        auto d = impl_func();
        auto key = d->get_key_from_dom();
        QString msg = QString("Confirm deletion of %1 record?").arg( key.c_str() );
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question( this, "Record to delete", msg,
                                       QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::No)
            return;

        emit d->cmDelete( key.c_str() );
        d->set_content_state( false );
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Delete records from List
void EdgesWidget::CmDeleteSelect()
{
    try {
        auto d = impl_func();
        d->cm_delete_select(select_keys_document("Please, select a records to delete."));
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void EdgesWidget::CmDisplaySearchResult()
{
    try {
        auto d = impl_func();
        d->db_keys->showResult("Edges Query Result window");
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "CmDisplaySearchResult", e.what() );
    }
}

void EdgesWidget::CmSearchQuery()
{
    try {
        auto d = impl_func();
        d->db_keys->showQuery( "Edges Query Widget", { d->current_schema_name } );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "CmSearchQuery", e.what() );
    }
}

/// Write multiple records to file fileName
void EdgesWidget::CmBackupQueriedRecordstoFile()
{
    try {
        auto d = impl_func();
        std::string fileName = file_schema_ext( d->current_schema_name, "json" );
        if(  ChooseFileSave( this, fileName,
                             "Please, select a file to write (document) records", domFilters, fileName  ))
        {
            std::vector<std::string> keysDocument = select_keys_document("Please, select a records to backup.");
            d->cm_backup_records( fileName, keysDocument );
        }
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Read multiple records from file fileName
void EdgesWidget::CmRestoreMultipleRecordsfromFile()
{

    try {
        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                             "Please, select a file with json object", domFilters  ))
        {
            auto d = impl_func();
            d->cm_restore_records(fileName);
        }
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write multiple records to file fileName
void EdgesWidget::CmBackupGraphtoFile()
{
    try {
        auto d = impl_func();
        std::string fileName = file_schema_ext( d->current_schema_name, "json" );
        if(  ChooseFileSave( this, fileName,
                             "Please, select a file to write (document) records", jsonFilters, fileName  ))
        {
            std::vector<std::string> keysDocument = select_keys_document("Please, select a records to backup graph.");
            d->cm_backup_graph( fileName, keysDocument );
        }
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Read multiple records from file fileName
void EdgesWidget::CmRestoreGraphfromFile()
{
    try {
        std::string fileName;
        if(  ChooseFileOpen( this, fileName,
                             "Please, select a file with json object", jsonFilters  ))
        {
            auto d = impl_func();
            d->cm_restore_graph(fileName);
        }
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Show incoming vertex window
void EdgesWidget::CmFollowInVertex()
{
    try {
        auto d = impl_func();
        std::string inV;
        d->json_tree->getValueViaPath<std::string>( "_to", inV, "" );
        if( inV.empty()  )
            return;
        std::string schemaName = d->extract_schema_from_id(  inV  );
        if( !schemaName.empty() )
            d->show_document_widget_function( true, schemaName, inV, jsonio17::DBQueryBase::emptyQuery() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "follow incoming vertex", e.what() );
    }
}

/// Show outgoing vertex window
void EdgesWidget::CmFollowOutVertex()
{
    try {
        auto d = impl_func();
        std::string outV;
        d->json_tree->getValueViaPath<std::string>( "_from", outV, "" );
        if( outV.empty() )
            return;
        std::string schemaName = d->extract_schema_from_id( outV );
        if( !schemaName.empty() )
            d->show_document_widget_function( true, schemaName, outV, jsonio17::DBQueryBase::emptyQuery() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "Follow outgoing vertex", e.what() );
    }
}

std::vector<std::string> EdgesWidget::select_keys_document( const char* title )
{
    const auto d = impl_func();
    std::vector<std::string> keysDocument;

    auto aValList = d->db_keys_table->tableValues();
    if( aValList.empty() )
        return keysDocument;

    SelectDialog selDlg( true, this, title, aValList, {}, MatrixTable::tbNoMenu|MatrixTable::tbSort );
    if( selDlg.exec() )
    {
        auto selNdx = selDlg.allSelected();
        for( int ndx:  selNdx )
            keysDocument.push_back( aValList[ndx][0].toString().toStdString() );
    }
    return keysDocument;
}


//------VertexWidgetPrivate-----------------------------------------------------------------------

EdgesWidgetPrivate::EdgesWidgetPrivate( const std::string& aschema_name, const jsonio17::DBQueryBase& query ):
    BaseWidgetPrivate( aschema_name ),
    qUi(nullptr), json_tree(nullptr),
    edge_query(query), is_default_query(!query.empty())
{}

EdgesWidgetPrivate::~EdgesWidgetPrivate()
{
    db_thread.quit();
    db_thread.wait();

    delete wait_dialog;
}

void EdgesWidgetPrivate::init()
{
    auto edgeNames = jsonio17::DataBase::getEdgesList();
    jsonio17::JSONIO_THROW_IF( edgeNames.empty(), "editors", 30, " no edges defined " );

    auto q = base_func();
    qUi.reset(new Ui::EdgesWidget);
    qUi->setupUi(q);

    qUi->keySplitter->setStretchFactor(0, 1);
    qUi->mainSplitter->setStretchFactor(0, 0);
    qUi->mainSplitter->setStretchFactor(1, 0);
    qUi->mainSplitter->setStretchFactor(2, 1);
    qUi->typeBox->setEnabled(!is_default_query);

    q->setAttribute(Qt::WA_DeleteOnClose); // automatically delete itself when window is closed
    QString title = qApp->applicationName()+" Structured Data Editor and Database Browser";
    q->setWindowTitle(title);

    // set up default data
    current_record = "";
    if( current_schema_name.empty() && !edgeNames.empty() )
        current_schema_name = edgeNames[0];
    //define schema checkbox
    for( size_t ii=0; ii<edgeNames.size(); ii++ )
        qUi->typeBox->addItem(edgeNames[ii].c_str());
    qUi->typeBox->setCurrentText(current_schema_name.c_str());

    // define all keys tables
    db_keys_table.reset( new DBKeysTable( qUi->keySplitter, [&]( const QModelIndex& index ){base_func()->openRecordKey( index );} ));
    qUi->keySplitter->insertWidget( 0, db_keys_table.get() );
    qUi->keySplitter->setStretchFactor(0, 4);
    QObject::connect( db_keys_table.get(), SIGNAL(clicked(const QModelIndex&)), q, SLOT(openRecordKey(const QModelIndex&)));

    // define table of incoming keys
    incoming_keys_table.reset( new DBKeysTable( qUi->inWidget,  []( const QModelIndex&  ){;} ) );
    qUi->verticalLayout_2->addWidget(incoming_keys_table.get());
    QObject::connect( incoming_keys_table.get(), &DBKeysTable::doubleClicked,
                      this, &EdgesWidgetPrivate::add_incoming);

    // define table of outgoing keys
    outgoing_keys_table.reset( new DBKeysTable( qUi->outWidget, []( const QModelIndex&  ){;} ));
    qUi->verticalLayout->addWidget(outgoing_keys_table.get());
    QObject::connect( outgoing_keys_table.get(), &DBKeysTable::doubleClicked,
                      this, &EdgesWidgetPrivate::add_outgoing);

    // define edit tree view
    QStringList aHeaderData;
    aHeaderData << "key" << "value" << "comment" ;
    json_tree.reset( new JsonView(  qUi->bsonWidget ) );
    auto schema_model = new JsonSchemaModel(  current_record, current_schema_name, aHeaderData, this/*qUi->centralWidget*/ );
    JsonSchemaDelegate * deleg_schema = new JsonSchemaDelegate();
    json_tree->setModel(schema_model);
    json_tree->setItemDelegate(deleg_schema);
    json_tree->setColumnWidth( 0, 150 );
    json_tree->expandToDepth(1);;
    qUi->gridLayout->addWidget( json_tree.get(), 1, 0, 1, 2);

    // define menu
    set_actions();
    wait_dialog = new WaitingSpinnerWidget( q, true/*Qt::ApplicationModal*/, true );
    // Open db connection and load keys table
    reset_DB_client( current_schema_name );
}

/// Change current Edge
void EdgesWidgetPrivate::type_changed( const QString& text )
{
    pLineTask->setText(text);
    current_schema_name = text.toStdString();
    current_record="";
    json_tree->updateModelData( current_record, current_schema_name );
    start_process();
    emit cmResetSchema( text );
}

void EdgesWidgetPrivate::reset_type_box( const QString& text )
{
    QObject::disconnect( qUi->typeBox, &QComboBox::currentTextChanged,
                         this, &EdgesWidgetPrivate::type_changed);
    qUi->typeBox->setCurrentText(text);
    pLineTask->setText(text);
    current_schema_name = text.toStdString();
    QObject::connect( qUi->typeBox, &QComboBox::currentTextChanged,
                      this, &EdgesWidgetPrivate::type_changed);
}

/// Start long database process
void EdgesWidgetPrivate::start_process()
{
    // possible open Waiting Spinner
    ui_logger->info("EdgesWidget start process");
    wait_dialog->start();
}

/// Finish long database process
void EdgesWidgetPrivate::finish_process()
{
    // possible close Waiting Spinner
    wait_dialog->stop();
    ui_logger->info("EdgesWidget finish process");
}

// Exception when database command execution
void EdgesWidgetPrivate::get_exception( const QString& title, const QString& msg )
{
    auto q = base_func();
    QMessageBox::critical( q, title, msg );
}


void EdgesWidgetPrivate::update_oid( QString oid )
{
    json_tree->setOid(oid.toStdString());
    if( oid.isEmpty() )
        json_tree->setValueViaPath("_rev", std::string(""));
    json_tree->hide();
    json_tree->show();
}

// After open new document
void EdgesWidgetPrivate::open_document(  QString json_document  )
{
    auto q = base_func();
    try {
        reset_json( json_document.toStdString(), "" );
        move_keys_list();
        set_content_state( false );
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( q, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( q, "std::exception", e.what() );
    }
}

/// Update db document query
void EdgesWidgetPrivate::change_DB_query( QString query )
{
    qUi->edgeQuery->setText( query );
}

void EdgesWidgetPrivate::add_incoming(  const QModelIndex& index )
{
    int row = index.row();
    QModelIndex add_index = index.sibling( row, 0);
    std::string key = add_index.model()->data(add_index).toString().toStdString();

    std::string id_get;
    // only to unsaved records
    if( !json_tree->getValueViaPath<std::string>( "_id", id_get, "" ) || id_get.empty()  )
    {
        json_tree->setValueViaPath("_to", key);
        json_tree->hide();
        json_tree->show();
    }
}

void EdgesWidgetPrivate::add_outgoing(  const QModelIndex& index )
{
    int row = index.row();
    QModelIndex add_index = index.sibling( row, 0);
    std::string key = add_index.model()->data(add_index).toString().toStdString();

    std::string id_get;
    // only to unsaved records
    if( !json_tree->getValueViaPath<std::string>( "_id", id_get, "" ) || id_get.empty()  )
    {
        json_tree->setValueViaPath("_from",key);
        json_tree->hide();
        json_tree->show();
    }
}

//----------------------------------------------------------------------------------

//  Connect all actions
void EdgesWidgetPrivate::set_actions()
{
    auto q = base_func();
    QObject::connect( qUi->typeBox, &QComboBox::currentTextChanged,
                      this, &EdgesWidgetPrivate::type_changed);

    // File
    QObject::connect( qUi->actionNew , &QAction::triggered, q, &EdgesWidget::CmNew);
    QObject::connect( qUi->action_Clone_Structured_Data_Object , &QAction::triggered, q, &EdgesWidget::CmClone);
    QObject::connect( qUi->actionE_xit, &QAction::triggered, q, &EdgesWidget::close);
    QObject::connect( qUi->actionExport_Json_File, &QAction::triggered, q, &EdgesWidget::CmExportJSON);
    QObject::connect( qUi->actionImport_Json_File, &QAction::triggered, q, &EdgesWidget::CmImportJSON);

    // Edit
    QObject::connect(qUi->actionAdd_one_field, &QAction::triggered, json_tree.get(), &JsonView::CmAddObject);
    QObject::connect(qUi->action_Clone_Current, &QAction::triggered, json_tree.get(), &JsonView::CmCloneObject);
    QObject::connect(qUi->action_Delete_field, &QAction::triggered, json_tree.get(), &JsonView::CmDelObject);
    QObject::connect(qUi->action_Delete_fields, &QAction::triggered, json_tree.get(), &JsonView::CmDelObjects);
    QObject::connect(qUi->actionRemove_Alternatives_Union, &QAction::triggered, json_tree.get(), &JsonView::CmDelObjectsUnion);
    QObject::connect(qUi->action_Add_fields, &QAction::triggered, json_tree.get(), &JsonView::CmAddObjects);
    QObject::connect(qUi->actionReset_Data_to_Defaults, &QAction::triggered, json_tree.get(), &JsonView::CmResetObject);
    QObject::connect(qUi->action_Resize_array, &QAction::triggered, json_tree.get(), &JsonView::CmResizeArray);
    QObject::connect(qUi->action_Calculator, &QAction::triggered, json_tree.get(), &JsonView::CmCalc);
    QObject::connect(qUi->actionCopy_Field_Path, &QAction::triggered, json_tree.get(), &JsonView::CopyFieldPath);
    QObject::connect(qUi->actionCopy_Field, &QAction::triggered, json_tree.get(), &JsonView::CopyField);
    QObject::connect(qUi->actionPaste_Field, &QAction::triggered, json_tree.get(), &JsonView::PasteField);

    // Help
    QObject::connect( qUi->action_Help_About, &QAction::triggered, q, &EdgesWidget::CmHelpAbout);
    QObject::connect( qUi->actionContents, &QAction::triggered, q, &EdgesWidget::CmHelpContens);
    QObject::connect( qUi->actionAuthors, &QAction::triggered, q, &EdgesWidget::CmHelpAuthors);
    QObject::connect( qUi->actionLicense, &QAction::triggered, q, &EdgesWidget::CmHelpLicense);

    // View
    QObject::connect( qUi->action_Show_comments, &QAction::toggled, &uiSettings(), &UIPreferences::CmShowComments);
    QObject::connect( qUi->action_Display_enums, &QAction::toggled, &uiSettings(), &UIPreferences::CmDisplayEnums);
    QObject::connect( qUi->action_Edit_id, &QAction::toggled, &uiSettings(), &UIPreferences::CmEditID);
    QObject::connect( qUi->actionKeep_Data_Fields_Expanded, &QAction::toggled, &uiSettings(), &UIPreferences::CmEditExpanded);
    updt_view_menu();
    QObject::connect( qUi->actionFollow_In_Vertex, &QAction::triggered, q, &EdgesWidget::CmFollowInVertex);
    QObject::connect( qUi->actionFollow_Out_Vertex, &QAction::triggered, q, &EdgesWidget::CmFollowOutVertex);

    // Record
    QObject::connect( qUi->actionNext_Record, &QAction::triggered, db_keys_table.get(), &DBKeysTable::CmNext);
    QObject::connect( qUi->actionPrevious_Record, &QAction::triggered, db_keys_table.get(), &DBKeysTable::CmPrevious);
    QObject::connect( qUi->action_Update, &QAction::triggered, q, &EdgesWidget::CmUpdate);
    //QObject::connect( qUi->action_Create, &QAction::triggered, q, &EdgesWidget::CmCreateInsert);
    QObject::connect( qUi->action_Delete, &QAction::triggered, q, &EdgesWidget::CmDelete);
    QObject::connect( qUi->actionDeleteMultiple, &QAction::triggered, q, &EdgesWidget::CmDeleteSelect);
    QObject::connect( qUi->actionSearch_Results, &QAction::triggered, q, &EdgesWidget::CmDisplaySearchResult);
    if( is_default_query )
    {
        qUi->actionSearch->setDisabled(true);
        qUi->action_Read->setDisabled(true);
    }
    else
    {
        QObject::connect( qUi->actionSearch, &QAction::triggered, q, &EdgesWidget::CmSearchQuery);
        QObject::connect( qUi->action_Read, &QAction::triggered, q, &EdgesWidget::CmRead);
    }
    QObject::connect( qUi->actionBackup_Queried_Records_to_File, &QAction::triggered,
                      q,  &EdgesWidget::CmBackupQueriedRecordstoFile);
    QObject::connect( qUi->actionRestore_Multiple_Records_from_File, &QAction::triggered,
                      q, &EdgesWidget::CmRestoreMultipleRecordsfromFile);
    QObject::connect( qUi->actionBackup_Graph_to_File, &QAction::triggered,
                      q,  &EdgesWidget::CmBackupGraphtoFile);
    QObject::connect( qUi->actionRestore_Graph_from_File, &QAction::triggered,
                      q, &EdgesWidget::CmRestoreGraphfromFile);
    QObject::connect( qUi->actionIncoming_Vertex_Query, &QAction::triggered, q, &EdgesWidget::CmIncomingQuery);
    QObject::connect( qUi->actionOutgoing_Vertex_Query, &QAction::triggered, q, &EdgesWidget::CmOutgoingQuery);

    pLineTask.reset( new QLineEdit( qUi->nameToolBar ) );
    pLineTask->setEnabled( true );
    pLineTask->setFocusPolicy( Qt::ClickFocus );
    pLineTask->setReadOnly( true );
    pLineTask->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
    QString title =  current_schema_name.c_str();
    pLineTask->setText(title);
    qUi->nameToolBar->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
    qUi->nameToolBar->addWidget( pLineTask.get() ); // setStretchableWidget( pLine );
}

void EdgesWidgetPrivate::reset_DB_client( const std::string& schema_name )
{
    //qRegisterMetaType<QVector<QString> >("QVector<QString>");
    qRegisterMetaType<std::vector<std::string> >("std::vector<std::string>");
    qRegisterMetaType<std::shared_ptr<jsonio17::DBQueryDef> >("std::shared_ptr<jsonio17::DBQueryDef>");

    try {
        auto q = base_func();
        db_keys.reset( new DBKeysObject( nullptr, db_keys_table.get(), q ) );
        incoming_keys.reset( new DBKeysObject( nullptr, incoming_keys_table.get(), q ));
        outgoing_keys.reset( new DBKeysObject( nullptr, outgoing_keys_table.get(), q ));

        db_request = new DBEdgeObject(  schema_name, edge_query  );

        QObject::connect( db_keys.get(), &DBKeysObject::updateQuery, db_request, &DBEdgeObject::dbChangeQuery );
        QObject::connect( incoming_keys.get(), &DBKeysObject::updateQuery, db_request, &DBEdgeObject::dbChangeIncomingQuery );
        QObject::connect( outgoing_keys.get(), &DBKeysObject::updateQuery, db_request, &DBEdgeObject::dbChangeOutgoingQuery );

        QObject::connect( db_request, &DBEdgeObject::changedDBClient, db_keys.get(), &DBKeysObject::changeDBClient );
        QObject::connect( db_request, &DBEdgeObject::changedIncomingDBClient, incoming_keys.get(), &DBKeysObject::changeDBClient );
        QObject::connect( db_request, &DBEdgeObject::changedOutgoingDBClient, outgoing_keys.get(), &DBKeysObject::changeDBClient );

        QObject::connect( db_request, &DBEdgeObject::changedModel,  db_keys.get(), &DBKeysObject::resetModel );
        QObject::connect( db_request, &DBEdgeObject::changedIncomingModel, incoming_keys.get(), &DBKeysObject::resetModel );
        QObject::connect( db_request, &DBEdgeObject::changedOutgoingModel, outgoing_keys.get(), &DBKeysObject::resetModel );

        // link from GUI

        QObject::connect( this, &EdgesWidgetPrivate::cmResetClient, db_request, &DBEdgeObject::dbResetClient );
        QObject::connect( this, &EdgesWidgetPrivate::cmResetSchema, db_request, &DBEdgeObject::dbResetSchema );
        QObject::connect( this, &EdgesWidgetPrivate::cmReadDocument, db_request, &DBEdgeObject::dbReadDocument );
        QObject::connect( this, &EdgesWidgetPrivate::cmUpdate, db_request, &DBEdgeObject::dbUpdate );
        QObject::connect( this, &EdgesWidgetPrivate::cmDelete, db_request, &DBEdgeObject::dbDelete );
        QObject::connect( this, &EdgesWidgetPrivate::cmDeleteList, db_request, &DBEdgeObject::dbDeleteList );
        QObject::connect( this, &EdgesWidgetPrivate::cmRestoreMultipleRecordsfromFile, db_request, &DBEdgeObject::dbRestoreMultipleRecordsfromFile );
        QObject::connect( this, &EdgesWidgetPrivate::cmBackupQueriedRecordstoFile, db_request, &DBEdgeObject::dbBackupQueriedRecordstoFile );
        //QObject::connect( this, &VertexWidget::cmImportFormat, db_request, &DBDocumentModel::dbImportFormat );
        //QObject::connect( this, &VertexWidget::cmExportFormat, db_request, &DBDocumentModel::dbExportFormat );
        QObject::connect( this, &EdgesWidgetPrivate::cmBackupGraphtoFile, db_request, &DBEdgeObject::dbBackupGraphtoFile );
        QObject::connect( this, &EdgesWidgetPrivate::cmRestoreGraphfromFile, db_request, &DBEdgeObject::dbRestoreGraphfromFile );
        QObject::connect( this, &EdgesWidgetPrivate::cmReloadQuery, db_request, &DBEdgeObject::dbReloadQuery );

        // link to GUI

        QObject::connect( db_request, &DBEdgeObject::changedQueryString, this, &EdgesWidgetPrivate::change_DB_query );
        QObject::connect( db_request, &DBEdgeObject::finished, this, &EdgesWidgetPrivate::finish_process );
        QObject::connect( db_request, &DBEdgeObject::isException, this, &EdgesWidgetPrivate::get_exception );
        QObject::connect( db_request, &DBEdgeObject::updatedOid, this, &EdgesWidgetPrivate::update_oid );

        QObject::connect( db_request, &DBEdgeObject::openedDocument, this, &EdgesWidgetPrivate::open_document );
        QObject::connect( db_request, &DBEdgeObject::deletedDocument, this, &EdgesWidgetPrivate::afterDeleteDocument );
        QObject::connect( db_request, &DBEdgeObject::loadedGraph, this, &EdgesWidgetPrivate::afterLoadGraph );

        QObject::connect( db_request, &DBEdgeObject::resetTypeBox, this, &EdgesWidgetPrivate::reset_type_box );
        QObject::connect( db_request, &DBEdgeObject::fixIncoming,
                          [&]() { qUi->actionIncoming_Vertex_Query->setDisabled(true); } );
        QObject::connect( db_request, &DBEdgeObject::fixOutgoing,
                          [&]() { qUi->actionOutgoing_Vertex_Query->setDisabled(true); } );

        // thread functions
        db_request->moveToThread(&db_thread);
        QObject::connect(&db_thread, &QThread::finished, db_request, &QObject::deleteLater);
        db_thread.start();

        // might be execute signals it other thread
        start_process();
        emit cmResetClient( schema_name.c_str() );
    }
    catch(std::exception& e)
    {
        ui_logger->error("Error when resetting edge DB client {}: {}", current_schema_name, e.what());
        throw;
    }
}

bool EdgesWidgetPrivate::reset_json( const std::string& json_string, const std::string& schema_name )
{
    // test legal schema name
    if( !current_schema_name.empty() && !schema_name.empty() && schema_name != current_schema_name )
        return false;
    current_record =  json_string;
    json_tree->updateModelData( json_string, current_schema_name );
    json_tree->update();
    return true;
}

std::string EdgesWidgetPrivate::get_key_from_dom() const
{
    std::string key_str = "";
    json_tree->getValueViaPath<std::string>( "_id", key_str, ""  );
    return key_str;
}

std::string  EdgesWidgetPrivate::extract_schema_from_id( const std::string& key_id )
{
    std::queue<std::string> names = jsonio17::split(key_id, "/");
    if(names.size()<=1)
        return "";
    std::string label = names.front();
    label.pop_back(); // delete last "s"
    return jsonio17::DataBase::getVertexName( label );
}

/// Change current View menu selections
void EdgesWidgetPrivate::updt_view_menu()
{
    qUi->action_Show_comments->setChecked( JsonSchemaModel::showComments );
    qUi->action_Display_enums->setChecked( JsonSchemaModel::useEnumNames);
    qUi->action_Edit_id->setChecked( JsonSchemaModel::editID );
    qUi->actionKeep_Data_Fields_Expanded->setChecked( JsonView::expandedFields );
}

/// Change model ( show/hide comments)
void EdgesWidgetPrivate::updt_model()
{
    json_tree->resetModel();
}

/// Change table (Show Enum Names Instead of Values)
void EdgesWidgetPrivate::updt_table()
{
    json_tree->hide();
    json_tree->show();
}

/// Update after change DB locations
void EdgesWidgetPrivate::updt_db()
{
    change_keys_list();
    open_document("");
}


} // namespace jsonui17

