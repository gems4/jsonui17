#include <QMessageBox>
#include "ui_QueryWidget.h"
#include "jsonui17/QueryWidget.h"
#include "jsonui17/SelectDialog.h"
#include "jsonui17/FileDialogs.h"
#include "jsonui17/BaseWidget.h"
#include "jsonui17/preferences.h"
#include "service/SchemaSelectDialog_p.h"
#include "models/query_model.h"
#ifndef IMPEX_OFF
#include "jsonimpex17/yaml_xml2file.h"
#endif

namespace jsonio17 {
std::string collectionNameFromSchema( const std::string& aschema_name );
}

namespace jsonui17 {


std::string QueryWidget::query_collection_name = "queries";

jsonio17::DBSchemaDocument* newDBQueryClient(  const jsonio17::DBQueryBase& query  )
{
    return  jsonio17::DBSchemaDocument::newSchemaDocumentQuery( uiSettings().resources_database(),  "Query",
                                                                QueryWidget::query_collection_name,  query );
}

/// private \class QueryWidget window to insert/edit  database query
class QueryWidgetPrivate
{

#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    Q_DISABLE_COPY_MOVE(QueryWidgetPrivate)
#endif

public:

    explicit QueryWidgetPrivate( QueryWidget *qdialog, const char* title,
                                 const std::vector<std::string>& schema_names,
                                 const jsonio17::DBQueryDef& query  );
    ~QueryWidgetPrivate()
    { }

    void AQL_toggled( bool checked );

    void schema_changed( const QString& text )
    {
        if( schema_name != text.toStdString())
        {
            schema_name = text.toStdString();
            dbquery->setQuery( new_schema_query() );
        }
    }

    void select_restore()
    {
        query_description.setFields( default_query.fields() );
        lstmodel->setStringList( query_description.fields() );
    }

    void where_apply()
    {
        auto collection = qUi->qSchema->currentText().toStdString();
        collection = jsonio17::collectionNameFromSchema( collection );
        auto qrstr = queryModel->getQuery( get_query_type(), collection );
        if( !qrstr.empty() )
            qUi->queryText->setText (qrstr.queryString().c_str() );
    }

    void where_restore()
    {
        auto selection = query_description.fields();
        query_description = default_query;
        query_description.setFields( selection );
        show_query();
    }

    void clear_tree()
    {
        query_edit->ClearAll();
    }

    void clone()
    {
        qId->setText("");
    }

    void read( const std::string& reckey)
    {
        dbquery->readDocument( reckey );
        fromJsonNode( dbquery->loaded_data() );
    }

    void update()
    {
        if( dbquery.get() == nullptr )
            return;
        auto  query_object = jsonio17::JsonFree::object();
        toJsonNode( query_object );
        dbquery->updateFromJson( query_object.dump(true) );
    }

    void delete_list( const std::vector<std::string>& keys )
    {
        for( const auto& key: keys)
            dbquery->deleteDocument(key);
        // dbquery->removeByKeys( keys );
    }

    /// Return query
    const jsonio17::DBQueryDef& get_query()
    {
        query_description.setName( qUi->qName->text().toStdString());
        query_description.setComment( qUi->qComment->text().toStdString());
        query_description.setSchema( qUi->qSchema->currentText().toStdString());
        std::string queryText = qUi->queryText->toPlainText().toStdString();
        query_description.setCondition( std::make_shared<jsonio17::DBQueryBase>( queryText, get_query_type() ));

        return query_description;
    }

    /// Return query schema
    std::string get_schema() const
    {
        return qUi->qSchema->currentText().toStdString();
    }

    /// Update query
    void update_query( const std::vector<std::string>& schema_names, const jsonio17::DBQueryDef& query )
    {
        default_query = query;
        query_description = query;

        qUi->qSchema->clear();
        for( size_t ii=0; ii<schema_names.size(); ii++ )
            qUi->qSchema->addItem(schema_names[ii].c_str());

        show_query();
    }

private:

    QueryWidget * const  base_ptr;
    inline QueryWidget* base_func() { return static_cast<QueryWidget *>(base_ptr); }
    inline const QueryWidget* base_func() const { return static_cast<const QueryWidget *>(base_ptr); }
    friend class QueryWidget;


    QScopedPointer<Ui::QueryWidget> qUi;
    QWidget* result_widget = nullptr;

    StringVectorModel* lstmodel = nullptr;
    QScopedPointer<QueryView> query_edit;
    QueryModel* queryModel = nullptr;
    QScopedPointer<QLineEdit> qId;

    std::string schema_name;
    std::shared_ptr<jsonio17::DBSchemaDocument> dbquery;
    jsonio17::DBQueryDef default_query;
    jsonio17::DBQueryDef query_description;

    /// Reset current DB client
    void reset_DB_client();
    ///  Connect all actions
    void set_actions();

    void show_query();
    jsonio17::DBQueryBase::QType get_query_type() const;
    void toJsonNode( jsonio17::JsonBase& object );
    void fromJsonNode( const jsonio17::JsonBase& object );
    jsonio17::DBQueryBase new_schema_query();

};

QueryWidgetPrivate::QueryWidgetPrivate( QueryWidget *qdialog, const char* title,
                                        const std::vector<std::string>& schema_names,
                                        const jsonio17::DBQueryDef& query ):
    base_ptr( qdialog ),
    qUi( new Ui::QueryWidget ),
    query_edit(nullptr),
    schema_name(schema_names[0]),
    default_query(query),
    query_description( query )
{
    auto q = base_func();
    qUi->setupUi(q);
    q->setWindowTitle(title);

    qUi->splitterSelect->setStretchFactor(0, 1);
    qUi->splitterSelect->setStretchFactor(1, 2);

    for( size_t ii=0; ii<schema_names.size(); ii++ )
        qUi->qSchema->addItem(schema_names[ii].c_str());

    // set up select model
    lstmodel = new StringVectorModel( query_description.fields(), q );
    qUi->fieldsList->setModel(lstmodel);
    //qUi->fieldsList->setSelectionBehavior(QAbstractItemView::SelectRows);
    qUi->fieldsList->setSelectionMode(QAbstractItemView::MultiSelection);
    qUi->fieldsList->setEditTriggers( QAbstractItemView::NoEditTriggers );

    // set up query edit mode
    query_edit.reset( new QueryView( qUi->frameTree  ) );
    queryModel = new QueryModel( q );
    QueryDelegate*  query_deleg =  new QueryDelegate( q );
    query_edit->setModel(queryModel);
    query_edit->setItemDelegate(query_deleg);
    query_edit->setColumnWidth( 0, 350 );
    qUi->gridLayout_2->addWidget( query_edit.get(), 1, 0, 1, 1 );

    // connect commands
    set_actions();
    reset_DB_client();
    // set up where part
    show_query();
}


//-----------QueryWidget-------------------------------------------------------------------------

QueryWidget::QueryWidget( const char* title,  const std::vector<std::string>& schema_names,
                          const jsonio17::DBQueryDef& query,   QWidget *parent ):
    QMainWindow(parent),
    impl_ptr( new QueryWidgetPrivate( this, title, schema_names, query ))
{
    impl_ptr->result_widget = parent;
}

QueryWidget::~QueryWidget()
{ }

std::string QueryWidget::getSchema() const
{
    const auto d = impl_func();
    return d->get_schema();
}

const jsonio17::DBQueryDef& QueryWidget::getQuery()
{
    const auto d = impl_func();
    return d->get_query();
}

void QueryWidget::updateQuery( const std::vector<std::string>& schema_names, const jsonio17::DBQueryDef& query )
{
    const auto d = impl_func();
    d->update_query(schema_names, query);
}

void QueryWidget::CmSaveQuery()
{
    try {
        std::string fileName = "*query.json";
        if(  ChooseFileSave( this, fileName, "Please, select a file to save current query",
                             domFilters, fileName  ))
        {
#ifndef IMPEX_OFF
            jsonio17::JsonYamlXMLFile file( fileName );
#else
            jsonio17::JsonFile file( fileName );
#endif
            auto  query_object = jsonio17::JsonFree::object();
            const auto d = impl_func();
            d->toJsonNode( query_object );
            file.save( query_object );
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


void QueryWidget::CmLoadQuery()
{
    try {
        std::string fileName;
        if(  !ChooseFileOpen( this, fileName, "Please, select a file with saved query to execute", domFilters  ))
            return;

#ifndef IMPEX_OFF
        jsonio17::JsonYamlXMLFile file( fileName );
#else
        jsonio17::JsonFile file( fileName );
#endif
        auto  query_object = jsonio17::JsonFree::object();
        file.load(query_object);
        const auto d = impl_func();
        d->fromJsonNode( query_object );
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Clone current query
void QueryWidget::CmClone()
{
    try {
        const auto d = impl_func();
        d->clone();
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void QueryWidget::CmRead()
{
    try {
        const auto d = impl_func();
        if( d->dbquery.get() == nullptr )
            return;

        auto reckey = SelectKeyFrom( d->dbquery.get(), this, "Please, select a record to read/view" );
        if( !reckey.empty() )
            d->read( reckey );
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Save current record to DB file
void QueryWidget::CmUpdate()
{
    try {
        const auto d = impl_func();
        d->update();
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void QueryWidget::CmDeleteList()
{
    try {
        const auto d = impl_func();
        if( d->dbquery.get() == nullptr )
            return;

        auto keys = SelectKeysFrom( d->dbquery.get(), this, "Please, select records to be deleted" );
        if( !keys.empty() )
            d->delete_list( keys );
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void QueryWidget::CmExecute()
{
    try {
        const auto d = impl_func();
        BaseWidget* parentdlg = dynamic_cast<BaseWidget*>(d->result_widget);
        if(parentdlg)
            parentdlg->setQueryEdited( this );
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }

}

void QueryWidget::CmSelectFields()
{
    try {
        const auto d = impl_func();
        SchemaSelectDialog dlg( this, "Please, mark data fields for the ouput table",
                                d->schema_name, d->query_description.fields());
        if( dlg.exec() )
        {
            d->query_description.setFields( dlg.allSelected());
            d->lstmodel->setStringList( d->query_description.fields() );
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void QueryWidget::CmAddSelectFields()
{
    try  {
        const auto d = impl_func();
        std::vector<std::string> f_list;
        SchemaSelectDialog dlg( this, "Please, mark addition data fields for the ouput table",
                                d->schema_name, f_list);
        if( dlg.exec() )
        {
            f_list = dlg.allSelected();
            d->query_description.addFields(f_list);
            d->lstmodel->setStringList( d->query_description.fields() );
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void QueryWidget::CmWhereApply()
{
    try {
        const auto d = impl_func();
        d->where_apply();
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void QueryWidget::CmWhereRestore()
{
    try {
        const auto d = impl_func();
        d->where_restore();
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void QueryWidget::CmAddWhereFields()
{
    try {
        const auto d = impl_func();
        std::vector<std::string> f_list;
        SchemaSelectDialog dlg( this, "Please, mark data fields for the query table",
                                d->schema_name, f_list);
        if( dlg.exec() )
        {
            f_list = dlg.allSelected();
            auto f_desc = dlg.allSelectedTypes();
            d->query_edit->CmAddObjects( f_list, f_desc );
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void QueryWidget::CmHelp()
{
    const auto d = impl_func();
    helpWin( d->schema_name, "" );
}

void QueryWidget::CmHelpAbout()
{
    helpWin( "QueryWidgetJsonui", "" );
}

bool QueryWidget::select_line( std::string& fldpath, const jsonio17::FieldDef** flddata )
{
    const auto d = impl_func();
    SchemaSelectDialog dlg( this, "Please, mark data fields for the query table",
                            d->schema_name );
    if( !dlg.exec() )
        return false;
    fldpath = dlg.selIndex(flddata);
    return !fldpath.empty();
}

//----------QueryWidgetPrivate------------------------------------------------------------


void QueryWidgetPrivate::AQL_toggled(bool checked)
{
    qUi->actionAdd_and->setEnabled(checked);
    qUi->actionAdd_or->setEnabled(checked);
    qUi->actionAdd_not->setEnabled(checked);

    qUi->actionAdd_gt->setEnabled(checked);
    qUi->actionAdd_gte->setEnabled(checked);
    qUi->actionAdd_lt->setEnabled(checked);
    qUi->actionAdd_lte->setEnabled(checked);
    qUi->actionAdd_noteq->setEnabled(checked);
    qUi->actionAdd_eq->setEnabled(checked);

    qUi->actionAdd_in->setEnabled(checked);
    qUi->actionAdd_nin->setEnabled(checked);
    qUi->actionAdd_like->setEnabled(checked);
    qUi->actionAdd_regexp->setEnabled(checked);
    qUi->actionAdd_not_regexp->setEnabled(checked);

    qUi->actionAdd_List_Fieldpaths->setEnabled(!checked);
    if( query_edit->AQLmode(checked) )
    { // changed mode
      if( ( checked && default_query.condition()->type() == jsonio17::DBQueryBase::qAQL)  ||
          ( !checked && default_query.condition()->type() ==  jsonio17::DBQueryBase::qTemplate) )
         qUi->queryText->setText(default_query.condition()->queryString().c_str());
      else
        qUi->queryText->setText("");
    }
}

void QueryWidgetPrivate::reset_DB_client()
{
    try{
        jsonio17::DBQueryBase qrdata( jsonio17::DBQueryBase::qAll );
        jsonio17::DBSchemaDocument* newClient = newDBQueryClient(  qrdata  );
        if( newClient == nullptr )
            dbquery.reset();
        else
            dbquery.reset( newClient );
    }
    catch(std::exception& e)
    {
        ui_logger->error("Error when resetting query DB client: {}", e.what());
        throw;
    }
}

//  Connect all actions
void QueryWidgetPrivate::set_actions()
{
    auto q = base_func();
    QObject::connect( qUi->qSchema, &QComboBox::currentTextChanged, [&](const QString& text) { schema_changed(text); } );
    QObject::connect( qUi->pAQL, &QRadioButton::toggled, [&](bool checked) { AQL_toggled(checked);  } );

    // File
    QObject::connect( qUi->action_Save_Current_Query_to_File, &QAction::triggered, q, &QueryWidget::CmSaveQuery);
    QObject::connect( qUi->action_Load_Query_from_File, &QAction::triggered, q, &QueryWidget::CmLoadQuery);
    QObject::connect( qUi->action_Execute_Current_Query, &QAction::triggered, q, &QueryWidget::CmExecute);
    QObject::connect( qUi->action_Accept_curent_Query, &QAction::triggered, q, &QueryWidget::CmApplay);
    QObject::connect( qUi->action_Close_Cancel, &QAction::triggered, q, &QueryWidget::close);

    // Select part
    QObject::connect( qUi->action_Select_Fieldpaths, &QAction::triggered, q, &QueryWidget::CmSelectFields);
    QObject::connect( qUi->action_Add_Fieldpaths, &QAction::triggered, q, &QueryWidget::CmAddSelectFields);
    QObject::connect( qUi->action_restote_select, &QAction::triggered, [&](){ select_restore(); } );

    // Generate part
    QObject::connect(qUi->action_Apply_from_Tree, &QAction::triggered, q, &QueryWidget::CmWhereApply);
    QObject::connect(qUi->action_Restore_Query, &QAction::triggered, q, &QueryWidget::CmWhereRestore);
    QObject::connect(qUi->action_Clear_Tree, &QAction::triggered, [&](){ clear_tree(); } );

    // FILTER
    QObject::connect(qUi->actionAdd_List_Fieldpaths, &QAction::triggered, q, &QueryWidget::CmAddWhereFields);
    QObject::connect(qUi->actionAdd_Fieldpath, &QAction::triggered, query_edit.get(), &QueryView::CmAddField);

    QObject::connect(qUi->actionAdd_and, &QAction::triggered, query_edit.get(), &QueryView::CmAddAnd);
    QObject::connect(qUi->actionAdd_or, &QAction::triggered, query_edit.get(), &QueryView::CmAddOr);
    QObject::connect(qUi->actionAdd_not, &QAction::triggered, query_edit.get(), &QueryView::CmAddNot);

    QObject::connect(qUi->actionAdd_eq, &QAction::triggered, query_edit.get(), &QueryView::CmAddEq);
    QObject::connect(qUi->actionAdd_noteq, &QAction::triggered, query_edit.get(), &QueryView::CmAddNotEq);
    QObject::connect(qUi->actionAdd_gt, &QAction::triggered, query_edit.get(), &QueryView::CmAddGt);
    QObject::connect(qUi->actionAdd_gte, &QAction::triggered, query_edit.get(), &QueryView::CmAddGte);
    QObject::connect(qUi->actionAdd_lt, &QAction::triggered, query_edit.get(), &QueryView::CmAddLt);
    QObject::connect(qUi->actionAdd_lte, &QAction::triggered, query_edit.get(), &QueryView::CmAddLte);

    QObject::connect(qUi->actionAdd_in, &QAction::triggered, query_edit.get(), &QueryView::CmAddIn);
    QObject::connect(qUi->actionAdd_nin, &QAction::triggered, query_edit.get(), &QueryView::CmAddNin);
    QObject::connect(qUi->actionAdd_like, &QAction::triggered, query_edit.get(), &QueryView::CmAddLike);
    QObject::connect(qUi->actionAdd_regexp, &QAction::triggered, query_edit.get(), &QueryView::CmAddRegexp);
    QObject::connect(qUi->actionAdd_not_regexp, &QAction::triggered, query_edit.get(), &QueryView::CmAddNotRegexp);

    QObject::connect(qUi->actionDelete_Line, &QAction::triggered, query_edit.get(), &QueryView::CmDelLine);

    // Database
    QObject::connect( qUi->action_Update, &QAction::triggered, q, &QueryWidget::CmUpdate);
    QObject::connect( qUi->action_Clone, &QAction::triggered, q, &QueryWidget::CmClone);
    QObject::connect( qUi->action_DeleteList, &QAction::triggered, q, &QueryWidget::CmDeleteList);
    QObject::connect( qUi->action_Read, &QAction::triggered, q, &QueryWidget::CmRead);

    // Help
    QObject::connect(qUi->actionHelp_About, &QAction::triggered, q, &QueryWidget::CmHelpAbout);
    QObject::connect(qUi->action_Help, &QAction::triggered, q, &QueryWidget::CmHelp);

    qId.reset( new QLineEdit( qUi->nameToolBar ) );
    qId->setEnabled( true );
    qId->setReadOnly( true );
    qId->setFocusPolicy( Qt::StrongFocus );
    qId->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
    QString title =  "";
    qId->setText(title);
    qUi->nameToolBar->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
    QLabel *aname = new QLabel( "_id",qUi->nameToolBar );
    qUi->nameToolBar->addWidget( aname );
    qUi->nameToolBar->addWidget( qId.get() ); // setStretchableWidget( pLine );
}

void QueryWidgetPrivate::show_query()
{
    qUi->qName->setText( query_description.name().c_str() );
    qUi->qComment->setText( query_description.comment().c_str() );
    qUi->qSchema->setCurrentText( query_description.schema().c_str());

    auto type = query_description.condition()->type();
    if( type ==  jsonio17::DBQueryBase::qAQL )
        qUi->pAQL->setChecked(true);
    /* else
        if( type ==  jsonio17::DBQueryBase::qEJDB )
            qUi->pEJDB->setChecked(true);*/
    else
        qUi->pTemplate->setChecked(true);
    AQL_toggled( qUi->pAQL->isChecked() );

    lstmodel->setStringList( query_description.fields() );
    qUi->queryText->setText( query_description.condition()->queryString().c_str() );

    // clear internal
    qId->setText("");
    clear_tree();
}

jsonio17::DBQueryBase::QType QueryWidgetPrivate::get_query_type() const
{
    jsonio17::DBQueryBase::QType type = jsonio17::DBQueryBase::qTemplate;
    if( qUi->pAQL->isChecked() )
        type = jsonio17::DBQueryBase::qAQL;
    return type;
}

// set data to json object
void QueryWidgetPrivate::toJsonNode( jsonio17::JsonBase& object )
{
    jsonio17::DBQueryDef quer = get_query();
    quer.toJson(object);
    object.set_oid(qId->text().toStdString());
    // Filter
    object.set_value_via_path( "filter_generator", queryModel->getFILTER() );
}

void QueryWidgetPrivate::fromJsonNode(  const jsonio17::JsonBase& object )
{
    query_description.fromJson(object);
    show_query();
    std::string stroid;
    object.get_value_via_path<std::string>( "_id", stroid, "" );
    qId->setText( stroid.c_str() );

    // Filter
    std::string filter;
    object.get_value_via_path<std::string>( "filter_generator", filter, "" );
    if( !filter.empty() )
    {
        queryModel->setFILTER(filter);
    }
}

jsonio17::DBQueryBase QueryWidgetPrivate::new_schema_query()
{
    std::string queryString = "";
    if( !schema_name.empty() )
    {
        queryString = "{ \"qschema\": \"";
        queryString += schema_name;
        queryString += "\" }";
    }
    return jsonio17::DBQueryBase(queryString, jsonio17::DBQueryBase::qTemplate );
}



} // namespace jsonui17
