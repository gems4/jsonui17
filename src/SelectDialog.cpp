
#include <QHeaderView>
#include "ui_SelectDialog4.h"
#include "jsonui17/SelectDialog.h"
#include "jsonui17/preferences.h"
#include "jsonio17/exceptions.h"
#include "jsonio17/dbdocument.h"

namespace jsonui17 {


// Select document key from current query result for collection
std::string SelectKeyFrom( const jsonio17::DBDocumentBase* documents_collection, QWidget* parent, const char* title, int mode )
{
    std::vector<std::string> aKeyList;
    std::vector<jsonio17::values_t> aValList;
    documents_collection->currentQueryResult().getKeysValues( aKeyList, aValList );
    if( aKeyList.empty() )
        return "";

    SelectDialog selDlg( false, parent, title, aValList, {}, mode  );
    if( selDlg.exec() )
    {
        auto selndx = selDlg.selIndex();
        if( selndx >= 0 )
            return aKeyList[selndx];
    }
    return "";
}


/// Select document keys from current query result for collection
std::vector<std::string> SelectKeysFrom( const jsonio17::DBDocumentBase* documents_collection, QWidget* parent, const char* title, int mode )
{
    std::vector<std::string> keys;
    std::vector<std::string> aKeyList;
    std::vector<jsonio17::values_t> aValList;
    documents_collection->currentQueryResult().getKeysValues( aKeyList, aValList );
    if( aKeyList.empty() )
        return keys;

    SelectDialog selDlg( true, parent, title,  aValList, {}, mode );
    if( selDlg.exec() )
    {
        auto selNdx = selDlg.allSelected();
        for( auto sel: selNdx )
            keys.push_back( aKeyList[sel] );
    }
    return keys;
}

// ---------------------------------------------------------------------------------------------------------


SelectTableContainer::SelectTableContainer( const char * aname,
                                            const std::vector<std::string>& list,
                                            const char  split_col_symb ):
    AbstractContainer(aname)
{
    if(list.size() >0 )
    {
        int jj;
        QString line;
        QStringList cells;

        // get values
        for(size_t ii=0;  ii<list.size(); ii++ )
        {
            line =  list[ii].c_str();
#if (QT_VERSION < QT_VERSION_CHECK(5, 15, 0))
            cells = line.split(split_col_symb, QString::KeepEmptyParts);
#else
            cells = line.split(split_col_symb, Qt::KeepEmptyParts);
#endif
            QVector<QVariant> vec;
            for( jj=0; jj<cells.count(); jj++ )
                vec.push_back( cells[jj] );
            matrix.push_back( vec );
        }

        // get header
        for( jj=0; jj<cells.count(); jj++ )
            col_heads.push_back( QString("%1").arg(jj) );
    }
}

SelectTableContainer::SelectTableContainer( const char * aname,
                                            const jsonio17::values_table_t& table_values,
                                            const jsonio17::values_t& header_data):
    AbstractContainer(aname)
{

    for( const auto& hdata: header_data )
        col_heads.push_back( hdata.c_str() );

    if( table_values.size() > 0 )
    {
        size_t jj;
        // get header
        if( col_heads.empty() )
            for( jj=0; jj< table_values[0].size(); jj++ )
                col_heads.push_back( QString("%1").arg(jj) );

        // get values
        size_t columns = col_heads.count();
        for( const auto& rdata: table_values )
        {
            QVector<QVariant> vec;
            for( jj=0; jj< columns; jj++ )
            {
                if( jj < rdata.size() )
                    vec.push_back( rdata[jj].c_str() );
                else
                    vec.push_back( "" );
            }
            matrix.push_back( vec );
        }
    }
}

//----------------------------------------------------------------

/// private \class SelectDialogPrivate - dialog for single or multiple
/// selection from list/table;
class SelectDialogPrivate
{

#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    Q_DISABLE_COPY_MOVE(SelectDialogPrivate)
#endif

public:

    /// Constructor
    explicit SelectDialogPrivate( SelectDialog *qdialog, bool is_multi_selection, int mode,
                                  std::shared_ptr<SelectTableContainer> adata_container ):
        base_ptr( qdialog ),
        qUi( new Ui::SelectDialogData ),
        selection_table(nullptr),
        data_container( std::move(adata_container) ),
        multi(is_multi_selection), tablemode(mode)
    {
        auto q = base_func();
        qUi->setupUi(q);
    }

    /// Destructor
    virtual ~SelectDialogPrivate()
    { }

    /// Set Singler selection
    void set_selection( int selrow )
    {
        selection_table->setCurrentRow( selrow );
    }

    /// Set Multiple selection
    void set_selection( std::set<size_t>& selrows )
    {
        selection_table->selectRows( selrows );
    }

    /// Returns single selection, \returns '-1' if nothing selected
    int sel_index() const
    {
        return selection_table->selectedRow();
    }

    /// Returns selection array, array is empty if nothing is selected
    std::set<size_t> all_selected() const
    {
        return selection_table->allSelectedRows();
    }

    void select_all()
    {
        if( multi == true )
            selection_table->selectAll();
    }

    void clear_all()
    {
        selection_table->clearSelection();
    }

private:

    SelectDialog * const  base_ptr;
    inline SelectDialog* base_func() { return static_cast<SelectDialog *>(base_ptr); }
    inline const SelectDialog* base_func() const { return static_cast<const SelectDialog *>(base_ptr); }
    friend class SelectDialog;

    QScopedPointer<Ui::SelectDialogData> qUi;
    QScopedPointer<MatrixTableProxy> selection_table;
    std::shared_ptr<SelectTableContainer> data_container;
    bool multi;
    int tablemode;

    void define_selection_table();
    void define_single_selection_table( const char* title );
    void define_multi_selection_table( const char* title );
};


void SelectDialogPrivate::define_selection_table()
{
    auto q = base_func();

    QObject::connect(qUi->PushButton1, &QPushButton::clicked, q, &QDialog::accept);
    QObject::connect(qUi->PushButton4, &QPushButton::clicked, q, &QDialog::reject);
    QObject::connect(qUi->pButton2, &QPushButton::clicked, q, &SelectDialog::CmSelectAll);
    QObject::connect(qUi->pButton3, &QPushButton::clicked, q, &SelectDialog::CmClearAll);

    MatrixModel* model = new MatrixModel( data_container, q );
    selection_table.reset( new MatrixTableProxy( q, tablemode/*TMatrixTable::tbNoMenu*/ ) );
    MatrixDelegate* deleg = new MatrixDelegate();
    selection_table->setItemDelegate(deleg);
    selection_table->setModel(model);  // proxy into

    if( !(tablemode & MatrixTable::tbSort) )
    {
        selection_table->horizontalHeader()->hide();
    }
    qUi->gridLayout->addWidget( selection_table.get(), 1, 0, 1, 5);
    selection_table->setSelectionBehavior(QAbstractItemView::SelectRows);
    //selection_table->setEditTriggers( QAbstractItemView::AnyKeyPressed );
}

void SelectDialogPrivate::define_single_selection_table(const char *title)
{
    auto q = base_func();
    q->setWindowTitle(title);

    define_selection_table();
    selection_table->setSelectionMode(QAbstractItemView::SingleSelection);
    selection_table->setCurrentRow( 0 );

    qUi->pButton3->hide();
    qUi->pButton2->hide();
    QObject::connect( qUi->bHelp, &QPushButton::clicked, q, &SelectDialog::CmHelp);
    QObject::connect( selection_table.get(), SIGNAL(doubleClicked(const QModelIndex&)), q, SLOT(accept()) );
    selection_table->setFocus();
}


void SelectDialogPrivate::define_multi_selection_table(const char *title)
{
    auto q = base_func();
    q->setWindowTitle(title);

    define_selection_table();
    selection_table->setSelectionMode(QAbstractItemView::MultiSelection);
    QObject::connect( qUi->bHelp, &QPushButton::clicked, q, &SelectDialog::CmHelp);
    selection_table->setFocus();
}


//----------------------------------------------------------------

// Single select constructor
SelectDialog::SelectDialog( bool is_multi_selection, QWidget* parent, const char* title,
                            const std::vector<std::string>& list, const char  split_col_symb, int mode ):
    QDialog(parent),
    impl_ptr( new SelectDialogPrivate( this, is_multi_selection, mode, std::make_shared<SelectTableContainer>("select",list,split_col_symb) ))
{
    auto d = impl_func();
    if( is_multi_selection )
        d->define_multi_selection_table( title );
    else
        d->define_single_selection_table( title );
}

SelectDialog::SelectDialog( bool is_multi_selection, QWidget* parent, const char* title,
                            const jsonio17::values_table_t& table_values, const jsonio17::values_t& header_data, int mode ):
    QDialog(parent),
    impl_ptr( new SelectDialogPrivate( this, is_multi_selection, mode, std::make_shared<SelectTableContainer>( "select", table_values, header_data) ))
{
    auto d = impl_func();
    if( is_multi_selection )
        d->define_multi_selection_table( title );
    else
        d->define_single_selection_table( title );
}

// Single select constructor.
SelectDialog::SelectDialog( bool is_multi_selection, QWidget* parent, const char* title,
                            const QVector< QVector<QVariant> >& table_values,  const QVector< QString >& header_data, int mode ):
    QDialog(parent),
    impl_ptr( new SelectDialogPrivate( this, is_multi_selection, mode, std::make_shared<SelectTableContainer>( "select", table_values, header_data) ))
{
    auto d = impl_func();
    if( is_multi_selection )
        d->define_multi_selection_table( title );
    else
        d->define_single_selection_table( title );
}

// Destructor
SelectDialog::~SelectDialog()
{ }

void SelectDialog::setSelection(int selrow)
{
    auto d = impl_func();
    d->set_selection( selrow );
}

void SelectDialog::setSelection(std::set<size_t> &selrows)
{
    auto d = impl_func();
    d->set_selection( selrows );
}

int SelectDialog::selIndex() const
{
    if( !result() )
        return -1;

    auto d = impl_func();
    return d->sel_index();
}

std::set<size_t> SelectDialog::allSelected() const
{
    if( !result() )
        return {};

    auto d = impl_func();
    return d->all_selected();
}

// Returns single selection,
// Throw exeption if nothing selected
size_t SelectDialog::getSelectedIndex() const
{
    auto selndx = selIndex();
    jsonio17::JSONIO_THROW_IF( selndx<0, "models", 11, "undefined single selection" );
    return static_cast<size_t>(selndx);
}

void SelectDialog::CmHelp()
{
    helpWin( "SelectJsonui", "" );
}

void SelectDialog::CmSelectAll()
{
    auto d = impl_func();
    d->select_all();
}

void SelectDialog::CmClearAll()
{
    auto d = impl_func();
    d->clear_all();
}

} // namespace jsonui17


