#include <QMessageBox>
#include <QFile>
#include "jsonio17/exceptions.h"
#include "ui_TableEditWindow.h"
#include "jsonui17/TableEditWindow.h"
#include "jsonui17/charts/GraphDialog.h"
#include "jsonui17/FileDialogs.h"
#include "jsonui17/preferences.h"
#include "service/CSVPage_p.h"

namespace jsonui17 {


void removeComments( QString& valCsv );


/// Private \class TableEditWidget  widget to work with table data.
class TableEditWidgetPrivate
{

#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    Q_DISABLE_COPY_MOVE(TableEditWidgetPrivate)
#endif

public:

    /// Constractor connect to extern table model
    explicit TableEditWidgetPrivate( TableEditWidget *qdialog, const char* title,
                                     std::shared_ptr<AbstractContainer> datacontainer,
                                     int table_mode, bool csv_mode ):
        base_ptr( qdialog ),
        qUi( new Ui::TableEditWidget ),
        csv_page(nullptr), graph_dlg(nullptr),
        widow_title(title),  mode_using(table_mode), use_csv(csv_mode)
    {
        set_widget_data( datacontainer );
        if( mode_using & MatrixTable::tbGraph )
            csv_page->addGraphic();
    }

    /// Destructor
    ~TableEditWidgetPrivate()
    {
        close_graph();
    }

    void set_axis(int ndxX, std::vector<int> ndxY)
    {
        csv_page->setXYaxis( ndxX, ndxY );
    }

    void update_table()
    {
        close_graph();
        csv_page->updateTable();
    }

    void exportCSV( const std::string& file_name ) const
    {
        csv_page->saveAllToCSV( file_name );
    }

    void export_selectedCSV( const std::string& file_name ) const
    {
        csv_page->saveSelectedToCSV(file_name);
    }

    /// Set new CSV data
    void open_new_CSV( const std::string& file_name, const QString& csv_data )
    {
        auto q = base_func();
        auto datcont =  dynamic_cast<CSVContainer*>( csv_page_private->data_CSV.get());
        jsonio17::JSONIO_THROW_IF( !datcont, "models", 21, " read new CSV file ( now only in case Constructor from csv file )" );

        q->setWindowTitle( QString("%1 ( %2 )").arg( widow_title, file_name.c_str() ));
        datcont->setCsvString(csv_data);
        update_table();
    }

private:

    TableEditWidget * const  base_ptr;
    inline TableEditWidget* base_func() { return static_cast<TableEditWidget *>(base_ptr); }
    inline const TableEditWidget* base_func() const { return static_cast<const TableEditWidget *>(base_ptr); }
    friend class TableEditWidget;

    /// Menu description
    QScopedPointer<Ui::TableEditWidget> qUi;
    /// Internal table view widget
    QScopedPointer<CSVPage> csv_page;
    CSVPagePrivate* csv_page_private = nullptr;
    /// Graphic window
    std::shared_ptr<GraphDialog> graph_dlg;

    /// Title of window
    QString widow_title;
    /// Mode of using ( see MatrixTable::ModeTypes )
    int mode_using;
    /// Used CSVContainer
    bool use_csv = true;

    /// Set up menu commands
    void set_actions();
    /// Set up widget
    void set_widget_data( std::shared_ptr<AbstractContainer> datacontainer );
    /// Show graph info
    void show_graph_data();

    /// Close graph
    void close_graph()
    {
        if( graph_dlg )
        {
            graph_dlg->close();
            graph_dlg.reset();
        }
    }
    /// Run context menu
    void context_menu(const QPoint &pos);

};


//-------------------------------------------------------------------------------------------------------


TableEditWidget::TableEditWidget( const char* title, std::shared_ptr<AbstractContainer> datacontainer,
                                  int mode, QWidget *parent):
    QMainWindow(parent), impl_ptr(nullptr)
{
    setWindowTitle( title );
    bool csv_mode = false;
    if( dynamic_cast<CSVContainer*>(datacontainer.get()) )
        csv_mode = true;

    impl_ptr.reset( new TableEditWidgetPrivate( this, title, datacontainer, mode, csv_mode ));
}

TableEditWidget::TableEditWidget( const char* title, const std::string& file_path,
                                  int mode, QWidget *parent):
    QMainWindow(parent), impl_ptr(nullptr)
{
    bool csv_mode = true;
    std::shared_ptr<CSVContainer> data_CSV = std::make_shared<CSVContainer>();
    impl_ptr.reset( new TableEditWidgetPrivate( this, title, data_CSV, mode, csv_mode ));
    openNewCSV( file_path );
}


TableEditWidget::~TableEditWidget()
{}

// Menu commands -----------------------------------------------------------

/// Write all table to csv file fileName
void TableEditWidget::CmExportCSV()
{
    try {
        std::string fileName;
        if(  ChooseFileSave( this, fileName, "Please, select file to write", csvFilters  ))
        {
            auto d = impl_func();
            d->exportCSV(fileName);
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write selected to csv file fileName
void TableEditWidget::CmExportSelectedCSV()
{
    try {
        std::string fileName;
        if(  ChooseFileSave( this, fileName, "Please, select file to write", csvFilters  ))
        {
            auto d = impl_func();
            d->export_selectedCSV(fileName);
        }
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Read table from csv file fileName
void TableEditWidget::CmImportCSV()
{
    try{
        std::string fileName;
        if(  ChooseFileOpen( this, fileName, "Please, select file with csv object", csvFilters  ))
            openNewCSV( fileName );
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void TableEditWidget::openNewCSV( const std::string& file_path )
{
    try {
        auto d = impl_func();
        auto new_data = read_new_CSV( file_path );
        d->open_new_CSV( file_path, new_data );
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void TableEditWidget::setXYaxis(int ndxX, std::vector<int> ndxY)
{
    auto d = impl_func();
    d->set_axis( ndxX, ndxY );
}

void TableEditWidget::updateTable()
{
    auto d = impl_func();
    d->update_table();
}

void TableEditWidget::plotTable()
{
    try {
        auto d = impl_func();
        d->show_graph_data();
    }
    catch( jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void TableEditWidget::CmHelp()
{
    helpWin("HelpTableWindow","");
}

void TableEditWidget::closeEvent(QCloseEvent* )
{
    auto d = impl_func();
    d->close_graph();
}

void TableEditWidget::slotPopupContextMenu(const QPoint &pos)
{

    auto d = impl_func();
    d->context_menu(pos);
}

QString TableEditWidget::read_new_CSV( const std::string& file_path )
{
    // read file
    QFile tmpStriam( file_path.c_str() );
    QString valCsv;
    if(tmpStriam.open( QIODevice::ReadOnly )) {
        valCsv = tmpStriam.readAll();
        tmpStriam.close();

        //delete all comments // #
        removeComments( valCsv );
        // remove all ' ' and '\t'
        valCsv.remove(QChar(' '), Qt::CaseInsensitive);
        valCsv.remove(QChar('\t'), Qt::CaseInsensitive);
    }
    else {
        ui_logger->error("Error when  open file : {}", file_path);
    }
    return valCsv;
}


//--------------------------------------------------------------------------

void TableEditWidgetPrivate::set_widget_data( std::shared_ptr<AbstractContainer> datacontainer )
{
    auto q = base_func();
    qUi->setupUi(q);
    //setAttribute(Qt::WA_DeleteOnClose); // automatically delete itself when window is closed

    // define table
    csv_page.reset( new CSVPage( datacontainer, mode_using, q ));
    csv_page_private = csv_page->impl_ptr.get();
    qUi->verticalLayout->addWidget(csv_page.get());
    QObject::disconnect( csv_page_private->table_CSV.get(), &MatrixTableProxy::customContextMenuRequested,
                         csv_page_private->table_CSV.get(), &MatrixTableProxy::slotPopupContextMenu);
    QObject::connect( csv_page_private->table_CSV.get(), &MatrixTableProxy::customContextMenuRequested,
                      q, &TableEditWidget::slotPopupContextMenu);

    // define menu
    set_actions();
}

//  Connect all actions
void TableEditWidgetPrivate::set_actions()
{
    auto q = base_func();
    // File
    QObject::connect( qUi->action_Help, &QAction::triggered, q, &TableEditWidget::CmHelp);
    QObject::connect( qUi->actionClose, &QAction::triggered, q, &TableEditWidget::close);
    QObject::connect( qUi->actionExport_to_CSV_File, &QAction::triggered, q, &TableEditWidget::CmExportCSV);

    if( (mode_using & MatrixTable::tbNoMenu) )
        qUi->actionExport_Selected_to_CSV_File->setVisible(false);
    else
        QObject::connect( qUi->actionExport_Selected_to_CSV_File, &QAction::triggered, q, &TableEditWidget::CmExportSelectedCSV);

    if( use_csv && (mode_using & MatrixTable::tbEdit) && !(mode_using & MatrixTable::tbNoMenu) )
        QObject::connect( qUi->actionImport_from_CVS, &QAction::triggered, q, &TableEditWidget::CmImportCSV);
    else
        qUi->actionImport_from_CVS->setVisible(false);

    // Edit
    if( (mode_using & MatrixTable::tbNoMenu) )
    {
        qUi->actionSelect_row->setVisible(false);
        qUi->actionSelect_co_lumn->setVisible(false);
        qUi->actionSelect_all->setVisible(false);
        qUi->action_Copy->setVisible(false);
        qUi->actionCopy_wit_h_names->setVisible(false);
    }
    else
    {
        QObject::connect(qUi->actionSelect_row, &QAction::triggered, csv_page.get(), &CSVPage::SelectRow);
        QObject::connect(qUi->actionSelect_co_lumn, &QAction::triggered, csv_page.get(), &CSVPage::SelectColumn);
        QObject::connect(qUi->actionSelect_all, &QAction::triggered, csv_page.get(), &CSVPage::SelectAll);
        QObject::connect(qUi->action_Copy, &QAction::triggered, csv_page.get(), &CSVPage::CopyData);
        QObject::connect(qUi->actionCopy_wit_h_names, &QAction::triggered, csv_page.get(), &CSVPage::CopyDataHeader);
    }

    if( (mode_using & MatrixTable::tbEdit) && !(mode_using & MatrixTable::tbNoMenu) )
    {
        QObject::connect(qUi->actionCalculator, &QAction::triggered, csv_page.get(), &CSVPage::CmCalc);
        QObject::connect(qUi->actionC_ut, &QAction::triggered, csv_page.get(), &CSVPage::CutData);
        QObject::connect(qUi->action_Paste, &QAction::triggered, csv_page.get(), &CSVPage::PasteData);
        QObject::connect(qUi->actionPaste_transposed, &QAction::triggered, csv_page.get(), &CSVPage::PasteTransposedData);
        QObject::connect(qUi->actionClear, &QAction::triggered, csv_page.get(), &CSVPage::ClearData);
    }
    else
    {
        qUi->actionCalculator->setVisible(false);
        qUi->actionC_ut->setVisible(false);
        qUi->action_Paste->setVisible(false);
        qUi->actionPaste_transposed->setVisible(false);
        qUi->actionClear->setVisible(false);
    }

    // tools
    if( (mode_using & MatrixTable::tbGraph) && !(mode_using & MatrixTable::tbNoMenu))
    {
        QObject::connect( qUi->actionMark_Columns_as_X, &QAction::triggered, csv_page.get(), &CSVPage::toggleX);
        QObject::connect( qUi->actionMark_Columns_as_Y, &QAction::triggered, csv_page.get(), &CSVPage::toggleY);
        QObject::connect( qUi->actionPlot_Results, &QAction::triggered, q, &TableEditWidget::plotTable);
    }
    else
    {
        qUi->menu_Tools->menuAction()->setVisible(false);
    }
}

void TableEditWidgetPrivate::show_graph_data()
{
    auto q = base_func();
    std::string title = "Graph for window: ";
    title  += widow_title.toStdString();

    if( !graph_dlg )
    {
        graph_dlg.reset( new GraphDialog( csv_page_private->chart_data.get(), q ) );
        QObject::connect( graph_dlg.get(), &GraphDialog::dataChanged,
                          csv_page.get(), &CSVPage::saveGraphData);
        QObject::connect( csv_page.get(), &CSVPage::updateGraphWindow,
                          [&]() { graph_dlg->UpdateAll( nullptr ); });
    }
    graph_dlg->UpdateAll( title.c_str() );
    graph_dlg->show();
}

void TableEditWidgetPrivate::context_menu(const QPoint &pos)
{
    auto q = base_func();
    if( mode_using & MatrixTable::tbNoMenu )
        return;

    QModelIndex index = csv_page_private->table_CSV->indexAt( pos );

    QMenu *menu = new QMenu(q);
    csv_page_private->table_CSV->makePopupContextMenu( menu, index );

    if( mode_using & MatrixTable::tbGraph )
    {
        bool addsep = false;
        QAction* act;

        if( csv_page->canBeToggled( index, true ) )
        {
            menu->addSeparator();
            addsep = true;
            act =  new QAction( "Toggle &X", q );
            act->setShortcut(q->tr("Ctrl+X"));
            act->setStatusTip("Mark columns as X (toggle)");
            QObject::connect(act, &QAction::triggered, csv_page.get(), &CSVPage::toggleX);
            menu->addAction(act);
        }

        if( csv_page->canBeToggled( index, false ) )
        {
            if( !addsep )
                menu->addSeparator();
            act =  new QAction("Toggle &Y", q );
            act->setShortcut(q->tr("Ctrl+Y"));
            act->setStatusTip("Mark columns as Y (toggle)");
            QObject::connect(act, &QAction::triggered, csv_page.get(), &CSVPage::toggleY);
            menu->addAction(act);
        }
    }
    menu->exec( q->mapToGlobal(pos) );
    delete menu;
}


} // namespace jsonui17
