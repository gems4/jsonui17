#pragma once

#include <QThread>
#include "jsonui17/models/json_view.h"
#include "jsonui17/EdgesWidget.h"
#include "BaseWidget_p.h"
#include "models/dbkeys_view.h"

namespace Ui {
class EdgesWidget;
}

class WaitingSpinnerWidget;

namespace jsonui17 {

class DBEdgeObject;


/// Private \class EdgesWidget - window to insert/update  Edges (TDBGraph)
class EdgesWidgetPrivate : public BaseWidgetPrivate
{
    Q_OBJECT

#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    Q_DISABLE_COPY_MOVE(EdgesWidgetPrivate)
#endif

signals:    // signals to DBEdgeObject

    /// Reset current DB client
    void cmResetClient(  QString aschema_name );

    /// Reset current DB schema
    void cmResetSchema(  QString aschema_name );

    /// Reload query
    void cmReloadQuery();

    /// Open document
    void cmReadDocument(  QString key_document  );

    /// Save json Document to database
    void cmUpdate( QString json_document );

    /// Delete keyDocument document from database
    void cmDelete( QString key_document );

    /// Delete documents from database
    void cmDeleteList( std::vector<std::string> keys_document );

    /// Read multiple records from file fileName
    void cmRestoreMultipleRecordsfromFile( QString file_name );

    /// Write multiple records to file fileName
    void cmBackupQueriedRecordstoFile( QString file_name, std::vector<std::string> keys_document );

    /// Write graph to file fileName
    void cmBackupGraphtoFile(QString file_name, std::vector<std::string> keys_document );

    /// Read multiple records from file fileName
    void cmRestoreGraphfromFile( QString file_name );

protected slots:

    /// Add Incoming key to edited document
    void add_incoming(  const QModelIndex& index );
    /// Add Outgoing key to edited document
    void add_outgoing(  const QModelIndex& index );

    void type_changed(const QString& text);

    void reset_type_box( const QString& text );

    /// Start long database process
    void start_process();

    /// Finish long database process
    void finish_process();

    /// Exception when database command execution
    void get_exception( const QString& titl, const QString& msg );

    /// Update oid
    void update_oid( QString oid );

    /// After open new document
    void open_document( QString json_document  );

    /// Update db document query
    void change_DB_query( QString query );

    /// Possible delete edges
    void afterDeleteDocument()
    {
        auto q = base_func();
        set_content_state( false );
        emit q->edgeDeleted();
    }

    /// Load records to different collections
    void afterLoadGraph()
    {
        auto q = base_func();
        emit q->graphLoaded();
    }

public:

    explicit EdgesWidgetPrivate( const std::string& aschema_name, const jsonio17::DBQueryBase& query );
    ~EdgesWidgetPrivate();

    /// Setup widget data
    void init();

    void change_keys_list()
    {
        db_keys->resetModel( get_key_from_dom().c_str() );
        // select lines into lists
        std::string inV, outV;
        json_tree->getValueViaPath<std::string>( "_to", inV, "" );
        json_tree->getValueViaPath<std::string>( "_from", outV, "" );
        incoming_keys->resetModel( inV.c_str() );
        outgoing_keys->resetModel( outV.c_str() );
    }

    void move_keys_list()
    {
        db_keys_table->move_to_key(get_key_from_dom());
        // select lines into lists
        std::string inV, outV;
        json_tree->getValueViaPath<std::string>( "_to", inV, "" );
        json_tree->getValueViaPath<std::string>( "_from", outV, "" );
        incoming_keys->resetModel( inV.c_str() );
        outgoing_keys->resetModel( outV.c_str() );
    }

    void cm_update()
    {
        current_record = json_tree->saveToJson();
        set_content_state( false );
        emit cmUpdate(current_record.c_str());
    }

    void cm_delete_select( const std::vector<std::string>& keys )
    {
        start_process();
        emit cmDeleteList( keys );
    }

    void cm_backup_records( const std::string& file_path, const std::vector<std::string>& keys )
    {
        start_process();
        emit cmBackupQueriedRecordstoFile( file_path.c_str(), keys );
    }

    void cm_restore_records( const std::string& file_path )
    {
        start_process();
        emit cmRestoreMultipleRecordsfromFile(file_path.c_str());
    }

    void cm_backup_graph( const std::string& file_path, const std::vector<std::string>& keys )
    {
        start_process();
        emit cmBackupGraphtoFile( file_path.c_str(), keys );
    }

    void cm_restore_graph( const std::string& file_path )
    {
        start_process();
        emit cmRestoreGraphfromFile( file_path.c_str() );
    }

protected:

    inline EdgesWidget* base_func() { return static_cast<EdgesWidget *>(base_ptr); }
    inline const EdgesWidget* base_func() const { return static_cast<const EdgesWidget *>(base_ptr); }
    friend class EdgesWidget;

    // widget data

    /// EdgesWidget.ui description
    QScopedPointer<Ui::EdgesWidget> qUi;

    QScopedPointer<QLineEdit> pLineTask;

    // tree view editor

    /// Tree view editor widget
    QScopedPointer<JsonView>  json_tree;
    /// Internal proxy data
    std::string current_record = "";
    /// Current edge query
    jsonio17::DBQueryBase edge_query;
    /// Edge query could be changed
    bool is_default_query = false;

    /// Database keys list model
    std::shared_ptr<DBKeysObject> db_keys;
    /// Keys list table
    QScopedPointer<DBKeysTable> db_keys_table;

    /// Incoming vertexes keys list model
    std::shared_ptr<DBKeysObject> incoming_keys;
    /// Incoming vertexes keys list table
    QScopedPointer<DBKeysTable> incoming_keys_table;

    /// Outgoing vertexes keys list model
    std::shared_ptr<DBKeysObject> outgoing_keys;
    /// Outgoing vertexes keys list table
    QScopedPointer<DBKeysTable> outgoing_keys_table;

    // database API

    /// Database connection object
    DBEdgeObject* db_request = nullptr;
    /// Thead moved db_request to
    QThread db_thread;
    /// Dialog when waiting db_rquest command ends
    WaitingSpinnerWidget* wait_dialog = nullptr;

    /// Set up menu commands
    void set_actions();
    /// Set up current json data to view model
    bool reset_json( const std::string& json_string, const std::string& schema_name );
    /// Setup/ update database connection
    void reset_DB_client( const std::string& schema_name );
    /// Get current record key from dom structure
    std::string get_key_from_dom() const;
    std::string extract_schema_from_id( const std::string& key_id );

    std::string key_from_index( const QModelIndex& index )
    {
        QModelIndex sel_index = index.sibling( index.row(), 0);
        return db_keys_table->model()->data(sel_index).toString().toStdString();
    }

    // update after change preferences
    void updt_view_menu() override;
    void updt_model() override;
    void updt_table() override;
    void updt_db() override;
};

} // namespace jsonui17

